@echo off

echo ====== build mybox ====
echo [cache] build_mybox 
echo [nocache] build_mybox nocache
echo =======================
echo 

IF [%1] == [nocache] (
  docker build --no-cache -t mybox .
) ELSE (
  docker build -t mybox .
)
