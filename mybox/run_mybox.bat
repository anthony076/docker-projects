@echo off

set IMAGE_NAME=mybox
set CONTAINER_NAME=mybox
set SSH_PORT=22

call :rm-old-container
call :run-container

docker attach %CONTAINER_NAME%%

echo [msg] stoping container
docker stop %CONTAINER_NAME%% > nul

GOTO :eof

:rm-old-container

  docker inspect --type=container %CONTAINER_NAME% > null

  IF %errorlevel% == 0 (

    echo [msg] remove old container
    docker container rm %CONTAINER_NAME% > null
  )
  
  EXIT /B 0

:run-container
  echo [msg] run new container
  set ProjectPath=%~dp0
  
  docker run -d -v %ProjectPath%:/share -p %SSH_PORT%:%SSH_PORT% --name %CONTAINER_NAME% -it %IMAGE_NAME% > null
  
  EXIT /B 0
  
:eof
  EXIT 0