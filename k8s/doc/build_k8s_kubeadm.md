## 使用 docker 作為 node主機
- 使用 docker 作為 node 設置上會較為複雜，可改用 virtualbox 或 VMWare 作為 node主機
  
- docker-run / docker-build 是將特定的操作，`重新封裝為高級的命令`，
  
  實際上 docker 是 [client/server 的架構](../../docker/readme.md)

- 一般狀況下使用 docker build / docker run 就可以建立和操作容器，
  但 k8s 需要`手動進行環境設置`，因此需要執行操作 docker-daemon 才能進行細部的配置，
  參考，[k8s網路架構](network_architecture.md) 和 [配置k8s集群的網路](../doc/build_k8s_overview.md)

- 注意，在 [Installing a Kubernetes Master Node via Docker](https://jamesdefabia.github.io/docs/getting-started-guides/docker-multinode/master/) 中，使用的是`舊版`的 docker-daemon，並未對新版的 docker 進行更新，僅供參考用

## 使用 virtualbox 作為 node主機
- [kubeadm 手動安裝](https://rickhw.github.io/2019/03/17/Container/Install-K8s-with-Kubeadm/)

## 參考
- [kubeadm 部署安裝](https://hackmd.io/@cnsrl/BkjmFubTw)

- [以 kubeadm建立k8s-cluster](https://ithelp.ithome.com.tw/articles/10235069)

- [使用 Kubeadm 佈署 K8S 集群](https://medium.com/jacky-life/%E4%BD%BF%E7%94%A8-kubeadm-%E5%AE%89%E8%A3%9D-k8s-abe1631aa600)

- [kubeadm 手動安裝](https://rickhw.github.io/2019/03/17/Container/Install-K8s-with-Kubeadm/)

- [用 kubeadm 在Ubuntu 上快速構建Kubernetes 測試集群](https://jimmysong.io/kubernetes-handbook/practice/install-kubernetes-on-ubuntu-server-16.04-with-kubeadm.html)

- [使用Kubeadm搭建Kubernetes學習環境](https://www.cnblogs.com/wtzbk/p/15216488.html)

- [Setup Kubernetes Cluster using Kubeadm on Ubuntu 20.04](https://www.youtube.com/watch?v=mMmxMoprxiY)