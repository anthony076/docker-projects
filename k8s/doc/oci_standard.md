## OCI 標準
- 主要定義
  - Image-Specification
  - Runtime-Specification

- [from image to runtime](https://www.ohmyrss.org/post/1618009279587)

  <img src="oci/from_image_to_runtime.png" width=700 height=auto>


## Image-Specification 映像標準
- 基本概念，內容可尋址的文件系統
  - 文件系統是根據路徑來查找文件的，對於同一個路徑，它所指向的文件的內容是可變的，
    因此，在不同的時間點上訪問同一個路徑的文件，它所包含的內容會產生變化
  
  - 容器鏡像的一個重要優勢是不可變，會於變動的文件內容會保存為 Layer，
    使用摘要算法得到該文件的摘要，使用摘要作為該文件的尋址方式，
    此時，文件系統是一個巨大的哈希表，哈希表的鍵是摘要，而對應的值則是文件的內容

  - image 由以下的元素組成，
    - `manifest`: 檔案清單，指向 cofig 的檔名，和 每一個 layer 的目錄名
    - `config檔`: 存放運行時的環境配置、layer-index、當前 layer 執行的指令、image的metadata
    - Layer資料夾:
      - `json 配置檔`，紀錄當前layer的容器(主機)狀態
      - `layer.tar`，用於構建 container 檔案系統的實際檔案
      - `VERSION檔`，layer 的版本號

- [規範內容](https://github.com/opencontainers/image-spec/blob/main/spec.md)
  - Image Manifest，組成一個容器image的組件描述文檔
  - Image Index，可選，更高層的manifest文件，指向一組manifests 和descriptors 列表
  - Image Layout，一個鏡像的內容佈局
  - Filesystem Layer，一個容器文件系統的一個"變化集合"
  - Image Configuration，配置文件包含如應用參數、環境等信息
  - Conversion，如何解析的描述文檔
  - Descriptor，描述了類型、元數據和內容尋址的格式描述文件

- 檢視image檔的實際內容
  - 將 docker 中以下載的 image 匯出成檔案
    > docker save -o 輸出檔名.tar 映像名
  
  - 將 image 解壓縮後得到

    <img src="oci/image_content.png" width=800 height=auto>

## Runtime-Specification 運行標準
- [規範內容](https://github.com/opencontainers/runtime-spec/blob/main/spec.md)
  - 檔案系統的結構
  - 運行時和生命週期
  - 運行配置檔的內容

- [容器的生命週期](https://www.ohmyrss.org/post/1618009279587)
  
  <img src="oci/container_lifecycle.png" width=500 height=auto>

  - created：  container 已建立，但未運行，處於可執行狀態
  - started：  container 執行中
  - stopped：  container 執行結束，或者出現錯誤，或執行stop命令後，當前處於停止
  - deleted：  container 已被刪除

## Linux 的 namespace + Cgroup 實現資源隔離
- container 可以實現將每一個容器的`資源隔離`，每一個容器都有自己的資源，不會與其他容器共享 (若要共享需要另外設定)，
  實現資源隔離的方式，就是透過 Linux 自帶的命名空間(namespace)系統實現，

- 要實現資源隔離，必須資源使用者分組 + 將資源的使用，依照分組進行排程，
  例如，學校的體育場，依年級區分使用，使用時該資源被鎖定呈現獨佔狀態，就不會有混用資源的情況

- 要共享 namespace 實現隔離，必須是資源都`擁有同一個特徵`，
  以生活作為例子，同一個學校/同一個家庭/同一個公司，才屬於同一個命名空間，也才能享有共同的資源

- 作為 namespace 的依據
  - ipc-namespace: System V IPC，POSIX message queue
  - network-namespace: 具有相同網域的資源，共享同一個命名空間
  - mount-namespace: 具有同一個掛載點的資源，共享同一個命名空間
  - pid-namespace: 具有同一個pie的資源，共享同一個命名空間
  - user-namespace，具有同一個使用者/群組的資源，共享同一個命名空間
  - uts-namespace，具有同一個 hostmane/NTS-domain-name的資源，共享同一個命名空間
  - 注意，實務上，每個命名空間就是一個 inode 檔案，該檔案是指向進程所屬的"真實"命名空間的符號鏈接(指針)，
    如果兩個不同進程的命名空間符號鏈接指向相同 inode，則它們屬於相同的命名空間

- 以 pid-namespace 為例
  - 每一個命名空間像一台獨立的Linux主機一樣，都有自己的init進程(PID為1)，其他進程的PID依次遞增

  - namespace-A 和 namespace-B 都有PID為1的init進程，是兩個獨立的命名空間，pid 值是分開計算的

  - 子命名空間的進程映射到父命名空間的進程上，父命名空間可以知道每一個子命名空間的運行狀態，
    而`子命名空間`與`子命名空間`之間是`隔離的`

- cgroups (control-group)，由內核提供，用於分配物理資源的 api
  - 利用 namespace 對進程(資源使用者)進行分組後，對`實際的物理資源(CPU、IO、Mem)`也要進行分組，
  - cgroups 是一組可以限制/紀錄/隔離`進程組`所使用的物理資源的`內核級api`
    - 通過文件操作實現cgroups 的組織管理
    - 可以實現線程級別的管理操作
    - 所有資源管理的功能都以 "subsystem"的方式實現，接口統一
    - 子進程創建之初與其父進程處於同一個cgroups 的控制組

## Ref
- OCI
  - [手把手教你了解OCI 鏡像規範](https://dockone.io/article/2434724)
  - [OCI Image Spec 鏡像格式規範](https://www.jianshu.com/p/712273fd1cfd)
  - [Docker (容器) 的原理](https://www.ohmyrss.org/post/1618009279587)

- Namespace
  - [linux namespace 概述](https://man7.org/linux/man-pages/man7/namespaces.7.html)
  - [容器技術原理：使用Namespace 實現進程隔離](https://www.waynerv.com/posts/container-fundamentals-process-isolation-using-namespace/)
  - [Linux Namespace 入門系列：Namespace API](https://codingnote.cc/zh-tw/p/82550/)