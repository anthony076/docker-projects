## 建構 k8s 環境的選擇
- Kubernetes 由多個組件組成，這些`組件必須單獨安裝和配置`，
  可分為全自動 / 半自動 / 半手動 / 全手動

- 安裝方案1，`手動`安裝
  - 每個組件都需要獨立安裝和配置
  - 需要熟悉 Linux
  - [參考](doc/build_k8s_env.md)

---

- 安裝方案2，`第三方套件`的一鍵安裝
  - 由第三方預先定義好的 k8s 組件和配置，
    可以簡化安裝的手續和配置，但可能有限制，適合新手學習
    
  - Minikube
    - 在 vm 中執行`單節點`的 kubernetes-cluster，供測試和學習用
    - 不能在生產環境中使用，沒有高可用性的單節點機器

  - kubeadm，推薦使用
    - 生產級的 k8s 的簡化管理工具，由官方推出的簡化版的 k8s，
      使用 docker 作為 node 節點主機
    - 提供了 kubeadm init 和 kubeadm join，是快捷建立叢集的最佳實踐
    - 只執行必要的操作來啟動和執行最小可用叢集，只關注啟動引導，而不是配置機器
    
  - MicroK8s
    - 由 ubuntu 維護，輕量，不須額外安裝 vm
    - 常用於測試開發 k8s 相關的軟件

  - Kind(Kubernetes in Docker)
    - 使用 Docker 容器作為 Nodes，在本地建立和執行 Kubernetes 群集的工具

---

- 安裝方案3，`免安裝`，
  - 透過 [Katacoda](https://katacoda.com/)，step by step 教學
  - 透過 [Play with kubernetes](https://labs.play-with-k8s.com/)，online-vm

## k8s 組件的選擇
- k8s 是開放協議的架構，並不依賴特定的實作方式，因此，每個組件可能有多個選擇

- CRI(Container Runtime Interface) 組件的選擇，
  - kubelet
  - Docker
  - CRI-O: OCI-based implementation of Kubernetes Container Runtime Interface
  - containerd
  - rtk: the pod-native container engine
  - kata-container
  - Frakti: The hypervisor-based container runtime for Kubernetes

- CNI(Container Network Interface) 組件的選擇
  - Flannel
  - Calico
  - Weave Net
  - AWS VPC CNI

## 工具推薦
- 監控工具
  - 內建工具，`資源管道` 和 `全度量管道`
    - 資源管道，集中在與各種控制器相關的指標，低資源消耗
    - 全度量管道，從所有集群中獲取狀態，並顯示更豐富的指標，高資源消耗
    - Heapster，官方提供的監控插件

  - 第三方監控工具
    - Prometheus，沒有可視化的 dashboard，但插件多，具有全功能數據收集能力，允許匯出匯入數據給其他解決方案
    - Grafana，優秀的儀錶盤、分析和數據可視化工具，但不具全功能數據收集能力 (Prometheus 和 Grafana 可以一起使用)

- 警報工具
  - Nagios
  - Prometheus Alertmanager

- 日誌收集工具
  - EFK
  
- 服務編排管理工具
  - 功能

    一個應用往往有多個服務，當這些服務之間直接互相依賴，需要有一定的組合的情況下，
    使用YAML文件的方式配置應用往往十分繁瑣還容易出錯，因此就需要服務編排工具，負責統籌各個服務之間的關係和依賴的。

  - [Helm](doc/helm.md)

## 配置網路
- [k8s 網路架構基礎](network_architecture.md)

- 配置網路實例
  - [配置 Flannel](https://jimmysong.io/kubernetes-handbook/concepts/flannel.html)
  - [配置 Calico(官方推薦)](https://jimmysong.io/kubernetes-handbook/concepts/calico.html)
  - [配置 eBPF](https://jimmysong.io/kubernetes-handbook/concepts/cilium.html)

## 事前準備
- 關閉 swap space
  - 為什需要關閉 swap
    - Linux會將內存中`不常訪問的數據保存到Swap`上，這樣系統就有`更多的物理內存`為各個進程服務，
      而當系統需要訪問Swap上存儲的內容時，再將Swap上的數據加載到內存中。
    - k8s關閉Swap一個是`性能問題`，開啟Swap會嚴重影響性能（包括內存和I/O；
    - 另一個是`管理問題`，開啟Swap後`通過Cgroups設置的內存上限就會失效`，
      如果不關閉k8s運行會出現錯誤， 即使安裝成功了，node重啟後也會出現k8s server運行錯誤;
  - 關閉方式
    ```shell
    $ swapoff -a
    $ nano /etc/fstab  # 註解 /swapfile 
    ```

- 關閉 SeLinux
  - SELinux(Secure Enhanced Linux)安全增強，是Linux的Linux安全策略機制，
    允許系統管理員更加靈活的來定義安全策略。
  
  - SELinux是一個內核級別的安全機制，對其配置文件的修改，`都是需要重新啟動操作系統`才能生效的。

  - 關閉方式
    ```
    setenforce 0
    sed -i "s/SELINUX=enforcing/SELINUX=disabled/g" /etc/selinux/config
    ```

## Ref
- 手動安裝
  - [在本地使用Vagrant 啟動三個虛擬機部署分佈式的Kubernetes 集群](https://github.com/rootsongjc/kubernetes-vagrant-centos-cluster/blob/master/README-cn.md)

- Kind
  - [使用 WSL 2 與 Docker Desktop 架設 Kubernetes 多節點叢集環境 (KinD)](https://blog.miniasp.com/post/2020/08/21/Install-Kubernetes-cluster-in-WSL-2-Docker-on-Windows-using-kind)

- MicroK8s
  - [MicroK8s 安裝與使用指南](https://xenby.com/b/254-%E6%95%99%E5%AD%B8-%E8%BC%95%E9%87%8F-kubernetes-microk8s-%E5%AE%89%E8%A3%9D%E8%88%87%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97)