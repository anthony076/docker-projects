## Helm 的使用
- Helm 的功能
  
  把服務中各種元件裡的 yaml 檔統一打包成一個叫做 chart 的集合，透過給參數的方式，去同時管理與設定這些 yaml 檔案。
  可以實現應用程序封裝、版本管理、依賴檢查，便於應用程序分發

## Ref
- [Helm 介紹與建立 Chart](https://cwhu.medium.com/kubernetes-helm-chart-tutorial-fbdad62a8b61)
- [使用Helm管理kubernetes應用](https://jimmysong.io/kubernetes-handbook/practice/helm.html)
