## 網路模型基礎
- 注意，以下幾種網路模型，是根據使用者配置的環境決定，而不是固定不變的模型

- `基礎`，組成網路架構的組件

  <img src="network/dependent-virtual-gateway.png" width=300 height=auto>

  - eth0，`node主機` 的實體網關，提供node主機的實體IP
  - docker0，作為連接實體網關(eth0)和虛擬網關(veth0) 的橋接器
  - vethx， `container自身`的虛擬網關，提供 container 的虛擬IP

- `發展1`，以容器實現`共享虛擬網關` (POD)

  <img src="network/shared-virtual-gateway.png" width=500 height=auto>

  - 和上一步不同，此處的 veth0 是一個`多容器共享的虛擬網關`，

    在 k8s 的實作上，是以一個 `pause容器實現共享虛擬網關的功能`，
    使得不同的 container看到的是同一個虛擬網關

  - 在此模型中，兩個 container 共享同一個虛擬網關，
    因此不能使用同一個端口

  - 參考，[利用 container 取代虛擬網關的設定](https://docs.docker.com/engine/reference/run/#network-settings) 的 Network: container 一節

- `發展2`，可跨越主機的pod通訊 (Service)
  
  - 問題，橋接器之間沒有聯繫，無法建立跨主機之間的 pod 通訊

    <img src="network/no-connection-between-bridge.png" width=500 height=auto>

    為了實現跨主機之間的 pod 通訊，node 主機中的橋接器(docker0)，

    必須要知道其他橋接器的存在，才能與其他node主機中的 pod 進行通訊

  - 解決方式，利用 node 上層的router/gateway 作為中繼，建立橋接器之間的聯繫

    <img src="network/direction-guide-in-master-node.png" width=500 height=auto>

    - step1，將傳統的橋接器(docker0) 替換為 客製化橋接路由(cusomer-bridege-router，cbr)
    - step2，將每個node主機的 cbr 都分配一個公共地址，和節點主機的IP不同的域名，但仍然由上層的router/gateway管理
    - step3，在上層的 router/gateway 中，會建立一張路由表，表明目的地址對應的 node 主機

      | destination | nexthop    | meaning                                                    |
      | ----------- | ---------- | ---------------------------------------------------------- |
      | 10.0.1.x    | 10.100.0.2 | 目標位址為 10.0.1.x 的流量，請前往 10.100.0.2 的 node 主機 |
      | 10.0.2.x    | 10.100.0.3 | 目標位址為 10.0.2.x 的流量，請前往 10.100.0.3 的 node 主機 |

## Service，pod 的反向代理
- 為什麼需要 Service
  - pod 的存取問題
    - 佈署 pod 時`無法指定 podIP`，podIP 是隨機分配的

    - pod 的生命週期不是並永久的，然後掛掉之後就不會自己恢復了，
      一旦重啟後，`podIP 也會跟著改變`

  - 使用 Service 的優點
    - 優點1，避免pod重啟造成的通訊失敗

      service 會主動與 apiserver 通訊，取得 pod 的最新位址，
      透過 service 可以使 pod 重啟後，外部`不會因 podIP不同造成通訊失敗`

    - 優點2，使用更簡便，
      
      改用 service 的`域名`來取代直接使用 pod-IP，
      k8s 內部有DNS服務器可將 service 的域名轉換為service被賦予的clusterIP

    - 優點3，Service 提供`負載均衡`的功能，
      
      node 內可以執行多個相同內容的 pod副本(replicas)，由 service 決定要將請求轉發到哪一個pod副本執行

- Sevice 的基礎認識
  - service 是一個`代理的抽象層`而不是網關，類似反向代理的功能，
    使用者透過`service的域名來存取目標pod`，而不需要知道目標pod的位置
    
    - 建立 service 後，必須透過 `service 的名稱(域名)`來存取 service，而不是 service 的IP，
    - k8s 內建有 dns-server，可以將 service 的域名，還原為 service 的IP
    - service 類似`反向代理`的功能，service 實際收到請求後，將轉發給 pod 的實際地址
  
  - 為了使 service 能夠處理網路請求，因此 `service 仍然需要被分為一個 virtual-ip (VIP)`，又稱為`clusterIP`，
    
    代表存取 pod-cluster 用的IP，使得 service `看起來像一個反向代理服務器`，
    service 有自己的IP和PORT，並可以將請求轉發到目的地，

  - pod所屬網域(pod-network) 和 service所屬網域(service-network)是不同的，但兩者是有聯繫的，

    參考，[跨越節點的pod通訊範例](#範例跨越節點的-pod-通訊範例)
    
  - 要先有 Pod 才會有定義 Service 的需求，兩種指定 service 對應 pod 的方法
    - 方法1，在 yaml 中，透過 `selector 欄位`，指定 sevice 要指向哪一個 pod
      ```js
      // 建立 service
      kind: Service
      apiVersion: v1
      metadata:
        name: my-service
      spec:
        type: ClusterIP
        
        // 選擇帶有 "app=MyApp" 的 pod
        selector:
          app: MyApp
        
        // Service 實際對外服務的設定
        ports:
        - protocol: TCP
          port: 80
          // Pod 對外開放的 port number
          targetPort: 9376
      ```

    - 方法2，不透過 select 欄位，就需要手動指定 podIP
      
      建立 service 時，不透過 selector 欄位指定對應的 pod，
      ```js
      kind: Service
      apiVersion: v1
      metadata:
        name: my-service
      spec:
        ports:
        - protocol: TCP
          port: 80
          targetPort: 9376
      ```

      手動建立 Endpoint resource object
      ```js
      kind: Endpoints
      apiVersion: v1
      metadata:
        // 對應的 service 域名
        name: my-service
      subsets:
        // pod 的位址
        - addresses:
            - ip: 1.2.3.4
          ports:
            - port: 9376
      ```

- Service 運作原理，service + iptables + kube-proxy
  - step1，將 service 的域名轉換為 clusterIP
    
    因為 service 會被分配一個 clusterIP，
    因此，客戶端的請求會先送往dns-server解碼後，得到指向 service 的 clusterIP

  - step2，經過 `netfilter` + `iptables` + `NAT` 匹配路由並轉換IP，
    - netfilter ，用來攔截請求的數據，並檢查是否有匹配的路由規則
    - iptables ，用來建立路由規則，並記錄每一個路由規則對應的轉換位址
    - NAT (Network Address Translation)，用於執行地址的轉換，
    
    service 的抽象層，會利用 `kernel-netfilter + iptables` 中的`路由規則`，
    將`原本發往 service 的IP，改成 pod 的IP`

  - step3，由 kube-proxy 定期更新 pod 的最新位址
    
    `kube-proxy` 會負責與 manager-node 上的 apiserver 進行通訊，
    `隨時取得 pod 的最新位置`，並通知 iptables 進行更新

  - 範例，見 [跨越節點的pod通訊範例](#範例跨越節點的-pod-通訊範例)

## Service 的類型
- service 有以下幾種類型
  - 類型1，ClusterIP-Service，預設類型，用於取代ip的 service
  - 類型2，Headless-Service，直接跟特定的 pod 進行通訊，而不是隨機分發 pod
  - 類型3，NodePort-Service，在 node主機上，直接開啟外界可直接存取的 port
  - 類型4，LoadBalancer-Service，在 node主機上，直接開啟 LoadBalancer 可直接存取的 port

- 類型1，clusterIP-Service 的配置範例
  - 示意圖
  
    <img src="network/service-clusterIP-flow.png" width=500 height=auto>

  - step1，發布 pod
    
    在 deploy 的yaml中，指定 pod 開放的 port，例如開放 3000 和 9000
    
    ```js
    apiVersion: v1
    // 發布 pod
    kind: Deployment
    metadata:
      name: microservice-one
      ...
    spec:
      // 建立 2個 pod
      replicas:2
      ...
      // pod 的建構模板
      template:
        metadata:
        // 定義 pod 的名稱，供 service 的 select 匹配用
        labels:
          app: microservice-one
        spec:
          containers:
          // pod中的容器1
          - name: ms-one
            image: aa/bb
            ports:
            - containerPort: 3000

          // pod中的容器2
          - name: log-collector
            image: cc/dd
            ports:
            - containerPort: 9000
    ```

  - step2，建立 service
    
    在 service的yaml中，指定 service 的類型和對接的 pod，
    被 service 連接的 pod 被稱為端點 (Endpoint)
    
    ```js
    apiVersion: v1
    kind: Service
    metadata:
      // 指定 serviceName
      name: microservice-one-service
    spec: 
      // 指定 service 的類型為 ClusterIP，預設類型，可不寫
      type: ClusterIP
    
      // 透過 selector 欄位，指定與 service 對接的 pod
      selector:
        // 指定連接名稱為 microservice-one 的 pod
        app: microservice-one

      ports:
        - protocol: TCP
          // 指定 servicePort
          port: 3200
          // 指定pod的port:3000 (與service 對接的pod)
          // 一個 pod 可能有多個 container，每個 container 具有自己的port
          targetPort: 3000
    ```

  - step3，建立 ingress
    
    在 ingress 的yaml中，指定對接的service
    
    ```js
    apiVersion: networking.k8s.io/v1beta1
    // 建立 Ingress
    kind: Ingress
    metadata:
      name: ms-one-ingress
      annotations: 
        kubernetes.io/ingress.class: "nginx"
    spec:
      rules:
      - host: microservice-one.com
        http:
          paths:
            - path: /
              backend:
                // 指定 ingress 對接的 service
                serviceName: microservice-one-service
                serverPort:3200
    ```
  
  - service 的 selece欄位 可以匹配多個 pod

    若在 pod 中建立多個副本，且多個副本都具有相同的名稱，
    則 service 就可以對接到多個 pod ，並根據負載均衡自動分配流量

    <img src="network/service-match-multiple-pod.png" width=800 height=auto>

  - server 會為連接的Endpoint(pod) 建立 Endpoint object，用來追蹤和更新 pod 的狀態

    <img src="network/service-endpoint-object.png" width=600 height=auto>

  - 開放多端口 service ，需要自定義 portName

    <img src="network/multiple-port-service-need-portName.png" width=800 height=auto>
  
  - podIP 由所有的node主機分配
    
    podIP 可以透過 `kubectl get pod -o wide` 查詢

    <img src="network/podIP-belongs-node.png" width=500 height=auto>

---

- 類型2，Headless-Service 的配置範例
  - Headless-Service 讓用戶可以直接跟特定的 pod 進行通訊，而不是隨機分發 pod

  - 使用場景
    - 不使用 Headless，用戶的請求會隨機分發給不同的 pod
    - 若客戶的請求是`具有時效性`，例如，存取 database，就需要跟指定的 pod 通訊，
      以下圖為例，只有 Master 能寫入資料，因此只有 Master 的資料是即時的

      <img src="network/why-headless-service.png" width=700 height=auto>

  - 啟用 Headless-service，將 clusterIP的欄位設置為 None
    ```js
    apiVersion: v1
    kind: Service
    metadata:
      name: microservice-one-service
    spec: 
      // 啟用 headless-service
      clusterIP: None
    ...
    ```

    啟用 headless-service 後，`dns-server 返回的是 podIP`，而不是 clusterIP

    透過 `kubectl get svc` 可檢視結果

    <img src="network/get-service-ip.png" width=600 height=auto>

  - 一個應用可以有多個 service，clusterIP-service 和 headless-service 可以共存
  
    <img src="network/multiple-service-in-one-app.png" width=700 height=auto>

--- 

- 類型3，NodePort-Service 的配置範例
  - 示意圖

    <img src="network/service-nodePort-flow.png" width=500 height=auto>

  - 使用場景，
    - 預設狀況下，外界無法直接與 service 通訊，必須透過 Ingress (參考類型1)
    - 使用 NodePort，在是 node主機上直接開啟與外接通訊的 port (稱為 NodePort)
    - NodePort 會於 內部的 service 相連接
    - NodePort 不適合用於生產環境，通常用於開發測試

  - 不推薦使用 NodePort-Service
    - 會有安全性問題，
    - 數據傳輸低效，因為使用 NodePort 仍然會使用到 service，多了一層關卡
  
  - 啟用 NodePort-Service
    ```js
    apiVersion: v1
    kind: Service
    metadata:
      name: microservice-nodeport
    spec: 
      // 啟用 NodePort
      type: NodePort
      selector:
        app: microservice-one

      ports:
        - protocol: TCP
          // 指定 servicePort
          port: 3200
          // 指定 podPort
          targetPort: 3000
          // 指定 nodePort
          nodePort: 30008
    ```

  - NodePort 的使用限制，只能開放 30000 - 32767 的端口作為 NodePort
  
---

- 類型4，LoadBalancer-Service 的配置範例
  - 示意圖

    <img src="network/service-LoadBalancer-flow.png" width=500 height=auto>

  - 使用場景，
    - 和 NodePort 類似，都是在 node主機上直接打開一個與外界通訊的接口

    - 和 NodePort 不同的是，LoadBalancer-Service 只接受來自 LoadBalancer 供應商的外部請求，不接受一般客戶端的請求，

  - 啟用 LoadBalancer-Service
    ```js
    apiVersion: v1
    kind: Service
    metadata:
      name: microservice-loadbalancer
    spec: 
      // 啟用 LoadBalancer
      type: LoadBalancer
      selector:
        app: microservice-one

      ports:
        - protocol: TCP
          // 指定 servicePort
          port: 3200
          // 指定 podPort
          targetPort: 3000
          // 指定 nodePort
          nodePort: 30010
    ```

## 範例，跨越節點的 pod 通訊範例
- step1，建立2個 server-pod，`pod1，IP=10.0.1.2，網域=10.0.0.x | pod2，IP=10.0.2.2，網域10.0.0.x`
  ```js
  // 透過 Deployment 發布多個 pod
  kind: Deployment
  apiVersion: extensions/v1beta1
  metadata:
    name: service-test
  // 建立兩個 server-pod
  //    pod-1，service-test-6ffd9ddbbf-kf4j2
  //    pod-2，service-test-6ffd9ddbbf-qs2j6
  spec:
    replicas: 2
    selector:
      matchLabels:
        app: service_test_pod
    template:
      metadata:
        labels:
          app: service_test_pod
      spec:
        // pod 的內容，為 python 實現的 http-server
        containers:
        - name: simple-http
          image: python:2.7
          imagePullPolicy: IfNotPresent
          command: ["/bin/bash"]
          args: ["-c", "echo \"<p>Hello from $(hostname)</p>\" > index.html; python -m SimpleHTTPServer 8080"]
          ports:
          - name: http
            containerPort: 8080
  ```

---

- step2，建立1個 client-pod，
  - 不推手動建立 client-pod
    ```js
    apiVersion: v1
    // 手動建立 pod
    kind: Pod
    metadata:
      name: service-test-client1
    // 建立1個 client-pod
    spec:
      restartPolicy: Never
      // client-pod 的內容是一個單行命令
      containers:
      - name: test-client1
        image: alpine
        command: ["/bin/sh"]
        
        // 缺點，一但 server-pod 重啟或失效，server-pod 就不再是 10.0.2.2
        // 造成 client-pod 通訊失效
        args: ["-c", "echo 'GET / HTTP/1.1\r\n\r\n' | nc 10.0.2.2 8080"]

    // 透過 kubectl logs service-test-client1 可以檢視結果
    ```

  - 2-1，先建立 service，`service，IP=10.3.241.152，網域=10.3.240.x`
    ```js
    // 建立 service
    kind: Service
    apiVersion: v1
    metadata:
      name: service-test
    spec:
      selector:
        // 連接到 step1 的 server-pod
        app: service_test_pod
      ports:
      - port: 80
        targetPort: http

    // 透過 kubectl get service service-test 判斷 service 是否已建立
    ```

    透過` kubectl describe services service-test` 檢查 service 的類型和檢視 cluster-ip
    ```js
    $ kubectl describe services service-test
    Name:                   service-test
    Namespace:              default
    Labels:                 <none>
    Selector:               app=service_test_pod
    
    // service 自身的連接資訊，clusterIP = serviceIP = 10.3.241.152
    Type:                   ClusterIP       
    IP:                     10.3.241.152
    Port:                   http    80/TCP

    // service 管理 server-pod1(10.0.1.2:8080) 和 server-pod2(10.0.2.2:8080)
    Endpoints:              10.0.1.2:8080, 10.0.2.2:8080   

    Session Affinity:       None
    Events:                 <none>
    ```

  - 2-2，再建立 client-pod
    ```js
    apiVersion: v1
    // 建立 client-pod
    kind: Pod
    metadata:
      name: service-test-client2
    spec:
      restartPolicy: Never
      containers:
      - name: test-client2
        image: alpine
        command: ["/bin/sh"]
        // 注意，nc 10.0.2.2 8080 改成 nc service-test 80
        // k8s 提供了一個內部集群 DNS 來解析服務名稱，會自動將 service-test 還原為正確的IP
        args: ["-c", "echo 'GET / HTTP/1.1\r\n\r\n' | nc service-test 80"]
    ```

---

- step3，已經建立的網路
  - 經過 step1 和 step2，我們可以得到目前建立的 pods，注意，service 代理 client-pod

    | 名稱    | IP           | 網域       |
    | ------- | ------------ | ---------- |
    | pod1    | 10.0.1.2     | 10.0.0.x   |
    | pod2    | 10.0.2.2     | 10.0.0.x   |
    | service | 10.3.241.152 | 10.3.240.x |

    <img src="network/pod-communication-1.png" width=500 height=auto>

  - 從上表可以看出，`pods 的網域`和` service網域`不是同一個網域，
    - pods 所在的網域稱為 pod-network，是 `10.0.0.x` 的區段
    - service 所在的網域稱為 service-network，是 `10.3.240.x` 的區段
    - 透過 2-1 的 `kubectl describe指令`可以發現，`pod-network 和 service-network 是連接的`

      ```js
      // service 管理 server-pod1(10.0.1.2:8080) 和 server-pod2(10.0.2.2:8080)
      Endpoints:              10.0.1.2:8080, 10.0.2.2:8080   
      ```

---

- step4，client-pod 到 service-pod 的通訊過程
  - 在 step2 中，client-pod 透過 `nc service-test 80` 命令向 service 發起請求

  - 4-1，client 透過 dns-server 將 service-test 的網址，還原為 serviceIP=10.3.241.152
  
  - 4-2，請求送往 client-pod 的虛擬網關(veth1)，但 veth1 無法識別 10.3.241.152，轉發請求給上一級網關(cbr0)
  
  - 4-3，請求送往 橋接器(cbr0)，但 cbr0 無法識別 10.3.241.152，轉發請求給上一級網關(eth0@10.100.0.2)

  - 4-4，kube-proxy 會監聽對 service 發起的請求，並`修改為正確的 pod-address`

    - 透過 netfilter + iptables，`將 10.3.241.152(service) 修改為 10.0.2.2:8080(server-pod)`
    - netfilter 是`內核級別的代理`，可以添加路由規則，用於匹配數據包的內容，
    - 當數據包的內容與某個路由規則匹配時，將數據包轉發給 iptables 中紀錄的目的地
    - 注意，kube-proxy與一般的代理不同，負責將數據包的地址重新定位到 pod，且是跟 api-server 中的數據是同步的

  - 4-5，請求的目的地改為 server-pod 後，轉發請求給上一級網關(router/gateway)
  - 4-6，在 router/gateway 得知 10.0.2.2:8080 位於 10.100.0.3 的 node主機中，轉發給 10.100.0.3 的網關
  - 4-7，轉發給下一級的橋接器(cbr0@10.0.2.1)
  - 4-8，轉發給下一級的虛擬網關(veth0@10.0.2.2)
  - 4-9，轉發給 server-pod

## Ingress，外界與 pod 通訊
- Ingress 的位置
  
  <img src="network/where-is-ingress.png" width=400 height=auto>
  
- Why need Ingress
  - 避免直接使用 ip:port 存取 service，將 service 變成內部私有網路
    直接使用 ip 存取 service 只適合開發階段使用

  - 可以設置 https，適合在生產端使用

- 比較用，`不使用 ingress` 的 service 配置 (service 曝露到外網)
  ```js
  apiVersion: v1
  kind: Service
  metadata:
    name: microservice-external-service
  spec: 
    selector:
      app: myapp
    // 需要設置為 LoadBalancer
    type: LoadBalancer
    ports:
      - protocol: TCP
      - // service-port
        // 透過 http:// microservice-external-service:35010 存取 service
        port: 8080
        // pod-port
        targetPort: 8080
        // node-port
        // 需要設置 nodePort
        nodePort: 35010
  ```

- 使用 ingress 範例

  - step1，建立 service
    ```js
    apiVersion: v1
    kind: Service
    metadata:
      name: myapp-internal-service
    spec: 
      // 不用設置為 LoadBalancer
      type: ClusterIP
      selector:
        app: myapp
      type: LoadBalancer
      ports:
        - protocol: TCP
        - // service-port
          port: 8080
          // pod-port
          targetPort: 8080
          // 不需要設置 nodePort
          // nodePort: xxxx
    ```

  - step2，建立 Ingress 組件
    ```js
    apiVersion: networking.k8s.io/v1beta1
    // 建立 Ingress
    kind: Ingress
    metadata:
      name: myapp-ingress
      annotations: 
        kubernetes.io/ingress.class: "nginx"
    spec:
      // routing-rules 建立路由規則用
      // 將請求轉發到內部 service 的路由規則
      rules:
      // 當使用者輸入 http://myapp.com 的請求，
      // 就轉發給 microservice-internal-service:8080 的 service
      - host: myapp.com
        // 不是指客戶端請求協定，而是 ingress 轉發給 service 使用的協定
        http:
          // the URL path，設置資源路徑
          paths:
            - backend:
                // 指定 ingress 對接的 service
                serviceName: microservice-internal-service
                serverPort:8080
    ```
  
  - step3，安裝 Ingress Controller
    - yaml-file 建立的組件，`只負責建立路由規則`，
      還需要一個 Ingress-Controller，`評估進入的請求是否符合路由規則`，
      並執行Ingress組件建立的路由規則

      <img src="network/ingress-flow.png" width=500 height=auto>

    - Ingress-Controller 的選擇
      - 有 k8s 內建的 Nginx-Ingress-Controller
      - [第三方供應商提供 Ingress-Controller 列表](https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/)

  - step4，設置 Ingress-entrypoint 的幾種方式
    
    Ingress-entrypoint 用於指向 Ingress-Controller

    <img src="network/setup-ingress-entrypoint.png" width=500 height=auto>

  - step4，選用，利用 hosts 檔案，簡單設置 Ingress-entrypoint (僅測試開發用，不推薦用於生產環境)
    - 以 linux 為例，編輯 /etc/hosts 檔案，添加以下
      ```js
      // 192.168.64.4 為本機IP，可透過 ifconfig 查詢
      192.168.64.4    myapp.com
      ```

    - 設置後，在瀏覽器輸入 http://myapp.com，就會進入 Ingress-Controller

  
- 範例，建立 ingress-entrypoint 的完整範例
  
  [以minikube為例](https://youtu.be/80Ew_fsV4rM?t=976)
  
- 設置 ingress 路由`在未匹配時的預設後端`

  <img src="network/default-backend-of-ingress.png" width=700 height=auto>

- 多個 service 可以設置為 `Multiple path` 或` Multiple sub-domains`，兩者是等效的
  - multiple-path
  - multiple-subdomains

  <img src="network/multiple-path-and-subdomains.png" width=750 height=auto>


- 設置 https

  <img src="network/use-https-in-ingress.png" width=750 height=auto>

  注意，tls.crt 和 tls.key `必須是是憑證檔的內容`，不是檔案路徑

## 範例，外部客戶端與 pod 通訊範例
- 實現方法1，不推薦，實際上此方式並不可靠

    <img src="network/pod-communication-2.png" width=500 height=auto>

  - 缺點，此方式必須依賴 node 主機穩定可靠，且 nodeIP不會改變，
    當 nodeIP 改變後，`router/gateway 之中的規則就失效了`

  - 1-1，外部客戶端向 service 的 clusterIP (10.3.241.152:80) 發起請求

  - 1-2，經過 router/gateway 的映射表，轉發到 node2 (10.100.0.3)

  - 1-3，在 eth0，請求的數據包被監聽的 kube-proxy + netfilter + iptables，
    將 clusterIP (10.3.241.152:80) 更改為 podIP (10.0.2.2)

  - 1-4，請求的數據包 傳遞給 cbr0 ，且 podIP 是 cbr0 可是別的網路設備

  - 1-5，cbr0 將請求轉發給 veth0 (10.0.2.2)

  - 1-6，veth0 將請求轉發給內部的 container

- 實現方法2，透過 nodePort 或 Ingress，`推薦使用 Ingress`
  
  <img src="network/pod-communication-3.png" width=500 height=auto>

  - 注意，此方法會在 node 建立 NodePort (此例中的 32213)，
    NodePort 會與 service 相連接，service 會指向最終的 podIP

  - 2-1，外部客戶端向 loadbalancer 發出請求，loadbalancer 會將請求分發給任意節點主機的 NodePort (nodeIP:nodePort)，
  
    注意，此時就不需要在 router/gateway 紀錄 node主機的位置了，
    因為 node 主機是透過 loadbalancer 隨機分派，不限定特定 node 主機

  - 2-2，node主機上的 kube-proxy 會監聽 nodePort 上的請求，並轉發給 service (10.3.241.152:80)

  - 2-3，node主機上的 kube-proxy + netfilter + iptables，
    將 clusterIP (10.3.241.152:80) 更改為 podIP (10.0.2.2:8080)

  - 2-4，請求的數據包 傳遞給 cbr0 ，且 podIP 是 cbr0 可是別的網路設備

  - 2-5，cbr0 將請求轉發給 veth0 (10.0.2.2)

  - 2-6，veth0 將請求轉發給內部的 container

## 參考
- [Kubernetes Service Overview](https://godleon.github.io/blog/Kubernetes/k8s-Service-Overview/)

- []

- [k8s service 的配置範例](https://ithelp.ithome.com.tw/articles/10194344)

- [Container Networking From Scratch](https://www.youtube.com/watch?v=6v_BDHIgOY8)
  
- [k8s-network-concept](https://jamesdefabia.github.io/docs/getting-started-guides/scratch/)

- [k8s網絡模型指南](https://sookocheff.com/post/kubernetes/understanding-kubernetes-networking-model/)

- [了解k8s網絡-基礎架構](https://medium.com/google-cloud/understanding-kubernetes-networking-pods-7117dd28727)
  
- [了解 Kubernetes 網絡 - Ingress](https://medium.com/google-cloud/understanding-kubernetes-networking-ingress-1bc341c84078)

- [建立外部服務與Pods的溝通管道 - Services](https://ithelp.ithome.com.tw/articles/10194344)

- [Kubernetes Service 深度剖析 - 存取路徑差異](https://tachingchen.com/tw/blog/kubernetes-service-in-detail-1/)

- [Kubernetes Services explained](https://www.youtube.com/watch?v=T4Z7visMM4E)
  and [code](https://gitlab.com/nanuchi/youtube-tutorial-series/-/tree/master/)

- [Kubernetes Ingress Tutorial for Beginners](https://www.youtube.com/watch?v=80Ew_fsV4rM)

- [動畫演示通訊流程](https://sookocheff.com/post/kubernetes/understanding-kubernetes-networking-model/) and [here](https://www.youtube.com/watch?v=NFApeJRXos4)