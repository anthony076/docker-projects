## k8s 的架構和組件
- 架構，
  - 示意圖，from [here](https://platform9.com/wp-content/uploads/2019/05/kubernetes-constructs-concepts-architecture.jpg) and [here](https://phoenixnap.com/kb/understanding-kubernetes-architecture-diagrams)
  
    <img src="doc/overview/k8s_componet_architecture-1.png" width=600 height=auto>

    <img src="doc/overview/k8s_componet_architecture-2.png" width=600 height=auto>
  
  - 相關的元件
    - kubectl，
  
      命令行工具，提供 管理員 和 api-server 的通訊界面
    
    - Manager-Node
      - `api-server`，
        - 接收 kubectl-cli 輸入的指令後，依照指令的類型，`分發給對應的組件`進行處理
        - 將組件的結果，輸出給 api-server，並由 api-server 轉發 worker-node
        - 負責 Authentication、Authorization、Access Control、API registration & discovery

      - `etcd組件`，負責以 key-value 的方式存儲資料，儲存集群數據、配置、狀態
      - `controller-manager組件`，負責控制 Worker-Node 狀態和預期狀態的差異，例如，auto scale
      - `scheduler組件`，負責將 task 分派給 Worker-Node

    - Worker-Node
      - 注意，和 docker 不同，`container 提供的 service 不是綁定在一起的`，
  
        在 k8s 中，container 和 service 是分離的，可以隨時抽換或取代
      
      - 多個網路組件
        - `kubelet組件`，pods 的老大
          - 與 `manager-node` 的通訊
          - 管理當前 node 上的所有 pods
          - 維護 container 的生命週期

        - `kube-proxy組件`，
          - 與 `client` 的通訊界面，負責處理 client 的請求
          - 根據 iptables 限制輸入/輸出的流量控制
          - 實現流量負載平衡
    
      - 與服務相關組件
        - `pods組件`，裝載 container 的組件
        - `runtime組件`，執行 container 內容的引擎
        - services，CRI組件將pod組件運行後，就能運行服務

      - 其他選用組件: 
        - UI組件
        - DNS組件
        - Ingress-controller組件，流量控制組件
        - Resource-monitoring組件，資源監控組件
        - 其他插件，可參考，[官方 addone](https://github.com/kubernetes/kubernetes/tree/master/cluster/addons)

    - Load Balancer
      
      負責終端用戶的負載平衡

## 各種標準介面 Interface
  - 透過一個標準介面，使得每個組件之間的通訊，不需要依賴特定的實現

  - [示意圖](https://godleon.github.io/blog/Kubernetes/k8s-CoreConcept-Cluster-Architecture/)

    <img src="doc/overview/protocol_in_k8s.jpg" width=˙00 height=auto>

  - JSON
  
    User 和 api-server 使用 RESTAPI 進行通訊，RESTAPI 需要傳遞 JSON　格式的數據
  
  - Master-Node 中，
    - 和 Scheduler、Controller-Manager、Worker-Node 之間，使用 Protobuf 進行資料的傳遞
    - 和 etcd 之間，使用 gRPC 進行資料傳遞
  
  - 在 Worker-Node 中，
    - CNI，Container Runtime Interface，`kubelet` 和 `本機網路` 之間的通訊協定
    - CRI，Container Runtime Interface，`kubelet` 和 `runtime` 之間的通訊協定
    - OCI，指 OCI 的 Runtime-SPEC，`runtime` 和 `本機OS` 之間的通訊協定

## k8s 基礎
- k8s 可部屬在實體主機、虛擬主機(virtual-machine)、容器主機(container)上，
  三種方式都可以作為 k8s 的節點

- 節點分為管理節點(Manager-Node)和工作節點(Worker-Node)，
  只有一台主機能夠擔任Manager-Node

- 網路架構基礎
  - 透過 `service` 建立的網路架構，可以避免 pod 重啟後的通訊失敗，
  - 透過 `Ingress`，避免使用 ip 與 service 通訊  
  - 參考，[k8s 網路架構基礎](doc/network_architecture.md)

## 其他章節
- [OCI，開放容器標準](doc/oci_standard.md)
- [k8s 網路架構基礎 + Service + Ingress](doc/network_architecture.md)

- [搭建k8s環境 - 概述](doc/build_k8s_overview.md)
- [搭建k8s環境 - kubeadm](doc/build_k8s_kubeadm.md)
- [搭建k8s環境 - kind]()

- [k8s的使用]()
- [Helm的使用](doc/helm.md)

## 工具
- Rancher: GUI 版的 k3s，可利用 k3s 管理多個 docker 容器
  - [rancher-tutorials](https://ithelp.ithome.com.tw/articles/10270982)
  - [介紹 Rancher](https://www.hwchiu.com/rancher-2.html)

- Portainer: 透過 WebUI 可以監控其他docker容器的狀況
  - [官方網站](https://www.portainer.io/)
  - [Portainer安裝](https://cutejaneii.wordpress.com/2017/04/17/docker-2-%E5%AE%89%E8%A3%9Ddocker%E5%8F%8Aportainer/)

## Ref
- [官網](https://kubernetes.io/docs/tutorials/)
  
- [Kubernetes 的基礎世界](https://fufu.gitbook.io/kk8s/kubernetes-basic)

- [Kubernetes中文指南/雲原生應用架構實戰手冊 @ Jimmysong](https://jimmysong.io/kubernetes-handbook/cloud-native/quick-start.html)

- [Kubernetes中文指南/雲原生應用架構實戰手冊 @ github/rootsongjc](https://github.com/rootsongjc/kubernetes-handbook)
  
- [kubernates原理與實作 + Rancher + DevOps與k8s @ hwchiu](https://www.hwchiu.com/ithome/2019/overview/day1.html)

- [Kubernetes 指南](https://feisky.gitbooks.io/kubernetes/content/)
- [一文搞懂容器運行時Containerd](https://www.qikqiak.com/post/containerd-usage/)

- [CNI 的選擇與介紹](https://www.hwchiu.com/cni-compare.html)