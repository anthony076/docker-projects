## Ansible Usage

- why Ansible
  - Ansible 是配置自動化工具，透過 yaml 的語法實現配置/安裝自動化，
    ，使用 SSH 作為安全保障，並搭配python使用簡單

  - 可以使不同平台的`命令標準化`，

    例如，ubuntu 透過 apt 安裝軟體，arch 透過 pacman，fedora 透過 dnf，
    透過 ansible 可以使用標準化的命令進行安裝
  
  - 可以`同時操作`不同的機器
  
  - 安裝軟體時，可以主動輸入，實現`軟體自動安裝`，[參考](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_prompts.html)

- ansible 的其他選擇: Puppet、SaltStack、Chef、Terraform、SaltStack
  - SaltStack 響應速度必 ansible 快 (使用 zeroMQ)，Ansible 雖慢但足以日常使用，
  - SaltStack 的安全性較 ansible 差，ansible 使用 SSH 連線
  - Ansible 的語法比 SaltStack 具有更好的可讀性，使用也簡單
  
## Ref
- [一定要知道的 Ansible 自動化組態技巧](https://ithelp.ithome.com.tw/users/20031776/ironman/1022)  

- [install vnc-server using Ansible](https://rbgeek.wordpress.com/2015/03/18/&installing-the-vnc-server-on-ubuntu-using-ansible/)

- [ 30 天入門 Ansible 及 Jenkins](https://tso-liang-wu.gitbook.io/learn-ansible-and-jenkins-in-30-days/ansible/ansible/ansible-role-intro)