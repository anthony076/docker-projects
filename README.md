## Overview
- [docker相關](docker/readme.md)，docker容器管理
- [k8s](k8s/readme.md)，容器管理
- [proxmox(PVE)](proxmox/readme.md) OS，vm/container 的管理工具，開源的服務器虛擬化和集群管理工具，輕量易用

- [vagrant](vagrant/readme.md) 
  - 利用 vagrant 快速建立vm環境(virtualbox + vmware + docker)
  - 需要手動建立 vm > 手動建立 box 檔 > (選用) 上傳到 vagrant-cloud > 利用 vagrant 啟動

- [packer](packer/readme.md)
  - 自動建立vm環境並打包成各個雲平台或各種容器支援的映象檔
  - 根據 json 檔的配置自動建立 vm 並打包為映像檔

- [ansible](ansible/readme.md) 多主機自動配置，自動化運維工具

---

- [ssl](ssl/readme.md)，建立安全憑證
- [ssh_proxy](ssh_proxy/readme.md)，ssh 和 proxy 相關，遠端連線
  
---

- [nginx](ngix-server/readme.md)，具有靜態資源服務器、代理、自動負載平衡的 web-server
- [Traefik](Traefik/readme.md)，比 nginx 更好用的 反向代理+自動平衡
- [HAProxy](HAProxy/readme.md)，免費、高效、穩定的負載均衡服務器
- [dns-server](dns-server/doc/dns_basic.md)，域名解析和手動建立 dns-server
- [WireGuard-VPN-Server](doc/../wireguard/readme.md)

---

- [usbip](usbip/readme.md)，共享usb
- [code-server](code-server/readme.md)，在瀏覽器中使用 vscode
- [vnc-server](vnc-server/readme.md)，透過 vnc 共享桌面

## 比較
- ssh 和 vpn 的差別
  - vpn = 虛擬專用網路，會在客戶端`建立新的網路連接`，
    並將客戶端的數據進行加密後，再透過客戶端的網卡發送指定的 VPN server，

    啟用VPN後，會將客戶端`所有的網路請求`都透過新的網路連接進行發送

  - ssh = secure shell，是一套通訊的協定，使用者和遠方主機通訊前，
    會先透過非對稱交握來驗證對方的身分，通過後，才能透過加密通訊存取對方的資源

    ssh 不會建立新的網路連接，並且只在單一個 shell 內有效

  - vpn 會和原來的網路連接斷開，並使用新的網路連接，SSH 不會斷開原來的網路連接
  
## Project on docker-hub
- mybox
  - [hub](https://hub.docker.com/repository/docker/chinghua0731/mybox)
  - [dockerfile](https://gitlab.com/anthony076/docker-projects/mybox)

- spinalhdl-dev
  - [hub](https://hub.docker.com/repository/docker/chinghua0731/spinalhdl-dev)
  - [dockerfile](https://gitlab.com/anthony076/spinalhdl_study/-/blob/main/docker/Dockerfile-spinalhdl-dev)

- riscv-gcc
  - [hub](https://hub.docker.com/repository/docker/chinghua0731/riscv-gcc)
  - [dockerfile](https://gitlab.com/anthony076/spinalhdl_study/-/blob/main/docker/Dockerfile-risc-gcc)

- icestorm-toolchain
  - [hub](https://hub.docker.com/repository/docker/chinghua0731/icestorm-toolchain)
  - [dockerfile](https://gitlab.com/anthony076/spinalhdl_study/-/blob/main/docker/Dockerfile-iceStorm)

- spinalhdl-bootcamp
  - [hub](https://hub.docker.com/repository/docker/chinghua0731/spinalhdl-bootcamp)
  - [dockerfile](https://gitlab.com/anthony076/spinalhdl_study/-/blob/main/docker/Dockerfile-bootcamp)

- arch-bochs
  - [hub](https://hub.docker.com/repository/docker/chinghua0731/arch-bochs)
  - [dockerfile]()
