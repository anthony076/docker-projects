## Docker-swarm 集群管理

- 基本概念
  ```mermaid
  flowchart LR
  b1[swarm\nmanager-node\n實體主機] --> 
  b2[service-object] -->
  b3[task\nworker-node\n實體主機]
  b2 --> b4[worker-node]
  b2 --> b5[worker-node]

  ```
  - 一個實體主機就是一個 node，只有 manaerger-node 能建立 swarm

  - 利用 swarm 管理多個節點 (管理多個主機執行的 swarm-service)

  - 在眾多主機中，只有一個主機可以成為 manager-node，透過 `docker node promote/demote` 切換腳色
  
  - 只有 manager-node 才可以建立群 (建立 swarm)

  - 在 manager-node 上分派建立 swarm-service 的任務
  
- service 和 swarm-service 的差異
  - 一般容器中執行的服務為 service，在集群環境中執行的服務為 swarm-service (集群服務)

    兩個 service 執行的內容相同，差別在執行環境不同，集群環境是可伸縮且高度可用的

  - 在一般容器中，透過` docker run` 建立容器和執行 service
  
  - 在swarm的集群環境中，
    - 透過 `docker service create` 或 `docker stack`，
      
      才能將容器建立的 swarm-service 部屬到高可用的集群環境中，並執行 swarm-service

    - step1，執行 `docker service create` 後，manager-node 會接受命令並建立 service-object
    - step2，為 service-object 建立 task
    - step3，為 task 分配 ip
    - step4，將 task 分發給 worker-node
    - step5，由 worker-node 中的 docker-engine，執行 service-object 的內容

    - [圖解流程](https://www.netadmin.com.tw/netadmin/zh-tw/feature/167CDFB3615E42229B5C7053DC452755?page=4)
      
      <img src="swarm/run_service_in_swarm.jpg" width=500 height=auto>

    - [serviceObject、task、node 的關係](https://www.netadmin.com.tw/netadmin/zh-tw/feature/167CDFB3615E42229B5C7053DC452755?page=4)

      <img src="swarm/serviceObject_task_node_relationship.jpg" width=400 height=auto>


- 建立 swarm-service 的兩種方式
  - 利用 `docker service` 命令，手動建立 swarm-service
  - 利用 `docker stack` 命令，讀取 docker-compose-file 的內容，並建立 swarm-service 

## docker service 的參數
- `--name`，swarm-service 的名稱
  
- `--replicas 數值`，建立的 swarm-service 數量 == task 的數量 == node 的數量

- `--publish 模式`，指定容器及node主機之間的連接埠映射關係
  > --publish mode=host,published=80,target=80：

- `--endpoint-mode 負載平衡模式`，
  > --endpoint-mode dnsrr：採用 dns-round-robin 負載平衡機制

- `-d 映像檔名稱`，背景執行容器

## swarm-service 在 docker-compose 中的配置
- deploy參數
  - `mode: replicated`
    
  -` replicas: 數值`，建立的 swarm-service 數量 == task 的數量 == node 的數量

  - `labels: [APP=VOTING]`

  - `update_config欄位`
    ```json
    update_config:
      parallelism: 2
      delay: 10s
    ```

  - `restart_policy欄位`
    ```json
    restart_policy:
      # 只要有一個掛點就重開container
      condition: on-failure
      delay: 10s
      max_attempts: 3
      window: 120s
    ```

  - `placement欄位`
    ```json
    placement:
      constraints: [node.role == manager]
    ```

  - `resource欄位`
    ```python
    resources:   # 每一個最多只能用10%的CPU / 50MB的ram
      limits:
        cpus: "0.1"
        memory: 50M
    ```

## 範例，建立 swarm-service
- step1，建立 swarm
  > docker swarm init

- step2，利用 docker-machine 建立 node
  - 建立 myvm1-node (建立主機)
    > docker-machine create --driver virtualbox myvm1

  - 指定 myvm1 為 manager-node
    > docker-machine ssh myvm1 "docker swarm init --advertise-addr <myvm1 ip>"

  - 建立 myvm2-node (建立主機)
    > docker-machine create --driver virtualbox myvm2

  - 指定 myvm2 為 worker-node
    > docker-machine ssh myvm2 "docker swarm join --token <token> 192.168.99.101:2377"

- step3，啟動 swarm-service
  - 方法1，直接透過 `docker service 命令`啟動 swarm-service
    ```python
    docker service create \
      --name=viz \
      --publish=8080:8080 \
      --constraint=node.role==manager \
      --mount=type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock \
      dockersamples/visualizer
    ```

  - 方法2，透過 `docker-compose-file + docker stack 命令` 啟動 swarm-service
    - 2-1，建立 docker-compose-file 的內容
      ```python
      # docker-compose.yaml
      version: "3.1"
      
      services:
        viz:
          image: dockersamples/visualizer
          ports:
            - 8080:8080
          volumes:
              - /var/run/docker.sock:/var/run/docker.sock
          deploy:
              placement: 
                  constraints: [node.role == manager]
      ```

      - 2-2，透過 docker 命令執行建立swarm-service和啟動
        > docker stack deploy -c docker-compose.yaml viz


- step4，檢視結果
  - 檢視Service清單
    > docker service ls

  - 檢視 Service內的執行的container 
    > docker service ps <app名稱>_<service名稱>

  - 關閉 app stack
    > docker stack rm <app名稱>

  - 關閉 swarm
    > docker swarm leave --force

## 範例，建立5個service/3個網路
- [blog](https://blog.51cto.com/lwc0329/3010855)
- [source](https://github.com/dockersamples/atsea-sample-shop-app)

<img src="swarm/shop-app-example.png" width=500 height=auto>

## docker-swarm 的缺點
- 跨平臺支持效果差
  
  Docker Swarm是一個爲Linux設計的平臺。雖然Docker支持Windows和Mac OS X，但它使用虛擬機在非linux平臺上運行。
  設計爲在Windows上的Docker容器中運行的應用程序不能在Linux上運行，反之亦然

- 不提供存儲選項
  
  Docker Swarm不提供將容器連接到存儲的簡便方法，這是主要缺點之一。它的數據量需要對主機和手動配置進行大量的改進。
  如果想要Docker Swarm解決存儲問題，也能辦到，但是方式並不高效，且對用戶並不友好。

- 監控信息不足

  Docker Swarm提供關於容器的基本信息，如果只需要基本的監控解決方案，那麼使用Stats命令就足夠了。
  如果尋找先進的監控，那麼Docker集群不是好的選擇。
  雖然有像CAdvisor這樣的第三方工具可以提供更多的監控，但是使用Docker本身實時收集更多關於容器的數據是不可行的。


## Ref
- [swarm concept](https://docs.docker.com/engine/swarm/key-concepts/)
- [swarm tutorial](https://docs.docker.com/engine/swarm/swarm-tutorial/)
- [swarm 和 kubernetes 的比較](https://www.twblogs.net/a/5d0ca0c7bd9eee1e5c81967a)
