## Docker-Compose 多容器管理(服務管理)

- 使用場景，
  
  一次性管理多個容器，將每個容器的 docker-build/docker-run 的命令參數檔案化，簡化參數的輸入

- docker-compose 的配置檔，yaml 檔案的格式

  <img src="compose/yaml-format.png" width=500 height=auto>

- 配置檔主要分為三個區塊
  - `services，服務配置區塊`
    - 每個 service 可以由一個至多個的容器組合而成

  - `networks，網路配置區塊`
    - 用於指定 service 使用獨立/共享網域
    - 若不指定，預設所有的 service 都使用相同的網域

  - `volumes，資料卷配置區塊`
    - 用於指定 service 使用獨立/共享的資料卷
    - 若不指定，預設不分配資料卷

- 比較，Layer-1 的 service 和 stack 都使用 docker-compose 構建
  - docker-compose for service，可以 build image

    docker-compose for stack，無法 build image

  - docker-compose for stack 只支援 version3 的 compose-file
  - 參考，stack 和 service 的差異，見 [docker元件概述](../readme.md)

## services 服務配置區塊
- build參數，建構 container
  - 方法1，指定 dockerfile 的目錄，預設尋找檔名為 Dockerfile
    ```python
    # 等效於，docker build -t xxx .
    build: dockerfile的目錄
    ```
  - 方法2，指定 dockerfile 的路徑
    ```python
    # 等效於，docker build -f dockerfile的路徑 -t xxx .
    build:
      context: .
      dockerfile: dockerfile的路徑
    ```

- image參數，指定image的名稱
  - 方法1，若是 docker-hub 上的 image
    ```
    image: chinghua0731/arch
    ```
  - 方法2，和 build 一起搭配使用，用來指定 build 建立的 image 名稱
    ```
    build: dockerfile的目錄
    image: myimage
    ```
  
- container_name參數，指定 container 的名稱

- environment參數，建立 container 的環境變數
    ```
    environment:
      AA : 123
      BB : 456
    ```

- port參數，指定 port-forwarding
  - 必須使用 list
  - 範例
    ```
    port:
      - 8080: 80
      - 8081: 81
    ```

- command參數，覆寫預設的指令

- deploy參數，指定容器部署及運行時的設定值

- depends_on參數，指定服務與其他服務間的相依性
  - depends_on 中的服務必須先被建立或啟動。

- entrypoint參數，覆寫預設的 entrypoint

- restart參數，自動重啟
  - 可用值，no | on-failure | always | unless-stopped
    - no，預設值，不要自動重啟 container
    - on-failure，發生錯務時重啟，出現非0的 exit-code 時重啟
    - always，停止時自動重啟，手動關閉或透過 docker-compose命令重啟後，會自動啟動
    - unless-stopped，類似 always，但手動關閉或透過 docker-compose命令重啟後，不會自動啟動
  
  - 例如，
    - restart: on-failure:5，發生錯務時重啟，最多嘗試5次
  
## services 服務配置區塊(swarm專用參數)
- deploy參數
  
  參考，[docker-swarm](docker-swarm.md#swarm-service-在-docker-compose-中的配置)

## networks 網路配置區塊
- 語法
  ```python
  services:
    aa:
      # 指定 aa-service 使用的網域
      networks:
          - frontend
    bb: 
      # 指定 bb-service 使用的網域
      networks:
          - backend

  networks:
    # 建立 frontend 網域
    frontend:
    # 建立 backend 網域
    backend:
  ```

## volumes 資料卷配置區塊
- 語法
  ```python
  services:
    aa:
      # 建立 aa-service 的資料卷映射
      volumes:
        - share-data:/path/to/specific
    bb: 
      # 建立 bb-service 的資料卷映射
      volumes:
        - share-data:/path/to/specific
    cc: 
      # 建立 cc-service 的資料卷映射
      volumes:
        - cc-data:/path/to/specific
  
  # 建立資料卷
  volumes:
    share-data:
    cc-data:
  ```