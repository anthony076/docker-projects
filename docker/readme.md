## Docker 概述

- Docker 的架構是 Client-Server 的架構，
  - Client 端稱為 `Docker-client`，
    - 使用 RestfulAPI 與 Docker-daemon 進行通巡，
    - 提供 command line 的方式讓使用者可以操作 docker

  - Server 端稱為`Docker-Daemon`，
    - 執行管理 Docker image、啟動 container、停止 container 的 service
    - 提供 Restful API 給 client 執行操作
    - Docker 1.11 之後，`將 Docker-Daemon 拆分為以下的組件`
      - `dockerd`，把 Docker-Daemon 中，server 的部分獨立出來
      - `containerd`，container-manager，高階的 runtime，用來啟動容器的運行時，管理主機、網路、命名空間等
      - `docker-containerd-shim`，containerd 和 OCI-runtime 的中介層，將 containerd 轉換為 OCI-runtime 格式
      - `OCI-runtime(runc)`，實際執行 container 內容的運行時
      - `container`，容器
  
    - [架構示意圖](https://iximiuz.com/en/posts/implementing-container-runtime-shim/)
      <img src="doc/docker-architecture.png" width=700 height=auto>

  - docker-run / docker-build 是將特定的操作，`重新封裝為高級的命令`

## Docker 元件
  
  <img src="doc/docker-component.png" width=600 height=auto></img>

  - 一般服務 (service)
    - container 容器 = 最小的組成單位，根據 image 的內容構建而成

    - service 服務 = container 的集合
      - 透過 `docker run命令`，可以建立容器，並啟動容器提供的`單一服務`
      - 或透過 `docker-compose命令`，讀取 docker-compose-file 的內容，並建立和啟動`多個 service` 

  - 在集群中的服務 (swarm-service)
    - stack 棧 = service 的集合，對集群才有作用
      
      利用 `docker stack命令`，讀取 docker-compose-file 的內容，並建立多個 swarm-service 

    - node 節點 = docker-engine的實例，用來`執行 swarm-service`，每個節點就是一台實體主機
      - worker-node 工作節點，帶有執行器可以執行 swarm-service
      - Manager-node 管理節點，具有調度功能的節點，不執行 service ，只負責調度

    - Swarm 集群 = 節點的集合，管理所有的節點

## Docker 和 systemd
- [設計原則] 使用 docker 設置微架構的應用時，`推薦一個 container 只執行一個應用` 的架構，
  Docker 的設計上只有一個 ENTRYPOINT 和 CMD，實際上也很難跑多個 process

- [問題] 為什麼 docker 不能執行 systemd

  - `衝突1`，在正常的主機中，systemd 是`第一個執行的程式(PID=1)`，負責所有服務的初始化和啟動，

    在容器化的 docker 中，docker 取代了 systemd 的地位，

    當容器啟動後，透過 CMD 或 ENTRYPOINT 啟動的程式，成為了第一個執行的程式(PID=1)，
    當該程式結束時，容器也就結束了
    
    當執行 docker stop ，實際上是對容器中的PID1發送SIGTERM訊號 ，若應用程式不處理SIGTERM訊號(例如，shell-script)，
    等待一段時間後就會發送SIGKILL訊號，強迫使該應用程式停止，

    例如，會處理 SIGTERM 訊號的 shell-script
    ```shell
    trap 'exit 0' SIGTERM     # 捕捉 SIGTERM 訊號
    while true; do :; done
    ```
    
  - `衝突2`，Docker 使用的是 Server/Client 架構，容器相當於是Client端，Docker守護進程是Server端，
    
    在正常主機下，systemd 在 service 準備就緒後，透過 sd_notify 通知其他相依的服務

    Docker守護進程不負責啟動 service，不知道 service 的狀態，無法確認發送時機，

    即使 Docker 可以獲知 service 的狀態，位於容器端的systemd位於Client端，
    無法主動發起 sd_notify 的功能通知相依的服務

  - `衝突3`，Linux的cgroup可用來定義每個服務的系統資源額度，比如CPU、內存及I/O限制
    但只限於對客務端有效，不對容器所在的守護進程有效

  - [參考](https://dockone.io/article/1093)

- [解決方式] 一個容器跑多個 process 的選擇

  - `方法1`，手動配置 `docker守護進程`

    參考，[自定義 Docker 守護進程選項](https://docs.docker.com/config/daemon/systemd/)

  - `方法2`，透過 Docker-Compose

    參考，[使用 systemd 管理 Docker container](https://blog.davy.tw/posts/use-systemd-to-manage-docker-container/)

  - `方法3`，透過 shell-script
    - 將要執行的多個應用寫在同一個 shell，
    - 若應用會阻塞，可在啟動命令的尾部加上 &，使該應用在背景執行
    - 在 shell 的尾部執行無窮迴圈，避免應用結束
    - 缺點，因為無窮迴圈，使得所有的應用都無法接收SIGTERM訊號

  - `方法4`，在容器內透過 supervisor 套件實現 systemd 的功能
    - supervisor 是能控制多個 process 執行的管理器
    - 優點，不需要 shell，且可接受SIGTERM訊號
    - [範例](https://ithelp.ithome.com.tw/articles/10252546)
    - [範例](https://doc.yonyoucloud.com/doc/docker_practice/cases/supervisor.html)

- [解決方法] 使應用程式可以接收訊號 (不能用於啟動多個應用)
  - 方法1，透過 [dumb-nit](https://github.com/Yelp/dumb-init)
  - 方法2，透過 [tini](https://github.com/krallin/tini)
  
## Docker + GUI

- [桌面系統基礎](https://gitlab.com/anthony076/linux-usage/-/blob/main/desktop.md)

- 狀況1，只顯示 GUI應用
  - 方法1，透過 x11 或 wayland
  - 方法2，透過 broadway，缺點，僅限 GTK 應用

- 狀況2，顯示整個桌面系統
  - 方法1，透過 vnc
  - 方法2，透過 rdp
  - 方法3，透過 x2go

- 狀況3，虛擬顯示器，
  - 方法1，透過 xvfb: 讓 xclient 傳送繪圖請求給虛擬的 xserver，不會真正的顯示，可以使 gui 應用正常運行
    - 參考，[使用 Xvfb 執行 GUI應用](https://officeguide.cc/linux-x-virtual-framebuffer-environment-run-x-window-client-tutorial-examples/amp/)
    - 參考，[Xfce+xvfb inside Docker](https://sick.codes/xfce-inside-docker-virtual-display-screen-inside-your-headless-container/)
    - 參考，[xvfb + x11vnc，顯示 xvfb 虛擬的畫面](https://blog.csdn.net/qq_36383272/article/details/114970803)

## [Dockerfile] ARG/ENV 的使用
- 使用差異
  - ARG，構建參數
    
    用於 docker build 的參數傳遞，docker run 時，ARG 參數值不可用，

  - ENV，環境變數
    
    可同時用於 docker build 和 docker run 的參數

- ARG 的使用
  - 只能用於`建構image階段`可動態指定參數值
  
  - ARG參數可定義預設值
    > 例如，在 dockerfile 中定義，`ARG FOO=123`

    也可以在執行 docker build 命令時，透過 --build-arg 參數修改
    > 例如，`$ docker build --build-arg FOO=456 .`

  - ARG 具有使用範圍，以 FROM 為界

    <font color=blue>只對 FROM 有效，且 FROM 之前的ARG變數都是共用的</font>
    ```shell
    ARG DOCKER_USERNAME=library     # 只在 FROM 之前生效
    FROM ${DOCKER_USERNAME}/alpine

    RUN set -x ; echo ${DOCKER_USERNAME}  # 錯誤，無法使用 FROM 之前的ARG參數
    
    # --------------------------------------
    FROM ${DOCKER_USERNAME}/alpine      # FROM 之前的ARG變數都是共用的

    RUN set -x ; echo ${DOCKER_USERNAME}  # 錯誤，無法使用 FROM 之前的ARG參數
    ```

    <font color=blue>FROM 之後的ARG變數都是獨立的</font>
    ```shell
    FROM ubuntu/v1

    ARG DOCKER_USERNAME=aa  # 僅在 FROM ubuntu/v1 之後使用
    RUN set -x ; echo ${DOCKER_USERNAME}

    # --------------------------------------
    FROM ubuntu/v2

    ARG DOCKER_USERNAME=bb  # 僅在 FROM ubuntu/v2 之後使用
    RUN set -x ; echo ${DOCKER_USERNAME}
    ```

  - 在 dockerfile 中透過 `${ARG參數名}` 獲取值
  
  - 注意，若 `${ARG參數名}` 用於字串中，`字串需要改用 ""`，避免取不到參數值，
    - 錯誤用法，`sed -i '/$a ${USER_NAME}' 123.ttt`，
    - 正確用法，`sed -i "/$a ${USER_NAME}" 123.ttt`

## [Dockerfile] CMD / ENTRYPOINT
- 比較表
  
  |                   | ENTRYPOINT                        | CMD                    |
  | ----------------- | --------------------------------- | ---------------------- |
  | 使用場景          | 主要命令`執行前`的`初始化工作`    | 容器要運行的`主要命令` |
  | 優先權            | 高                                | 低                     |
  | docker 命令       | 被添加到 ENTRYPOINT 參數後方      | 覆蓋原來的 CMD 參數    |
  | docker 命令的限制 | docker run 需要 --entrypoint 參數 | 不需要添加任何參數     |

- 差異
  - docker run 指定的命令，會`覆蓋`dockerfile中的`CMD`參數，或`添加到`dockerfile中的`ENTRYPOINT`參數，二選一
    
    docker run 指定的命令，例如，`$ docker run -it 容器名 命令或參數`，

    其中，`命令或參數`的位置，是`CMD命令`或是`ENTRYPOINT參數`，
    依照 docker run 是否添加 --entrypoint 參數決定

    - 有添加 --entrypoint 參數: `命令或參數`的位置，實際上是 ENTRYPOINT的添加參數，會添加到 ENTRYPOINT 參數後方
    - 無添加 --entrypoint 參數: `命令或參數`的位置，實際上是 CMD的覆蓋命令，會取代原本的 CMD 參數的命令

    <font color=blue>docker run 覆蓋 CMD 參數範例</font>
    ```shell
    # dockerfile 中
    CMD [ "curl", "-s", "http://www.google.com" ]

    # docker run 指定要執行的命令
    docker run 容器名 -i

    # 實際執行的命令，錯誤方式
    -i 會取代 curl -s http://www.google.com   # error，不是預期的結果

    # 實際執行的命令，正確方式
    docker run 容器名 curl -s http://www.google.com -i
    ```

    <font color=blue>docker run 添加 ENTRYPOINT 參數範例</font>
    ```shell
    # dockerfile 中
    ENTRYPOINT [ "curl", "-s", "http://www.google.com" ]

    # docker run 指定要執行的命令
    docker run 容器名 -i

    # 實際執行的命令
    curl -s http://www.google.com -i
    ```

## Multi-Stage Build 最小image建構
- 最小映像檔的選擇
  - `scratch > base > slim > full` (一般官方映像檔的命名方式) 或 `alpine` (獨立的最小映像檔)
  
  - scratch: 不是真實存在的 docker-image，代指空的內容
  - alpine: 最精簡系統，只有5M，使用 apk 作為包管理器，沒有多餘的套件
  - slim: full image 的瘦身版，沒有安裝開發工具

- [編譯出Go語言最小Image](https://blog.wu-boy.com/2017/04/build-minimal-docker-container-using-multi-stage-for-go-app/)
- [打造最小PythonDocker容器](https://blog.wu-boy.com/2021/07/building-minimal-docker-containers-for-python-applications/)
- [把玩Alpine linux(一)](https://www.linuxprobe.com/alpine-linux.html)

## [範例] 在 docker 中使用 x11 的最小設置 (不使用ssh)

- 注意，若考慮安全性，docker 也可以透過 ssh 與 x11 進行連接，
  
  範例，見[透過 SSH 進行 x11 的轉發](../ssh_proxy/doc/%E9%80%8F%E9%81%8Essh%E5%9F%B7%E8%A1%8Cgui%E7%9A%84%E6%87%89%E7%94%A8-x11.md)

- 概念
  - docker 內的容器有自己的網路介面，因此容器和 host 之間的網路介面是隔離的
  - docker 有提供容器可以存取 host 主機的網路流量轉發，已寫在 docker 中，
  - 在 docker 內部要存取 host 的網路資源，可以透過 `host.docker.internal` 進行存取
  - <font color=red>要顯示gui應用不需要安裝整個桌面系統</font>

- step1，在 windows-host 中安裝 vcxsrv，`$ choco install vcxsrv`
- step2，啟動 vcxsrv，預設安裝在 `C:\Program Files\VcXsrv` 的目錄中
- step3，下載 ubuntu image，`$ docker pull ubuntu`
- step4，啟動容器，`$ docker run -it ubuntu`
- step5，在容器中設置 DISPLAY 環境變數，`$ export DISPLAY=host.docker.internal:0.0`
- step6，安裝測試軟體，`$ apt install x11-apps`
- step7，測試，`$ xeyes`

## [範例] 將容器/image推送到 docker-hub
- 查看 container名，`$ docker ps -a`
- 將 container 轉換為 image，`$ docker commit container名`，
- 查看image的hash值，`$ docker images -a`
- 將hash值轉換為tag，`$ docker tag <HASH值> 用戶名/<自定義image名>`
- 登入 docekr，`$ docker login`
- 將指定image推送到docker-hub，`$ docker push chinghua0731/<自定義image名>`

## 其他主題
- [docker 的使用]()
- [dockerfile 的使用]()
- [docker-compose 多容器管理](doc/docker-compose.md)
- [docker-swarm 集群管理](doc/docker-swarm.md)
- 範例，[docker + ssh + gui應用](../ssh_proxy/doc/透過ssh執行gui的應用.md) 

## Ref
- [執行 Docker 容器可使用 dumb-init 或 tini 改善程序優雅結束的問題](https://blog.miniasp.com/post/2021/07/09/Use-dumb-init-in-Docker-Container)