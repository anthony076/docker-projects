## Traefik 反向代理+自動負載平衡(webUI版的nginx)
- why Traefik
  - 配置比 nginx 更容易
  - 跟現行幾乎所有的基礎架構整合 (Docker、k8s、Rancher、 ... )
  - 具有 webUI 可檢視狀態
  - 支援 Let's Encrypt 的 SSL，設定容易
  - 缺點，占用資源較 nginx 大

## Ref
- [比Nginx更方便的反向代理工具Traefik](https://www.hellosanta.com.tw/knowledge/category-38/post-28)