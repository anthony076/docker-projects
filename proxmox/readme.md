## Proxmox 的使用

- proxmox 不是軟體，是一個作業系統

  proxmox 是一個基於 Debian 並使用基於Ubuntu的客製化核心，
  虛擬化技術基於使用`基於容器的LXC`和`硬體抽象層全虛擬化的KVM`

  使得 proxmox 可以同時創建虛擬機和容器

- 優缺點
  - 優，提供 webUI，可以實時監測每個虛擬機器的狀態
  - 優，將創建虛擬機器的過程簡化，透過 GUI 就可以建立
  
## 範例，在 virtualbox 上建立 proxmox
- step1，下載 [proxmox-iso](https://www.proxmox.com/en/downloads)

- step2，在 virtualbox 中新增 VM，Linux 類型選擇 `Debian(x64)`

- step3，(選用) 設置加速
  - 開啟虛擬化 VT-x

    `virtualbox > vm > settings > System > Processor > Enable Nested VT-x/AMD-V`

  - 選擇虛擬化優化技術
    
    `virtualbox > vm > settings > System > Acceleration > Paravirtualization > KVM`

- step4，(選用) 添加額外的硬碟
  
  非必要，可以將 proxmox 中的數據進行分區，
  無論是否增加額外硬碟，安裝時都會自動創建 lvm 的磁碟分區
  
  `virtualbox > vm > settings > Storage > Controller:SATA > Adds hard disk > Create` 

- step5，設置網路
  - `方法1`， 使用 `Bridge-network` (使用主機網卡)

    - 注意，安裝過程必須手動設置 ip

      使用此方式時，在安裝過程中需要手動設置 proxmox 的 ip(vm-ip)，
      且需要加 vm-ip 和 host-ip 設置為同一個區段，

      使用此方式，virtualbox 會將 vm 的網路橋接到 host 的網卡，
      使得 vm 和 host 處於同一個網域，使得 proxmox 安裝後提示的連接網址有效
    
    - 5-1，在 virtualbox 中設置使用橋接網卡

      `virtualbox > vm > settings > Network > Adapter1 > Attached to: Bridged-Adapter`

    - 5-2，查詢 host 的 ip，例如，`host-ip = 192.168.10.72`，

    - 5-3，在安裝過程中，手動將 `proxmox 的 ip 設置為 192.168.10.75` (75 可取代為任意值)

      host 在瀏覽器中，可透過 https://192.168.10.75:8006 進行連接
  
  - `方法2`， 使用 `NAT-network` (Network-Address-Translation)
    
    - 注意，virtualbox 中需要手動設置 port-fowarding
  
      在安裝過程中不需要設置 proxmox 的 ip (不需要設置 vm-ip)，
      但需要在 virtual 中手動設置 port-fowarding

      使用此方式，virtualbox 會將 host 和 vm 區隔開來，使得 vm 和 host 處於不同一個網域，
      必須透過 port-fowarding 的設置，只針對特定的 port 進行連接，
      使用上較安全，但安裝後 proxmox 安裝後提示的連接網址是無效的，主機需要透過 `https://127.0.0.1:8006` 進行連接

    - 5-1，在 virtualbox 中設置使用橋接網卡

      `virtualbox > vm > settings > Network > Adapter1 > Attached to: NAT`

    - 5-2，在 virtualbox 中設置 port-fowarding
      ```shell
      host-ip = 127.0.0.1
      host-port = 8006
      vm-ip = (留白)
      vm-port = 8006
      ```

      host 在瀏覽器中，可透過 https://127.0.0.1:8006 進行連接

- step6，啟動 vm，並進行安裝
  - hostname(FQDN): 填入主機名，必須符合網址的格式，例如，user.example.com
  - 設置 proxmox-ip (vm-ip)，見 step5 的說明
  - 安裝成功後，在重啟前，手動將光碟機內的ISO檔移除

- step7，測試登入
  - 登入帳號: root
  - 登入密碼: 安裝時設置的密碼

- step8，透過 webUI 登入
  - 注意，https 是必要的，proxmox 不支援 http 的連接
  - 若使用 Bridged-network，使用 https://vm-ip:8006 登入，例如，https://192.168.10.75:8006
  - 若使用 NAT-network，使用 https://127.0.0.1:8006 登入
  - 認證方式: 選擇 `Linux PAM Authentication`

## Ref
- [My Proxmox Home Server Walk Through](https://www.youtube.com/watch?v=_sfddZHhOj4)
- [Try Out ProxMox Virtual Environment in VirtualBox](https://www.youtube.com/watch?v=is2INkjxmVc)
- [Proxmox VE 準備與安裝](https://ithelp.ithome.com.tw/articles/10265378?sc=iThomeR)
- [Proxmox VE增加第二顆硬碟](https://blog.pulipuli.info/2014/10/proxmox-ve-add-another-hard-disk-in.html)
