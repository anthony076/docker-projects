## Docker for DNS-Server
- [DNS基礎](doc/dns_basic.md)

## 範例
- 範例，[利用 python-socket 實現的 DNS-server](doc/dns_socket.py)
- 範例，[利用 python-dnslib 實現的 DNS-server](doc/dns_dnslib.py)