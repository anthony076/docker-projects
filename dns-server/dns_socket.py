# 測試命令
#   注意，不要使用 dig 之外的其他命令進行測試，
#   此範例僅完成初階A紀錄的功能，並未完善完整地的功能
#   因此，使用 dig 之外的命令時，會附加其他本範例位添加的功能，造成 dns-server 出現錯誤
#   功能未完善前，先使用 dig myweb.com @127.0.0.1 進行測試

import json
import socket
import sys
import glob
from typing import Tuple, List

ip = "127.0.0.1"
port = 53
zoneData = None

# socket.AF_INET，使用 ipv4 / socket.SOCK_DGRAM，使用 UDP
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((ip, port))

def loadZones() -> dict:
  """
  讀取所有 zone-files 的內容
  """
  zoneFiles = glob.glob('zone/*.zone')
  zones = {}

  for zone in zoneFiles:
    with open(zone) as json_str:
      json_data = json.load(json_str)

      zoneName = json_data["$origin"]
      zones[zoneName] = json_data

  return zones

def buildFlags(flags: bytes) -> int:
  """
  建立 Response 用的 flags
  """
  byte1 = flags[:1]   # bit15-8 of flags (QR*1 OP*4 AA*1 TC*1 RD*1)

  QR = "1"  # meaning the message is response

  OPCODE = ''
  for bit in range(1, 5):
    OPCODE += str( ord(byte1) & (1 << bit) )

  AA = '1' # current response-server own the domain name

  TC ='0' # assume truncation will not happen

  RD = '0' # no recursion query in this sample code

  RA = '0' # no recursion query support in this sample code

  Z = "000" # future use, keep 0 for all bit

  RCODE = '0000' # no error 

  return int((QR+OPCODE+AA+TC+RD), 2).to_bytes(1, byteorder='big') +  int((RA+Z+RCODE), 2).to_bytes(1, byteorder='big')

def parseQuestionSection(qname:bytes) -> Tuple:
  """
  取得客戶端詢問的域名，用於計算ANCOUNT
    - Label = 字元長度N + 字元1 + 字元2 + 字元3 ... 字元N 組成，
    - data = 0x3, 0x61, 0x61, 0x61, 0x2, 0x62, 0x62, 0x0, 
             N    字元1 字元2 字元3  N    字元1 字元2 域名結束
  """

  isLen = 0 # 判斷當前位置是否是字數位N
  expectLength = 0 # 若當前位置是字數位N，將當前 byte值 保存在 expectLength 中
  ci = 0 # 當前 label 的移動指針
  bi = 0 # 紀錄 byte 最後的位置，用於取得 QType，位於 QName 後方的 2B 
  tmp = "" # 用於串接字元
  domainArr = [] # 保存完成串接的域名

  # 遍歷 QName
  for byte in qname:
    if isLen == 1:
      # 位於非字數位或結束位(0x0)
      
      if byte != 0:
        # 一般字元位
        tmp += chr(byte)

      # 移動到下一個字元位
      ci += 1
      if ci == expectLength:
        # 已經到達字串尾部
        domainArr.append(tmp)

        # 重置參數
        tmp = ''
        isLen = 0
        ci = 0

      if byte == 0:
        # 結束位
        domainArr.append(tmp)
        break

    else:
      # 位於字數位
      isLen = 1
      expectLength = byte

    bi += 1

  questionType = qname[bi:bi+2]

  return (domainArr, questionType)

def getZone(domain: List[str]) -> dict:
  """
  根據 domain，從 zoneData 中取得 domain 對應的 zone-file 內容
  """
  global zoneData

  zoneName = '.'.join(domain)

  return zoneData[zoneName]

def getRecords(qname:bytes) -> tuple:
  """
  獲取 Question-section 中的域名和問題類型 -> 在 zone-flie 尋找目標網域 -> 返回目標網域對應的Record
  """
  questionType = None

  # 從 qname 中取得 domain 和 question-type
  domainArr, qt = parseQuestionSection(qname)

  if qt == b"\x00\x01":
    questionType = 'a'

  # 利用 domain 取得 zone-file 中對應的資料
  zone = getZone(domainArr)

  return (zone[questionType], questionType, domainArr)

def buildQuestion(domainArr:List[str], qType:str) -> bytes:
  """
  還原 Question-section (Question-section = QName + QType + QClass)
  因為 Question-section 是動態長度，無法直接從 data 擷取，因此手動還原 Question-section

  QName 是由多個 Lable 組成，Label = 字元長度N + 字元1 + 字元2 + 字元3 ... 字元N 組成，
  QClass 在大部分場景為 IN ，因此固定寫入 0x1
  """
  qBytes = b''

  # === add QName part ====
  for part in domainArr:
    # 添加長度
    length = len(part)
    qBytes += bytes([length])

    # 添加字元
    for char in part:
      qBytes += ord(char).to_bytes(1, byteorder="big")

  # === add QType part ====
  if qType == 'a':
    qBytes += (1).to_bytes(2, byteorder="big")

  # === add QClass part ====
  qBytes += (1).to_bytes(2, byteorder="big")

  return qBytes

def record2byte(recordType:str, recordTTL:str, recordValue:str) -> bytes:
  """
  根據 zone-file 提供的資訊，完成 RR 的 format
    RR = Owner-name + RR-Type + RR-Class + TTL + RDLength(in byte) + RData
    
    若為A紀錄，record 內容為 a.b.c.d，以 32bit (4B) 的空間表示，a b c d 各占用 1B
    RR = 自定義名稱 + 01 + 01 + TTL值 + 0x4 + IP
  """
  rBytes = b''

  # ==== add Owner-name ====
  # 透過壓縮方法，將Owner-name 指定為 myweb.com
  #     0xc00c = 0x1100_0000_0000_1100
  #     根據報文壓縮方法，0x1100_0000_0000_1100 = 0x11 + OFFSET
  #     得到 OFFSET 為 12，此位置是 Question-section 中的第一個詢問的網域，
  #     即 myweb.com
  rBytes += b'\xc0\x0c'

  # ==== add RR-Type of A = 0x1 ====
  if recordType  == 'a':
    rBytes += bytes([0, 1])

  # ==== add RR-Class = IN = 0x1 ====
  rBytes += bytes([0, 1])

  # ==== add TTL ====
  rBytes += int(recordTTL).to_bytes(4, byteorder="big")

  # ==== add RDLength ====
  # RDLength of A record = 32bit
  if recordType == "a":
    rBytes += bytes([0, 4])

    # ==== add RData ====
    # RData of A-records in zone-file，corresponding to value-field of record
    #   { "name": "@", "ttl": 400, "value": "255.255.255.255" }
    #   RData = 255.255.255.255
    for part in recordValue.split('.'):
      rBytes += bytes([int(part)])

  return rBytes
  
def buildResponse(data:bytes):
  """
  建立給用戶的 Response，
  Response = Header-section + Question-section + Answer-section 
  """
  
  # ==== Header-section ====

  # 參考，/doc/dns_basic.md 的 封包分區1，標頭區格式 (Header-section-format)
  transactionID = data[:2]

  flags = buildFlags(data[2:4])

  QDCOUNT = b"\x00\x01"

  # Answer-Count，根據 zone-file 中，對應欄位的數量決定
  # 獲取目標網域在 zone-files 中的 records --> 計算 records 的數量

  records , questionType, domainArr = getRecords(data[12:])

  ADCOUNT = len(records).to_bytes(2, byteorder="big")
  NSCOUNT =(0).to_bytes(2, byteorder="big")
  ARCOUNT =(0).to_bytes(2, byteorder="big")

  respHeader = transactionID + flags + QDCOUNT + ADCOUNT + NSCOUNT + ARCOUNT
  
  # ==== Question-section ====
  respQuestion = buildQuestion(domainArr, questionType)

  # ==== Answer-section ====
  respAnswer = b''
  
  for record in records:
    respAnswer += record2byte(questionType, record["ttl"], record["value"])
  
  return respHeader + respQuestion + respAnswer

def main():
  global zoneData 
  zoneData = loadZones()

  try:
    while True:
      data, addr = sock.recvfrom(512)
      # print(f"[Incoming] {len(data.hex())}\n{data.hex()}")
      
      r = buildResponse(data)

      sock.sendto(r, addr)

  except KeyboardInterrupt:
      sys.exit(0)

if __name__ == "__main__":
  main()