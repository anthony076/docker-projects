## DNS 基礎
- DNS-server 的功能
  - 正解，將域名轉換為 IP
  - 反解，將IP轉換為域名

- DNS-server 開放的端口，`53/tcp` 和 `53/udp`
  - dns-query 的請求，預設會使用 53/udp 的 port 進來
  - 若 query 失敗或超過一定的長度，會再次以 53/tcp 進行詢問
  - 長度的限制
    - Label，每個最多 63 個字節
    - names，每個域名最多 255 個字節，
    - TTL，有符號、正整數、32bit
    - 利用 UDP 傳輸，最多 512 個字節，若超過 512 字節，應使用 tcp 傳輸

- 圖解 DNS 解析流程
  - [From Emmie Lin](https://tiahi5914.medium.com/dns-查詢流程概念筆記-3a420460d396)

  - 以 www.google.com 為例，

    <img src="dns-query-process.png" width=800 height=auto>

    - 先查看本身有沒有紀錄，若無則向 `.`(root-server)  查詢
    - 向 `.`(root-server) 詢問，得到 .com 的 name-server(NS) 的紀錄
    - 向 `.com` 的 name-server 詢問，得到 google.com 的 name-server(NS) 的紀錄
    - 向 `.google.com` 的 name-server 詢問，得到 google.com 的 A 紀錄，紀載著實際IP

  - 幾個參與DNS解析的 name-server
    - Local-DNS-Server，
      - 又稱遞歸DNS服務器 (recursive-dns-server)
      - 負責對外的 DNS-Server，提供用戶查詢IP，通常由本地ISP供應商提供的網域查詢服務器
      - 執行遞歸查詢時，會多次向不同的 name-server 進行詢問
      - 會將查詢過的的結果保存在內存中，紀錄過期才會重新詢問

    - Root-DNS-Server，
      - Local-DNS-Server 第一個詢問的 DNS server
      - 存放具有 Top-level-DNS Server 位置的NS紀錄

    - Top-level-DNS-Server (TLS-Server)，
      - 存放具有 二級Authoritative-DNS-Server 位置的NS紀錄
      
    - Authoritative-DNS-Server，
      - 經過認證的 name-server，
      - 存放具有其他 Authoritative-DNS-Server 位置的NS紀錄，或具有 IP 的A紀錄

- DNS-Server 返回的是 DNS資源紀錄(RR)
  - `Domain = Zone = RR1 + RR2 + RR3 ...`
  
  - 每個要解析的領域 (domain)，就稱為一個區域 (zone)
    每一個網址在 DNS-server 都保存為自己的區域檔案 (zone-file)

  - zone-file 是`由多個記錄所組成`，每一個紀錄稱為 DNS資源紀錄(Resource-Record，RR)
  
  - 每個DNS資源紀錄依功能不同，可區分為各種不同類型，
    每個類型的DNS資源紀錄的格式，都受到 [RFC-1035](https://datatracker.ietf.org/doc/html/rfc1035) 的規範

    <img src="RR-example.png" width=450 height=auto>

- 兩種查詢方式
  - 向 dns-server 傳送 dns-query 的身分的有兩種
    - client，普通的用戶
    - Authoritative-name-server，詢問的請求來自 dns-server，代替 client 詢問

  - 遞歸查詢

    本機向本地的 dns-server 發出一次性的查詢請求，就靜待最終的結果。
    如果 dns-server 無法解析，自己會以 client 的身份向 other-dns-server 查詢，直到得到最終的IP地址告訴本機

  - 迭代查詢
    dns-server 向 root-name-server 查詢，root-name-server 告訴它下一步到哪一個 name-server 查詢，
    然後 dns-server 再去下一個 name-server 查詢，每次 dns-server 都是以 client 的身份去各個服務器查詢

  - 成功查詢後，DNS 會將此`紀錄保存在內存`中，避免重複的查詢，保存的時間是有時效性的，過了時效後就會自動釋放以節省內存


## DNS 資源紀錄 (Resource-Record，RR) 的格式
- 參考，[rfc1035-3.2](https://datatracker.ietf.org/doc/html/rfc1035#section-3.2)
  
  <img src="RR-format.png" width=450 height=auto>

- RR 位於DNS封包的[回答區 (Answer-section)](#dns-封包解析)

- RR分區1，NAME 欄位，`2B`，查詢的域名，
  
  注意，需要利用報文壓縮的方法指定域名，參考 [dns封包數據的壓縮](#dns封包數據的壓縮)

- RR分區2，TYPE欄位，`2B`，用於標記 RR 的類型
  
  - TYPE=6，代表 SOA 紀錄，Start-Of-Authority，
    ```
     @    IN      SOA    school.edu.tw. root.school.edu.tw.  ( 
          1999051401      ; Serial 
          3600            ; Refresh 
          300             ; Retry 
          3600000         ; Expire 
          3600 )          ; Minimum 
    ```
    - zone-file 所有紀錄中的第一個 (不是 zone-file 的開頭，開頭是 "$origin" )，每個 Zone 只能有一個SOA紀錄
    - SOA 用於描述 Zone 的相關參數，Zone 的 meata-data
      - SOA <負責的網域> <zone-file的負責人>
      - Serial， zone file 的版本
      - Refresh，slave server 檢查 master server 上的 serial number 的間隔時間
      - Expire，當時間超過 Expire 所定的秒數而 slave server 都無法和 master 取得連絡，那麼 slave 會刪除自己的這份 copy。
      - Minimum，zone file 中所有 record 的內定的 TTL 值，也就是其它的 DNS server cache 這筆 record 時，最長不應該超過這個時間
  
  - TYPE=2，代表 NS 紀錄，Name-Server
    ```
    IN  NS   dns.twnic.net.tw.
    ^^                            class-segment
        ^^                        type-segment
    ```
    - 指向下一個 要詢問的DNS主機的名稱，注意不可以IP位址表示
  
  - TYPE=1，代表 A 紀錄，IPv4 Address，
    ```
     server  IN  A  140.123.102.10
    ```
    - 32位，IPv4 格式的實際IP地址

  - TYPE=28，代表 AAAA 紀錄，IPv6 Address
    ```
    twnic.net.tw.  86400  IN  AAAA  3ffe: :bbb:93:5
    ```
    - 128位，IPv6 格式的實際IP地址

  - TYPE=12，代表 PTR 紀錄，PoinTeR，
    ```
    20   IN  PTR  mail.twnic.net.tw.
    ```
    - 用於反解查詢時，返回該 IP 對應的域名

  - TYPE=5，代表 CNAME 紀錄，Canonical-Name，
    ```
    www  IN  CNAME  mix
    ```
    - 用來設定別名，指向另一個域名而不是IP地址
    - DNS解析伺服器遇到CNAME記錄會以映射到的目標重新開始查詢
    - DNS 協議不允許為 Top-level-DNS-server 創建別名記錄(CNAME)，

      例如，若註冊了DNS 名稱example.com，則頂級域名為example.com，
      因此，不能為 example.com 創建CNAME 記錄，但可以為 xxx.example.com 等子網域自定義別名

  - TYPE=15，代表 MX 紀錄，Mail-eXchanger，
    ```
    server  IN   MX  10  mail.twnic.net.tw.  
    ```
    - 擔任郵件伺服器的主機，所有要送往那部機器的 mail 都要經過 mail exchanger 轉送
    - 會記錄優先值，代表該主機郵件傳遞時的優先次序，值越低表示有越高的郵件處理優先權。

  - TXT 紀錄，
    - 某個主機名或域名的說明，例如，紀錄管理員的信箱、電話
    - 可以用於反垃圾信件 SPF（Sender Policy Framework）

- RR分區3，CLASS 欄位，`2B`，用於紀錄詢問請求的來源類別
  - `CLASS=1`，代表 IN，the Internet，來自 Internet 的詢問
  - `CLASS=2`，代表 CS，the CSNET class，來自 RFC 的詢問
  - `CLASS=3`，代表 CH，the CHAOS class，用於查詢 DNS 服務器版本之類
  - `CLASS=4`，代表 HS，Hesiod，不常用

- RR分區4，TTL 欄位，`4B`
  - 需要對緩存的紀錄再次進行DNS解晰的間隔時間
  - 32bit有號數，TTL=0，不使用快取
  - 建議值為 3600，每小時更新一次紀錄

- RR分區5，RDLENGTH 欄位，`2B`，紀錄 RDATA 的字節長度
  - 不同的 TYPE 類型，要填入的 RDATA 的內容不同
  - 參考，[rfc1035-3.3](https://datatracker.ietf.org/doc/html/rfc1035#section-3.3) 

- RR分區6，RDATA 欄位，`動態長度`，當前紀錄的實際內容，根據 TYPE 和 CLASS 值決定內容
  - 若是A紀錄，就填入IP，例如，172.217.163.36，就填入 `0xac 0xd9 0xa3 0x24`
  - 若是NS紀錄，就填入 name-server 的名稱
  - 參考，[rfc1035-3.3](https://datatracker.ietf.org/doc/html/rfc1035#section-3.3) 
  
## 完整的 DNS 封包解析
- 利用 Adapter for loopback traffic capture 可以捕捉 DNS 的封包

  <img src="wireshark_interface.png" width=300 height=auto>

- 在 wireshark 捕獲的封包中會包含其他協定的封包，只有DNS封包是需要的

  <img src="wireshark_dns_packet.png" width=500 height=auto>

- DNS封包的分區
  - Header-section，標頭區，必要
  - Question-section，問題區
  - Answer-sction，回答區
  - Authority-section，認證服務器區
  - Additional-section，額外區
  
---

- 封包分區1，標頭區格式 (Header-section-format)
    - [參考，rfc1035-4.1.1](https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.1)

      <img src="message-header-section.png" width=500 height=auto>

    - TransactonID (ID)，`2B`，
      
      由 client 提供，dns-server 需要回應相同的ID給 client，
      client 才能正確識別收到的是哪一個 dns-server 的回應
    
    - Flags，`2B`，
      - bit-15，1bit，Query 或 Response (QR)  
        - QR=0，message 為 query
        - QR=1，message 為 response

      - bit14-11，Opcode (OP)
        - OP=0，標準查詢
        - OP=1，反向查詢
        - OP=2，服務器狀態查詢
        - OP=4，通知(notify)
        - OP=5，更新(update)
        - 查詢和應答時使用相同的OP值
      
      - bit-10，Authoritative Answer (AA)
        - 只在 `response` 中有效
        - 表示當前應答的DNS服務器，是否是目標域名的Authoritative-DNS-Server(是否是域名的擁有者)
        - AA=0，當前的 Authoritative-DNS-Server 不擁有查詢的目標域名
        - AA=1，當前的 Authoritative-DNS-Server 擁有查詢的目標域名
        - 在自定義的 DNS-server 中，需要設置為 1，

      - bit-9，TrunCation (TC)，
        - 指明 message 是否被截斷
        - 當 message 長度 > 傳輸通道的長度時會被截斷
        - 用於UDP的查詢，超過長度會改為 TCP，UDP 限制查詢報文的長度為 512 Byte
        - TC=0，未截斷
        - TC=1，已截斷
  
      - bit-8，Recursion Desired (RD)
        - 表示 client 端希望DNS使用遞歸查詢，不一定會被執行，DNS不一定支援遞歸查詢
        - 應答時，需要設置為與請求相同的RD值
        - RD=0，不希望進行遞歸查詢，希望執行迭代查詢
        - RD=1，希望進行遞歸查詢
        - 在自定義的 DNS-server 中，需要設置為 0

      - bit-7，Recursion Available (RA)
        - server 回應的專用欄位，告知 client 遞歸查詢是否支持

      - bit6-4，Reserved for future use (Z)，保留位

      - bit3-0，Response-code (RCODE)，用於提供查詢狀態
        - RCODE=0，無錯誤
        - RCODE=1，格式錯誤，name-server 無法理解請求
        - RCODE=2，name-server 錯誤，name-server 出錯無法查詢 
        - RCODE=3，名稱錯誤，
          - 只對 Authoritative-DNS-Server 有意義
          - 表示解析的域名不存在
        - RCODE=4，未實現錯誤，name-server 不支持請求的查詢類型
        - RCODE=5，拒絕錯誤，name-server 因政策關係，拒絕執行指定的操作
        - RCODE=6~15，保留
    
    - Question-RRs-count (QDCOUNT)，`2B`，Question-section中RR的數量

    - Answer-RRs-count (ANCOUNT)，`2B`，Answer-sction中RR的數量
      ```
      計算流程
      step1，從客戶的詢問的數據包中，取出 domain 和 question-type
      step2，根據 domain，尋找 zone-file 中對應的 records
      step3，計算 records 的數量，並轉換為 bytes
      ```     
    - Authority-RRs-count (NSCOUNT)，`2B`，Authority-section中RR的數量
     
    - Additional-RRs-count (ARCOUNT)，`2B`，Additional-section中RR的數量

---

- 封包分區2，問題區格式 (Question-section-format)
  - [參考，rfc1035-4.1.2](https://datatracker.ietf.org/doc/html/rfc1035#section-4.1.2)

    <img src="message-question-section.png" width=500 height=auto>
  
  - 不可為空，至少包含一個問題
  
  - QName 欄位，查詢的域名
    - `nB`，由域名組成的多個 label
      - 域名會被拆分為`多個 label `的組合，將域名中`.`作為拆分點
        
        例如，www.google.com，會被拆分為 www、google 和 com 三個 label
      
      - 每個 label 以 `後方字符的數量 + 域名字符的ASCII碼` 的格式表示
        
        以 www、google 和 com 三個 label 為例
        - www = 0x3 `+` 0x77、0x77、0x77、0x77、
        - google = 0x6 `+` 0x67、0x6f、0x6f、0x67、0x6c、0x65
        - com = 0x3 `+` 0x63、0x6f、0x6d

    - `1B`，域名的結束位，固定填入 0x0，在最後一個 label 後須添加結束位
    - 以上述為例
      ```
      QName = (label-www) + (label-google) + (label-com) + 0x00
            = (0x3、0x77、0x77、0x77、0x77) + (0x6、0x67、0x6f、0x6f、0x67、0x6c、0x65) + (0x3、0x63、0x6f、0x6d) + 0x00
      ```
  
  - QType 欄位，`2B`，問題的類型
    - 參考 RR 中的 `TYPE` 欄位，問題的類型會和RR的TYPE對應
    - 例如，詢問 IP，QType應填入A紀錄 = 0x01

  - QCLASS 欄位，`2B`，詢問請求的來源類別
    - 參考 RR 中的 `CLASS` 欄位，
    - 例如，詢問 Internet，QCLASS 應填入IN = 0x01

---

- 封包分區3，答案區格式 (Answer-sction-format)
  - 若找不到對應的紀錄時，此分區可為空
  - 回答的內容，須符合 [RR 的格式](#dns-資源紀錄-resource-recordrr-的格式)
  - 建立給 client 的答案區
    ```
    step1，從客戶的詢問的數據包中，取出 domain 和 question-type
    step2，根據 domain，尋找 zone-file 中對應的所有 records
    step3，將每一個 record 值，依照答案區格式排列
    step4，將所有 record 串接
    ```
---

- 封包分區4，認證服務器區格式 (Authority-section-format)
  - 可為空
  - 指向下一個 authority-name-server 的資源紀錄集合(RRs)
---

- 封包分區5，Additional records，dig 額外添加的區段
  - 可為空
  - 和請求相關的，但不是必須回答的資源紀錄集合(RRs)

## DNS封包數據的壓縮
- 目的
  - 為了減小報文，DNS使用一種壓縮方法來消除報文中域名的重複。
  - 壓縮方法，只有`第一次出現的域名會被保留`，後面出現的重複域名都會被`替換成指針`，指向第一次出現的位置
  - 格式，`11 + OFFSET` (重複域名用) 或 `00 + OFFSET` (重複 label 用)
    - 用`開頭的兩個 byte`，用於區分是重複出現的是域名或是 label，
    - OFFSET 代表先前出現過的域名或label的位置，
      - OFFSET 的起始位置從DNS封包的起始位址開始算起，
      - 例如，若 OFFSET = 12，表示從 TransactionID 開始為第0個封包，第12個封包的位置就是取用域名的起始位置

- 優缺點
  - 優點，節約了空間、降低報文的容量、避免數據包截斷、保證了數據的一致性
  - 缺點，需要所有的程序都應該能夠理解收到的報文中包含的指針

- 範例
  - 以請求三個域名 F.ISI.ARPA、FOO.F.ISI.ARPA、ARPA 為例
  - 第1個域名為，`F.ISI.ARPA`
  - 第2個的域名 `FOO.F.ISI.ARPA` = FOO + 第1個域名的指針
  - 示意圖

    <img src="QName_and_compression.png" width=800 height=auto>

## 範例集
- 範例，[利用 python-socket 實現的 DNS-server](/dns_socket.py)
- 範例，[利用 python-dnslib 實現的 DNS-server](/dns_dnslib.py)
  
## 工具
- dig / nslookup / host on Windows
  - 下載，https://www.isc.org/download/
  - 選擇，BIN9 `v9.16.28`，新版已不包含 windows 版本
  - 下載後解壓縮，不需要安裝，直接複製以下檔案，並添加到環境變數中
    
    <img src="dns-tool-on-windows.png" width=180 height=auto>

- dig 命令，推薦
  - 正解查詢，`dig www.google.com`
  - 反解查詢，得到PTR值，`dig -x 172.217.163.36 ptr`
  - 查詢 soa 紀錄的內容，`dig -t soa www.google.com`
  - 查詢 ns 紀錄的內容，`dig -t ns google.com`	

- nsloop 命令
	- 正解查詢，`nslookup www.google.com`

- host 命令
  - 正解查詢，`host www.google.com`
  - 正解查詢，且列出所有重要資訊，`host -a www.google.com`
  - 正解查詢，並指定查詢的DNS主機，`host www.google.com 1.1.1.1`
  - 反解查詢，`host 142.251.43.4`

# Ref
- [Make your own DNS Server](https://www.youtube.com/playlist?list=PLBOh8f9FoHHhvO5e5HF_6mYvtZegobYX2)