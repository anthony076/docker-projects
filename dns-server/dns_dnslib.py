from dnslib import RR, A, QTYPE, RCODE
from dnslib.server import DNSServer, DNSLogger
import sys, time

class TestResolver:
  def resolve(self, request, handler):
    reply = request.reply()
    qname = request.q.qname
    qtype = request.q.qtype

    if qname == 'www.aa.bb' and QTYPE[qtype] =='A':
      answer = RR(
        rname = qname, 
        ttl = 60, 
        rdata = A('123.123.123.123')
      )

      reply.add_answer(answer)
      return reply

    reply.header.rcode = getattr(RCODE, 'NXDOMAIN')
    return reply

def main():
  DNSServer(
    TestResolver(), 
    port=53, 
    address='127.0.0.1', 
    logger=DNSLogger(prefix=False)
  ).start_thread()

  try:
    while True:
      time.sleep(600)
      sys.stderr.flush()
      sys.stdout.flush()

  except KeyboardInterrupt:
    sys.exit(0)

if __name__ == "__main__":
  main()