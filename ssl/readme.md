## SSL (Secure Sockets Layer) 基礎
- 在傳輸層和應用層之間添加的安全認證層，用於`保護HTTP層`的訊息不被監聽

  <img src="doc/http-https-layer.png" width=500 height=auto>

- 公私鑰的兩大功能，加解密 + 簽名驗證
  - 公私鑰是一對同時產生，且互為一對的非對稱序列，兩把金鑰都可以互相的加解密
    - 用於`加解密`，公鑰加密訊息，私鑰解密訊息
    - 用於`簽名驗證`，私鑰加密檔案，公鑰解密檔案，用於`驗證使用者身分`

  - 範例，加解密範例
    - 場景，A 有一段密文，B 有公私鑰
    - 流程，
      ```mermaid
      flowchart LR
      b1[B將公鑰給A] --> b2[A利用公鑰加密] --> b3[B利用私鑰解密]
      ```

  - 範例，驗證簽名範例
    - 場景，A 公私鑰和未加密的簽名檔，B 為認證單位
    - 流程，
      ```mermaid
      flowchart LR
      b1[A將簽名檔進行加密] --> b2[B利用A給公鑰對簽名檔進行解密]
      ```

- 基於`公私鑰加密`和`第三方認證機構的簽名` 保證安全性
  - 由於公私鑰的加密機制，client 的公鑰會公開，但`只有擁有私鑰的 client` 可以解密
  
  - 任何人都可以透過`驗證簽名的機制`，確認網站是私鑰的持有人

    透過 `server提供的認證檔` 和 `第三方認證機構提供的公鑰`，若用戶可以利用公鑰將認證檔解密，就可以確保server是密鑰的持有人，
    因為用戶可以用公鑰解密，代表 server 具有私鑰才能將檔案加密，但此步驟`無法確認是網站的真實性`，
    有可能是偽造網站，而公私鑰由偽造者自行產生的公私鑰
  
  - 第三方認證機構確認網站的正確性
    - 若是直接信任由 server 提供公鑰和簽名檔，容易錯信`偽裝的網站`，
      因此，身分認證一般`由第三方的認證機構`所執行，除了`驗證公鑰的正確性`外，也確保`網站擁有者的真實性`
    
    - 第三方認證機構通常是國際認證過的機構，不會隨意簽發憑證，且申請憑證大部分需要費用，

    - 有免費的SSL認證機構，或是部分網路運營商也提供 htts 的連結，免去了申請 SSL 的麻煩，但通常有額外的限制，例如，限制使用的天數

    - 第三方認證機構不是必需的，server 可以使用自簽憑證，但 client 會有使用限制
      - 第三方客戶端默認不允許自簽憑證，但可以透過配置，忽略對自簽憑證的限制
      - 瀏覽器默認不允許自簽憑證，若要使用自簽憑證，需要進行設置(chrome://flags/#allow-insecure-localhost)
      - curl 可以透過 --insecure 來忽略對自謙憑證的檢查


## 建立網站憑證檔 (certificate)
- server 必須具有認證檔才能與客戶端進行安全通訊，分為第三方憑證和自簽憑證兩種
  
- 方法1，建立第三方憑證
  - 注意，各家認證機構的流程和費用若有不同，[參考](https://free.com.tw/ssl-for-free/)
    
  - step1，把 server-public-key 和 網站資訊 放進一個`CSR的檔案`中(certificate-signing-request)，再送給憑証中心進行簽署認証
  
  - step2，憑證中心完成網站擁有者的身分認證後，憑證中心利用自己的私鑰將憑證檔加密，任何擁有認證公鑰的 client，都可以對憑證檔進行驗證，

    認證後的檔案，就稱為`憑證檔(certificate)`或`認證公鑰(certificate-public-key)`，認證檔包含了 `server-public-key` 和 `網站的相關訊息`

  - step3，將憑證檔交付給 server 擁有者


- 自簽憑證的基本概念
  - 沒有第三方機構認證的憑證檔，稱為自簽憑證
    
  - 自簽憑證是`偽裝自己是認證機構`，因此，需要`第二組公私鑰`來對網站的公鑰進行簽名，
    有第二組公私鑰才能產生憑證檔

- 方法2，透過 openssl 產生自簽憑證
  - step1，建立認證中心的私鑰

    `openssl genrsa -aes256 -out root/ca.key 4096`
  
  - step2，建立認證中心自我簽署的憑證
    ```
    openssl req -new -x509 -days 365 -sha256 \
        -subj "/C=TW/ST=Taipei/O=FOO/OU=BAR/CN=xueyuan.dev/emailAddress=denon@xueyuan.dev" \
        -key root/ca.key \
        -out root/ca.crt
    ```
  
  - step3，產生網站用的公鑰

    `openssl genrsa -out server/server.key 4096`

  - step4，產生網站未簽名的憑證檔
    ```
    openssl req -new -sha256 -key server/server.key \
        -subj "/C=TW/ST=Taipei/O=FOO/OU=BAR/CN=xueyuan.dev/emailAddress=denon@xueyuan.dev" \
        -out server/server.csr
    ```
  
  - step5，利用認證中心自己簽發的憑證，對網站未簽名的憑證檔進行簽名
    ```
    openssl x509 -req -CAcreateserial -days 30 -sha256 \
        -CA root/ca.crt -CAkey root/ca.key \
        -in server/server.csr \
        -out server/server.crt 
    ```

- 方法2，透過 [.NET-Core-SDK 產生自簽憑證](https://blog.miniasp.com/post/2020/08/23/curl-and-self-signed-certificated-or-CA-certificate)


## 建立 SSL 連線的過程
- [圖解](https://www.cnblogs.com/zhuqil/archive/2012/07/23/2604572.html)

  <img src="doc/ssl-communication-flow.png" width=600 height=auto>
  
- step1，client 向 server 發起請求

- step2，server 向 client 發送憑證公鑰(certificate-public-key)，

- step3，client 檢視憑證單位，並利用憑證單位的公鑰，對憑證進行驗證

- step4，client 通過憑證的驗證後，
  - 4-1，建立 client-public/private-key，
  - 4-2，將 client-public-key 利用 server-public-key 進行加密
  - 4-3，傳送加密後的密文給 server

- step5，server 利用 server-private-key 將密文解密，得到 client-public-key

- step6，利用密鑰進行通訊，`用對方的公鑰加密，用自己的私鑰解密`
  - client 發起請求，client 用 server-public-key 加密，server 以 server-private-key 進行解密
  - server 返回回應，server 用 client-public-key 加密，client 以 client-private-key 進行解密

## Ref
- [建立自簽憑證](https://blog.miniasp.com/post/2020/08/23/curl-and-self-signed-certificated-or-CA-certificate)
- [How does HTTPS work](https://www.youtube.com/watch?v=T4Df5_cojAs&list=WL&index=6&t=418s)