## 代理相關指令
- 概念，代理緩衝區
  - 不使用緩衝區的情況下

    來自目標主機的響應會保存在nginx中，等待所有數據傳輸完畢後，
    再由 nginx 傳遞給客戶端，對於慢速的客戶端，會花費更多的時間在等待

  - 使用緩衝區的情況下

    來自目標主機的響應會先保存在nginx的緩衝區中，只要緩衝區滿了之後，
    nginx就會立刻傳遞給客戶端

- proxy_pass參數，指定轉發的目的主機，
  - 語法，`proxy_pass 目標主機位址;`
  - 語法，`proxy_pass 集群名稱;`
  - 語法，`proxy_pass http://集群名稱;`

- proxy_buffering參數，是否開啟代理緩衝
  - 語法，`proxy_buffering on或off`
  - 預設值，proxy_buffering on

- proxy_buffers參數，配置緩衝區數量和大小
  - 語法，`proxy_buffers 數量 大小`
  - 預設值，proxy_buffers 8  4k/8k

- proxy_buffer_size參數，只配置緩衝區的大小(不配置數量)
  - 語法，`proxy_buffer_size 大小`
  - 預設值，proxy_buffers 4k/8k

- proxy_max_temp_file_size參數，超過緩衝區的嚮應，會放在 temp_file 中

- proxy_bind參數，將請求的IP綁定到指定的位置
  - 語法，`proxy_bind 127.0.0.1;`
  - 使用場景，如果目標主機被配置為，只接受來自特定IP網絡或IP位址範圍的連接時使用

- proxy_set_header參數，重設客戶端請求中的標頭，再傳遞給目標主機 
  - 語法，`proxy_set_header 欄位名 新欄位值`
  - 例如，`proxy_set_header Host $host;`，重設客戶請求的 Host 標頭
  - 例如，`proxy_set_header X-Real-IP $remote;`，重設將客戶請求的 X-Real-Ip 標頭
  - 例如，`proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;`，重設客戶請求的 X-Forwarded-For 標頭
