## 存取靜態資源相關指令，root/index/try_files  
- root指令，`設置搜尋的目錄`，
  - 語法，`root 指定搜尋路徑的根目錄`
  - 可以放在 server配置區 或 location指令內
  - 尋找的路徑會從 server配置區的 root指令，指定的位置開始尋找，
  - 若要更改尋找的路徑，在 location 內，透過 root指令重新指定搜尋位置

- index指令，`未指定資源時`，預設返回的資源內容
  - 語法，`index 檔案名`
  - 需要搭配 root指令使用
  - 默認 nginx 會自動添加 index index.html;
  - 例如，
    ```python
    location / {
      root /app;
      # 當客戶端請求 http://localhost/ 時 (未指定資源)，
      # 會開啟 /app/index.html 的檔案，並返回檔案的內容
      index index.html;
    }
    ```
  
- try_files指令，指定要開啟的檔案，
  - 語法，`try_files 檔案1 檔案2 檔案3 ... =狀態碼`
  - 會依序從左至右，依序尋找指定的檔案，並返回檔案的內容
  - 需要搭配 root指令使用， 
    - 優先存取 location指令內的root指令
    - 若 location指令內沒有root指令，會使用 server配置區的 root 指令

  - 若找到檔案，就返回檔案的內容
  - 若沒有找到檔案，就返回最後的 code

- 透過 `$uri` 獲得客戶端請求的資源
  - $uri 指向的資源有可能是`檔案`或`目錄`
  - 例如 http://localhost/aa.html，$uri = /aa.html
  - 例如 http://localhost/aa/bb，$uri = /aa/bb

# 範例集
- try_files $uri /$uri /index.html;
  - $uri，用於指定檔案
  - /$uri，用於含目錄的檔案
  - /index.html，若都找不到上述兩個檔案，直接返回 index.html

- try_files $uri /$uri =404;
  - $uri，用於檔案
  - /$uri，用於含目錄的檔案
  - /index.html，若都找不到上述兩個檔案，直接返回 404

- 範例，綜合範例
  ```python
  server {
      ... 省略 ...
      # 設置全局的根目錄
      # 不指定路徑時，預設的開啟內容
      # 請求為 http://localhost/，開啟VM的檔案路徑為 /app/index.html
      root /app;
      index index.html;

      # ==== 返回指定檔案 ====
      location /hello {
        # 請求為 http://localhost/hello，開啟的檔案路徑為 /app/res/hello.html
        try_files /res/hello.html =404;
      }

      # ==== 重新指定搜尋位置，直接在根目錄下尋找 ====
      # 匹配，http://localhost/cat1.png，uri = /cat1.png
      location ~* \.(jpg|jpeg) {
          # 重新設置檔案根目錄
          root /app/res;
          
          # 開啟檔案為 /app/res/image/cat5.jpeg
          try_files $uri =404;
      }

      # ==== 重新指定搜尋位置，帶有目錄的檔案路徑 ====
      # 匹配，http://localhost/image/cat5.jpeg，uri = /image/cat5.jpeg
      location ~* \/image\/.+\.(jpg|jpeg) {
          # 重新設置檔案根目錄
          root /app/res;
          
          # 開啟檔案為 /app/res/image/cat5.jpeg
          try_files $uri =404;
      }
      ... 省略 ...
    }
  ```

## Ref
- [try_files on official-site](http://nginx.org/en/docs/http/ngx_http_core_module.html#try_files)
