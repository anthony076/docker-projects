## nginx 基礎
- nginx 位於 socket 和 server 之間的代理層

- nginx 的正向/反向代理
  - 正向代理 : 轉發請求給`指定目標主機`，目標主機可以是網路的主機或內部網路的主機
  - 反向代理 : 基於負載策略，自動將請求分配到`主機集群`的其中一台主機中

- `nginx`/`ssh` 反向代理的差異
  - 使用場景的差異
    - nginx-reverse-proxy通常用於`內部主機集群的均衡負載`，收到客戶端請求後，依照負載均衡策略，自動/動態轉發給其中一台主機，
    - ssh-reverse-proxy通常用於`內網穿透`，在目標主機可上網的狀況下，主動與 ssh-server 建立通道，使 sshd-server 可以實現反向代理

  - 是否需要`主動建立通訊通道`
    - nginx-reverse-proxy，只有在客戶端發起請求後，才需要與目標主機建立 SSL通道(若有配置SSL)， 
    - ssh-reverse-proxy，需要目標主機主動與 sshd-server 建立 SSH通道，
  
- nginx 的進程模型
  ```mermaid
  flowchart LR

  b1[tcp\n連接] --> b2[nginx\nsocket] 
  
  subgraph Nginx
    b2 --> b3[nginx\nevent] 
    b3 --> b4[worker]
    b3 --> b5[worker]
  end
  
  b4 -- request --> b6[server-1]
  b4 -- request --> b7[server-2]
  b5 -- request --> b8[server-3]
  b5 -- request --> b9[nginx-server]
  ```

## 安裝 nginx
- on Archlinux
  - pacman -S nginx
  - nginx.conf 的位置，`/etc/nginx/nginx.conf`

## nginx 命令
- 啟動 nginx-server，`nginx`
- 停止 nginx-server，`nginx -s stop`
- 離開 nginx 程式，`nginx -s quit`
- 重啟 nginx-server，`nginx -s reload`，配置熱更新，不需要停機

- 查看 nginx 運行的狀態，
  - `ps axw -o pid,ppid,user,%cpu,vsz,wchan,command | egrep 'nginx|PID'`
  - `ps aux | grep nginx`

- 阻塞模式 (for docker)，`nginx -g "daemon off;"`

## nginx 的配置
- 以下的配置區依照數據的流動方向進行配置，
  
  參考，[nginx 的進程模型](#nginx-基礎)
  
- 全局配置區，`nginx程式的配置`
  - 未使用 { } 包裹的配置，都屬於全局配置
  
  - worker_processes 參數，
    - 語法，`worker_processes 值;`
    - 功能，設置 worker 進程的最大數量
    - 例如，`workder_processes auto`
    - 默認值為1，可使用 auto

  - user 參數，
    - 語法，`user 用戶名 用戶組;`，
    - 功能，限制執行 nginx 命令的使用者和其群組

  - access_log 參數
    - 語法，`access_log on或off`
    - 功能，是否開啟存取紀錄
    - 注意，此參數可以放在全局配置區、http配置區、server配置區
  
  - error_log 參數，
    - 語法，`error_log 路徑 error等級; `
    - 功能，設置 error log 檔案的位置
    - 注意，此參數可以放在全局配置區、http配置區、server配置區
    - `error_log /var/log info; `，info 類型的 error-log 位置
    - `error_log /var/log debug;`，debug 類型的 error-log 位置
    - `error_log /var/log error;`，error 類型的 error-log 位置 

  - pid 參數，
    - 語法，`pid 路徑;`
    - 功能，紀錄 nginx-master-process 的pid值到指定檔案
    - 例如，`pid /var/run/nginx.pid;`

  - worker_rlimit_nofile 參數，
    - 語法，`worker_rlimit_nofile 值;`
    - 功能，限制進程可以打開的文件總上限，
      - linux 中萬物皆文件，進程(worker) 每建立一個連接就是打開一個文件
      - 透過此參數限制所有worker連接的上限值
    - 例如，`worker_rlimit_nofile 65536;`

---

- event配置區，`nginx進程(worker)的配置`
  - 以 event { ... } 開頭，必要，有 events{} 才是有效的配置檔
  
  - use 參數，
    - 語法，`use IO模型;`，可用值為 epoll、select、poll、kqueue、eventport、resig
    - 功能，選擇網路IO的模型
    - 例如，`use epoll;`

  - worker_connections 參數，
    - 語法，`worker_connections 數值;`
    - 功能，單個進程(worker)的最大連接數
    - 例如，`worker_connections 1024;`

---

- http配置區，`對 client-http-request 的相關限制和配置`
  - 以 http { ... } 開頭

  - include 參數，
    - 語法，`include 類型或檔案;`，
    - 功能，匯入內建的類型，或指定檔案的內容
    - 例如，匯入內建的 mime.types，`include mime.types;`
    - 例如，利用 include 匯入指定的配置檔內容
      ```nginx
      # /etc/nginx/server.conf 
      server {
        # 預設使用 port 80
        # 任何網址都會返回 Welcome
        return 200 "Welcome";
      }

      # nginx.conf
      events {}

      http {
        include /etc/nginx/server.conf 
      }
      ```

  - default_type 參數，
    - 語法，`default_type content-type類型;`，預設值為 text/plain
    - 功能，限制客戶端 http-request 的 content-type
    - 例如，`default_type application/octet-stream;`

  - client_header_buffer_size 參數
    - 語法，`client_header_buffer_size 大小;`
    - 功能，
      - 配置 client_header緩衝區的大小，用來保存客戶請求標頭的緩衝區，
      - 若超過指定大小，就會改用 large_header_buffers ，若還是超過，返回錯誤給客戶
    - 例如，`client_header_buffer_size 1K;`

  - large_header_buffers 參數
    - 語法，`large_header_buffers 大小;`
    - 功能，配置 large_header緩衝區的大小，拋出 400 的錯誤
    - 例如，`large_header_buffers 21K;`

  - client_body_buffer_size 參數
    - 語法，`client_body_buffer_size 大小;`
    - 功能，
      - 配置 client_body緩衝區的大小，用來保存客戶請求正文的緩衝區，
      - 若超過指定大小，就會寫入臨時文件 ，
    - 例如，`client_body_buffer_size 16K;`

  - client_max_body_size 參數
    - 語法，`client_max_body_size 大小;`
    - 功能，限制客戶請求正文的最大值，超過時，拋出 413 的錯誤
    - 例如，`client_max_body_size 8m;`

  - sendfile 參數，
    - 語法，`sendfile on或off;`，
    - 功能，
      - 開啟高效文件傳輸模式，
      - 若不開啟，默認會將檔案複製到本地的緩衝區 -> 複製到 socket 的緩衝區 
      - 開啟後，會調用 sendfile()，此函數會傳遞檔案的指針，加快傳輸效率
    - sendfile、tcp_nopush、tcp_nodelay 通常會一起使用
    - 注意，此參數可以放在全局配置區、http配置區、server配置區

  - tcp_nopush 參數
    - 語法，`tcp_nopush on或off;`
    - 功能，優化 tcp 一次性優化數據的發送量
    - sendfile、tcp_nopush、tcp_nodelay 通常會一起使用

  - tcp_nodelay 參數
    - 語法，`tcp_nodelay on或off;`
    - 功能，關閉流量阻塞算法帶來暫時性的死鎖，可優化約200ms
    - sendfile、tcp_nopush、tcp_nodelay 通常會一起使用

  - keepalive_requests 參數，
    - 語法 `keepalive_requests 數值`，
    - 功能，和客戶端的長連接中，最大可接受的請求數量，超過此值需要重新建立連接
    - 若使用 SSL，增加此值可以提高響應時間
    - 例如，`keepalive_requests 100000;`

  - keepalive_timeout 參數，
    - 語法 `keepalive_timeout 數值`，
    - 功能，
      - 和客戶端進行長連接的超時配置，單位秒
      - 代表指定時間內的請求都不會重新建立連接，會重複使用當前的連接
    - 若使用 SSL，增加此值可以提高響應時間
    - 例如，`keepalive_timeout 120;`

---

- server配置區，`配置 nginx-server 的功能`
  - 注意，在 server 區中若未設定代理時，nginx-server 自身就是一台靜態的服務器
  - 以 server { ... } 開頭，且位於 http配置區內

  - listen 參數，
    - 語法，`listen 數值`，
    - 功能，設置 連接的端口

  - root 參數，
    - 語法，`root 本地路徑`
    - 功能，設置資源的根目錄
    - 例如，

  - server_name 參數，監聽客戶請求的地址
    - 語法， `server_name 客戶請求的域名;`，
    - 功能，客戶端發起請求的域名

  - location 參數，匹配路由
    - 功能，匹配客戶請求的路徑，並進行對應的處理
    - 語法，`location ~ re表達式 { action };`，利用正則表達式匹配路徑，區分大小寫，立即採用
    - 語法，`location ~* re表達式 { action };`，利用正則表達式匹配路徑，不區分大小寫，立即採用
    - 語法，`location = { action };`，精準匹配，須完全一致，且會立即採用
    - 語法，`location ~ re表達式 { action };`，最長路徑匹配，不會立即採用，會採用最長路徑
    - 注意，匹配的優先級， `= 大於 ~ 大於 /`

    - 例如，匹配檔案路徑，並添加過期時間
      ```nginx
      # 匹配 gif/jpg/jpeg/png/bmp/swf ，並添加 expires 的 header
      location ~ .*\.(gif|jpg|jepg|png|bmp|swf)$ {
        expires 10d;
      } 
      ```

    - 例如，匹配資源路徑，並返回頁面內容
      ```nginx
      location / {
        # 若路由為 location/root，返回 /usr/share/nginx/html 的內容
        root  /usr/share/nginx/html;

        # 若路由為 location/index，返回 index.html 的內容
        index  index.html;
      } 
      ```

    - 例如，匹配資源路徑，並轉發到其他主機
      ```nginx
      location /app/ {
        proxy_pass http://127.0.0.1:123;  # 設置轉發的目的主機
        proxy_set_header Host $host;      # 重設客戶請求的 Host 標頭
        proxy_set_header X-Real-IP $remote;  # 重設將客戶請求的 X-Real-Ip 標頭
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;  # 重設客戶請求的 X-Forwarded-For 標頭
      }
      ```

  - error_page 參數，
    - 語法，`error_page 錯誤代碼 用於返回的錯誤頁面;`
    - 功能，請求錯誤時，要返回給客戶的錯誤頁面
    - 例如，`error 404 400 /40x.html;`
    - 推薦使用
      ```
      # 產生502 503時，給使用者的返回狀態是200，內容是50x.html
      error_page 502 503 =200 /50x.html;
      location = /50x.html {
          root /usr/share/nginx/html;
      }  
      ```

  - deny 參數
    - 語法，`deny IP;`
    - 功能，設置禁止存取的IP

  - allow 參數
    - 語法，`allow IP;`
    - 功能，設置允許的IP
  
  - return 參數
    - 語法，`return 301 跳轉地址`
    - 語法，`return 200 回應內容`
    - 功能，直接返回 response 內容

  - resolver 參數
    - 語法，`resolver IP;`
    - 功能，指定DNS-server的IP地址，
    - 例如，`resolver 8.8.8.8;`

  - 功能，存取靜態資源，[root指令/index指令/try_files指令](access_static_file.md)

  - 功能，[proxy相關指令](directive_proxy.md)

---

- upsteam配置區，設置主機集群，可用於正向代理/反向代理
  - 以 upsteamm 集群名 { ... } 開頭，且位於 http配置區內，

  - 若 server配置區`有設置代理(proxy_pass)`時，可搭配upsteamm配置區一起使用
    - 用於正向代理，則 upsteamm 配置區內只有一個主機
    - 用於反向代理，則 upsteamm 配置區內有多個主機，nginx 會根據負載均衡策略自動分配

  - 可用的負載均衡策略
    - 輪詢(poll)，預設方法
    - 權重(weight)
    - ip_hash，同一IP的請求，分配給固定的主機
    - 
  
  - 範例，預設輪詢的負載策略
    ```nginx
    http {
      upstream cluster {
        # 設置集群中的主機
        server 192.168.1.163:8080
        server 192.168.1.164:8080
        server 192.168.1.165:8080
      }

      server {
        location /app/ {
          # 設置代理
          # 將 /app/ 的請求轉發給集群
          proxy_pass http://cluster/;
        }
      }
    }
    ```
  - 範例，指定權重的負載策略
    ```nginx
      upstream cluster {
        # 設置集群中的主機
        server 192.168.1.163:8080 weight = 1;
        server 192.168.1.164:8080 weight = 1;
        server 192.168.1.165:8080 weight = 10;
      }
    ```
  - 範例，指定 ip_hash 的負載策略
    ```nginx
      upstream cluster {
        # 每個客戶端分配給固定的主機
        ip_hash;

        # 設置集群中的主機
        server 192.168.1.163:8080;
        server 192.168.1.164:8080;
        server 192.168.1.165:8080;
      }
    ```
  - 範例，指定 響應時間的負載策略
    ```nginx
    upstream cluster {
      # 按照集群主機的響應時間分配
      fair;

      # 設置集群中的主機
      server 192.168.1.163:8080;
      server 192.168.1.164:8080;
      server 192.168.1.165:8080;
    }
    ```
  - 範例，檢查集群主機的狀態 (被動健康檢查)
    ```python
    upstream backend {
        server aa.bb.com;
        # 主機 30秒內無反應就標記為 fail
        # 若 fail 超過3次，就將主機標記為不可用
        # 不可用的主機就不會被分派請求
        server aa.cc.com max_fails=3 fail_timeout=30s;
    }
    ```

## 配置範例，最小服務器的配置
```python
# /etc/nginx/nginx.conf
events {}

http {
  server {
    # 預設使用 port 80
    # 任何網址都會返回 Welcome
    return 200 "Welcome";
  }
}
```

## 配置範例，最小靜態檔案服務器 (需要手動指定子目錄的路徑)
```python
# 注意，開放瀏覽檔案系統有危險性，考慮加上密碼認證
# 注意，頂層也要加上 autoindex on，內層的 autoindex on 才有作用
events {}
http {
  server {
    listen 80;
    autoindex on;

    location / {
      # http://localhost/index.html，存取 /app/index.html
      # http://localhost/res/cat1.jpg，存取 /app/res/cat1.jpg
      # http://localhost/res/hello.html，存取 /app/res.hello.html
      root /app;
      autoindex on;
    }
  }
}
```

## 配置範例，靜態檔案服務器的配置
```python
  http {
    # 引入 http 類型
    include mime.types;
    # 使用二進制傳輸
    default_type application/octet-stream;
    # 加速檔案傳輸
    sendfile on;

    server {

      server_name localhost;
      listen 80;
      
      # 不指定路徑時，預設的開啟內容
      # 請求為 http://localhost/，開啟VM的檔案路徑為 /app/index.html
      root /app;
      index index.html;

      # 返回指定檔案，
      location /hello {
        # 請求為 http://localhost/hello，開啟VM的檔案路徑為 /app/res/hello.html
        try_files /res/hello.html =404;
      }

      # 重新指定搜尋位置，直接在root指定的目錄下尋找
      # 匹配，http://localhost/cat1.png，uri = /cat1.png
      location ~* \.(jpg|jpeg) {
          # 重新設置檔案根目錄
          root /app/res;
          
          # 開啟檔案為 /app/res/image/cat5.jpeg
          try_files $uri =404;
      }

      # ==== 重新指定搜尋位置，uri帶有目錄的檔案路徑 ====
      # 匹配，http://localhost/image/cat5.jpeg，uri = /image/cat5.jpeg
      location ~* \/image\/.+\.(jpg|jpeg) {
          # 重新設置檔案根目錄
          root /app/res;
          
          # 開啟檔案為 /app/res/image/cat5.jpeg
          try_files $uri =404;

      }
      
      access_log /var/log/nginx/access.log;
      error_log  /var/log/nginx/error.log debug;
    }
  }
```

## 配置範例，正向代理的配置
```python
# /etc/nginx/nginx.conf
events {}

http {

  # 方法1，upstream localservers {
  #   server host.docker.internal:5566;
  # }

  server {
    # 監聽客戶請求的地址
    server_name localhost;
    # 監聽 port
    listen 80;

    # 匹配任何路徑，任何路徑都轉給內部的主機 host.docker.internal
    location ~*^.+$ {
      
      # 方法1，使用 upsteam 配置區
      # proxy_pass http://localservers;
      
      # 方法2，只使用 proxy_pass
      proxy_pass http://host.docker.internal:5566;

    }
  }
}
```

## 配置範例，反向代理的配置
```python
events {}

http {

  # 轉發服務器
  upstream localservers {
    # 使用輪詢均衡負載
    server host.docker.internal:8080;
    server host.docker.internal:8081;
    server host.docker.internal:8082;
  }

  server {
    server_name localhost;
    listen 80;

    # 匹配任何路徑，任何路徑都轉給內部的主機 host.docker.internal
    location ~*^.+$ {
      
      # 使用代理
      proxy_pass http://localservers;
      
    }
  }
}
```

## 配置範例，啟用 https
```python
# for http，若使用 http，會自動跳轉到 https
server {
    listen 80;
    listen [::]:80;
    server_name DOMAIN_NAME;
    return 301 https://$host$request_uri;
}

# for https
server {
  # 啟用 ssl
  listen 443 ssl http2;
  listen [::]:443 ssl http2;

  server_name 監聽的地址;
  # 設置根目錄
  root /home/ryan;
  ...
  # 啟動 ssl
  ssl on;
  # 設置憑證檔位置
  ssl_certificate <檔案路徑>;
  # 設置憑證金鑰位置
  ssl_certificate_key <檔案路徑>;
  ...
}
```

## 配置範例，啟用 gzip 壓縮
```python
server {
  ...
  # 啟動壓縮
  gzip on;  
  # 要壓縮的檔案類型
  gzip_types text/plain application/xml application/json;
  # 壓縮等級
  gzip_comp_level 9;
  # 文件內容小於 n 以下不壓縮
  gzip_min_length 1000;
  ...
}
```

## 配置範例，存取限制(連線IP/連線數/連線頻率/下載速度)
- https://blog.gtwang.org/linux/nginx-restricting-access-authenticated-user-ip-address-tutorial/2/

## 配置範例，存取檔案或路徑時，需要驗證密碼
- https://kejyuntw.gitbooks.io/ubuntu-learning-notes/content/web/nginx/web-nginx-auth.html
- https://blog.gtwang.org/linux/nginx-restricting-access-authenticated-user-ip-address-tutorial/

## 配置範例，啟用檔案快取
https://kejyuntw.gitbooks.io/ubuntu-learning-notes/content/web/nginx/web-nginx-cache.html

## 其他工具
- Nginx Proxy Manager
  - nginx 啟動後，可視化各主機的狀態
  - [官網](https://nginxproxymanager.com/guide/#project-goal)
  - [反向代理神器](https://blog.laoda.de/archives/nginxproxymanager)
  - [反向代理神器—Nginx Proxy Manager](https://www.youtube.com/watch?v=Z2zl2TlDzd8)

- [Traefik，webUI版 Nginx](../../Traefik/readme.md) 

## Ref
- [手動編譯 nginx](https://bbs.huaweicloud.com/blogs/301714)
- [手動編譯 nginx](https://blog.51cto.com/u_15294985/2994027)
- [Docker Swarm + Nginx 佈署與配置](https://github.com/Ci-Jie/docker-swarm-cluster)