## Docker for nginx
- [nginx 基礎](doc/nginx_basic.md)
  
- [mini-webserver-example](mini-webserver-example)
  - docker-compose -f start-minimum-example.yaml up -d
  - view http://127.0.0.1 @ browser

- [static-webserver-example](static-webserver-example)
  - docker-compose -f start-static-example.yaml up -d
  - view http://127.0.0.1 @ browser

- [forward-proxy-example](forward-proxy-example)
  - docker-compose -f start-forward-example.yaml up -d
  - view http://127.0.0.1 @ browser

- [reverse-proxy-example](reverse-proxy-example)
  - docker-compose -f start-reverse-example.yaml up -d
  - view http://127.0.0.1 @ browser
