
## 虛擬機加速

- 硬體加速`只和host主機有關`，與guest主機無關，例如，windows上的 intel-haxm 可用於 android / linux / windows / mac 的虛擬機

  各平台host主機可用的硬體加速方案

  | 加速方案                              | HostOS                                       | Host架構                                         |
  | ------------------------------------- | -------------------------------------------- | ------------------------------------------------ |
  | KVM                                   | Linux                                        | Arm (64 bit only), MIPS, PPC, RISC-V, s390x, x86 |
  | Xen                                   | Linux(as dom0)                               | Arm, x86                                         |
  | Hypervisor Framework (hvf)            | MacOS                                        | x86 (64 bit only), Arm (64 bit only)             |
  | Windows Hypervisor Platform (whpx)    | Windows (new)                                | x86                                              |
  | Intel HAXM (hax)                      | Windows (old)、MacOS <br> (2023.01 停止維護) | x86                                              |
  | NetBSD Virtual Machine Monitor (nvmm) | NetBSD                                       | x86                                              |
  | Tiny Code Generator (tcg)             | Linux, other POSIX, Windows, MacOS           | Arm, x86, Loongarch64, MIPS, PPC, s390x, Sparc64 |

- 方案1，啟用 HAXM on Windows

  - 注意，Hyper-V `會獨佔使用 VT-x`，並`阻止 HAXM 和其他第三方程式`（例如，VMware | VirtualBox | 檢測工具）查看 VT-x 的功能，
    因此，<font color=red>先關閉 Hyper-V 再執行其他檢測工具</font>

  - 流程: `停用 hyper-v` -> `開啟 VT-x` -> `檢查HAXM使用資格` -> `安裝並檢查HAXM` 
    
  - step，透過 dsim.exe 關閉 Hyper-V 等 windows-features

    需要透過管理員權限執行以下代碼
    ```batch
    @echo off

    @REM 注意，dism.exｅ　需要透過管理員權限開啟
    @REM 列出所有的特性開關，dism.exe /online /get-features > log
    @REM 查詢特定的特性開關，dism /Online /Get-FeatureInfo /FeatureName:Microsoft-Windows-Subsystem-Linux | findstr State
    @REM 關閉特定的特性開關，dism.exe /online /disable-feature /featurename:特性名

    dism.exe /online /disable-feature ^
    /featurename:VirtualMachinePlatform ^
    /featurename:Microsoft-Windows-Subsystem-Linux ^
    /featurename:Microsoft-Hyper-V-All ^
    /featurename:Microsoft-Hyper-V-Tools-All ^
    /featurename:Microsoft-Hyper-V-Management-PowerShell ^
    /featurename:Microsoft-Hyper-V-Hypervisor ^
    /featurename:Microsoft-Hyper-V-Services ^
    /featurename:HypervisorPlatform ^
    /featurename:Microsoft-Hyper-V-Management-Clients
    ```

    執行後，透過`$ systeminfo.exe`，檢查 Hyper-V 是否完全被關閉，若有其他虛擬機或應用程式使用 hyper-v 的技術，
    有可能會使得 hyper-v 無法完全被關閉

    - 若出現，"Hyper-V Requirements: A hypervisor has been detected. Features required for Hyper-V will not be displayed."，
      表示 Hyper-V 尚未完全停用

    - 若出現，"Hyper-V Requirements: VM Monitor Mode Extensions: Yes ..."，
      表示 Hyper-V 已完全停用，因此此處列出開啟的條件

    - 可能占用 hyper-v 的程式
      - BIOS 的 secure-boot
      - windows 與 wsl 相關的feature
      - [Device Guard 和 Credential Guard](https://learn.microsoft.com/zh-tw/xamarin/android/get-started/installation/android-emulator/troubleshooting?tabs=vswin&pivots=windows#disabling-device-guard)，會防止在 Windows 電腦上停用 Hyper-V

  - step，在BIOS設置中，確保以下功能開啟
    - 啟用 VT-x
    - 啟用 執行停用位 (Execute Disable Bit)

  - step，檢查是否支援 HAXM 加速
    
    利用[checktool.exe](https://github.com/intel/haxm/releases/tag/checktool-v1.1.0) 檢查

    ```
    CPU vendor          *  GenuineIntel
    Intel64 supported   *  Yes      # 系統支援64位Intel處理器，HAXM 只能在64bit OS 下運行
    VMX supported       *  Yes      # 支持虛擬機擴展，是一組支持硬體虛擬化的CPU指令，在開啟 VT-x 後才會支持
    VMX enabled         *  Yes      # CPU 允許虛擬化
    EPT supported       *  Yes      # CPU支援EPT(擴展頁表)，通過減少記憶體管理的開銷，提高虛擬化性能
    NX supported        *  Yes      # CPU支援NX，NX（不執行）是一種安全特性，防止從數據記憶體頁面執行代碼
    NX enabled          *  Yes      # NX目前已啟用，從而增強安全性，防止從數據記憶體頁面執行代碼
    Hyper-V disabled    *  Yes      # hyperv 與HAXM會發生衝突，必須禁用才能使用 HAXM
    OS version          *  Windows 10.0.22000
    OS architecture     *  x86_64
    Guest unoccupied    *  Yes. 0 guest(s)
    ```

  - step，安裝[haxm]((https://github.com/intel/haxm/releases/tag/v7.8.0))
    
    安裝後，以管理員身分打開 cmd，`$ sc query intelhaxm`，檢查 haxm-service 是否正常工作

    若否，透過 `$ sc start intelhaxm` 啟用

## 幾種通訊模式

- `方法1`，NAT模式 (Network Address Translation，網路地址轉換)
  - host主機充當中間人，將虛擬機的網路流量轉換為主機的 IP 地址，並在需要時轉換回虛擬機IP
  - 將内部私有網路地址（IP地址）翻译成合法網路IP地址的技術
  - 對外只暴露一個IP位址，當有內部連接要通訊的時候,通過NAT將內網IP轉換成對外的IP,充當了網關的作用。
  - 可使多台計算機共享網路連接，所有內部網計算機對於公共網路來說是不可見的，而內部網計算機用戶通常不會意識到NAT的存在
  - 家用路由器一般都是NAT模式，在虛擬機中，guest 主機就相當於家用路由器的內網主機
  - 虛擬路由器是host主機上一張虛擬的網卡，而虛擬機則將預設網關指向了這張網卡
  - 注意，在NAT下，虛擬機之間是無法直接進行通信的，需要特殊配置

  - 示意圖
    <img src="doc/NAT-example.png" width=800 height=auto>

- `方法2`，橋接模式
  - 使用橋接模式的guest主機和host主機的關係，就像連接在同一個Hub上的兩臺電腦，
    需要手工為guest主機配置IP位址、子網掩碼，而且還要和host主機處於同一網段
  
  - 橋接更加適合於虛擬機對外提供服務，因為它是可以被外部訪問到的
  
  - 示意圖
    <img src="doc/bridge-1.png" width=800 height=auto>

    <img src="doc/bridge-2.png" width=800 height=auto>

    在上圖中，br0 相當於是虛擬交換機(虛擬網卡)，物理網卡eth0和虛擬機網卡vnet0都通過虛擬的網線連接到br0上，
    這樣eth0和vnet0之間可以互相交換數據

- `方法3`，Host-Only 模式
  - 用於將真实环境和虚拟环境隔离开
  - 所有的guest主機之間是可以相互通信的，但虚拟系统和真实的网络是被隔离开的

  - 示意圖

    <img src="doc/host-only.png" width=800 height=auto>

## 快速啟用|關閉 windows-feature的腳本

- 注意，執行以下腳本需要管理員權限，且部分特性`需要重啟`才能生效，若不需要重啟，可加上 `/norestart`

- dism命令使用範例

  - 列出所有的特性開關，`$ dism.exe /online /get-features > log`
  
  - 查詢特定的特性開關，`$ dism /Online /Get-FeatureInfo /FeatureName:Microsoft-Windows-Subsystem-Linux | findstr State`

  - 關閉特定的特性開關，`$ dism.exe /online /disable-feature /featurename:特性名`

- 啟用 hyperV | docker | BlueStack

  ```
  dism.exe /online /enable-feature ^
  /featurename:VirtualMachinePlatform ^
  /featurename:HypervisorPlatform ^
  /featurename:Microsoft-Windows-Subsystem-Linux ^
  /featurename:Microsoft-Hyper-V-All ^
  /featurename:Microsoft-Hyper-V-Tools-All ^
  /featurename:Microsoft-Hyper-V-Management-PowerShell ^
  /featurename:Microsoft-Hyper-V-Hypervisor ^
  /featurename:Microsoft-Hyper-V-Services ^
  /featurename:Microsoft-Hyper-V-Management-Clients ^
  /all
  ```
  

- 關閉 hyperV

  ```
  dism.exe /online /disable-feature ^
  /featurename:VirtualMachinePlatform ^
  /featurename:Microsoft-Windows-Subsystem-Linux ^
  /featurename:Microsoft-Hyper-V-All ^
  /featurename:Microsoft-Hyper-V-Tools-All ^
  /featurename:Microsoft-Hyper-V-Management-PowerShell ^
  /featurename:Microsoft-Hyper-V-Hypervisor ^
  /featurename:Microsoft-Hyper-V-Services ^
  /featurename:HypervisorPlatform ^
  /featurename:Microsoft-Hyper-V-Management-Clients
  ```

## ref

- [NAT模式、路由模式、桥接模式的区别](https://www.cnblogs.com/Courage129/p/14287850.html)