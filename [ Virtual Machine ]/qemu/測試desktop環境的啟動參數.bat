@echo off

cd %~dp0

@REM 以下命令在 alpine linux 上進行測試

@REM WORK
@REM qemu-system-x86_64.exe ^
@REM -m 8G -smp 4 -M q35 ^
@REM -accel whpx,kernel-irqchip=off ^
@REM -machine vmport=off ^
@REM -boot order=c ^
@REM -drive file=alpine-disk.qcow2,if=virtio ^
@REM -display sdl ^
@REM -device qemu-xhci,id=xhci ^
@REM -usb -device usb-tablet ^
@REM -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

@REM 可用，但是會出現 libEGL initialize failed
@REM qemu-system-x86_64.exe ^
@REM -m 8G -smp 4 -M q35 ^
@REM -accel whpx,kernel-irqchip=off ^
@REM -machine vmport=off ^
@REM -boot order=c ^
@REM -drive file=alpine-disk.qcow2,if=virtio ^
@REM -display sdl,gl=on ^
@REM -device qemu-xhci,id=xhci ^
@REM -usb -device usb-tablet ^
@REM -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

@REM 可用，若變更視窗尺寸，會造成畫面顯示不正確
@REM 臨時添加 -vga virtio 屬於硬體變更，有可能造成桌面系統不可用
@REM 在 alpine 啟動 sway 後出現 failed to open virtio_gpu, no virtio_gpu_dri.so 的錯誤

@REM qemu-system-x86_64.exe ^
@REM -m 8G -smp 4 -M q35 ^
@REM -accel whpx,kernel-irqchip=off ^
@REM -machine vmport=off ^
@REM -boot order=c ^
@REM -drive file=alpine-disk.qcow2,if=virtio ^
@REM -vga virtio ^
@REM -display sdl ^
@REM -device qemu-xhci,id=xhci ^
@REM -usb -device usb-tablet ^
@REM -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

@REM 不可用

@REM qemu-system-x86_64.exe ^
@REM -m 8G -smp 4 -M q35 ^
@REM -accel whpx,kernel-irqchip=off ^
@REM -machine vmport=off ^
@REM -boot order=c ^
@REM -drive file=alpine-disk.qcow2,if=virtio ^
@REM -vga virtio ^
@REM -device virtio-gpu-pci ^
@REM -device qemu-xhci,id=xhci ^
@REM -usb -device usb-tablet ^
@REM -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
