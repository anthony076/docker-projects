@echo off

cd %~dp0

:: ENV
:: - windows 11 23H2 with hyperV enable
:: - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版

:: step1，download installer-iso
::curl -O https://iso.pop-os.org/22.04/amd64/intel/40/pop-os_22.04_amd64_intel_40.iso

:: step2，create disk-image
qemu-img create -f qcow2 pop-disk.img 15G

:: step3，start install process
:: below command is tested under QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5)

:: -clock host，[error] invalid option
:: -display dxg，[error]  Parameter 'type' does not accept value 'dxg'
:: -audio sdl,device=alsa，[error] Parameter 'device' is unexpected
:: -cpu host，[error] unable to find CPU model 'host'
::qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=pop-disk.img,if=virtio -cdrom pop-os_22.04_amd64_intel_40.iso -vga virtio -display sdl,gl=on -usb -device usb-tablet -net nic -net user,hostfwd=tcp::2222-:22

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=dc ^
-drive file=pop-disk.img,if=virtio ^
-cdrom pop-os_22.04_amd64_intel_41.iso ^
-device virtio-vga-gl ^
-display sdl,gl=on ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

