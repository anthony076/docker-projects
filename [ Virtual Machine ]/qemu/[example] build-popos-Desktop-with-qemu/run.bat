@echo off

cd %~dp0

:: 未使用硬體加速前，滑鼠和按鍵反映緩慢，使用 -accel whpx 可順暢運行
::qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -drive file=pop-disk.img,if=virtio -vga virtio -display sdl,gl=on -usb -device usb-tablet -accel whpx -net nic -net user,hostfwd=tcp::2222-:22

:: 可正常啟動並順暢運行，惟執行命令後，不可以移動或變更視窗的大小，直到進入系統登入畫面後才可以

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=c ^
-drive file=pop-disk.img,if=virtio ^
-device virtio-vga-gl ^
-display sdl,gl=on ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
