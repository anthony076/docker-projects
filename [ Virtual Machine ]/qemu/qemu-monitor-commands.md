## qemu monitor 的使用

- 使用場景

  QEMU Monitor 提供了一個強大的接口來控制和管理正在運行的 QEMU 虛擬機器，
  透過 monitor，可以`輸入各種命令`來查看虛擬機狀態和日志。
  具體提供的功能如下 : 

  - 虛擬機的啟動、停止、暫停
  - 添加、移除、配置、查詢設備
  - 查詢系統狀態，可顯示和修改內存
  - 性能監控、除錯和診斷
  - 虛擬機狀態管理
  
- 啟用 monitor 功能
  - 方法，將 Monitor 綁定到標準輸入/輸出，`$ qemu-system-x86_64 -hda disk.img -monitor stdio`
  - 方法，將 Monitor 綁定到 telnet 端口，`$ qemu-system-x86_64 -hda disk.img -monitor telnet:127.0.0.1:1234,server,nowait`

## info 命令

- 列出虛擬機內所有裝置，`$ info qtree`
- 顯示 CPU 資訊，`$ info cpus`

- 其他可用的 info 命令

  ```shel
  info balloon  -- show balloon information
  info block [-n] [-v] [device] -- show info of one block device or all block devices (-n: show named nodes; -v: show details)
  info block-jobs  -- show progress of ongoing block device operations
  info blockstats  -- show block device statistics
  info capture  -- show capture information
  info chardev  -- show the character devices
  info cpus  -- show infos for each CPU
  info cryptodev  -- show the crypto devices
  info dirty_rate  -- show dirty rate information
  info dump  -- Display the latest dump status
  info history  -- show the command line history
  info hotpluggable-cpus  -- Show information about hotpluggable CPUs
  info iothreads  -- show iothreads
  info irq  -- show the interrupts statistics (if available)
  info jit  -- show dynamic compiler info
  info kvm  -- show KVM information
  info lapic [apic-id] -- show local apic state (apic-id: local apic to read, default is which of current CPU)
  info mem  -- show the active virtual memory mappings
  info memdev  -- show memory backends
  info memory-devices  -- show memory devices
  info memory_size_summary  -- show the amount of initially allocated and present hotpluggable (if enabled) memory in bytes.
  info mice  -- show which guest mouse is receiving events
  info migrate  -- show migration status
  info migrate_capabilities  -- show current migration capabilities
  info migrate_parameters  -- show current migration parameters
  info mtree [-f][-d][-o][-D] -- show memory tree (-f: dump flat view for address spaces;-d: dump dispatch tree, valid with -f only);-o: dump region owners/parents;-D: dump disabled regions
  info name  -- show the current VM name
  info network  -- show the network state
  info numa  -- show NUMA information
  info opcount  -- show dynamic compiler opcode counters
  info pci  -- show PCI info
  info pic  -- show PIC state
  info qdm  -- show qdev device model list
  info qom-tree [path] -- show QOM composition tree
  info qtree  -- show device tree
  info ramblock  -- Display system ramblock information
  info rdma  -- show RDMA state
  info registers [-a|vcpu] -- show the cpu registers (-a: show register info for all cpus; vcpu: specific vCPU to query; show the current CPU's registers if no argument is specified)
  info replay  -- show record/replay information
  info rocker name -- Show rocker switch
  info rocker-of-dpa-flows name [tbl_id] -- Show rocker OF-DPA flow tables
  info rocker-of-dpa-groups name [type] -- Show rocker OF-DPA groups
  info rocker-ports name -- Show rocker ports
  info roms  -- show roms
  info sev  -- show SEV information
  info sgx  -- show intel SGX information
  info snapshots  -- show the currently saved VM snapshots
  info spice  -- show the spice server status
  info stats target [names] [provider] -- show statistics for the given target (vm or vcpu); optionally filter byname (comma-separated list, or * for all) and provider
  info status  -- show the current VM status (running|paused)
  info sync-profile [-m] [-n] [max] -- show synchronization profiling info, up to max entries (default: 10), sorted by total wait time. (-m: sort by mean wait time; -n: do not coalesce objects with the same call site)
  info tlb  -- show virtual to physical memory mappings
  info tpm  -- show the TPM device
  info trace-events [name] [vcpu] -- show available trace-events & their state (name: event name pattern; vcpu: vCPU to query, default is any)
  info usb  -- show guest USB devices
  info usbhost  -- show host USB devices
  info usernet  -- show user network stack connection states
  info uuid  -- show the current VM UUID
  info vcpu_dirty_limit  -- show dirty page limit information of all vCPU
  info version  -- show the version of QEMU
  info virtio  -- List all available virtio devices
  info virtio-queue-element path queue [index] -- Display element of a given virtio queue
  info virtio-queue-status path queue -- Display status of a given virtio queue
  info virtio-status path -- Display status of a given virtio device
  info virtio-vhost-queue-status path queue -- Display status of a given vhost queue
  info vm-generation-id  -- Show Virtual Machine Generation ID
  info vnc  -- show the vnc server status
  ```
  
## 可用命令分類

- 模擬器控制命令
cont|c - 恢復模擬。
quit|q - 退出模擬器。
one-insn-per-tb [on|off] - 以每個翻譯塊一個來賓指令的方式運行模擬。

- 虛擬機管理
delvm tag - 根據標籤刪除虛擬機快照。
loadvm tag - 從標籤中恢復虛擬機快照。

- CPU 和記憶體操作
balloon target - 請求虛擬機更改其內存分配（以 MB 為單位）。
cpu index - 設置默認 CPU。
dump-guest-memory [-p] [-d] [-z|-l|-s|-w] [-R] filename [begin length] - 將客戶端的記憶體轉存到名為 'filename' 的文件中。
memsave addr size file - 將從 'addr' 開始的虛擬內存轉存保存到磁碟中，大小為 'size'。
pmemsave addr size file - 將從 'addr' 開始的物理內存轉存保存到磁碟中，大小為 'size'。

- 塊設備管理
block_job_cancel [-f] device - 停止正在進行的後台塊操作。
block_job_complete device - 結束正在進行的後台塊操作。
block_job_pause device - 暫停正在進行的後台塊操作。
block_job_resume device - 恢復已暫停的後台塊操作。
block_job_set_speed device speed - 設置後台塊操作的最大速度。
block_resize device size - 調整塊設備的大小。
block_set_io_throttle device bps bps_rd bps_wr iops iops_rd iops_wr - 更改塊驅動器的 I/O 節流限制。
block_stream device [speed [base]] - 將數據從支持文件複製到塊設備。
commit device|all - 將更改提交到磁盤映像或支持文件。
drive_mirror [-n] [-f] device target [format] - 啟動設備的實時存儲遷移。

- 設備管理
device_add driver[,prop=value][,...] - 添加設備。
device_del device - 移除設備。
chardev-add args - 添加字符設備。
chardev-change id args - 更改字符設備。
chardev-remove id - 移除字符設備。
chardev-send-break id - 在字符設備上發送中斷信號。

- 網絡相關命令
hostfwd_add [netdev_id] [tcp|udp]:[hostaddr]:hostport-[guestaddr] - 將主機的 TCP 或 UDP 連接重定向到來賓。
hostfwd_remove [netdev_id] [tcp|udp]:[hostaddr] - 移除主機到來賓的 TCP 或 UDP 重定向。
netdev_add [user|tap|socket|stream|dgram|vde|bridge|hubport|netmap|vhost-user],id=str[,prop=value][,...] - 添加主機網絡設備。
netdev_del id - 移除主機網絡設備。

- 遷移相關命令
migrate [-d] [-b] [-i] [-r] uri - 遷移到 URI。
migrate_cancel - 取消當前的虛擬機遷移。
migrate_continue state - 從給定的暫停狀態繼續遷移。
migrate_incoming uri - 從 -incoming defer 狀態繼續接收遷移。
migrate_pause - 暫停正在進行的遷移（僅適用於 postcopy 模式）。
migrate_recover uri - 繼續暫停的 postcopy 模式下的遷移。
migrate_set_capability capability state - 啟用或禁用遷移功能。
migrate_set_parameter parameter value - 設置遷移的參數。
migrate_start_postcopy - 在遷移命令後續操作中將遷移切換為 postcopy 模式。

- 其他設備與系統操作
announce_self [interfaces] [id] - 觸發 GARP/RARP 宣告。
boot_set bootdevice - 為啟動設備列表定義新值。
cancel_vcpu_dirty_limit [cpu_index] - 取消髒頁率限制。
change device [-f] filename [format [read-only-mode]] - 更改可移動媒介。
eject [-f] device - 彈出可移動介質。
gdbserver [device] - 在給定設備上啟動 gdbserver。
log item1[,...] - 激活指定項目的日誌記錄。
logfile filename - 將日誌輸出到 'filename' 文件中。
mce [-b] cpu bank status mcgstatus addr misc - 在給定的 CPU 上注入 MCE。
nmi - 注入一個 NMI（非屏蔽中斷）。
object_add [qom-type=]type,id=str[,prop=value][,...] - 創建 QOM 對象。
object_del id - 銷毀 QOM 對象。
pcie_aer_inject_error [-a] [-c] id <error_status> [<tlp header> [<tlp header prefix>]] - 注入 PCIe AER 錯誤。
qemu-io [-d] [device] "[command]" - 在塊設備上運行 qemu-io 命令。
qom-get path property - 列印 QOM 屬性。
qom-list path - 列出 QOM 屬性。
qom-set [-j] path property value - 設置 QOM 屬性。
expire_password protocol time [-d display] - 設置 spice/vnc 密碼過期時間。
- I/O 操作
i /fmt addr - I/O 端口讀取。
o /fmt addr value - I/O 端口寫入。
print|p /fmt expr - 列印表達式值。

- 滑鼠相關操作
mouse_button state - 更改滑鼠按鍵狀態（1=左鍵, 2=中鍵, 4=右鍵）。
mouse_move dx dy [dz] - 发送滑鼠移动事件。
mouse_set index - 設定接收事件的滑鼠設備。

- 虛擬地址與物理地址操作
gpa2hva addr - 列印對應於來賓物理地址的主機虛擬地址。
gva2gpa addr - 列印對應於來賓虛擬地址的來賓物理地址。