@echo off

cd %~dp0

@REM WORK
@REM qemu-system-x86_64.exe ^
@REM -m 8G -smp 4 -M q35 ^
@REM -accel whpx,kernel-irqchip=off ^
@REM -machine vmport=off ^
@REM -boot order=c ^
@REM -drive file=alpine-disk.qcow2,if=virtio ^
@REM -display sdl ^
@REM -device qemu-xhci,id=xhci ^
@REM -usb -device usb-tablet ^
@REM -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

@REM BETTER，需要等待到出現檢測畫面才可以變更qemu視窗大小
@REM 添加 -display sdl,gl=on ^
@REM - 優，字不會出現歪斜，變更qemu視窗尺寸時，會自動調整解析度
@REM - 優，啟動sway會出現 libEGL initialize failed，但可用
@REM - 優，畫面延遲更小

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=c ^
-drive file=alpine-disk.qcow2,if=virtio ^
-display sdl,gl=on ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
