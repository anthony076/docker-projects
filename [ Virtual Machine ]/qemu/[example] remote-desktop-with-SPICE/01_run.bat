@echo off

cd %~dp0

:: 流程 : SPICE-client(主機上的套件) -> SPICE-Server(QEMU) -> VirtIO 串行總線設備(QEMU) -> 字符設備(QEMU) -> 虛擬串行端口設備(QEMU) ->  SPICE vdagent (虛擬機中的套件)
:: - `SPICE-server`，接收到客戶端的請求後，會將這些請求轉換為 SPICE 格式的數據。然後，通過 QEMU 內部的 VirtIO 串行總線設備，將數據傳遞給虛擬機
:: - `VirtIO 串行總線設備`，提供了高效的通信通道，將來自 SPICE 服務器的數據發送到虛擬機內的相應設備
:: - `字符設備`，為 SPICE 虛擬機通信的中介。它負責在主機和虛擬機之間傳遞 SPICE 數據
:: - `虛擬串行端口設備`，在虛擬機內作為 VirtIO 串行總線設備的終端，接收來自字符設備的數據，並將這些數據傳遞給虛擬機內的 SPICE vdagent
:: - `SPICE vdagent`，接收數據後，處理各種請求，注意，Linux 內需要安裝 SPICE-vdagent

:: ==== 準備工作1，在 host-os(windows)中安裝 SPCIE-client ==== 
:: 到 https://www.spice-space.org/download.html，找 virt-viewer，https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi

:: ==== 準備工作2，在 guest-os(alpine)中安裝 SPICE-vdagent ==== 
:: doas apk add spice-vdagent
:: doas rc-update add spice-vdagentd default
:: doas service spice-vdagentd start
:: 參考，How to run Spice Agent，https://wiki.alpinelinux.org/wiki/How_to_run_Spice_Agent

:: ====  準備工作3，修改 qemu 的啟動參數 ==== 
:: 配置 SPICE 服務器監聽的端口號，其中
:: disable-ticketing，禁用 SPICE 連接中的身份驗證(ticketing)
::-spice port=5930,disable-ticketing=on (必要)

:: 創建一個 VirtIO 串行總線設備，其中
:: virtio-serial-pci，指明這是一個 VirtIO 設備，用於提供高性能的串行通信通道。
::-device virtio-serial-pci (選用)

:: 創建字符設備（chardev），用於和虛擬機之間傳輸信息，其中
:: spicevmc，指明和 SPICE 虛擬機通信（spicevmc）
::-chardev spicevmc,id=vdagent,name=vdagent (選用)

:: 創建虛擬串行端口設備，用於串接字符設備和vdagent，其中
:: virtserialport，指定當前設備類型，是虛擬串行端口
:: chardev=vdagent，將這個串行端口設備與 ID 為 vdagent 的字符設備關聯起來
:: name=com.redhat.spice.0，指定設備的名稱，這裡使用的是 SPICE 通信標準名稱
::-device virtserialport,chardev=vdagent,name=com.redhat.spice.0 (選用)

:: 注意，使用SPICE，就不能使用gl的參數??

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=c ^
-drive file=alpine-disk.qcow2,if=virtio ^
-display default,show-cursor=on ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-spice port=5930,disable-ticketing=on ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22

:: 注意，使用SPICE，就不會打開 quest-os 的視窗，因此不能使用 -display sdl,gl=on

:: ==== 準備工作4，使用 spice-client 進行連接 ==== 
:: - 打開 virt-viewer
:: - 透過 spice://localhost:5930 進行連接
