## 編譯qemu可用的配置選項

- 以下為 qemu v8.2.1的編譯選項

- 編譯目標
  `--target-list=LIST`，設定要構建的目標列表（默認是構建所有目標）。適用於只想構建特定架構的情況。
  `--target-list-exclude=LIST`，從默認目標列表中排除一組目標。適用於想要排除某些架構的情況。

- 編譯器選項
  - `-Dmesonoptname=val`，傳遞未修改的選項給 Meson。適用於需要自定義 Meson 編譯選項的情況。
  - `--cc=CC`，使用指定的 C 編譯器。適用於需要特定 C 編譯器的情況。
  - `--cxx=CXX`，使用指定的 C++ 編譯器。適用於需要特定 C++ 編譯器的情況。
  - `--objcc=OBJCC`，使用指定的 Objective-C 編譯器。適用於需要特定 Objective-C 編譯器的情況。
  - `--extra-cflags=CFLAGS`，附加額外的 C 編譯器標誌。適用於需要添加特殊編譯標誌的情況。
  - `--extra-cxxflags=CXXFLAGS`，附加額外的 C++ 編譯器標誌。適用於需要添加特殊 C++ 編譯標誌的情況。
  - `--extra-objcflags=OBJCFLAGS`，附加額外的 Objective-C 編譯器標誌。適用於需要添加特殊 Objective-C 編譯標誌的情況。
  - `--extra-ldflags=LDFLAGS`，附加額外的鏈接器標誌。適用於需要添加特殊鏈接標誌的情況。

- 交叉編譯選項
  - `--host-cc=CC`，交叉編譯時，為編譯運行於構建時間的代碼使用的 C 編譯器。適用於交叉編譯時。
  - `--cross-prefix=PREFIX`，為編譯工具使用 PREFIX。適用於交叉編譯時。
  - `--cross-prefix-ARCH=PREFIX`，為構建 ARCH 客戶端測試用例使用的交叉編譯前綴。適用於交叉編譯時。
  - `--cross-cc-ARCH=CC`，為構建 ARCH 客戶端測試用例使用的編譯器。適用於交叉編譯時。
  - `--cross-cc-cflags-ARCH=`，為構建 ARCH 客戶端測試用例使用的編譯器標誌。適用於交叉編譯時。

- Python 和 Ninja
  - `--python=PYTHON`，使用指定的 Python。適用於需要特定 Python 解釋器的情況。
  - `--ninja=NINJA`，使用指定的 Ninja 構建系統。適用於需要特定 Ninja 構建系統的情況。

- 構建特性
  - `--static`，啟用靜態構建（默認為否）。適用於需要靜態鏈接的情況。
  - `--without-default-features`，將所有 --enable-* 選項設置為“禁用”。適用於需要禁用默認特性的情況。
  - `--without-default-devices`，不包括任何不需要啟動模擬器的設備。適用於需要精簡設備的情況。
  - `--with-devices-ARCH=NAME`，覆蓋默認設備配置。適用於需要自定義設備配置的情況。
  - `--enable-debug`，啟用常見的調試構建選項。
  - `--cpu=CPU`，為主機 CPU 構建（預設為 x86_64）。

- 容器選項
  - `--disable-containers`，在交叉構建時不使用容器。
  - `--container-engine=TYPE`，使用的容器引擎類型（預設為 auto）。

- GDB
  - `--gdb=GDB-path`，用於 gdbstub 測試的 GDB 路徑。

- Audio
  - `--audio-drv-list=CHOICES`，設置音頻驅動程序列表（預設為 default）。

- 目錄和安裝選項
  - `--bindir=VALUE`，可執行文件目錄（預設為 bin）。
  - `--block-drv-ro-whitelist=VALUE`，設置塊驅動程序只讀白名單。
  - `--block-drv-rw-whitelist=VALUE`，設置塊驅動程序讀寫白名單。
  - `--datadir=VALUE`，數據文件目錄（預設為 share）。
  - `--disable-coroutine-pool`，禁用協程自由列表。
  - `--disable-debug-info`，禁用調試信息。
  - `--disable-hexagon-idef-parser`，禁用 Hexagon 前端的 idef 解析器。
  - `--disable-install-blobs`，不安裝提供的固件 Blob。
  - `--disable-qom-cast-debug`，禁用 QOM 強制轉換調試支持。
  - `--disable-relocatable`，禁用可重定位安裝。
  - `--docdir=VALUE`，文檔安裝基目錄（可為空，預設為 share/doc）。
  - `--enable-block-drv-whitelist-in-tools`，在工具中也使用塊白名單。
  - `--enable-cfi`，啟用控制流完整性（CFI）。
  - `--enable-cfi-debug`，啟用 CFI 違規的詳細錯誤信息。
  - `--enable-debug-graph-lock`，啟用圖鎖定調試支持。
  - `--enable-debug-mutex`，啟用互斥調試支持。
  - `--enable-debug-stack-usage`，測量協程堆棧使用情況。
  - `--enable-debug-tcg`，啟用 TCG 調試。
  - `--enable-fdt[=CHOICE]`，是否以及如何查找 libfdt 庫。
  - `--enable-fuzzing`，構建模糊測試目標。
  - `--enable-gcov`，啟用覆蓋率跟蹤。
  - `--enable-lto`，使用鏈接時優化（LTO）。
  - `--enable-malloc=CHOICE`，選擇內存分配器（預設為 system）。
  - `--enable-module-upgrades`，嘗試從替代路徑加載模塊進行升級。
  - `--enable-rng-none`，啟用虛擬隨機數生成器。
  - `--enable-safe-stack`，啟用 SafeStack 保護。
  - `--enable-sanitizers`，啟用默認的檢測器。
  - `--enable-strip`，在安裝時剝離目標文件。
  - `--enable-tcg-interpreter`，使用 TCG 內部字節碼解釋器（慢速）。
  - `--enable-trace-backends=CHOICES`，設置可用的跟蹤後端（預設為 log）。
  - `--enable-tsan`，啟用線程檢測器。
  - `--firmwarepath=VALUES`，固件文件的搜索路徑（預設為 share/qemu-firmware）。
  - `--iasl=VALUE`，ACPI 反匯編器的路徑。
  - `--includedir=VALUE`，頭文件目錄（預設為 include）。
  - `--interp-prefix=VALUE`，共享庫等的查找路徑。
  - `--libdir=VALUE`，庫目錄（系統默認）。
  - `--libexecdir=VALUE`，庫可執行文件目錄（預設為 libexec）。
  - `--localedir=VALUE`，本地數據目錄（預設為 share/locale）。
  - `--localstatedir=VALUE`，本地狀態數據目錄（預設為 var/local）。
  - `--mandir=VALUE`，手冊頁目錄（預設為 share/man）。
  - `--prefix=VALUE`，安裝前綴（預設為 usr/local）。
  - `--qemu-ga-distro=VALUE`，qemu-ga 註冊表條目的第二路徑元素（預設為 Linux）。
  - `--qemu-ga-manufacturer=VALUE`，qemu-ga 註冊表條目的“製造商”名稱（預設為 QEMU）。
  - `--qemu-ga-version=VALUE`，qemu-ga 安裝程序的版本號。
  - `--smbd=VALUE`，slirp 網絡使用的 smbd 路徑。
  - `--sysconfdir=VALUE`，系統配置數據目錄（預設為 etc）。
  - `--tls-priority=VALUE`，默認的 TLS 協議/密碼優先級字符串（預設為 NORMAL）。
  - `--with-coroutine=CHOICE`，協程後端（可選 auto、sigaltstack、ucontext、windows）。
  - `--with-pkgversion=VALUE`，使用指定的字符串作為包的子版本。
  - `--with-suffix=VALUE`，QEMU 數據/模塊/配置目錄的後綴（可為空，預設為 qemu）。
  - `--with-trace-file=VALUE`，簡單後端的跟蹤文件前綴。

## 選用特性(optional-features)

- 以下為 qemu v8.2.1的選用特性

- 以下特性可透過`--enable-特性名`來啟用，或透過`--disable-特性名`來禁用該特性，可用的特性名請參考以下

- 網絡和通信支持
  - `af-xdp`，支持 AF_XDP 網絡後端，用於高性能的網絡數據包處理。
  - `l2tpv3`，支持 l2tpv3 網絡後端，用於創建點對點的 VPN 連接。
  - `netmap`，支持 netmap 網絡後端，用於高性能網絡 I/O。
  - `slirp`，支持 libslirp 用戶模式網絡後端，適用於需要在沒有管理員權限的情況下設置網絡的情況。
  - `slirp-smbd`，在 slirp 網絡中使用 smbd，適用於共享文件系統。
  - `vde`，支持 vde 網絡後端，用於虛擬化網絡環境。
  - `vmnet`，支持 macOS 的 vmnet.framework 網絡後端。

- 加速和優化
  - `avx2`，啟用 AVX2 指令集優化，提高性能。
  - `avx512bw`，啟用 AVX512BW 指令集優化，提高性能。
  - `avx512f`，啟用 AVX512F 指令集優化，提高性能。
  - `kvm`，支持 KVM 硬件加速，用於提高虛擬化性能。
  - `hvf`，支持 macOS 的 HVF 硬件加速。
  - `nvmm`，支持 NetBSD 的 NVMM 硬件加速。
  - `whpx`，支持 Windows 的 WHPX 硬件加速。

- 音頻支持
  - `alsa`，支持 ALSA 音頻系統，用於 Linux 系統。
  - `coreaudio`，支持 CoreAudio 音頻系統，用於 macOS 系統。
  - `dsound`，支持 DirectSound 音頻系統，用於 Windows 系統。
  - `jack`，支持 JACK 音頻連接套件，用於專業音頻應用。
  - `oss`，支持 OSS 音頻系統，用於某些 Unix 系統。
  - `pa`，支持 PulseAudio 音頻系統。
  - `pipewire`，支持 PipeWire 音頻系統。
  - `sndio`，支持 sndio 音頻系統，用於 OpenBSD。

- 圖形和用戶界面
  - `cocoa`，支持 macOS 的 Cocoa 用戶界面。
  - `gtk`，支持 GTK+ 用戶界面，用於 Linux 和其他 Unix 系統。
  - `sdl`，支持 SDL 用戶界面，用於跨平台圖形界面。
  - `opengl`，支持 OpenGL，用於加速圖形渲染。
  - `virglrenderer`，支持 virglrenderer，用於虛擬機中的 3D 圖形加速。
  - `vnc`，支持 VNC 服務器，允許遠程圖形連接。
  - `vnc-jpeg`，支持 VNC 服務器的 JPEG 有損壓縮。
  - `vnc-sasl`，支持 VNC 服務器的 SASL 認證。

- 存儲和文件系統
  - `blkio`，支持 libblkio 塊設備驅動。
  - `bochs`，支持 Bochs 磁盤鏡像格式。
  - `bzip2`，支持 bzip2 壓縮，用於處理 DMG 鏡像。
  - `cloop`，支持 cloop 磁盤鏡像格式。
  - `dmg`，支持 DMG 磁盤鏡像格式。
  - `fuse`，支持 FUSE 塊設備導出。
  - `fuse-lseek`，支持 FUSE 導出的 SEEK_HOLE 和 SEEK_DATA。
  - `qcow1`，支持 qcow1 磁盤鏡像格式。
  - `qed`，支持 qed 磁盤鏡像格式。
  - `rbd`，支持 Ceph 塊設備驅動。
  - `vdi`，支持 vdi 磁盤鏡像格式。
  - `vhdx`，支持 vhdx 磁盤鏡像格式。
  - `vmdk`，支持 vmdk 磁盤鏡像格式。
  - `vvfat`，支持 vvfat 磁盤鏡像格式。

- 加密和安全
  - `auth-pam`，支持 PAM 訪問控制，用於安全認證。
  - `cap-ng`，支持 cap_ng，用於細粒度的權限控制。
  - `crypto-afalg`，支持 Linux 的 AF_ALG 加密後端驅動。
  - `gcrypt`，支持 libgcrypt 加密庫。
  - `gnutls`，支持 GNUTLS 加密庫。
  - `keyring`，支持 Linux 密鑰環。
  - `libkeyutils`，支持 Linux keyutils。
  - `nettle`，支持 nettle 加密庫。
  - `seccomp`，支持 seccomp，用於系統調用過濾。
  - `selinux`，支持 SELinux，在 qemu-nbd 中使用。

- 虛擬化和仿真
  - `guest-agent`，構建 QEMU Guest Agent，用於虛擬機中的管理操作。
  - `guest-agent-msi`，為 QEMU Guest Agent 構建 MSI 安裝包。
  - `multiprocess`，支持進程外設備仿真。
  - `numa`，支持 NUMA（非均勻內存訪問）。
  - `pvrdma`，啟用 PVRDMA 支持，用於虛擬化 RDMA。
  - `tpm`，支持 TPM（可信平台模塊）。
  - `virtfs`，支持 virtio-9p，用於虛擬機文件系統共享。
  - `virtfs-proxy-helper`，支持 virtio-9p 代理助手。
  - `vhost-crypto`，支持 vhost-user 加密後端。
  - `vhost-kernel`，支持 vhost 內核後端。
  - `vhost-net`，支持 vhost-net 內核加速。
  - `vhost-user`，支持 vhost-user 後端。
  - `vhost-user-blk-server`，構建 vhost-user-blk 服務器。
  - `vhost-vdpa`，支持 vhost-vdpa 內核後端。
  - `xen`，支持 Xen 後端。
  - `xen-pci-passthrough`，支持 Xen PCI 直通。

- 其他特性
  - `attr`，支持 attr/xattr，用於文件系統擴展屬性。
  - `bpf`，支持 eBPF（擴展的 BPF），用於內核級網絡數據包過濾和處理。
  - `brlapi`，支持 brlapi 字符設備驅動，用於盲文顯示。
  - `canokey`，支持 CanoKey 安全密鑰。
  - `capstone`，支持 Capstone 反匯編框架。
  - `curl`，支持 CURL 塊設備驅動。
  - `curses`，支持 curses 用戶界面。
  - `dbus-display`，支持 -display dbus，用於通過 D-Bus 進行圖形顯示。
  - `docs`，構建文檔支持。
  - `gio`，使用 libgio 支持 D-Bus。
  - `glusterfs`，支持 Glusterfs 塊設備驅動。
  - `gtk-clipboard`，支持 gtk UI 的剪貼板（實驗性，可能會掛起）。
  - `iconv`，支持字體字形轉換。
  - `libdaxctl`，支持 libdaxctl。
  - `libdw`，支持調試信息。
  - `libiscsi`，支持 libiscsi 用戶空間發起程序。
  - `libnfs`，支持 libnfs 塊設備驅動。
  - `libpmem`，支持 libpmem。
  - `libssh`，支持 ssh 塊設備。
  - `libudev`，使用 libudev 枚舉主機設備。
  - `libusb`，支持 libusb，用於 USB 直通。
  - `libvduse`，構建 VDUSE 庫。
  - `linux-aio`，支持 Linux AIO。
  - `linux-io-uring`，支持 Linux io_uring。
  - `live-block-migration`，支持塊設備遷移。
  - `lzfse`，支持 lzfse，用於 DMG 鏡像。
  - `lzo`，支持 lzo 壓縮。
  - `malloc-trim`，啟用 libc malloc_trim()，用於內存優化。
  - `membarrier`，支持 membarrier 系統調用（Linux 4.14+ 或 Windows）。
  - `modules`，支持模塊（非 Windows 系統）。
  - `mpath`，支持多路徑持久保留直通。
  - `opengl`，支持 OpenGL。
  - `parallels`，支持 parallels 鏡像格式。
  - `plugins`，支持通過共享庫加載 TCG 插件。
  - `png`，支持 libpng 處理 PNG 圖像。
  - `replication`，支持複製。
  - `rutabaga-gfx`，支持 rutabaga_gfx。
  - `sdl-image`，支持 SDL 圖像處理圖標。
  - `smartcard`，支持 CA 智能卡仿真。
  - `snappy`，支持 snappy 壓縮。
  - `sparse`，支持 sparse 檢查。
  - `spice`，支持 Spice 服務器，用於圖形和輸入設備的遠程訪問。
  - `spice-protocol`，支持 Spice 協議。
  - `stack-protector`，支持編譯器提供的堆棧保護。
  - `tcg`，支持 TCG（Tiny Code Generator）。
  - `tools`，構建 QEMU 附帶的支持工具。
  - `u2f`，支持 U2F 仿真。
  - `usb-redir`，支持 libusbredir，用於 USB 重定向。
  - `vfio-user-server`，支持 vfio-user 服務器。
  - `vte`，支持 gtk UI 的 vte。
  - `werror`，將警告視為錯誤進行處理。
  - `xkbcommon`，支持 xkbcommon，用於鍵盤布局處理。
  - `zstd`，支持 zstd 壓縮。
  - `system`，構建所有系統仿真目標。
  - `user`，構建所有支持的用戶仿真目標。
  - `linux-user`，構建所有 Linux 用戶模式仿真目標。
  - `bsd-user`，構建所有 BSD 用戶模式仿真目標。
  - `pie`，支持位置無關可執行文件。