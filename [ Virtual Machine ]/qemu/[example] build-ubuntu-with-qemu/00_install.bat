@echo off

cd %~dp0

:: ENV
:: - windows 11 23H2 with hyperV enable
:: - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版

curl -O https://cdimage.ubuntu.com/ubuntu-legacy-server/releases/20.04/release/ubuntu-20.04.1-legacy-server-amd64.iso

qemu-img create -f qcow2 ubuntu-disk.qcow2 15G

qemu-system-x86_64.exe -m 4G -smp 4 ^
-boot d -cdrom ubuntu-20.04.1-legacy-server-amd64.iso ^
-drive file=ubuntu-disk.qcow2,format=qcow2
