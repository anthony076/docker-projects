@echo off

cd %~dp0

qemu-system-x86_64.exe -m 8G -smp 4 ^
-drive file=ubuntu-disk.qcow2,format=qcow2 ^
-net nic -net user,hostfwd=tcp::2222-:22
