## use ubuntu server with qemu

- step，創建虛擬硬碟映像(ubuntu-disk.qcow2)
  > qemu-img create -f qcow2 ubuntu-disk.qcow2 15G

- step，加載安裝光碟(*.iso)
  > qemu-system-x86_64.exe -m 4G -smp 4 -boot order=d -cdrom ubuntu-20.04.1-legacy-server-amd64.iso -drive file=ubuntu-disk.qcow2,format=qcow2

  - `-m 4G`，4G 記憶體，[提高記憶體反而容易有延遲現象](#提高記憶體反而容易有延遲現象)
  - `-smp 4`，Symmetric Multiprocessing，對稱多處理核心，指定CPU核心數
  - `-boot order=d`，使用光碟開機
  - `-drive file=硬碟映像路徑,format=qcow2`，指定硬碟映像的路徑
  - `-cdrom 光碟映像路徑`，指定光碟映像的路徑

- step，在 qemu 中進行安裝

- step，僅透過硬碟映像(ubuntu-disk.qcow2)開機
  > qemu-system-x86_64.exe -m 8G -smp 4 -drive file=ubuntu-disk.qcow2,format=qcow2 -net nic -net user,hostfwd=tcp::2222-:22
  
  - `-m 4G`，增加運行的效率
  - 移除光碟映像的配置，僅保留應碟映像的配置
  - `-net nic -net user,hostfwd=tcp::2222-:22`，用於ssh的連接，需要在系統中啟用ssh才會有效

## 提高記憶體反而容易有延遲現象

- `-m 4G -smp 4` : 0130分脫離黑畫面，0202(+32s)完全進入系統
- `-m 8G -smp 4` : 0121分脫離黑畫面，0158(+37s)完全進入系統
- `-m 8G -smp 4 + accel` : 0245分脫離黑畫面，0259(+15s)完全進入系統