## 編譯qemu

- 可用的編譯選項
  - 方法，在 qemu 源碼的目錄中，透過 `$ ./configure --help`，查看可用的編譯選項，
  - 方法，或查看[編譯qemu可用的配置選項](qemu-compile-options.md)

- 範例，編譯qemu範例，[compile qemu v8.2.1 with MSYS on windows](../../[%20Android%20]/BlissOS/bliss-on-qemu.md#blissos-on-compiled-qemu-work)

- 範例，[為xilinx硬體添加qemu的支持](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/822247454/What+is+QEMU#WhatisQEMU-WhyUseQEMU)，
  透過編譯qemu，使qemu可以模擬任何硬體