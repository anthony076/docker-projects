@echo off

cd %~dp0

:: ENV
:: - windows 11 23H2 with hyperV enable
:: - QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5) 安裝版

:: step1，download installer-iso
::curl -O https://iso.cachyos.org/240512/cachyos-kde-linux-240512.iso

:: step2，create disk-image
::qemu-img create -f qcow2 cachyos_disk.qcow2 20G

:: step3，start install process
:: below command is tested under QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5)

qemu-system-x86_64.exe ^
-m 8G -smp 4 -M q35 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot order=dc ^
-drive file=cachyos_disk.qcow2,if=virtio ^
-cdrom cachyos-kde-linux-240609.iso ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22