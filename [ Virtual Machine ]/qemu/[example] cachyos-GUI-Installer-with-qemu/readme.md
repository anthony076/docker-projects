
## cachyOS

- 優缺點
  - 優，基於 archlinux，高度可客製化系統
  - 缺，安裝於qemu的過程耗時，安裝過程會使用 udev 和 rcu 機制進行安裝，過程緩慢且會出現可忽略的錯誤
  - 缺，在VM中使用cachyOS不會得到優化系統和軟體的優勢，推薦在實體機器上使用

- 系統需求
  - https://wiki.cachyos.org/installation/installation_prepare/

- 注意，預設會使用 copy to ram 可能會導致安裝更為緩慢，可透過以下方式取消
  -	step，選中 `CachyOS default (x86_64, BIOS)` 
  -	step，按下 `<tab>` 編輯安裝選項
  -	step，將 `copytoram=y` 修改為 `copytoram=n`

  <img src="copy-to-ram-error.png" width=800 height=auto>
  
## testlog on Qemu 9.0

- 產生硬碟檔
  
  > qemu-img create -f qcow2 cachyos_disk.qcow2 40G

- 安裝參數

  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,if=virtio -cdrom cachyos-kde-linux-240512.iso -vga virtio -display sdl,gl=on -usb -device usb-tablet -net nic -net user,hostfwd=tcp::2222-:22`

  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,if=virtio -cdrom cachyos-kde-linux-240512.iso -vga virtio -display sdl,gl=on -usb -device usb-tablet -net nic -net user,hostfwd=tcp::2222-:22`

  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -vga virtio -display sdl,gl=on -usb -device usb-tablet`

  Success fix "Create swap on /dev/ze" issue
  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet`

  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet -vga virtio -display sdl,gl=on`

  `qemu-system-x86_64w.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet -vga virtio -display sdl,gl=on`

  `qemu-system-x86_64w.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet`

  ```
  - 根據 cachy-installer-boot-menu 的 Hardware-Information(HDT)，偵測到 VESA Bios
  - 根據 https://www.qemu.org/docs/master/system/qemu-manpage.html#description
  qemu 模擬 Cirrus CLGD 5446 PCI VGA card or dummy VGA card with Bochs VESA 的 VGA
  - 手動指定 -vga std 或 -vga virtio
  ```

  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet -vga std (選擇 "CachyOS with NVIDIA" in boot-menu)`
  ISSUE : stuck at "Network Configuration"

  `qemu-system-x86_64w.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet -vga std (選擇 "CachyOS with NVIDIA" in boot-menu)`
  ISSUE : stuck at "Create swap on /dev/zram0"

  ```
  只要指定 -vga 就會出問題，取消手動指定 -vga
  ```

  `qemu-system-x86_64w.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,format=qcow2 -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet (選擇 "CachyOS with NVIDIA" in boot-menu)`
  PASS: "Create swap on /dev/zram0"
  PASS: "Network Configuration"
  ISSUE: stuck at "Graphical Interface"

  `qemu-system-x86_64.exe -m 8G -smp 4 -M q35 -boot order=dc -drive file=cachyos_disk.qcow2,if=virtio -cdrom cachyos-kde-linux-240512.iso -usb -device usb-tablet -vga std -accel whpx,kernel-irqchip=off -net nic -net user,hostfwd=tcp::2222-:22`

## Issues 

- stuck on Create swap on /dev/zram0

  [cachy linux iso not booting in linux, host machine freezes](https://gitlab.com/qemu-project/qemu/-/issues/1484)

## ref

- [QEMU 中的 VGA 和其他顯示設備](https://www.kraxel.org/blog/2019/09/display-devices-in-qemu/)