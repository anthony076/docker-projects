## qemu-remote-desktop

- 對於 remote desktop 有以下幾種方案

  - 方法，透過[VNC](#利用-vnc-進行遠端桌面)
    - 使用簡單，
    - guestOS不需要安裝 vnc-server，或其他第三方套件，qemu 本身支援 vnc-server
    - hostOS 安裝 vnc-client

  - 方法，透過[SPICE](#利用-spice-進行遠端桌面)
    - QEMU 支援 SPICE
    - 支援VNC缺少的高級特性 : 自動縮放解析度、剪貼簿同步、USB 裝置重新導向
    - 使用了高效的壓縮和加密技術，在網路差的情況下，提供低延遲的互動體驗，提供了高質量的視頻和音訊流
    - 多平台支援
    - 支援 SSL/TLS 加密，保護了遠端桌面會話的安全性
    - 缺，對特定linux有可能不支援SPICE (沒有SPICE相關的庫)

  - 方法，透過`RDP`
  - 方法，透過`NoVNC`
  - 方法，透過`WebRTC`

  - other
    
    https://serverfault.com/questions/1058126/qemu-monitor-via-remote-desktop
    ```
    qemu-system-x86_64 --help 2>&1 | grep -- '^-display' | sed 's/^/    /'
    -display spice-app[,gl=on|off]
    -display gtk[,grab_on_hover=on|off][,gl=on|off]|
    -display vnc=<display>[,<optargs>]
    -display curses[,charset=<encoding>]
    -display egl-headless[,rendernode=<file>]
    ```

## 利用 SPICE 進行遠端桌面

- qemu + SPICE 基本通訊架構

  ```mermaid
  flowchart LR
  
  a[SPICE-client @ hostOS] --> b[SPICE-server]

  subgraph QEMU
    b--> c["VirtIO 串行總線設備\n(建立與guestOS通訊的通道)"]
    c --> d["字符設備\n(SPICE to 字符)"] 
    d --> e["虛擬串行端口設備\n(傳輸字符)"]
  end
  
  subgraph guestOS
    e --> f[SPICE-vdagent]
  end
  ```

  其中，

  - `SPICE-server`，接收到客戶端的請求後，將請求轉換為 SPICE 格式的數據，最終，將數據傳遞給guestOS。SPICE-server自身不會處理和傳送數據，因此還需要以下的組件

  - `VirtIO 串行總線設備`，在 QEMU 和 guestOS 之間`建立高速的通訊通道`，負責將來自 SPICE 服務器的數據發送到虛擬機內的相應設備

  - `字符設備`，VirtIO串行總線設備建立通訊通道，由`字符設備`實際將SPICE數據轉換為字符

  - `虛擬串行端口設備`，作為 VirtIO 串行總線設備的終端，接收來自字符設備的數據，並將這些數據傳遞給虛擬機內的 SPICE vdagent

  - `SPICE vdagent`，接收數據後，處理各種請求，注意，Linux 內需要安裝 SPICE-vdagent

- 範例，利用SPICE與alpinelinux進行遠端桌面連接

  - [完整範例](./[example]%20remote-desktop-with-SPICE/01_run.bat)
  
  - step，在 host-os(windows)中安裝 SPCIE-client
    - 到 https://www.spice-space.org/download.html ，找 virt-viewer
    - 安裝 [virt-viewer-x64-11.0-1.0](https://releases.pagure.org/virt-viewer/virt-viewer-x64-11.0-1.0.msi)
  
  - step，在 guest-os(alpinelinux)中安裝 SPICE-vdagent

    ```shell
    doas apk add spice-vdagent
    doas rc-update add spice-vdagentd default
    doas service spice-vdagentd start
    ```

  - step，修改 qemu 的啟動參數
    
    - 注意，使用SPICE會使qemu以headless-mode啟動guestOS，
      因此，無法使用`-display sdl,gl=on`的參數

    ```batch
    qemu-system-x86_64.exe ^
    -m 8G -smp 4 -M q35 ^
    -accel whpx,kernel-irqchip=off ^
    -machine vmport=off ^
    -boot order=c ^
    -drive file=alpine-disk.qcow2,if=virtio ^
    -display default,show-cursor=on ^   @REM -display sdl,gl=on 無法使用
    -device qemu-xhci,id=xhci ^
    -usb -device usb-tablet ^
    -spice port=5930,disable-ticketing=on ^   @REM 必要，創建spice-server
    -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
    ```

    其中，
    - `-spice port=5930,disable-ticketing=on`，必要，用於配置spice-server監聽的端口號
      - disable-ticketing=on，指明禁用 SPICE 連接中的身份驗證(ticketing)

    - `-device virtio-serial-pci`，選用，創建一個VirtIO串行總線設備，用於建立與guestOS的通訊管道

    - `-chardev spicevmc,id=vdagent,name=vdagent`，選用，建立一個字符設備(chardev)
      - 用途，字符設備(chardev)會接收上游的數據，並將該數據轉換為字符數據後，再將字符數據傳遞給虛擬串行端口設備
      - spicevmc，指明字符設備用於和 SPICE 虛擬機通信(spicevmc)

    - `-device virtserialport,chardev=vdagent,name=com.redhat.spice.0`，選用，建立一個虛擬串行端口設備
      - virtserialport，指定當前設備類型，是虛擬串行端口，用於接收字符輸入
      - chardev=vdagent，指定上游字符輸入的設備是`name=vdagent`的字符設備
      - name=com.redhat.spice.0，是gusetOS中，spice-vdagentd 根據SPICE協議預設用於通訊的字段，
        可透過`/etc/init.d/spice-vdagentd`的command_args修改，例如，`command_args="-d /dev/virtio-ports/com.example.custom.0"`
      - 目的，虛擬串行端口設備將字符設備和spice-Agent串聯起來

  - step，在 hostOS 使用 spice-client 進行連接
    - 打開 virt-viewer
    - 透過 spice://localhost:5930 進行連接

- 需不需要添加`-display spice-app`參數
  - 啟用 SPICE 後，QEMU 會自動配置使用 SPICE 協議進行遠程顯示，
    因此，即使不指定 -display spice-app，也能正常啟用 SPICE 遠程顯示

  - 明確指定`-display spice-app`，可以確保使用期望的顯示輸出方式，避免因自動選擇而導致的意外行為
  - 如果需要特定的顯示配置或需要在多個顯示輸出之間進行選擇時，可以通過 -display 參數進行詳細配置

## 利用 VNC 進行遠端桌面

- 為什麼guestOS不需要安裝 vnc-server
  - qemu 本身自帶 vnc-server，當使用者添加`-vnc`參數時，qemu 會自動啟動vnc-server
  - qemu 為 guestOS模擬了虛擬顯卡(Cirrus、QXL、Virtio-GPU 等)，通過虛擬顯卡捕捉guestOS的屏幕畫面，
    並將圖形數據寫入 QEMU 提供的共享內存區域，從而獲取guestOS的圖形數據。

- step，修改 qemu 的啟動參數，添加`-vnc 0.0.0.0:5`

- step，透過 vnc-client 進行連接，
  - 安裝[vnc-viewer by REALVNC](https://www.realvnc.com/en/connect/download/viewer/windows/)
  - 透過`localhost:5`進行連接

## ref

- spice
  - [Remote viewing with Spice in QEMU](https://www.youtube.com/watch?v=DZ68O_pgqLc)
  - [How to run Spice Agent @ alpinelinux官方說明](https://wiki.alpinelinux.org/wiki/How_to_run_Spice_Agent)
  - [通訊流程圖](https://cloud.tencent.com/developer/article/2168913)