## 基本概念

- 專為虛擬機優化的 virtIO devices 和 vhost-user backends
  
  virtIO devices是一種標準化的虛擬設備框架，通過優化的驅動程序，使得虛擬機可以直接訪問主機的硬體資源，這些`優化的驅動程序運行在用戶空間`，而不是內核空間，這`減少了上下文切換的開銷`，提高了性能
  
  vhost-user backends則是`在virtIO的基礎上`進一步`優化了數據傳輸`，特別是在網路通信方面。它通過創建專門的通道來加速數據的傳輸，這種方法尤其適用於需要`高帶寬和低延遲`的應用場景
  
  virtIO 具有更高的通用性，vhost-user backends 支援的設備就不如 virtIO 多，
  [查看 vhost-user device 支援的設備](https://www.qemu.org/docs/master/system/devices/vhost-user.html#vhost-user-device)

  添加 virtIO 的硬碟設備
  > qemu-system-x86_64 -drive file=image.qcow2,if=virtio
  
  添加 virtIO 的網路設備
  > qemu-system-x86_64 -netdev type=virtio,fd=3 -device virtio-net-device,netdev=net0,id=nic1

  [使用vhost-user backends 範例](https://www.qemu.org/docs/master/system/devices/vhost-user.html#example)

- qemu 支援的圖形3D加速器

  - 基於OpenGL的3D加速器
    - Virgil: 使用virglrenderer庫，允許虛擬機通過OpenGL API直接訪問宿主機的顯卡。它是最早且最廣泛使用的QEMU 3D加速解決方案之一。
    - Virtio-GPU: 雖然不是專門針對3D加速設計的，但通過使用SPICE協議，可以實現基本的2D和3D圖形加速。Virtio-GPU通常與SPICE QXL或VMVGA驅動一起使用。

  - 基於Direct Rendering Manager (DRM) 的3D加速器
    - Mesa 3D: Mesa是一個開源的3D圖形庫，提供了OpenGL和其他圖形API的實現。通過Mesa，QEMU可以利用宿主機的DRM子系統來進行3D渲染。
    - I915/i965: Intel的DRM驅動（如i915和i965）也可以被QEMU用於3D加速。這些驅動支持Intel集成顯卡，通過Mesa庫與QEMU進行交互。
    - Nouveau: Nouveau是Linux下的開源NVIDIA顯卡驅動，通過Mesa庫與QEMU進行交互，支持NVIDIA顯卡的3D加速。
  
  - 其他
    - Radeon DRM: 對於AMD Radeon顯卡，QEMU可以通過Mesa庫與Radeon DRM驅動進行交互，以實現3D加速。
    - SwiftShader: SwiftShader是一個高性能的軟件渲染引擎，支持OpenGL ES 2.0和WebGL。雖然不是直接與顯卡進行交互，但可以作為一種軟件級別的3D加速解決方案。

  - 注意
    - 選擇哪種3D加速器取決於虛擬機的需求、宿主機的顯卡型號以及操作系統的兼容性。
    - 對於某些高端圖形應用程序，可能需要結合使用多個3D加速技術以獲得最佳性能。
    - 在配置QEMU時，確保正確安裝了所需的驅動和庫，以及啟用了相應的3D加速選項。

## qemu 的使用

- qemu[下載](https://qemu.weilnetz.de/w64/) 

- [啟用 UEFI 開機](qemu-uefi.md)

- [啟用 IOMMU 和 pci-passthrough](qemu-iommu-pci-passthrough.md)

- [使用 monitor 對虛擬機進行管理和操作](qemu-monitor-commands.md)

- 查詢
  - 查詢硬體規格，在 PowerShell 中，`$ Get-ComputerInfo`
  - 查詢可用參數，例如，`$ qemu-system-x86_64 -vga help`

## 常見的啟動參數 

- 記憶體選項
  - `-m 4096`，指定記憶體大小

- CPU選項
  - `-smp 4`，指定CPU數量
  - `-smp cores=4,threads=2`，使用 4 個核心和 2 個線程的 CPU，總共8個邏輯CPU
  - `-cpu host`，使用host的 CPU 特性

- 開機選項
  - `-boot 啟動裝置`，可用的啟動裝置有，
    - `-boot a`，從軟盤啟動
    - `-boot b`，從第二個軟盤啟動
    - `-boot c`，從第一個硬盤啟動
    - `-boot d`，從CD-ROM啟動
    - `-boot n`，從網絡啟動啟動
    - `-boot p`，從PXE啟動

  - `-boot order=dc`，指定開機順序，先從光碟開機，若失敗，改用硬碟開機
  - `-boot menu=on|off`，開啟或關閉 BIOS 啟動菜單，適用於多個開機媒體
  - `-boot splash-time=...`，設置啟動菜單顯示時間（毫秒）
  - `-boot reboot-time=...`，設置虛擬機器自動重新啟動的時間（毫秒），0 表示不重新啟動

  - `-cdrom ISO檔路徑`，指定光碟機內容

- 硬碟選項
  - `-hda 虛擬硬碟路徑`，舊參數，用於指定虚拟机中的第一个 IDE 硬盘驱动器，只能指定一個

  - `-drive file=虛擬硬碟路徑,format=qcow2`，新參數，可指定多個虛擬硬碟，其中
    - drive，啟用虛擬硬碟
    - file，指定虛擬硬碟的實際檔案
    - virtio，專為虛擬化環境設計的標準，旨在提供高效能的虛擬化I/O
    - format，設置虛擬硬碟的格式
    - 比較，為什麼 virtio 必 NVMe 更好
      - virtio 是一種專為虛擬化環境設計的標準，在某些情況下，virtio能夠超越 NVMe 的性能
      - Direct-Virtio 框架能夠直接訪問硬件，而不是通過虛擬化層次，這有助於減少延遲並提高效能
      - virtio 提供了一種通用的、跨平台的方式來實現高效能的虛擬化I/O，都能夠提供良好的性能和兼容性
      - NVMe 主要是為了`物理硬件`設計的，並且其性能優勢在虛擬環境中可能不如 virtio
      - 使用 virtio 通常比配置 NVMe 磁碟更為簡單
  
  - 範例，使用`NVMe裝置(SSD)`來模擬硬碟
    ```
    # file，指定磁碟映像檔的位置，
    # if=none，表示不使用任何特定的介面（如IDE或SCSI）
    # id=nvm，給這個磁碟映像檔分配了一個唯一的ID，以便後續參數可以引用
    -drive file=C:\qemu\qemu_nvme\disk1.qcow,if=none,id=nvm

    # nvme，添加了一個NVMe裝置到虛擬機器中
    # serial=deadbeef，給這個裝置分配了一個序列號
    # drive=nvm，指定NVMe裝置的內容為之前建立的磁碟映像檔(id=nvm)
    -device nvme,serial=deadbeef,drive=nvm
    ```

    ```
    -drive file=/var/nvme.qcow2,if=none,id=nvme0 
    -device nvme,serial=foo,drive=nvme0
    ```

  - 範例，使用`Virtio-blk-device`來模擬硬碟
    
    ```
    # file，指定磁碟映像檔的位置，
    # if=none，表示不使用任何特定的介面（如IDE或SCSI）
    # id=hd0，給這個磁碟映像檔分配了一個唯一的ID，以便後續參數可以引用
    -drive if=none,file=E:\systemtool\arm\arm-centos8\disk1.qcow2,id=hd0

    # virtio-blk-device，添加了一個Virtio-blk-device到虛擬機器中
    # drive=hd0，指定Virtio-blk-device的內容為之前建立的磁碟映像檔(id=hd0)
    -device virtio-blk-device,drive=hd0
    ```

    ```
    -drive file=$os_image,format=qcow2,if=none,id=disk0,cache=none,aio=native 
    -device virtio-blk-pci,bootindex=0,drive=disk0
    ```

- 圖形化選項
  - `-vga 圖形適配器`，指定虛擬顯卡，可用值為
    - std : 使用標準的VGA圖形適配器，預設值
    - cirrus : 使用Cirrus Logic GD5446圖形適配器
    - vmware : 使用VMware SVGA II圖形適配器，高性能的圖形加速
    - qxl : 使用QXL圖形適配器
    - virtio : 使用VirtIO圖形適配器，適合用於高性能的圖形加速
    - none : 不使用任何圖形適配器，圖形輸出將重定嚮到控製臺

  - `-display`，指定guestOS`輸出畫面的顯示方式`，包含指定顯示協議和顯示設備
    - gtk : 使用 GTK 窗口顯示虛擬機的圖形界面，提供更豐富的用戶界面元素和更好的可用性
    - sdl : 使用 SDL 窗口顯示虛擬機的圖形界面，提供更快的響應時間和更低的延遲
    - vnc : 通過 VNC 協議，遠程顯示虛擬機的圖形界面
    - spice-app : 使用 SPICE 客戶端顯示虛擬機的圖形界面，
      提供高質量的遠程桌面體驗，包括音頻、視頻和高級輸入設備支持
    - curses : 使用基於字符的界面顯示虛擬機的圖形界面
    - none : 不顯示任何圖形界面，適用於無頭(headless)模式
    - 例如，`-display gtk,gl=on`

  - 使用更快速的virtio裝置
    - 沒有GPU時，使用 virtio-vga[-BACKEND]，BACKEND 有 gl 和 rutabaga 兩種後端
      - 例如，`virtio-vga-gl`，使用 `virglrenderer` 庫來調用`opengl-api`與主機的顯示器控制器進行直通
      - 例如，`virtio-vga-rutabaga`，使用 `rutabaga_gfx` 庫來調用`gfxstream-api`與主機的 Wayland 進行直通
      - 注意，在 windows 上，可透過 `patch` 和`編譯qemu`的方式，使virtio-vga-gl可以在windows上使用，

        詳見，[編譯qemu，並在windows上啟用virglrenderer3D加速](../../[%20Android%20]/BlissOS/bliss-on-qemu.md#blissos-on-compiled-qemu-work)的範例
        
        啟用後，就可以透過以下使用3D加速器
        ```
        -display sdl,gl=on,show-cursor=off ^
        -device virtio-vga-gl ^
        ```

    - 有GPU時，virtio-gpu[-BACKEND][-INTERFACE]

- 硬體加速選項
  - `-accel`，可用值為
    - 選項 tcg : qemu內建加速器，預設使用，效能低
    - 選項 kvm、xen : Linux-host 適用，可搭 virt-manager
    - 選項 hvf : macOS 10.10-host 適用，可搭 homebrew-virt-manager

    - 選項 hax : win7-host 適用，使用 intel-hxam 的硬體加速方案
      - 確保已經設置好環境(禁用 hyperv、啟用 VT-x、安裝[HAXM](../readme.md#虛擬機加速))，再添加`-accel hax`
      - 注意，qemu v2.9 開始支援 hax 的選項
      - 注意，qemu v8.2 [已棄用hax的選項](https://www.qemu.org/docs/master/about/removed-features.html#haxm-accel-hax-removed-in-8-2)

    - 選項 whpx : win7-host 適用，使用 hyperv 的硬體加速方案
      - 注意，開啟whpx選項有可能反而使效能變低，
      - 若使用 `-accel whpx` 有問題，可改用 `-accel whpx,kernel-irqchip=off`，詳見範例中的說明，問題包含
        - 鍵盤滑鼠失效
      - 透過 `-machine q35` 改善效能變低的[問題](https://gitlab.com/qemu-project/qemu/-/issues/1820)

    - 其他可用的加速選項
      - 透過[網站查詢](https://www.qemu.org/docs/master/system/introduction.html#id1)
      - 透過命令查詢，`$ qemu-system-x86_64 -accel help`

- 硬體選項
  - `-device`，指定要添加的硬體裝置
    - 例如，`-device VGA`，添加VGA顯示器
    - 例如，`-device nec-usb-xhci`，添加NEC-USB-XHCI-USB-controller，支持USB 3.0
    - 例如，`-device qemu-xhci,id=xhci`，使用 qemu 提供通用的USB控制晶片驅動
    - 例如，`-usb -device usb-mouse -device usb-kbd`，添加USB滑鼠、USB鍵盤
    - 例如，`-usb -device usb-tablet`，tablet可以同時模擬鼠標和鍵盤的輸入，
      且提供一個整合的輸入解決方案，這可能有助於減少整體的輸入延遲，有很好的兼容性和靈活性
    - 注意，指定USB晶片也可以一起指定USB的晶片
      ```
      -device qemu-xhci,id=xhci
      -usb -device usb-tablet -device usb-kbd
      ```
    - 查詢支援的 device，`$ qemu-system-x86_64.exe -device ?`

  - `-machine|-M`，指定虛擬機的硬體配置和類型，例如
    - 選擇虛擬機架構：例如，x86、ARM、PowerPC
    - 定義處理器類型：例如，Intel 的 i386、i486、Pentium、Core2duo ，或 ARM 的 Cortex-A 系列
    - 配置記憶體大小
    - 指定固件：設置虛擬機啓動時使用的韌體類型，例如，BIOS、UEFI
    - 定義芯片組和設備模型：例如，芯片組、網路控製器、USB 控製器
    - 啓用虛擬化擴展：例如，Intel VT-x 或 AMD-V。
    - 指定 ACPI 行為
    - 定義虛擬機啓動順序
    - 啓用或禁用虛擬機的各種特性：例如，CPU 的虛擬化支援、SMBIOS 信息等。
    - 例如，`-M q35`，q35是一種PCI Express主機架構，常用於模擬現代的PC硬體環境
    - 例如，`-M virt`，virt是一種虛擬化主機架構，適合用於模擬虛擬機器
    - 例如，`-M vmport=off`，VMPORT是QEMU虚拟机中的一种特殊端口，允许宿主机直接访问虚拟机内存，關閉vmport可以
      - 阻止了不必要的直接內存訪問，避免內存洩漏或其他安全問題
      - VMPORT通常用於遠程管理和監控虛擬機，禁用可以減少網絡流量，提高性能和節省資源
      - 某些硬件或firmware可能不支持或不推薦使用VMPORT，關閉後可以提高VM的穩定性，減少硬體出錯的機率

- 網路選項
  - [網路通訊基礎](../readme.md#幾種通訊模式)
  - `-net`，可定義前端和後端
  - `-netdev`，只能定義後端
  - `-nic`，一條命令定義前端和後端
  - 範例，設定 NAT 模式，`-nic user,model=virtio`
  - 範例，設定 橋接模式，`-nic tap,model=virtio`

- 時鐘同步選項
  - `-clock host`，同步虛擬機器的系統時鐘與主機的系統時鐘，可減少時鐘漂移並提高時間同步的準確性 (qemu 8.2 失效)

  - `-rtc`，設置虛擬機的時鐘行為

    - 用途
      - 同步主機時間：確保虛擬機的時間與主機系統保持一致。
      - 設置虛擬機啟動時的時間：指定虛擬機啟動時的起始時間。
      - 配置時區：模擬虛擬機在不同的時區運行。
      - 配置時鐘模式：選擇是否使用 UTC（協調世界時）或本地時間。
    
    - base子參數，用於設置基準時間，可用參數值為
      - `utc` : 使用 UTC 時間（默認值）
      - `localtime` : 使用主機的本地時間
      - `date` : 設置指定日期和時間作為啟動時間
    
    - clock子參數，用於設置虛擬機的時鐘源，可用參數值為
      - `host` : 使用主機的時鐘（默認值）
      - `rt` : 使用實時時鐘（可能適合於需要高精度的情況）
      - `vm` : 使用虛擬機內部的時鐘
    
    - driftfix子參數，是否使用飄移修正
      - 用途，通過逐漸調整虛擬機時鐘使其與宿主機時鐘同步，可以有效地修正時間漂移問題，確保虛擬機長時間運行時仍能保持時間精確同步
      - 啟用飄移修正，`driftfix=slew`
      - 不啟用飄移修正，`driftfix=none`
    
    - 範例，`-rtc base=utc,clock=host,driftfix=slew`
      - 使用utc作為基準時間
      - 使用主機的時鐘
      - 啟用飄移修正

- 其他選項
  - `-vnc 0.0.0.0:100`

## 編譯 qemu

- [手動編譯qemu範例](../../[%20Android%20]/BlissOS/bliss-on-qemu.md#blissos-on-compiled-qemu-work)

- [可用的編譯參數](qemu-compile-options.md)

## qemu-gui 的使用

- Qtemu (已停止更新)
  - [How To Install And Correctly Use QEMU And QtEMU On Windows 10/11 + Intel HAXM +QEMU ](https://www.youtube.com/watch?v=cJzBHXZWxy4)

- EmuGUI
  - [EmuGUI repo](https://github.com/Tech-FZ/EmuGUI)

## 使用 whpx 加速

- [硬體加速基礎](../readme.md#虛擬機加速)

- on-going
  - [Windows加速QEMU](https://zhuanlan.zhihu.com/p/599381200)
  - [在windows10上使用qemu安装Windows 10虚拟机](https://blog.csdn.net/quentin_d/article/details/123167286)
  - [EmuGUI ＋ QEMU，Windows系統安裝虛擬機軟體](https://ivonblog.com/posts/qemu-on-windows/)
  - [流暢運行Android x86 的 Windows 主機上的 QEMU + WHPX + Virgl](https://www.youtube.com/watch?v=48JPfxnPcow)
  - [Windows运行QEMU](https://blog.csdn.net/weixin_41446370/article/details/131205560)

- 加速技巧
  - 虛擬硬碟映像不要使用 vhdx 格式，改用 qcow2 格式
  - 指定主機架構，`-accel whpx,kernel-irqchip=off -cpu qemu64 -machine q35`
  - 使用NVMe裝置(SSD)或VirtioIO來模擬硬碟

- 啟動範例

  ```
  qemu-system-x86_64.exe -M q35 -smp 4 -m 4G 
  -drive file=C:\qemu\qemu_nvme\disk1.qcow,if=none,id=nvm -device nvme,serial=deadbeef,drive=nvm 
  -cdrom C:\qemu\ubuntu-22.04.3-desktop-amd64.iso
  ```
  
  ```
  qemu-system-aarch64.exe -m #内存 
  -cpu cortex-a72 -smp 8,sockets=4,cores=2 -M virt #cpu
  -bios E:\systemtool\arm\arm-centos8\QEMU_EFI.fd  # 引导
  -device VGA -device nec-usb-xhci -device usb-mouse -device usb-kbd  
  -drive if=none,file=E:\systemtool\arm\arm-centos8\disk1.qcow2,id=hd0   #硬盘
  -device virtio-blk-device,drive=hd0  
  -drive if=none,file=E:\systemtool\arm\CentOS-8-aarch64-1905-dvd1.iso,id=cdrom,media=cdrom # 系统文件
  -device virtio-scsi-device -device scsi-cd,drive=cdrom  
  -net nic -net tap,ifname=tap0 # 桥接网卡
  ```

  ```
  qemu-system-x86_64 -hda mydisk.qcow2 -cdrom ubuntu.iso -boot d -vga std -display gtk,gl=on
  ```

- GPT 推薦配置
  - 檢測自己的VGA類型，`$ lspci -v`
  - 檢測自己的硬體類型，`$ sudo lshw`
  - 根據以上的訊息，選擇最優的 qemu 啟動配置參數
  - 推薦啟動參數
  
    ```
    qemu-system-x86_64 -m 8192M 
    
    # cpu
    -cpu host -clock host  (qemu 8.2 失效)

    # disk
    -drive file=your_disk_image.qcow2,if=virtio

    # network
    -netdev virtio-net-pci,id=net0
    -device virtio-net-pci,netdev=net0

    # vga 
    -display dxg  # for windows-host，qemu (qemu 8.2 失效)
    -display sdl,gl=on  # for linux-host

    # hardware
    -usb -device usb-tablet
    -audio sdl,device=alsa (qemu 8.2 失效)
    ```

## 範例

- `範例`，建立虛擬硬碟
  
  > qemu-img create -f qcow2 nixos_disk.qcow2 20G

- `範例`，啟動並透過光碟機安裝linux

  > qemu-system-x86_64 -m 4096 -smp 4 -boot d -cdrom nixos-minimal-23.11.5097.880992dcc006-x86_64-linux.iso -drive file=nixos_disk.qcow2,format=qcow2

- `範例`，透過虛擬硬碟啟動 linux (無法透過 ssh 連接)

  > qemu-system-x86_64 -m 4096 -smp 4 -drive file=nixos_disk.qcow2,format=qcow2

- `範例`，透過虛擬硬碟啟動 linux，並使 ssh 生效

  > qemu-system-x86_64 -m 4096 -smp 4 -drive file=nixos_disk.qcow2,format=qcow2 -net nic -net user,hostfwd=tcp::2222-:22

  - `-net nic` : 建立網路接口，使其能夠連接到網路
    
    NIC是虛擬機器與網路之間的橋樑，允許虛擬機器發送和接收網路封包

  - `-net user` : 為虛擬機器建立一個使用者模式網路堆疊
    
    讓虛擬機器透過主機的網路連接到外部網路，允許主機（即Windows系統）透過連接埠轉送與虛擬機器進行通訊

  - `hostfwd=tcp::2222-:22` : 設置端口轉發

  進行以上設置後，在 windows 中可透過以下命令連接到 qemu 中的 ssh-server

  > ssh -p 2222 帳號@localhost

- `範例`，將固定大小磁碟配置改成動態配置
  
  > qemu-img convert disk1.img -O qcow2 disk1-sparse.img

- `範例`，變更磁碟映像的大小
  
  - 基本概念和流程
    - 在本機中，透過 qemu-img resize 增加硬碟映像檔的大小
    - 進入vm中，透過 fdisk 重新調整 partition-table
    - 重新啟動vm，使新的partition-table生效
    - 透過 resize2fs 命令調整分區的大小 (重新調整fstab)
    - 在 partition-table 正確的狀況下，才能透過 resize2fs 命令調整 fstab
  
  - 實際操作，以 alpine-vm 為例

    考慮以下情境，
    
    在qemu中，若原來的硬碟映像大小為 10G，
    - 透過`$ qemu-img resize disk_image +10G`增加了 10G 後，
    - 透過`$ qemu-img info alpine-disk.qcow2`確認 virtual size 有增加

    進入 alpine-vm，輸入`$ fdisk -l`檢視以下訊息

    | Device    | Boot | StartCHS   | EndCHS     | StartLBA | EndLBA   | Sectors  | Size  | Id  | Type       |
    | --------- | ---- | ---------- | ---------- | -------- | -------- | -------- | ----- | --- | ---------- |
    | /dev/vda1 | *    | 2,0,33     | 611,8,56   | 2048     | 616447   | 614400   | 300M  | 83  | Linux      |
    | /dev/vda2 |      | 611,8,57   | 1023,15,63 | 616448   | 5859327  | 5242880  | 2560M | 82  | Linux swap |
    | /dev/vda3 |      | 1023,15,63 | 1023,15,63 | 5859328  | 20971519 | 15112192 | 7379M | 83  | Linux      |

    輸入`$ doas fdisk /dev/vda`重新調整 partition-table 的內容

    ```shell
    # 在 host
    qemu-img resize disk_image +10G

    # 在 alpine-vm
    fdisk /dev/vda
    p # 列出partition-table
    d # 執行刪除partition的命令
    3 # 刪除第三個partition
    n # 執行建立partition的命令
    p # 類型為 primary-partition
    3 # 設置編號
    First sector = 5859328
    Last sector =  41943039 # 使用預設值
    t # 執行變更partition類型的命令
    Hex code (type L to list codes) = 83
    w # 執行寫入

    doas reboot   # 重啟，使新的partition-table生效
    ```
    
    重啟後，輸入`$ fdisk -l`檢視以下訊息

    | Device    | Boot | StartCHS   | EndCHS     | StartLBA | EndLBA   | Sectors  | Size  | Id  | Type       |
    | --------- | ---- | ---------- | ---------- | -------- | -------- | -------- | ----- | --- | ---------- |
    | /dev/vda1 | *    | 2,0,33     | 611,8,56   | 2048     | 616447   | 614400   | 300M  | 83  | Linux      |
    | /dev/vda2 |      | 611,8,57   | 1023,15,63 | 616448   | 5859327  | 5242880  | 2560M | 82  | Linux swap |
    | /dev/vda3 |      | 1023,15,63 | 1023,15,63 | 5859328  | 41943039 | 36083712 | 17.2G | 83  | Linux      |

    透過 resize2fs 重新調整 /dev/vda3 的大小，
    - 安裝 e2fsprogs-extra 套件才有 resize2fs 命令可用，`$ apk add e2fsprogs-extra`
    - `$ resize2fs /dev/vda3`

    透過 `$ df -h` 檢視結果

- `範例`，在 windows 上使用圖形安裝ubuntu

  > qemu-system-x86_64 -m 4096 -smp 4 -boot d -cdrom nixos-minimal-23.11.5097.880992dcc006-x86_64-linux.iso -drive file=mydisk.qcow2,format=qcow2
  > -L Bios 
  > -usbdevice mouse -usbdevice keyboard
  > -rtc base=localtime,clock=host
  > -parallel none
  > serial none
  > --accel hax,thread=multi
  > -no-acpi -no-hpet -no-reboot

- `範例`，在 windows 上啟用 whpx 硬體加速範例

  > qemu-system-x86_64 -m 4096 -smp 2 -boot d -cdrom nixos-gnome-23.11.5097.880992dcc006-x86_64-linux.iso -drive file=mydisk.qcow2,format=qcow2 -accel whpx,kernel-irqchip=off

  - 若遇到 `whpx: injection failed, MSI (0, 0) delivery: 0, dest_mode: 0, trigger mode: 0, vector: 0, lost (c0350005)` 的錯誤，
    是由於QEMU的內核中斷控製器（IRQ chip）設定導致的，該錯誤會導致鍵盤無法使用，需要透過`kernel-irqchip=off`解決

- `範例`，完整指定硬件規格的範例

  ```
  qemu-system-x86_64.exe -m 3072 ^
  -cpu Haswell,vendor=GenuineIntel,+invtsc,vmware-cpuid-freq=on ^
  -machine pc-q35-2.9 ^
  -smp 4,cores=2 ^
  -usb -device usb-kbd -device usb-tablet ^
  -smbios type=2 ^
  -device ich9-intel-hda ^
  -device hda-duplex ^
  -accel hax ^
  -device ide-drive,bus=ide.1,drive=DebHDD ^
  -drive id=DebHDD,if=none,file=E:\OS\deb_hdd.img,format=qcow2 ^
  -device ide-drive,bus=ide.0,drive=DebDVD ^
  -drive id=DebDVD,if=none,snapshot=on,media=cdrom,file=E:\OS\debian-9.6.0-amd64-DVD-1.iso
  ```

  - `-cpu Haswell,vendor=GenuineIntel,+invtsc,vmware-cpuid-freq=on`
    - CPU型號為Haswell
    - CPU供應商為Intel
    - +invtsc啓用了非易失性時間戳計數器（Invariant TSC）特性
    - vmware-cpuid-freq=on啓用了VMware CPUID頻率報告特性
  
  - `-machine pc-q35-2.9`
    - 虛擬機的機器類型為Q35，一種基於Intel Q35芯片組的PC機型

  - `-smp 4,cores=2 `
    - 設定了虛擬機的CPU核心數為4，每個核心有2個線程，總共8個邏輯CPU
  
  - `-usb -device usb-kbd -device usb-tablet`
    - -usb，啟用了對usb裝置的支援
    - usb-kbd添加usb鍵盤
    - usb-tablet添加觸控板

  - `-smbios type=2`
    - 設定了虛擬機的SMBIOS（System Management BIOS）類型為2，這通常用於指定系統的類型為服務器
    - SMBIOS是一種用於描述系統硬體的標準，它可以幫助操作系統識別和管理硬體設備
  
  - `-device ich9-intel-hda -device hda-duplex`
    - 添加了音頻設備到虛擬機中
    - ich9-intel-hda 是Intel的高級定義音頻(HD Audio)控製器
    - hda-duplex 是一個雙嚮音頻設備，用於支援音頻輸入和輸出

  - `device ide-drive,bus=ide.1,drive=DebHDD`
    - 定義了一個IDE硬碟驅動器
    - ide-drive 是IDE硬碟設備
    - bus=ide.1 指定了它連接到的IDE總線
    - drive=DebHDD 是設備的ID

  - `drive id=DebHDD,if=none,file=E:\OS\deb_hdd.img,format=qcow2`
    - 進一步設置id為DebHDD的硬碟
    - if=none 表示这个驱动器没有直接连接到任何主机设备
    - 設置硬碟映像文件路徑，用於指定硬碟的內容

  - `-device ide-drive,bus=ide.0,drive=DebDVD`
    - 定義了一個IDE光碟驅動器
    - ide-drive 是IDE光碟設備
    - bus=ide.0 指定了它連接到的IDE總線
    - drive=DebDVD 是設備的ID
  
  - `-drive id=DebDVD,if=none,snapshot=on,media=cdrom,file=E:\OS\debian-9.6.0-amd64-DVD-1.iso`
    - 進一步設置id為DebDVD的硬碟
    - if=none 表示这个驱动器没有直接连接到任何主机设备
    - snapshot=on 啟用了快照功能
    - media=cdrom 指定了设备的媒体类型为CD-ROM
    - 設置光碟映像文件路徑，用於指定光碟的內容

- `範例`，在 windows 上安裝和運行 nixos 的啟動命令

  安裝命令
  > qemu-system-x86_64 -m 4096 -smp 4 -boot d -cdrom nixos-minimal-23.11.5097.880992dcc006-x86_64-linux.iso -drive file=mydisk.qcow2,format=qcow2

  啟動命令 (含port-forwarding，需要在nixos中啟用openssh)
  > qemu-system-x86_64 -m 4096 -smp 4 -drive file=nixos_disk.qcow2,format=qcow2 -net nic -net user,hostfwd=tcp::2222-:22

- `範例`，<a id="black-screen-solutions">在 LG gram 14z980 上，推薦安裝和啟動linux-desktop的命令</a>

  <font color=blue>安裝命令-1</font>，qemu需要支援virglrenderer
  
  - 啟用 hyperv + whpx 進行加速
  - 要使用此命令，必須使用`自編譯的qemu`，並啟用 `virglrenderer` 的功能
  - 執行命令後，不可以移動或變更視窗的大小，直到進入系統登入畫面後才可以

  ```batch
  qemu-system-x86_64.exe ^
  -m 8G -smp 4 -M q35 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -rtc base=utc,clock=host,driftfix=slew ^
  -boot order=dc ^
  -drive file=磁碟映像檔,if=virtio ^    @REM 指定光碟裝置
  -cdrom 光碟映像檔 ^
  -device virtio-vga-gl ^   @REM 指定顯示裝置
  -display sdl,gl=on ^      @REM 指定渲染方式
  -device qemu-xhci,id=xhci ^   @REM 指定USB晶片
  -usb -device usb-tablet ^     @REM 指定USB裝置
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

  <font color=blue>安裝命令-2</font>，啟用基本的圖形加速

  ```batch
  qemu-system-x86_64.exe ^
  -m 8G -smp 4 -M q35 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -rtc base=utc,clock=host,driftfix=slew ^
  -boot order=dc ^
  -drive file=磁碟映像檔,if=virtio ^
  -cdrom 光碟映像檔 ^
  -display sdl ^      @REM 指定渲染方式，僅使用SDL，不啟用opengl加速
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

  <font color=blue>安裝命令-3</font>，相容性最高，支援的圖形加速最少

  - 部分 distro 若設置 `display選項`會連安裝畫面都是黑畫面時，可改用此版本
  - 使用`vga選項`來取代display選項，以提高通用性

  ```batch
  qemu-system-x86_64.exe ^
  -m 8G -smp 4 -M q35 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -rtc base=utc,clock=host,driftfix=slew ^
  -boot order=dc ^
  -drive file=磁碟映像檔,if=virtio ^
  -cdrom 光碟映像檔 ^
  -vga std ^    @REM 使用兼容性最高的設置
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

  <font color=blue>安裝後的啟動命令(run.bat)</font>

  目前穩定且延遲低的啟動參數，[來源](./[example]%20build-alpine-with-qemu/run.bat)

  ```  
  @REM BETTER，需要等待到出現檢測畫面才可以變更qemu視窗大小
  @REM 添加 -display sdl,gl=on ^
  @REM - 優，字不會出現歪斜，變更qemu視窗尺寸時，會自動調整解析度
  @REM - 優，啟動sway會出現 libEGL initialize failed，但可用
  @REM - 優，畫面延遲更小

  qemu-system-x86_64.exe ^
  -m 8G -smp 4 -M q35 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -rtc base=utc,clock=host,driftfix=slew ^
  -boot order=c ^
  -drive file=alpine-disk.qcow2,if=virtio ^
  -display sdl,gl=on ^
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
  ```

  安裝後重新啟動系統遇到黑畫面，可以透過以下兩種方法解決

  - 解決方法1，仍然使用`-vga std`或`-display sdl`的選項，例如
    
    ```batch
    qemu-system-x86_64.exe ^
    -m 8G -smp 4 -M q35 ^
    -accel whpx,kernel-irqchip=off ^
    -machine vmport=off ^
    -boot order=c ^   @REM  從硬碟開機
    -drive file=磁碟映像檔,if=virtio ^
    -vga std ^
    @REM -display sdl ^
    -device qemu-xhci,id=xhci ^
    -usb -device usb-tablet ^
    -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
    ```
  
  - 解決方法2，手動更改啟動參數，將 `quiet` 變更為 `nomodeset`
    
    - step，採用此方法可以使用下面的啟動命令進入系統

      ```batch
      qemu-system-x86_64.exe ^
      -m 8G -smp 4 -M q35 ^
      -accel whpx,kernel-irqchip=off ^
      -machine vmport=off ^
      -boot order=c ^   @REM  從硬碟開機
      -drive file=磁碟映像檔,if=virtio ^
      -device virtio-vga-gl ^
      -display sdl,gl=on ^
      -device qemu-xhci,id=xhci ^
      -usb -device usb-tablet ^
      -net nic,model=virtio-net-pci -net user,hostfwd=tcp::2222-:22
      ```
    - step，跳出視窗後，按下任意按鍵進入開機選單
    - step，選中目標後，按下 tab 修改啟動選項
    - step，手動將 quiet 修改為 nomodeset 後，按下 Enter 重新進入系統

- `範例`，qemu 啟動最小系統的幾種方式
  - 方法，可以不建立 boot-file，直接指定 kernel 和 initramfs 的檔案後可直接開機，
    
    - 例如，`$ qemu-system-x86_64.exe -kernel <bzImage在本機的路徑> -initrd <init.cpio在本機的路徑>`

    - kernel參數 = linux-kernel(bzImage)的檔案路徑
    - initrd參數 = init-ram-disk，為初始的root目錄，為進入系統後預設的根目錄，即上述建立的檔案系統(init.cpio)建立的根目錄
  
    - 缺，此方式會將 linux 處於臨時的環境中(內存中)，所有在此環境安裝的套件會再重開機後消失
    - 缺，因為 initramfs 的大小有效，可能無法安裝大型套件

  - 方法，qemu啟動後，再指定 initramfs 的位置
    - 透過`$ qemu-system-x86_64 boot`啟動 qemu-vm
    - 看到 boot: 提示後，輸入 `/bzImage -initrd=/init.cpio`
      - `/bzImage`，用於指定 kernel 在 boot-file 中的路徑
      - `-initrd=/init.cpio`，用於指定 initramfs 在 boot-file 中的路徑
    - 若正常開機，bootloader 會自動執行 /init 的程序，進到 shell 的提示命令中
  
  - 方法，qemu 啟動時，直接指定 kernel、initramfs、磁碟映像
    - `$ qemu-system-x86_64 -nographic -no-reboot -kernel kernel/bzImage -initrd initrd/root.cpio.gz -hda disk.img`
    - `$ qemu-system-x86_64 -nographic -no-reboot -kernel kernel/bzImage -initrd initrd/root.cpio.gz -drive format=raw,file=disk.img`
    - 以上命令可以啟動，但是要使用硬碟需要額外的設置

  - 範例，[在windows 10上使用qemu安裝Windows10虛擬機](https://zhuanlan.zhihu.com/p/473240416)
  - 範例，[在windows中的qemu，運行linux虛擬機的範例](https://github.com/intel/haxm/issues/148)

## 常見問題

- 顯示黑畫面，使用 nomodeset 
  - 原理，藉著關閉圖形驅動的自動載入，並指定兼容性更高的圖形驅動，可以加速進入系統的時間和解決黑頻問題
  - 以 BlissOS為例，進入選單後，選中 BlissOS-15.8.2 2022-12-14後，按下e進入編輯
  - 將`quiet`改成以下，用於不載入特定顯示驅動， 修改後，按下 ctrl+x 後，套用並立即重新開機
    - 範例，`nomodeset`，關閉圖形驅動的自動載入
    - 範例，`nomodeset xforcevesa`關閉圖形驅動的自動載入，並載入強制使用兼容性更高的`VESA圖形模式`來啟動系統，vesa可以兼容所有系統
    - 範例，`nomodeset xforcevesa video=1920x1080 DPI=400`

  - 參考，[黑畫面的通用解](#black-screen-solutions)
  - 參考，[與顯示相關的內核啟動選項](https://gitlab.com/anthony076/linux-usage/-/blob/main/linux-core/03_display.md#與顯示相關的內核啟動選項)

- 偶爾顯示黑畫面，在啟動命令中添加`-monitor stdio`

  添加後，在 (qemu) console 中輸入，c，系統才能在啟動後繼續執行

  不確定原因，但是添加後更容易進入，更不容易出現黑畫面

## ref

- [QEMU 速查表](https://jackkuo.org/post/qemu_cheatsheet/)

- How to build bare minimal Linux system with qemu
  - [y2b @ Monkey see, monkey do](https://www.youtube.com/watch?v=c4j6z2huJxs)
  - [github](https://github.com/maksimKorzh/cmk-linux/blob/8068de3d7ec713c00c7df0442e8d2dad18ade6cc/tutorials/busybox/initrd.md)

- 教學資源
  - [qemu的详细资料大全](https://blog.csdn.net/kangkanglhb88008/article/details/126299695)
  - [五分鐘開始玩 qemu-kvm 虛擬機](https://newtoypia.blogspot.com/2015/02/qemu-kvm.html)
  - [用Qemu模擬ARM](https://jasonblog.github.io/note/qemu/yong_qemu_mo_ni_arm.html)
  - [在archlinux中使用qemu](https://wiki.archlinux.org/title/QEMU)
  - [qemu的使用 @ archlinux](https://wiki.archlinux.org/title/QEMU)