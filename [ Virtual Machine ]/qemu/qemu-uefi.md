
## 使用 UEFI 開機

- 將 linux 安裝到 grub 或是 uefi，通常是由bios-fw決定

  以 alpine 為例，自動安裝程序 setup-alpine 會根據bios-fw來決定如何對磁碟進行分區
  - 若bios-fw是grub，setup-alpine會建立grub的分區
  - 若bios-fw是uefi，setup-alpine會建立uefi的分區

  參考，[install-alpine-with-uefi](https://gitlab.com/anthony076/linux-usage/-/blob/main/alpinelinux/readme.md#%E5%AE%89%E8%A3%9D%E7%82%BA-uefi)

- qemu 預設使用傳統的BISO模式開機(MBR)，若要使用UEFI開機，需要額外的啟動參數

- 使qemu啟用UEFI的方式，有兩種啟動參數 : `-bios` 或 `-pflash`
  
  - uefi-bios-fw(fd檔案)，在QEMU虛擬機中，
    用於模擬UEFI-bios的環境，允許用戶在虛擬機內安裝和運行支持UEFI的操作系統

  - uefi-bios-fw 的選擇，有`edk2.fd`或`ovmf.fd`兩種
    - [edk2](https://github.com/tianocore/tianocore.github.io/wiki/EDK-II)，
      是UEFI規範的參考實現，也是開發UEFI固件的工具集，不只針對VM環境，它允許開發者各種平台和設備創建自定義的UEFI固件
    - [Open Virtual Machine Firmware，ovmf](https://github.com/tianocore/tianocore.github.io/wiki/OVMF)，
      是基於edk2的專案，專門啟用VM環境對UEFI的支持
    - ovmf 和 edk2 的選擇 : 如果想在虛擬機中運行支持UEFI的操作系統，使用OVMF就夠了，
      如果需要開發自己的UEFI固件或對UEFI規範有深入的需求，才需要改用edk2
      
  - 獲取fd檔案的方法
    - 方法1，利用edk2編譯出ovmf.fd
      - [如何建構ovmf](https://github.com/tianocore/tianocore.github.io/wiki/How-to-build-OVMF)
      - [利用edk2編譯出ovmf.fd](doc/如何利用edk2編譯出ovmf.fd.png)
      - [UEFI安裝環境](https://ithelp.ithome.com.tw/articles/10317238)
      - [在ubuntu上編譯ovmf.fd](https://wiki.ubuntu.com/UEFI/EDK2)

    - 方法2，推薦，[從clearlinux直接獲取ovmf.fd](https://github.com/clearlinux/common)

    - 方法3，透過包管理器獲得，例如，`$ apt-get install ovmf`，linux限定

    - 手動合併fd檔案

      注意，`合併順序很重要`，必須先 vars.fd 再使用 code.fd，例如，
      
      ```
      cat OVMF_VARS.fd OVMF_CODE.fd > OVMF.fd
      ```

    - 注意，對於`安裝版`或`自行編譯`的qemu，透過 `qemu\share` 目錄提供的檔案，
      合併出來的`edk2-x86_64.fd`都<font color=red>是不可用的</font>，原因不明

      合併命令
      ```batch
      type edk2-i386-vars.fd edk2-i386-code.fd > edk2-x86_64.fd
      ```

  - `-bios參數`
    - 可用於傳統的bios映像文件，例如，`-bios /path/to/bios.bin`
    - 可用於指定UEFI映像文件，例如，`-bios /path/to/ovmf.fd`
    - 使用 -bios 是只讀的，且功能受限的，可用於啟動，啟動後無法修改UEFI變量（如引導順序等）
    - 適用於模擬系統不需要更改BIOS內容的情況

  - `-pflash參數`
    - 用於指定一個可寫入的閃存映像文件，通常用於啟動UEFI固件（例如OVMF）
    - 例如，
      ```
      -drive if=pflash,format=raw,readonly=on,file=/path/to/ovmf_code.fd
      -drive if=pflash,format=raw,file=/path/to/ovmf_vars.fd
      ```
    - 注意，`-pflash` 有可能和其他加速選項衝突
    
      [Qemu/WHPX fails on applying UEFI firmware with -pflash](https://gitlab.com/qemu-project/qemu/-/issues/513)

      若啟用 whpx 加速，建議不要使用 -pflash 來啟用 UEFI，改用 -bios 避免衝突產生

- issues
  - [the UEFI issue with WHPX](https://github.com/msys2/MINGW-packages/issues/9276)

## ref

-[詳解UEFI](https://wiki.osdev.org/UEFI#Emulation_with_QEMU_and_OVMF)