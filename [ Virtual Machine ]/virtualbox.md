## virtualbox

- [virtualbox 設置參數](../packer/readme.md)

- 調整硬碟大小
  - 以 /dev/sda3 為例
  - step1，在 windows 上執行，`$ VBoxManage modifyhd <YOUR_HARD_DISK.vdi> --resize <SIZE_IN_MB>`
  - step2，進入 linux 系統中，調整 partition-table
    ```shell
    fdisk /dev/sda << EOF
    print
    d
    3
    n
    3
    \n
    \n
    EOF
    ```
  - step3，調整 filesyste-table，`$ resize2fs /dev/sda3`