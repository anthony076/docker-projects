## 基本概念
- 利用 vagrant 可快速建立vm環境(virtualbox / vmware / docker / Hyper-v)

- box 類似 docker 的 container，是已經安裝好的 vm，省略安裝和配置的步驟

- vagrantfile 用來`配置 vm 管理軟體的設定`，用來取代 virtualbox/vmware的GUI設定介面
  例如，修改 vagrantfile，實際上是在修改 vm 在 virtualbox 中的設定值

- `vagrant up` 的行為
  - 使用此命令前，確保 box 已經出現在 `vagrant box list 中`

  - 讀取 vagrantfile 中的 config.vm.box 欄位獲取 box名

  - 若 box名 已經存在 `vagrant box list` 的列表中，
    讀取 vagrantfile 的配置，建立新的vm並啟動

  - 若 box名 不存在 `vagrant box list` 的列表中，但符合`使用者名/box名`的格式，
    從 vagrant-cloud 下載 box 並添加到 box-list 中 

  - 執行內容

    vagrant 預設 box 都有安裝 openssh 和 Guest Additions，
    因此，透過 `vagrant up` 第一次啟動 vm 時，預設都會`執行ssh連線`和`安裝Guest-Additions`

    執行ssh連線需要 vagrantfile 中提供 ssh 連線的資訊，若不提供，預設使用密鑰登入，
    且 box 內必須寫入官方提供的公鑰，

    vm 必須預先設置好`root密碼`和`一般用戶免密碼執行shdo命令`，
    若vm沒有預先對上述兩項進行配置，則`vagrant up`第一次啟用vm時就會報錯

- 取得 box 的幾種方式
  - 方法1，手動至 vagrant-cloud 下載後，手動透過 `vagrant box add box名 本地路徑` 添加到`vagrant box list`中
  - 方法2，不需要手動下載 box，手動透過 `vagrant box add box名 網址` 下載並添加到`vagrant box list`中
  - 方法3，執行 `vagrant up` 時，會檢查 vagrantfile 得到使用的 box，若 box 不存在，會自動下載 box

- vagrantfile 的`載入順序`和`設定值的覆寫`
  - `載入路徑1`: 和 box 一起打包的 Vagrantfile 檔，例如，`sample.box/Vagrantfile`
    
    若將 Vagrantfile 和 VM 一起打包，使用者就可以使用簡化的Vagrantfile

  - `載入路徑2`: vagrant 在主機上的預設配置檔目錄，位於 `C:\Users\ching\.vagrant.d\` 的目錄中，用於通用的設定值

  - `載入路徑3`: 當前用戶目錄中的 Vagrantfile 檔

  - 優先權: 路徑1 > 路徑2 > 路徑3

  - 覆寫: 先載入的配置，會被後載入的配置覆寫

- vagrant 會`自動掛載共享資料夾`
  
  第一次啟動 vm，vagrant 會檢查 Guest-Additions 是否已經安裝，若否，vagrant 會主動進行安裝，
  安裝後，vagrant 預設會自動建立共享資料夾，將本機當前的工作目錄，映射到 vm 的 /vagrant 的路徑
  
  此共享資料夾是自動建立的，不需要在 vagrantfile 中進行設置

  可以透過 `config.vm.synced_folder '.', '/vagrant', disabled: true` 取消
  
- `provision` 是第一次建立vm才會執行的指令，只會執行一次，

  provision 的命令可以透過以下幾種方式執行
  - 方法1，透過 `vagrant up`，第一次建立vm
  - 方法2，透過 `vagrant provision` 命令，執行 vagrantfile 中所有的 provision 指令
  - 方法3，透過 `vagrant reload --provision` 命令執行

## vagrant 和 SSH

- 概念，vagrant 會`自動添加 ssh 的 port-forwarding`

  透過 box 建立 vm 實體時，vagrant 會根據 vagrantfile 中的配置，
  自動在 virtualbox 中建立 port-forwarding 的規則

- 概念，若 vagrantfile 設置 ssh 的帳號密碼，就會以帳號登入，否則，會使用密鑰登入

- 概念，密鑰登入失敗的幾個原因
  - `原因1`，VM 設置的權限或擁有身分不對，必須確保
    - .ssh 目錄的權限為 700
    - authorized_keys 檔案的權限為 600，
    - authorized_keys 檔案的擁有者是 vagrant

  - `原因2`，使用 config.ssh.insert_key = true 的設置時，主機上公私鑰未同步
    - 透過 `$ vagrant sshd_config` 查看私鑰位置
    - 刪除私鑰後，重新執行 `$ vagrant sshd_config`，使 vagrant 重新產生 insecure 的私鑰
    - 重新執行 `$ vagrant halt` 和 `$ vagrant up`

  - `原因3`，使用自定義的公私鑰時，找不到本機上的私鑰，或公鑰未重新寫入VM
    - 透過 `config.ssh.private_key_path` 指定主機上私鑰的位置
    - 透過 [其他方式](https://dev.to/serhatteker/add-ssh-public-key-to-vagrant-1gb9)將公鑰寫入VM

- 預設登入密碼
  - 在 vagrant-cloud 下載的 box，預設都會使用 vagrant/vagrant，可透過 `$ su vagrant` 進行切換

  - 若無法使用 vagrant 進行登入，該 box 可能未建立 vagrant 的帳號，參考 box 建立者的使用說明

  - 或者尋找 `~/.vagrant.d/boxes` 或 `~/.vagrant.d/boxes/` 或 `~/` 的目錄中，是否有 Vagrantfile 的檔案

- 透過`密鑰`登入 vm

  - 條件1，VM 寫入公鑰
  
    若需要`第一次啟動vm就以vagrant內建私鑰成功登入`，需要預先在 box 中就放入SSH的公鑰，
    大部分的 vagrant-cloud 上的box都會使用[官方提供且不安全的SSH公鑰](https://max.computer/blog/breaking-in-and-out-of-vagrant/)

    [公鑰下載1](https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub)、
    [公鑰下載2](https://github.com/hashicorp/vagrant/blob/master/keys/vagrant.pub)

    使用vagrant提供的公鑰雖然不安全，但能確保使用者第一次啟動VM就可以透過SSH私鑰登入，
    vagrant內建的私鑰和上述的公鑰配對的，透過此方式不需要公開密碼，以避免密碼外洩的風險

    為安全起見，使用者應`手動創建新的公私鑰`後，替換vagrant內建的私鑰，
    並在成功登入vm後，手動替換掉vm內建的公鑰，以確保安全

  - 條件2，設置 `config.ssh.insert_key = true`，替換內建的公鑰

    當 vagrant 偵測到 VM 內建的不安全公鑰時，vagrant 會自動重新產生公私鑰，
    並將新的公鑰寫入 VM 的 authorized_keys 檔案中

    預設值為 true，若要使用自己建立的公私鑰，應設置為 false，
    並透過 `config.ssh.private_key_path = "私鑰路徑"` 指定私鑰位置

- 範例，建立 box 時，寫入 vagrant公鑰範例
  ```shell
  # 建立公鑰的新目錄
  mkdir -p /home/vagrant/ .ssh
  
  # 變更目錄權限，僅限使用者使用
  chmod 0700 /home /vagrant/ .ssh
  
  # 下載公鑰並保存到新目錄
  wget –no-check-certificate \
    https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub \
    -O /home/vagrant/ .ssh/authorized_keys
  
  # 變更公鑰的權限
  chmod 0600 /home /vagrant/ .ssh/authorized_keys
  
  # 變更子目錄的權限
  chown -R vagrant /home/vagrant/ .ssh
  
  # 將 openssh 存取公鑰的目錄變更
  sudo nano /etc/ssh/sshd_config
  
  # 添加以下
  AuthorizedKeysFile %h/.ssh/authorized_keys

  # 重啟 service
  sudo service ssh restart
  ```

- 範例，在 vagrantfile 中，指定自己建立的公私鑰
  ```vagrantfile
  pub_key = `ssh-keygen -y -e -f '~/.aws/private_key.pem'`
  config.ssh.insert_key = false
  config.vm.provision "shell", inline: <<-EOC
    echo '#{pub_key}' >> /home/vagrant/.ssh/authorized_keys
    sed --in-place=.bak -r 's/^#?(PermitRootLogin|PermitEmptyPasswords|PasswordAuthentication|X11Forwarding) yes/\1 no/' /etc/ssh/sshd_config
    sed --in-place=.bak '/== vagrant insecure public key$/d' /home/vagrant/.ssh/authorized_keys
    sed --in-place=.bak '$!N; /^\(.*\)\n\1$/!P; D' /home/vagrant/.ssh/authorized_keys
    sudo service ssh restart
  EOC
  end
  ```

## 基本使用
- step1，下載
  - 下載 virtualbox 或 VMWare 或 Hyper-V
  - 下載和安裝 [Vagrant](https://www.vagrantup.com/)

- step2，安裝 vm 的執行環境，virtualbox / vmware / docker / Hyper-v

- step3，至 [Vagrant Cloud](https://app.vagrantup.com/boxes/search) 查看要使用的 box

- step4，透過 vagrant init box名，建立 vagrantfile

- step5，依照 box 的實際情況，修改 vagrantfile

- step6，透過 vagrant up 啟動 vm

- step7，透過 vagrant ssh 連接到 vm

## [範例] (選用) 建立自己的 box，以 virtualbox 為例

- 參考，[官方 box 的環境設置](https://developer.hashicorp.com/vagrant/docs/boxes/base)

- 流程
  - step1，建立 vm，注意，需要安裝 Guest Additions，並確保 share-folder 可用
  - step2，將 vm 轉換為 box 檔
  - step3，上傳至 vagrant-cloud

- `step1`，建立vm

  - `必要1`，安裝並配置 SSH
    
    - 1-1，檢查 ssh 是否啟用，`sudo service ssh status`

    - 1-2，(選用) 變更 .ssh 目錄 + 變更 ssh-server 的存取權限

      注意，以下步驟不是必要，但可以讓 ssh 更安全
      ```shell
      # 新建立 .ssh 目錄
      mkdir -p /home/vagrant/.ssh
      # 變更 .ssh 目錄的存取權限，限定只有用戶本人可讀/可寫/可執行，禁用同群組和其他群組的存取權限
      chmod 0700 /home/使用者帳戶/.ssh
      # 變更 authorized_keys 的存取權限，限定只有用戶本人可讀/可寫/不可執行，禁用同群組和其他群組的存取權限
      chmod 0600 /home/使用者帳戶/.ssh/authorized_keys
      # 設置 .ssh 目錄的擁有者，並套用子目錄和檔案(-R)
      chown -R 使用者帳戶 /home/使用者帳戶/.ssh
      # 變更 .ssh 目錄的預設位置
      修改 nano /etc/ssh/sshd_config
      添加 AuthorizedKeysFile /home/使用者帳戶/.ssh/authorized_keys
      重啟 service ssh restart
      ```

    - 1-3，測試 ssh-server 是否啟用
      - 在 virtualbox 上設置好 ssh 的 forwarded-port
        ```
        host = 127.0.0.1
        host-port = 22
        guest = (空白)
        guest-port = 22
        ```

      - 在 windows 上，可以透過`$ ssh 使用者帳戶@127.0.0.1` 進行測試

    - 注意，自己建立的 box，`不需要預先在 virtualbox 設置 ssh 的 port-forwarding`
      
      vagrant 透過 box 建立 vm 實體時，會根據 vagrantfile 中的配置，
      自動在 virtualbox 中建立 port-forwarding 的規則

      若 vagrantfile 中沒有 ssh 的相關設置，
      vagrant 會以`vagrant ssh-config`中顯示的配置，自動為`vagrant up`建立的 vm 添加 port-forwarding 的規則，
      並且透過 `vagrant ssh` 連線時，也會自動ssh的配置，不需要人為透過 ssh 命令進行連線

  - `必要2`，確保有設置`root用戶密碼`和`一般用戶免密碼執行命令`
    - vm 若沒有設置超級用戶，vagrant 的許多命令在執行時就會出錯

    - 2-1，提高 ssh 連線的權限: 設置 root 帳號的密碼

      > 注意，sudo 命令只提高單次權限的命令，su 命令會啟動一個新的 shell，同時允許使用 root 權限運行儘可能多的命令，
        vagrant 透過 ssh 對vm下達命令，ssh 會建立新的 shell 與 vm 進行通訊，因此會使用大量 su 命令，
        透過 su 命令，vagrant 對 vm 下達命令就不需要輸入密碼

      - `sudo passwd root` 設置密碼

      - `su -` 測試以 root 登入

    - 2-2，修改 sudoers 列表，為一般用戶設置無密碼提示運行 sudo 命令
      - `sudo visudo -f /etc/sudoers.d/一般用戶名`

        visudo 是修改 sudoers 檔案的內建命令，不能透過一般文字編譯器修改
      
      - 添加以下
        ```shell
        一般用戶名 ALL=(ALL) NOPASSWD:ALL
        ```

      - 進行測試

        執行 `sudo pwd`，若設置正常，就不會提示密碼

- `step2`，降低 vm 的大小
  - [移除不需要的檔案](scripts/cleanup.sh)
  - [降低硬碟空間](scripts/zerodisk.sh)
  - [最大程度降低vm大小](scripts/maximum-reduce-vm-size.sh)
  
- `step3`，(選用) 透過命令將 vm 轉換為 box 檔
  - 3-1，切換到含有 vagrantfile 的目錄
  
  - 3-2，執行轉換
    - 語法: `vagrant package --base <virtualbox中的vm名> --output 檔案名.box`

    - 其他可用參數
      - `--include x,y,z`，添加額外的檔案到 box 中
      - `--vagrantfile <vagrantfile路徑>`，將 vagrantfile 添加到 box 中

    - 注意， --base 參數只支持VirtalBox
  
    - 注意，若建立box時有添加 vagrantfile，用戶建立的 vagrantfile 就可以越簡化，不需要將配置都曝露在 vagrant-cloud，
      參考，[vagrantfile 的載入順序和設定值的覆寫](#基本概念)一節
    
  - 建立完成後，box 會位於當前的目錄中

- `step3`，(選用) 透過 packer 建立 vm 並轉換為 box 檔，[以 myarch 為例](../packer/example/packer-vbox-myarch/readme.md)

- `step4`，(選用) 上傳至 vagrant-cloud
  - 4-1，建立 box 檔案的 md5 碼
    > 在 windows 中，輸入 `certutil -hashfile box名 md5`

  - 4-2，將 box 上傳至 vagrant-cloud
    - 到 New Version 的頁面，建立版本號
    - 在建立的版本號的下方，點擊 Add a provider
    - 輸入 md5 碼和相關訊息 > 選擇 box 檔進行上傳
    - 點擊 release 正式發布 box

- `step4`，(選用) 若要在本機進行測試，不需要上傳至 vagrant-cloud，直接使用本地的 box 檔
  - 4-1，將 step2 建立的 box 檔添加到 box-list 中，`vagrant box add 自定義box名 box路徑`
  - 4-2，建立 vm 並啟動，`vagrant up`
  
## [範例] 使用 vagrant-cloud 上的 box

- step1，`$ vagrant init archlinux/archlinux`
- step2，`$ vagrant up`
- step3，`$ vagrant ssh`
    
  若無法連接，透過 `vagrant ssh-config` 檢查配置後，可手動輸入`ssh 帳號@ip -p 連接埠` 進行手動測試

  vagrant 會以`vagrant ssh-config`中顯示的配置，自動為`vagrant up`建立的 vm 添加 port-forwarding 的規則

## [範例] 透過 vagrant-ssh 執行 gui 應用
- 參考，[x11基礎 和 透過ssh執行gui應用範例](../ssh_proxy/doc/透過ssh執行gui的應用.md)

- 以 vagrant-cloud 的 [u2204x64-server v1.0.1](https://app.vagrantup.com/ching/boxes/u2204x64-server) 為例
  
  - step1，在 vm 中進行配置
    - 確保 vm 的 `/etc/ssh/sshd_config` 設置 `X11Forwarding yes`
      ```shell
      sed -i 's/^#X11Forwarding no/X11Forwarding yes/1' /etc/ssh/sshd_config
      ```

    - 在 vm 中安裝 xauth，`$ sudo apt install xauth`
    
    - 關閉 vm
  
  - step2，在 vagrantfile 中開啟 forward_agent 和 forward_x11 的選項
    ```vagrantfile
    Vagrant.configure("2") do |config|
      config.vm.box = "ching/u2204x64-server"
      config.vm.box_version = "1.0.1"

      config.ssh.username = 'user'
      config.ssh.password = 'user'
      config.ssh.keep_alive = true
      
      # 需要啟用 forward_agent 和 forward_x11
      config.ssh.forward_agent = true
      config.ssh.forward_x11 = true
      
      config.vm.synced_folder ".", "/share"
      config.vm.synced_folder '.', '/vagrant', disabled: true
    end
    ```

  - step2，在本地端(ssh-client)設置 DISPLAY 的環境變數
    - 方法1，設置臨時變數，`$ set DISPLAY=127.0.0.1:0.0`
    - 方法2，設置永久變數，`$ setx DISPLAY "127.0.0.1:0.0"`
    - 注意，若設置 setx ，需要重啟cmd視窗 或 重啟vscode 後才會生效

  - step3，啟用 xserver 軟體，例如，`xming`，使用預設的配置即可
  
  - step4，透過 `$ vagrant ssh` 進行登入

  - step5，登入 vm 後，以`xeyes`命令進行測試

## 常用命令
- 建立 vagrantfile，`vagrant init 要使用的box名稱`，例如，`vagrant init ubuntu/trusty64`
  - 注意，要使用的box名稱，必須是 vagrant-cloud 上的box，或是本地手動添加的box名
  - 注意，`vagrant init box名`，只是建立`box欄位`為指定名稱的`vagrantfile模板`，仍然需要手動配置vagrantfile

- 啟動 vm，
  - `vagrant up`
  - `vagrant up --debug`

- 查看 vm 狀態，`vagrant status`
- 關閉 vm，`vagrant halt`
- 刪除 vm，
  - `vagrant destroy --force vm名`
  - `vagrant destroy --force vm-id`

- 下載 box 並添加到`vagrant box list`中，
  - `vagrant box add box名 本地路徑`
  - `vagrant box add box名 網址`
  - `vagrant box 已下載的box名`，注意，需要透過`vagrant box list`確認已下載的box名

- 查看安裝的 box，`vagrant box list`

- 移除 box，`vagrant box remove box名`
  > 注意，只是從 vagrant box list 的列表中移除，vm的實體檔案還在

- 刪除 vm 實體，
  - 查看 vmId，`vagrant global-status --prune`
  - 執行刪除，`vagrant destroy <vmId>` 或 `vagrant destroy 虛擬機名`
  - 注意，vagrant destroy 命令不會將 box 從 box-list 中移除，

- 透過 ssh 連線至 vm，`vagrant ssh`
  > 注意，此命令要有設置 ssh 才會有效

- 安裝插件名，`vagrant plugin install 插件名`

- 透過 rdp 進行桌面連線，`$ vagrant rdp -- 參數`，
  - 此命令只對支援RDP的VM有效
  - 常用於 windows-vm

- 調整硬碟大小
  - 以 /dev/sda3 為例
  - 手動調整，不透過 vagrant 調整硬碟，參考，[在vbox中調整硬碟大小](../virtuabox/readme.md)
  - step1，安裝套件，`$ vagrant plugin install vagrant-disksize`
  - step2，修改 vagrantfile
    ```
    onfig.disksize.size = '20GB'
    ```
  - step3，進入 linux 系統中，調整 partition-table
    ```shell
    fdisk /dev/sda << EOF
    print
    d
    3
    n
    3
    \n
    \n
    EOF
    ```
  - step4，調整 filesyste-table，`$ resize2fs /dev/sda3`

## vagrantfile 的使用
- 指定要使用的 box 名，`config.vm.box = "bento/ubuntu-14.04"`

- 指定虛擬機名，`config.vm.define = "myvm"`，vagrant 會將 vm 命名為 vm_虛擬機名_id

- 是否要自動更新 box，`config.vm.box_check_update = false`

- ssh 相關配置
  - 設置帳號，`config.ssh.username = 帳號`
  - 設置密碼，`config.ssh.password = 密碼`
  - 避免ssh斷線，`config.ssh.keep_alive = 'ture'`
  - 變更 SSH 的連接埠，`config.vm.network :forwarded_port, guest: 22, host: 3200, id: 'ssh'`，
  - 替換內建的公鑰，`config.ssh.insert_key = true`，VM 需要內建公鑰
  - 指定自定義私鑰的位置，`config.ssh.private_key_path = 路徑`  
  - 注意，配置 ssh 不需要特別建立轉發埠的設置，
  
    vagrant 會自動建立(主機)127.0.0.1:2222，映射到(vm)127.0.0.1:22的配置

- 選用，設置 port-forwarding
  - `config.vm.network "自定義設置名", guest: vm開放的埠, host: 主機連接的埠`
  - `config.vm.network "forwarded_port", guest: 80, host: 8080`
  - `config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"`

- 選用，建立只有主機能存取的私有網路，`config.vm.network "private_network", ip: "192.168.33.10"`

- 共享目錄
  - 建立自定義的共享目錄，`config.vm.synced_folder "../data", "/vagrant_data"`
  - 停用預設共享目錄的掛載，`config.vm.synced_folder '.', '/vagrant', disabled: true`
    
    注意，此命令只會停止掛載，不會取消共享目錄的設置
  
- 設置特定平台用的選項，以 virtualbox 為例
  ```vagrantfile
  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = "1024"
  end
  ```

- 透過 provision 進行環境初始化
  ```vagrantfile
  // 使用 shell 執行 bootstrap.sh 
  config.vm.provision :shell, path: "bootstrap.sh"
  ```

## 已經放在 vagrant-cloud 的環境
- env-u2204x64-server
  - [cloud](https://app.vagrantup.com/ching/boxes/u2204x64-server)
  - [vagrantfile](env-u2204x64-server/Vagrantfile)

- env-proxmox
  - [cloud](https://app.vagrantup.com/ching/boxes/proxmox72)
  - [vagrantfile](env-proxmox72/Vagrantfile)

- env-bsv2201
  - [cloud](https://app.vagrantup.com/ching/boxes/bsv2201)
  - [vagrantfile](env-bsv2201/Vagrantfile)

- myarch
  - [cloud](https://app.vagrantup.com/ching/boxes/myarch)
  - [packer-vbox-myarch](../packer/example/packer-vbox-myarch/readme.md)

## Ref
- [Vagrant 學習筆記](https://vagrantpi.github.io/2018/02/11/vagrant/)

- [從頭製作 vbox](https://www.engineyard.com/blog/building-a-vagrant-box-from-start-to-finish/)
  或 [中文版](https://blog.levi-g.info/building-a-vagrant-box.html)
  或 [官方-建立基礎box](https://developer.hashicorp.com/vagrant/docs/boxes/base)

- [Vagrant 介紹](https://ithelp.ithome.com.tw/articles/10191495)

- [create docker-vm with vagrant](https://blog.gslin.org/archives/2014/05/17/4656/%E7%B8%BD%E7%AE%97%E6%98%AF%E6%90%9E%E5%AE%9A-vagrant-docker/)

- [virtualbox+vagrant学习-1-环境安装及vagrantfile的简单配置](https://www.cnblogs.com/wanghui-garcia/p/10139258.html)

- [使用Vagrant進行伺服器環境部署](https://www.cc.ntu.edu.tw/chinese/epaper/0040/20170320_4006.html)

- [vagrantfile 官方文檔](https://developer.hashicorp.com/vagrant/docs/vagrantfile)

- [virtualbox 可用設定值(全)](https://www.virtualbox.org/manual/ch08.html)