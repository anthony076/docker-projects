## env-u2204x64-server
- v1.0.0
  - iso : ubuntu-22.04.1-live-server-amd64.iso
  - openssh

- v1.0.1
  - shrink box size to 1.33G

  - **x11 forwarding enable** for windows-host
      - step1，install [vcxsrv](https://sourceforge.net/projects/vcxsrv/) on windows-host
      - step2，$ setx DISPLAY "127.0.0.1:0.0"
      - step3，`$ vagrant up`  then `$vagrant ssh`

- v1.0.2
  - installed package: xorg、slim、lxde-core、tightvncserver、novnc、websockify
  - replace x11 to noVNC
    - use `$ bash ~/startvnc.sh` to start noVNC
    - browse `http://127.0.0.1:6080/vnc.html` on host, and use `12345678` for login pass.
      Or use `$ vncpasswd` to change the pass.
