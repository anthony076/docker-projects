## LXC 的使用

- 常用命令
  - 下載容器
    ```shell
    sudo lxc-create -t download -n mycontainer \
    -f /usr/share/lxc/config/common.conf -- \
    --dist ubuntu --release bionic --arch amd64
      
    sudo lxc-create -t download -n mycontainer \
    -f /usr/share/lxc/config/common.conf -- \
    -d ubuntu -r jammy -a amd64 
    ```
    
  - 檢查狀態，`$ sudo lxc-ls`
  - 啟動容器，`$ sudo lxc-start -n mycontainer`
  - 連接到已啟動的容器，`$ sudo lxc-attach -n mycontainer`
  - 停止已啟動的容器，`$ sudo lxc-stop -n mycontainer`
  - 刪除已停止的容器，`$ sudo lxc-destroy -n mycontainer`