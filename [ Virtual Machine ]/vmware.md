## 介紹

- 使用 Linux 核心的KVM功能和 virtio 半虛擬化技術，可以讓虛擬機器有較少的效能損失，運型更加流暢
- 限制在 linux 上使用，windows 無法使用
- 是Libvirt 的圖形化前端，除了要安裝virt-manager，也需要安裝並配置好Libvirt

## ref

- [Virt-manager @ archlinux](https://wiki.archlinux.org/title/Virt-manager)
- [virt-manager 的安裝與使用](https://sspai.com/post/80638)