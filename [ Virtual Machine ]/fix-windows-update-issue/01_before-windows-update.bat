@echo off

@REM set color
setlocal
call :setESC
cls

:: Windows Update Isn't Working
:: https://www.youtube.com/watch?v=-SgSFn35L7w

:: Easily fix broken Windows files now with System File Checker (sfc)
:: https://www.youtube.com/watch?v=acxCueZ2dVQ

:: Windows Update 20H2 pushed and incompatible Conexant Audio Drivers not yet updated, stuck at 61% repeateadly
:: https://answers.microsoft.com/en-us/windows/forum/all/windows-update-20h2-pushed-and-incompatible/90edceea-d94c-4c7f-8062-8eb65e8d7d33

:: Fix Windows 10 update stuck at 61% (Percent) – Easy ways
:: https://www.howto-connect.com/fix-windows-10-update-stuck-at-61-percent-easy-ways/

echo %CODE%[92m ==== MAKE SURE Use Command-Prompt With ADMINISTRATION ===== %CODE%[0m
pause
echo.

echo %CODE%[92m step1，Uninstall Conexant from App-uninstall-interface %CODE%[0m
pause
echo %CODE%[92m step2，Uninstall all Conexant driver shown in device-manager, including hidden device %CODE%[0m
pause
echo %CODE%[92m step3，!! DO NOT REBOO and DO NOT execute windows-update, !! otherwise, Conexant HD Audio will back again %CODE%[0m
pause
echo.

@REM =============================
echo %CODE%[92m [system] reset bootmenupolicy %CODE%[0m

@REM bcdedit /set {current} bootmenupolicy all         禁用任何啟動項
@REM bcdedit /set {current} bootmenupolicy standard    允许系统加载基本的启动项和服务，但不加載用戶定義的啟動項
bcdedit /set {current} bootmenupolicy standard
echo DONE
echo.

@REM =============================
::echo %CODE%[92m [system] stoping non-microsoft service %CODE%[0m
::setlocal enabledelayedexpansion
:: 定義 microsoft-service 的儲放路徑
::set "microsoftPath=C:\Windows\System32\"
:: 獲取所有service列表
::for /f "tokens=2 delims=: " %%i in ('sc query state^= all ^| findstr /v /c:"SERVICE_NAME"') do (
::    set "serviceName=%%i"
::    :: 若service-path為microsoft-service的儲放路徑，且service-name包含Microsoft時，停止該服務
::    if "!serviceName:%microsoftPath%=!"=="serviceName!" && "!serviceName:Microsoft=Microsoft!" neq "!serviceName!" (
::        echo Stopping Microsoft service:!serviceName!
::        :: 停止服务
::        sc stop "!serviceName!"
::    )
::)
::endlocal

@REM =============================
echo %CODE%[92m [system] stoping services %CODE%[0m
@REM bits = Background Intelligent Transfer Service (BITS)
@REM wuauserv = Windows Update
@REM appidsvc = Application Identity
@REM cryptsvc = Cryptographic Services

:: 調整以下service的啟動策略，避免又自動重新啟動
sc config "bits" start= demand
sc config "wuauserv" start= demand
sc config "appidsvc" start= demand
sc config "cryptsvc" start= demand
sc config "Audiosrv" start= disabled
sc config "AudioEndpointBuilder" start= disabled

:: 停用服務
net stop bits
net stop wuauserv
net stop appidsvc
net stop cryptsvc
net stop Audiosrv
net stop AudioEndpointBuilder

echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] deleting Downloader folder %CODE%[0m
del /F /S /Q "c:\ProgramData\Application Data\Microsoft\Network\Downloader\*.*"
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] deleting SoftwareDistribution folder %CODE%[0m
rmdir /S /Q C:\Windows\SoftwareDistribution
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] deleting catroot2 folder %CODE%[0m
rmdir /S /Q C:\Windows\System32\catroot2
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] execute sc.exe sdset %CODE%[0m
sc.exe sdset bits D:(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;AU)(A;;CCLCSWRPWPDTLOCRRC;;;PU)
sc.exe sdset wuauserv D:(A;;CCLCSWRPWPDTLOCRRC;;;SY)(A;;CCDCLCSWRPWPDTLOCRSDRCWDWO;;;BA)(A;;CCLCSWLOCRRC;;;AU)(A;;CCLCSWRPWPDTLOCRRC;;;PU)
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] regsvr32 dlls %CODE%[0m
regsvr32.exe /s atl.dll
regsvr32.exe /s urlmon.dll
regsvr32.exe /s mshtml.dll
regsvr32.exe /s shdocvw.dll
regsvr32.exe /s browseui.dll
regsvr32.exe /s jscript.dll
regsvr32.exe /s vbscript.dll
regsvr32.exe /s scrrun.dll
regsvr32.exe /s msxml.dll
regsvr32.exe /s msxml3.dll
regsvr32.exe /s msxml6.dll
regsvr32.exe /s actxprxy.dll
regsvr32.exe /s softpub.dll
regsvr32.exe /s wintrust.dll
regsvr32.exe /s dssenh.dll
regsvr32.exe /s rsaenh.dll
regsvr32.exe /s gpkcsp.dll
regsvr32.exe /s sccbase.dll
regsvr32.exe /s slbcsp.dll
regsvr32.exe /s cryptdlg.dll
regsvr32.exe /s oleaut32.dll
regsvr32.exe /s ole32.dll
regsvr32.exe /s shell32.dll
regsvr32.exe /s initpki.dll
regsvr32.exe /s wuapi.dll
regsvr32.exe /s wuaueng.dll
regsvr32.exe /s wuaueng1.dll
regsvr32.exe /s wucltui.dll
regsvr32.exe /s wups.dll
regsvr32.exe /s wups2.dll
regsvr32.exe /s wuweb.dll
regsvr32.exe /s qmgr.dll
regsvr32.exe /s qmgrprxy.dll
regsvr32.exe /s wucltux.dll
regsvr32.exe /s muweb.dll
regsvr32.exe /s wuwebv.dll
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] restore image %CODE%[0m
dism /online /cleanup-image /restorehealth
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] executing sfc %CODE%[0m
sfc /scannow
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] reset network %CODE%[0m
netsh winsock reset
netsh winsock reset proxy
netsh int ip reset
ipconfig /flushdns
echo DONE
echo.

@REM =============================
echo %CODE%[92m [system] re-activate services %CODE%[0m

:: 還原以下service的啟動策略
:: audio services 延後到 windows update 完成後才啟動，避免 windows update 自動安裝 Conexant driver
sc config "bits" start= auto
sc config "wuauserv" start= delayed-auto
sc config "appidsvc" start= auto
sc config "cryptsvc" start= auto

:: 啟動 windows-update 相關的 service
net start bits
net start wuauserv
net start appidsvc
net start cryptsvc
echo DONE
echo.

@REM =============================
echo %CODE%[92m ==== REBOOT NOW, Try windows-update again ===== %CODE%[0m
PAUSE
:quit

:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set CODE=%%b
  exit /B 0
)
exit /B 0

:quit
exit 0