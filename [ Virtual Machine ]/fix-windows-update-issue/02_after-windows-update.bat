@echo off

@REM set color
setlocal
call :setESC
cls

@REM =============================
echo %CODE%[92m ==== MAKE SURE YOU HAVE DONE Windows-Update ===== %CODE%[0m
pause
echo.

@REM =============================
echo %CODE%[92m [system] re-activate audio services %CODE%[0m
net start Audiosrv
net start AudioEndpointBuilder
echo DONE
echo.
:quit

:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do (
  set CODE=%%b
  exit /B 0
)
exit /B 0

:quit
exit 0