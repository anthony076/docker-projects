## wsl 相關

- 常用 wsl.exe 命令
  - 列出已安裝的distro，`$ wsl --list` 或 `$ wsl -l`
  - 列出已安裝的distro，並列出詳細資料，包含運行狀態，$ wsl -l -v
  - 停止正在運行中的wsl-linux-distro，`$ wsl --terminate distro名`
  - 註銷distro，並刪除root-filesystem，`$ wsl wsl --unregister distro名`

## 啟用 wsl/wsl2

- 需求：
  - Windows系統版本至少是Windows 10 2004（建立19041）或更高版本
  - 以管理員身份運行PowerShell，並執行以下命令
  - 啟用 WSL 支援
    - DISM.exe

      Deployment Image Servicing and Management，
      用于服務和管理Windows映像，包括部署、更新和修復

    - 啟用命令，`$ dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart`
      - `/online`，告訴dism命令用於處理正在運行的系統，而不是離線模式下的映像檔
      - `/enable-feature`，要啟用的功能
      - `/all`，啟用指定功能及其依賴項
      - `/norestart`，防止在啟用功能後自動重啟系統

- 啟用 wsl，並安裝 distro
  - 狀況，只啟用但不安裝 distro，`$ wsl --install --no-distribution`
  - 狀況，啟用且安裝 distro，`$ wsl --install -d Ubuntu`
  - 狀況，啟用安裝並指定版本，`$ wsl --install -d Ubuntu-22.04`

- 進入 distro，`$ wsl -d Ubuntu`

- 啟用 wsl2
  - 啟用虛擬平台，`$ dism.exe /online /enable-feature /featurename:VirtualMachinePlatform /all /norestart`
  - 重啟電腦
  - 將wsl設置為wsl2`，$ wsl --set-default-version 2`
  - 將現有的 distro 設置為 wsl2，`$ wsl --set-version <Distro> 2`

## 自定義 wsl-linux-distro

參考，[建構wsl-linux-distro範例](https://gitlab.com/anthony076/linux-usage/-/blob/main/WSL/readme.md)

## ref

- diy wsl-linux-distro
  - [Creating a Custom Linux Distribution for WSL by Microsoft](https://learn.microsoft.com/en-us/windows/wsl/build-custom-distro)
  
  - by Launcher
    - [wsldl documentation](https://wsldl-pg.github.io/docs/)

  - by Import
    - [Import any Linux distribution to use with WSL (docker)](https://learn.microsoft.com/en-us/windows/wsl/use-custom-distro)
    - [Creating a WSL distribution from a Dockerfile](https://source.coveo.com/2019/10/18/wsl-from-dockerfile/)
    - [推薦，Linux Tips - Create WSL Distro from Docker Container (2023)](https://youtu.be/vgCkjPBL6Yk?si=QySWyFwdBVRIcaTL&t=189)

    - NixOS-wsl
      - [github](https://github.com/nix-community/NixOS-wsl)
      - nixos-wsl-starter : 使用 NixOS-wsl 自定義 nixos 環境
        - [github](https://github.com/LGUG2Z/nixos-wsl-starter)
        - [video](https://www.youtube.com/watch?v=UmRXXYxq8k4)