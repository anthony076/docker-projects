## Why Packer

- 自動化建構映像檔，支援多個平台: docker、VirtualBox、VMware、lxc
- 可以將建構的配置參數化
- 支援更多的GUI的配置參數，例如，virtualbox 的 large-pages 無法透過 GUI 配置

## Packer 

- 使用流程
  - 建立配置檔
  - (選用)安裝必要的套件，`$ packer init .`
  - 執行建構流程，`$ packer build 配置檔`

- 配置檔有兩種形式
  - 使用 json 檔
  - 使用 hcl 檔 (HashiCorp-Configuration-Language)，推薦使用
  - 配置檔需要特定的副檔名: `*.pkr.json` 或 `*.pkr.hcl`

- 常用區塊
  - variable區塊: 用於定義模板變數，可詳細定義模板變數
  - variable區塊: 定義多個模板變數，只能定義模板變數的值
  - locals區塊: 用於定義本地變數
  - source 區塊: 必要，定義虛擬機的硬體參數
  - build 區塊: 必要，安裝程式需要執行的命令和配置
  - data 區塊: 用於建構過程中，傳遞各種類型的數據，並使用模板引擎 {{}} 存取

  - 注意，source區塊 和 build區塊 [可以寫在不同的檔案中](#範例-source-和-build-可以寫在不同的檔案中並透過特定參數存取或使用對方的參數)，
    並透過特定參數存取或使用對方的參數

- packer 內建 http-server ，用於傳遞 windows-host 上的配置檔給VM，不需要手動輸入命令

- 命令執行順序: boot_command > provisioner > post-processor
  
  在創建VM之後，第一次進入系統中會執行 boot_command ，透過 boot_command 執行初始化和配置的工作，

  boot_command 執行完畢後，會等待 VM 設置的 SSH 生效，SSH 生效後才會繼續執行 provisioner 和 post-processor 的命令

- builder 建構過程
  - step1，啟動 http-server，並將 http_directory參數值做為server的根目錄
  
  - step2，上電啟動VM，進入安裝程式，依照 boot_wait參數值，等待系統進入就緒狀態
  
  - step3，執行 boot_command參數設置的命令，

    <font color=blue>注意，需要在此步驟設置並啟動SSH，之後provisioner區塊和 post-processor區塊的命令才會有效</font>
    
    builder 在執行完畢 boot_command 中的所有命令後，就會進入等待狀態，
    確認 SSH 有效後才會繼續往下執行，否則就會進入等待狀態，或超時退出
  
  - step4，透過 SSH 重新連接到目標主機，並執行 provisioner區塊和 post-processor區塊的命令
    
## Packer-CLI 的使用
- `packer init`: 用於自動安裝配置檔案中，`packer >  required_plugins` 定義的插件

- `packer plugins`: 
  - 用於手動管理插件
  - [可用插件](https://developer.hashicorp.com/packer/docs/plugins#installing-plugins)  

- `packer build`: 根據配置執行建構，可並行建構多個建構目標
  - 語法1，`$ packer build foo.pkr.hcl`
  - 語法2，`$ packer build 路徑`
  - 語法3，`$ packer build -debug <配置檔或目錄>`，可用於查看環境變數

  - `-parallel-builds=N參數`，平行建構的數量
  - `-var參數`，設置變數，可具有多個 var 參數，常用於沒有固定值的變數，例如，版本號
  - `-var-file參數`，設置變數檔
  - `-on-error參數`，建構過程中，發生錯誤的處理方式，可用值 cleanup | abort | ask | run-cleanup-provisioners

- `packer console`: 
  - 用於檢視配置檔中的實際值
  
  - 語法: `packer console -var "外部變數=值" hcl檔`

  - 範例

    ```shell
    # ./test.pkr.hcl

    # 外部變數，透過 commandline 傳遞進來的參數 (無固定值)
    variable "foo" {
      type = string
    }

    # 內建變數 (有固定值)
    variables {
      name = "ching"
    }

    # 內建路徑或目錄
    locals {
      root = "${path.root}"
      home = "${local.root}/home"
    }
    ```

    在 shell 中輸入，`$ packer console -var "foo=bar" ./test.pkr.hcl`，

    進入 console 後，
    
    - 輸入`var.foo`，查看`外部變數`輸入的值 (bar)
    - 輸入`var.name`，查看`內部變數`的值 (ching)
    - 輸入`local.home`，查看使用`${內建變數}`的值 (./home)


  - [可用模板](https://developer.hashicorp.com/packer/docs/templates/hcl_templates)

- `packer inspect`: 讀取配置模板，以互動方式建立配置， 

- `packer validate json檔名`: 驗證 json 檔是否正確


## [配置] variable(s) | locals 區塊: 用於定義變數

- 類型1， `variable區塊`，用於定義`模板變數`
  - 運行 `packer build` 時，變數值可透過 `packer -var "變數名=變數值"` 傳入或覆寫 或在`配置文件中預先定義`使用，
  - 可外部調用，參考類型3
  - 可以透過 `${var.模板變數名}` 的表達式存取模板變數的值

  - 可用參數
    - `type參數`: 變數值類型
    - `default參數`，預設變數值
    - `sensityive參數`，設置為 true 時，變數值在 packer 的 output 中會被遮蔽，以避免顯示敏感值

  - 例如
    ```shell
    # 定義單一模板變量 | 可自定義類型
    variable "foo" {
      type = string
      default = "bar"
    }

    # 定義多個模板變量 | 自動類型推斷
    variables {
      name = "ching"
      nick_ame = "hua"
    }
    ```

- 類型2，`locals區塊`，用於定義`本地變量`
  - 只能在配置文件中定義和使用，
  - 可以透過 `${local.本地變數名}` 的表達式存取
  - 範例
    ```shell
    locals {
      root = "${path.root}"         # packer 內建變數
      home = "${local.root}/home"   # 使用者自定義的內部變數
    }
    ```

- 類型3，`共享變量`，
  - step1，將`模板變量`寫在 `共享變量.hcl` 的檔案中
  - step2，透過 `file "共享變量.hcl"` 導入
  - step3，透過 `${var.模板變量名}` 存取

- 獲取變數值的多種方式，

  - 方法1，`${變數類型.變數名}`
    ```shell
    ${var.xxx}                # 從模板變數中獲取值
    ${lookup(var.xxx, "key")} # 利用內建函數獲取值
    ${path.module}            # 使用內建函數獲取值
    ${module.xxx.output.yyy}  # 透過 module 獲取值
    ```

  - 方法2，`{{user 變數名}}` 

    user 是 packer 的其中一種內置函數，代表取用使用者自定義的變數，

    注意，需要先透過 `variable` 或 `variables` 定義變數後，
    且有`指定預設的變數值`，或從 `packer build 傳遞變數值`後，
    `{{ }}` 在 `packer build` 命令的期間才會轉換為實際值 

    ```shell
    # ttt.json
    variables {
      "user_name" = "{{user `name`}}",
      "user_msg" = "{{user `msg`}}"
    }
    
    # 設置值 packer build -var "name=123" -var "msg=456" ttt.json
    ```
  
  - 兩者的差別`只有語法`上的不同，`{{ }}` 是模板引擎的插植，`${}` 是 HCL 語言，作用相同

  - 兩者都可以透過 `packer build` 命令的 `-var` 或 `-var-file` 設置變數值

  - 兩者都需要先定義變數，才能獲取變數值

## [配置] source 區塊: 運行虛擬機的硬體參數

- 語法，`source <運行平台類型> <映像檔ID> { 其他參數 }`
  
  - `運行平台類型`: 
    - amazon-ebs
    - amazon-instance
    - docker
    - virtualbox-iso
    - vmware-iso
    - azure
    - googlecompute
    - qemu
    - null: 用於設置 ssh
    - file: 從檔案中建立 artifact，常用於 debug post-processor 不需要等待長時間

  - `映像檔ID`: 通常用來識別使用的iso檔

    例如，若使用的iso檔名稱為 bionic-server-cloudimg-amd64.img，
    
    則 映像檔ID 可以自定義為 `bionic-cloud-x64`

  - 注意，可用參數會根據`運行平台類型`的不同而有差異
  
- <font color=red>注意，根據虛擬機在運行平台的不同，source 區塊會有不同的配置選項</font>

- [通用] 可用參數 (按照字幕順序排列)

  - `boot_command參數`: 使用 iso 檔開機後，在安裝流程中需要`輸入指令`或`鍵盤操作`
    - 可利用 <按鍵名> 模擬安裝過程需要提供的輸入
  
    - [ubuntu] 用於 ubuntu 安裝的 boot_command 值
      - 可透過發送 `<ESC><ESC><ENTER>` 按鍵繞過圖形安裝程序，並進入`引導提示符 >`
      
      - 進入引導提示符後，鍵入配置指令，可參考，[使用 Preseed 文件修改安裝程序行為](https://help.ubuntu.com/community/InstallCDCustomization)，
        或[無人應答安裝](https://zhangguanzhang.github.io/2019/08/06/preseed/)

      - 例如，ubuntu 的 boot_command
        ```json
        {
          "boot_command" : [
              "<esc><esc><enter><wait>" ,   // 繞過圖形安裝的按鍵
              "/install/vmlinuz noapic" ,   // 使用 /install/vmlinuz 內核，並禁用 apic
              "preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }} /preseed.cfg " ,  // preseed 是自動安裝的命令，並指定配置文件的位置和檔名
              "debian-installer=en_US auto locale=en_US kbd-chooser/method=us" ,
              "hostname={{user `hostname`}}" ,
              "fb=false debconf/frontend=noninteractive" , // fb=false 禁用文本，debconf/frontend=noninteractive 指定使用非交互式前端
              "keyboard-configuration/modelcode=SKIP keyboard-configuration/layout=USA " ,
              "keyboard-configuration/variant=USA console-setup/ask_detect=false " ,
              "initrd=/install/initrd.gz -- <enter>"
            ]
        }
        ```

    - [archlinux] 用於 archlinux 安裝的 boot_command 值
      - archlinux 的安裝會直接進入 bash，不需要提供按鍵按鍵來跳過圖形介面安裝
      - 例如，archlinux 的 boot_command
        ```shell
        boot_command = [
          # 下載 bootstrap.sh 後，執行 bootstrap ${local.iso_ssh_user} ${local.iso_ssh_password} 
          "curl http://{{ .HTTPIP }}:{{ .HTTPPort }}/bootstrap.sh | bash -s -- ${local.iso_ssh_user} ${local.iso_ssh_password}<enter>"
        ]
        ```

  - `boot_keygroup_interval參數`: "250ms"
  
  - `boot_wait參數`: 
    - boot_wait 是VM上電後，到使用者可以輸入之前的等待時間，boot_wait會在執行 boot_command 前的等待
    - boot_command 會在 boot_wait 之後執行，例如，"10s" 或 "1m30s"

  - `cpus參數`: 指定 cpu 的數量

  - `datastore參數`: iso 下載到本機後的存放位置

  - `disk_size參數`: 磁碟大小，以MB為單位

  - `firmware參數`: 指定 bootloader 的類型
    - "bios": use BIOS, default
    - "efi": use EFI

  - `gfx_controller參數`: 設置顯示控制器類型
    - "vmsvga": use VMware SVGA
    - "vboxvga": use VirtualBox VGA，default
    - "vboxsvga": use VirtualBox SVGA
    - "none": disable graphics contrller
  
  - `guest_additions_mode參數`: guest_additions 安裝的方式
    - "upload": 將 guest additions ISO 的內容上傳到 guest_additions_path 的路徑，default
    - "attach": 以光碟安裝，會將 guest additions ISO 插入光碟
    - "disable": 不安裝 guest_additions

  - `guest_os_type參數`: 作業系統類型
    - x64 archlinux: "ArchLinux_64"

  - `hard_drive_interface參數`: 主硬碟的控制器類型
    - "ide":  use IDE controller, default
    - "sata": use AHCI SATA controller
    - "scsi": use LsiLogic SCSI controller
    - "pcie": use NVMe controller (需要 extension-pack - enable EFI mode)
    - "virtio": use VirtIO controller

  - `hard_drive_nonrotational參數`: 設置主硬碟類型，HDD 或 SSD
    - true: 將 DISK 視為是 SSD，並且禁止使用硬碟碎片化，應手動設置 hard_drive_discard = true
    - false: 將 DISK 視為是 HDD

  - `hard_drive_discard參數`: 是否支持 TRIM 功能
    - true: 支持 TRIM 功能，適用於 SSD
    - false: 不支持 TRIM 功能

  - `headless參數`: packer 是否啟用 headless 進行安裝，可用值 "true" | "false"

  - `host參數`: iso 存放的主機位置

  - `http_directory參數`: 用於設置存取 http-server 的根目錄

    packer build 執行時，會建立一個 http-server，透過 http-server，可以將用戶主機的資料，傳遞給虛擬機

    http_directory 用於指定 http-server 的跟目錄

  - `iso_checksum參數`: iso 的 hash 值，驗證ISO檔案的正確性

  - `iso_interface參數`: 設置光碟機控制器類型
    - "ide":  use IDE controller, default
    - "sata": use AHCI SATA controller
    - "virtio": use VirtIO controller

  - `iso_url參數`
    - 例如，指定網址，`"iso_url": "http://aa.bb/cc.iso"`
    - 例如，指定本機檔案，`"iso_url": "file://C:\\ISO\\archlinux.iso"`

  - `iso_urls參數`: 指定多個 iso 位置
    ```shell
    iso_urls = [
      "file://C:\\Users\\ching\\Desktop\\archlinux-2022.12.01-x86_64.iso",
      "http://free.nchc.org.tw/arch/iso/2022.12.01/archlinux-2022.12.01-x86_64.iso"
    ]
    ```

  - `memory參數`: 記憶體大小

  - `nested_virt參數`: 是否允許在虛擬機中建立另一個虛擬機

  - `output參數`: 要複製到輸出目錄的檔案或指定目錄
  
  - `password參數`: 登入主機的使用者密碼

  - `ssh_username參數`: ssh 登入帳號
    - 用於安裝系統階段，packer 會透過 ssh 登入後，透過 ssh 輸入安裝相關指令
    - 若使用 root，root 是安裝程式預設的帳號，使用 root 就不需要在安裝階段設置額外的帳號，可以簡化指令，但會有曝露root密碼的風險
    - 基於安全考量，可以設置為 vagrant 的公開帳號，需要在 boot_command 執行 enable_ssh.sh 的指令
    - 在 win 上，`{{.HTTPIP }}`的設置可能會導致[packer-http-server無法正常工作](#常用內建常數)，
      建議先設置為 root，沒問題後再使用自定義的帳號

  - `ssh_password參數`: ssh 登入密碼

  - `ssh_wait_timeout參數`: ssh 超時時間(s)，例如，"30s"
  
  - `ssh_timeout參數`

  - `shutdown_command參數`: 必要，關機命令

  - `user_name參數`: 登入主機的使用者名稱
  
  - `vm_name參數`: 設置VM名稱

--- 

- virtualbox主機專用參數

  - 透過 `$ VBoxManage showvminfo VM名`，可以檢視已建立的參數

  - `vboxmanage參數`: 手動設置 vboxmanage 的參數，用於創建、配置、控制虛擬機器

    - 例如，["modifyvm", "{{ .Name }}", "--firmware", "efi64"]
      - 指定虛擬機器使用的FW為efi64，可加快開機順序
      - 注意，支援的作業系統才可開啟，若在不支援EFI的作業系統中開啟EFI會造成無法開啟的錯誤

    - 例如，["modifyvm", "{{ .Name }}", "--biosbootmenu", "disabled"]，停用開機選單
    - 例如，["modifyvm", "{{ .Name }}", "--boot1", "dvd"]，設置開機裝置順序，可用值 "dvd" | disk | "none"
  
  - `vboxmanage_pos參數`: 手動設置 vboxmanage_post 的參數，在`創建虛擬機器後`，執行額外的設置或檢查
  
  - `[ "modifyvm", "{{.Name}}", "--firmware", "EFI"]`，設置 bootloader 類型
    - 可用值 "BIOS" | "EFI"

  - `["modifyvm", "{{.Name}}", "--ioapic=on"]`，是否開啟單核心的高級中斷控制器 (I/O Advanced Programmable Interrupt Controller)
    - APIC 是 x86的硬件功能，用於取代PIC，使用 APIC 時，OS 可使用超過 16個中斷請求
    - 位於 System > Motherboard > Enable I/O APIC

  - `["modifyvm", "{{.Name}}", "--apic=on"]`，是否開啟多核心的高級中斷控制器 (Advanced Programmable Interrupt Controllers，APICs)
    - 多核心時建議開啟，如果要在一台虛擬機中使用多個虛擬 CPU 時會需要此功能

  - `["modifyvm", "{{.Name}}", "--x2apic=on"]`，是否開啟 CPU 的 x2APIC 功能
    - 必須本機的CPU支援此功能，--x2apic 的設置才會有效
    - 預設為開啟，必須透過 VBoxManage 設置
    - 使OS能夠在高內核數配置上更高效地運行，並優化虛擬化環境中的中斷分配

  -`["modifyvm", "{{.Name}}", "--bios-apic=x2apic"]`，指定 VM firmware 的 APIC 級別
    - 有效值為 apic | x2apic | disable
    - 必須透過 VBoxManage 設置
    - 若指定 x2apic 但主機不支援，會自動降級
    - firmware 的 APIC 級別，一般和主機的APIC的設置一致

  - `["modifyvm", "{{.Name}}", "--acpi=on"]`，是否启用電源進階配置 (Advanced Configuration and Power Interface，ACPI)
    - 主機有支援才會顯示該選項
  
  - `["modifyvm", "{{.Name}}", "--pae=on"]`，是否啟用物理地址擴展，
    - 是否允許32位操作係統使用超過4GB的物理記憶體
    - 注意，如果虛擬機內存設置為 4096MB，開啟 PAE 並不會有顯著的效果
    - 優，可以支持超过 4GB 的物理内存，提高系统性能和稳定性
    - 缺，需要更多的系统资源
    - 缺，開啟這些選項可能導致驅動程式問題
    - 位於 System > Processor > Enable PAE/NX
  
  - `["modifyvm", "{{.Name}}", "--long-mode=on"]`，是否啟用 64bit vm 的 long mode
    - 以 64bit 記憶體執行 64bit 的應用程式，並以相容模式執行 32bit應用
    - 要安裝64bit作業系統時需要開啟後，才能安裝 64bit作業系統

  - `["modifyvm", "{{.Name}}", "--large-pages=on"]`，是否使用更大的記憶體分頁
  
  - `["modifyvm", "{{.Name}}", "--nested-paging=on"]`，虛擬機是否使用主機的分頁記憶體
    - 主機需要啟用 VT-x 或 AMD-V
    - 可以提高虛擬機的效能，但可能會降低宿主主機的效能
    - 位於 System > Acceleration > Enable Nested Paging

  - `["modifyvm", "{{.Name}}", "--hwvirtex=on"]`，是否啟用虛擬化 (啟用 VT-x 或 AMD-V)
    - 位於 System > Processor > Enable Nested VT-x/AMD-V

  - `["modifyvm", "{{.Name}}", "--paravirt-provider=default"]`，選擇提供虛擬技術的接口提供者
    - default: 根據VM選擇合適的虛擬接口，預設值
    - legacy: 使用舊版的 virtualbox 作為虛擬接口
    - minimal: 用於 MAC VM
    - hyper: 用於 window VM
    - kvm: 用於 Linux VM
    - 位於 System > Acceleration > Paravirtualization Interface

  - `["modifyvm", "{{.Name}}", "--nested-hw-virt=on"]`，啓用虛擬機的嵌套硬體虛擬化
    - 是否允許在虛擬機中運行另一個虛擬機
    - 主機需要啟用 VT-x 或 AMD-V
    - 只能透過 VBoxManage 設置

  - `["modifyvm", "{{.Name}}", "--tpm-type=2.0"]`，設置 TPM 類型

  - `["storagectl", "{{.Name}}", "--name", "IDE Controller", "--remove"]`，移除 IDE 控制器

  - `["storagectl", "{{.Name}}", "--name", "SATA Controller", "--hostiocache=on"]`，使用 host IO Cache

## [配置] build 區塊: 安裝程式需要執行的命令和配置
- 基本概念
  - 在 build 區塊中，有 n 個 source ，就會執行 n 次 build 區塊

- 可用參數
  - `name參數`: 自定義的 builder 名稱，建構時會將該名稱顯示於 shell 中

  - `source參數`，建立 build 自己的 source 區塊
    ```shell
    build {
      # 可以`覆寫頂層 source 的設定值`，優先權比頂層的 source 高
      source "x" "y" {
        參數1 = aa
        參數2 = bb
      }
    }
    ```

  - `sources參數`: 用於指定使用的 source 區塊 (用於指定使用的機器配置)，可指定多個
    ```shell
    buld {
      # 使用 source "virtualbox-iso" "arch64" {} 的區塊
      sources = ["sources.virtualbox-iso.arch64"]
    }

## [配置] build > provisioner 區塊: VM 環境建立後，第一次進入系統要執行的命令

- 語法，`provisioner 執行任務的類型 { 其他選項 }`

- 執行任務的類型，可用值有
  
  - `provisioner "file"  { ... }` 用於將檔案或目錄複製到VM中
    ```shell
    # ./scripts -> /tmp/scripts
    provisioner "file" {
      # 指定檔案或目錄來源
      source = "${path.root}/scripts"
      # 指定檔案或目錄目的
      destination = "/tmp/scripts"
    }
    ```

  - `provisioner "shell"  { ... }` 執行 shell-script
    - source參數，用於指定腳本的目錄
    - scripts參數，用於指定多個待執行的腳本
    - inline參數，用於指定腳本的內容
    - execute_command參數，指定執行 inline 腳本的命令，常用於傳遞環境變數給腳本，
      > 注意，沒有 execute_command，inline參數指定的腳本內容也會被執行

    - 範例，scripts 執行多個腳本
      ```shell
      provisioner "shell" {
          #scripts = fileset(".", "scripts/{install,secure}.sh")
          scripts = ["script1.sh", "script2.sh"]
      }
      ```

    - 範例，inline 執行單一腳本，並指定執行腳本的命令和傳遞環境變量
      ```shell
      provisioner "shell" {
        inline = "echo hello world"
        
        # execute_command，指定執行 inline 腳本的命令
        # {{ .Vars }} {{ .Path }} 用於傳遞當前的環境變數和路徑變數給要執行的腳本
        execute_command = "sh -c '{{ .Vars }} {{ .Path }}'" 
      }
      ```

      另一個傳遞環境變量範例
      ```shell
      provisioner "shell" {
        inline = [
          "echo ISO_SCRIPTS=$ISO_SCRIPTS",
          "echo ISO_FILES=$ISO_FILES",
          "bash $ISO_SCRIPTS/initial.sh"
        ]

        # 建立 scripts 和 files 路徑的環境變數，供 initial.sh 使用
        env = {
          "ISO_SCRIPTS" = "/tmp/scripts"
          "ISO_FILES" = "/tmp/files"
        }
      }
      ```

  - `provisioner "windows-shell"  { ... }` 使用 windows-shell 執行腳本

  - `provisioner "ansible"  { ... }`，執行 ansible 腳本，需要在VM中安裝 ansible

  - `provisioner "chef"  { ... }`，執行 chef 腳本，需要在VM中安裝 chef

## [配置] build > post-processor 區塊: 建立環境後，用於將VM打包為映像檔
- 語法，`post-processor 後處理類型 { 其他選項 }`

- 後處理類型，可用的值有
  - docker-import：將映像導入到 Docker 中。
  - docker-push：將映像推送到 Docker 註冊錶中。
  - docker-tag：為映像添加標記。
  - virtualbox-import：將映像導入到 VirtualBox 中。
  - virtualbox-ova: 
  - vmware-vmx：將映像轉換為 VMware vmx 用於使用。
  - null：不執行任何操作。
  - vagrant: 將 vm 轉換為 vagrant box 檔
  - shell: 執行 bash 命令，shell 的可用參數與provisioner相同

- 常用的其他選項
  - `keep_input_artifact參數`：是否保留原始輸入檔案
    > 例如，執行 packer build packer.json 命令時，packer.json 就是原始輸入檔案

  - `output參數`：後處理產生的輸出檔案位置，注意，output 輸出檔案的副檔名，需要與後處理器的類型一致，
    > 例如，vagrant 後處理器輸出的附檔名，必須是 .box

- 範例，
  ```shell
  post-processor "shell" {
    inline = [
      "echo 'Hello, World!'",
      "ls -l",
      "pwd"
    ]
  }
  ```

- 用於 vagrant 的後處理器，[參考](https://developer.hashicorp.com/packer/tutorials/aws-get-started/aws-get-started-post-processors-vagrant)

- 用於 docker 的後處理器，[參考](https://developer.hashicorp.com/packer/tutorials/docker-get-started/docker-get-started-post-processors)

## [配置] data 區塊: 用於建構過程中，傳遞各種類型的數據，並使用模板引擎 {{}} 存取

- 範例
  ```shell
  
  data "template_file" "my_config" {
    template = <<EOF
    key1 = "value1"
    key2 = "value2"
    EOF
  }
  
  # 透過 {{data.template_file.my_config.template}} 存取
  ```

## [配置] packer 區塊: 用於管理 packer 的插件

- 範例
  ```shell
  packer {
    required_plugins {
      amazon = {
        version = ">= 0.0.1"
        source  = "github.com/hashicorp/amazon"
      }
    }
  }
  ```

## 常用內建常數
- {{ .Name }}，虛擬機名稱

- {{ .HTTPIP }}，http-server 的IP

  - 注意，virtualbox 將VM內置的網路設置為 10.0.2.2，造成 HTTPIP 的值錯誤而無法在 windows 存取 http-server

  - 方法1: 透過`virtualbox > File > Tools > NetManager > IPv4 Prefix` 查看，將 {{ .HTTPIP }} 替換

  - 方法2: 透過 `$ vboxmanage list hostonlyifs`，將 {{ .HTTPIP }} 替換

- {{ .HTTPPort }}，provider "http" 中設置的PORT

## [範例] source 和 build 可以寫在不同的檔案中，並透過特定參數存取或使用對方的參數

<font color=blue>定義 source 區塊的檔案 </font>

```shell
# 定義 sources.pkr.hcl
source "amazon-ebs" "example-1" {
  ami_name = "example-1-ami"
}

source "virtualbox-iso" "example-2" {
  
  # 定義 build 區塊的 boot_command 參數
  boot_command = <<EOF
  <esc><esc><enter><wait>
  /install/vmlinuz noapic
  ...
  EOF
}
```

<font color=blue>定義 build 區塊的檔案 </font>

```shell
# folder/build.pkr.hcl

build {
  sources = [     
    "source.amazon-ebs.example-1",
    "source.virtualbox-iso.example-2"   # <--- 存取位於 sources.pkr.hcl 檔案中的 source 區塊
  ]

  provisioner "shell" {
    inline = [
      "echo 'it is alive !'"
    ]
  }
}
```

## [範例] 將 virtualbox-vm 打包為 vagrant-box

- 見 [完整範例](packer-vbox-arch-example/packer-vbox-myarch/readme.md)

- 添加 post-processors 將 vm 打包為 box
  ```shell
  post-processors {  
    post-processor "vagrant" {
      output = "output/myarch.box"
      # 將 Vagrantfile 打包進 box 檔案中
      vagrantfile_template = "files/Vagrantfile"
    }
  }  
  ```

## [範例] 範例集

- packer + virtualbox + archlinux 系列
  - 範例，[packer-vbox-arch-basic](example/packer-vbox-arch-basic/readme.md)
  - 範例，[packer-vbox-arch-optionA](example/packer-vbox-arch-optionA/readme.md)
  - 範例，[packer-vbox-arch-optionB](example/packer-vbox-arch-optionB/readme.md)
  - 範例，[packer-vbox-arch-optionC](example/packer-vbox-arch-optionC/readme.md)
  - 範例，[packer-vbox-arch-myarch](example/packer-vbox-myarch/readme.md)

- 網路範例
  - [shiguredo/packer-templates](https://github.com/shiguredo/packer-templates)
  - [kaorimatz/packer-templates](https://github.com/kaorimatz/packer-templates)
  - [vagrant-arch64-base](https://github.com/michael-slx/vagrant-arch64-base)
  - [vagrant-arch64-template](https://github.com/michael-slx/vagrant-arch64-template)
  - [AlexandreCarlton/packer-archlinux](https://github.com/AlexandreCarlton/packer-archlinux/blob/master/archlinux.json)
  - [archlinux-x86_64](https://github.com/kaorimatz/packer-templates/blob/master/archlinux-x86_64.json)

- 其他範例
  - 範例，[將vagrant-cloud上的box重新包裝成新的 box](example/example-vagrant-cloud-to-box.md)
  - 範例，[建立 vbox 環境，不建立 image 檔](example/example-build-vbox-vm.md)
  - 範例，[Packer Linux template](https://ithelp.ithome.com.tw/articles/10160243)

## Ref

- 官方文檔
  - [官方文檔](https://developer.hashicorp.com/packer/docs/templates/legacy_json_templates/builders)
  - [官方文檔-vagrant 打包](https://developer.hashicorp.com/packer/plugins/builders/vagrant)
  - [HCL配置檔語法](https://developer.hashicorp.com/packer/docs/templates/hcl_templates)
  - [內置函數的使用](https://developer.hashicorp.com/packer/docs/templates/hcl_templates/functions)
  - [建構時期可用的內建變數](https://developer.hashicorp.com/packer/docs/templates/hcl_templates/contextual-variables)
  - [內建的路徑變數](https://developer.hashicorp.com/packer/docs/templates/hcl_templates/path-variables)
  - [virtualbox 專用參數 of packer](https://developer.hashicorp.com/packer/plugins/builders/virtualbox)
  - [vboxmanage的使用](https://www.virtualbox.org/manual/ch08.html)
  - [vmware 專用參數 of packer](https://developer.hashicorp.com/packer/plugins/builders/vmware)
  - [amazon-ebs 專用參數 of packer](https://developer.hashicorp.com/packer/plugins/builders/amazon)

- [從零到可啟動的 VirtualBox Ubuntu](https://kappataumu.com/articles/creating-an-Ubuntu-VM-with-packer.html)
- [系列影片](https://www.youtube.com/playlist?list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj)
