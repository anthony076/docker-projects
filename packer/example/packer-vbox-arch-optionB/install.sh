set -e

sgdisk -o -g /dev/sda
sgdisk -n 1:0:+512M -c 1:"efi" -t 1:ef00 /dev/sda
sgdisk -n 2:0:+8G -c 2:"root" -t 2:8304 /dev/sda
sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda
sync

mkfs.fat -F32 -n "EFI" /dev/sda1
mkfs.ext4 -L "ROOT" /dev/sda2
mkswap -L "SWAP" /dev/sda3
swapon /dev/sda3

mount /dev/sda2 /mnt

mkdir -p /mnt/boot
mount /dev/sda1 /mnt/boot

mkdir -p /mnt/etc

genfstab -U /mnt > /mnt/etc/fstab
sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab

pacman -Sy archlinux-keyring --noconfirm
pacstrap /mnt base linux nano systemd-resolvconf openssh reflector sudo

# nano /mnt/chroot.sh
# arch-chroot /mnt bash chroot.sh
