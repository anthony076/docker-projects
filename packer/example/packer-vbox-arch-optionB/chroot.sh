set -e 

# =====
ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime
hwclock --systohc

# =====
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# =====
echo 'KEYMAP=us' > /etc/vconsole.conf

# =====
echo 'vagrant-pc' > /etc/hostname

# =====
echo '127.0.0.1   localhost' >> /etc/hosts
echo '::1         localhost' >> /etc/hosts

# =====
echo "
[Match]
Name=en*

[Network]
DHCP=ipv4
LinkLocalAddressing=ipv4
IPv6AcceptRA=no

[DHCPv4]
UseDomains=yes
" > /etc/systemd/network/20-default-wired.network

systemctl enable systemd-networkd.service systemd-resolved.service

# =====
ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime

# =====
useradd -m  vagrant

cat <<eof | chpasswd
root:root
vagrant:vagrant
eof

# =====
sed -i '/^#.*CheckSpace/s/^#//' /etc/pacman.conf
sed -i '/^#.*Color/s/^#//' /etc/pacman.conf
sed -i '/^#.*VerbosePkgLists/s/^#//' /etc/pacman.conf
sed -i '/^#NoExtract/s/^#//' /etc/pacman.conf
sed -i '/^NoExtract /s/$/ pacman-mirrorlist/' /etc/pacman.conf

# =====
systemctl mask modprobe@drm
systemctl mask systemd-remount-fs.service

# =====
echo "
--save /etc/pacman.d/mirrorlist
--protocol https
--latest 10
--sort rate
" > /etc/xdg/reflector/reflector.conf

echo "
[Unit]
Description=Run Reflector on first boot
Wants=network-online.target
After=network-online.target nss-lookup.target
ConditionPathIsReadWrite=/etc
ConditionFirstBoot=yes

[Service]
Type=oneshot
RemainAfterExit=yes
ExecStart=/usr/bin/sh -c 'systemctl start reflector.service'

[Install]
WantedBy=multi-user.target
" > /usr/lib/systemd/system/reflector-firstboot.service

systemctl enable reflector.timer reflector-firstboot.service

# =====

echo "
MODULES=(ext4 ahci)
BINARIES=(fsck fsck.ext4 e2fsck)
FILES=()
HOOKS=(base systemd autodetect modconf sd-vconsole)
COMPRESSION='cat'
" > /etc/mkinitcpio.conf

echo "
ALL_config='/etc/mkinitcpio.conf'
ALL_kver='/boot/vmlinuz-linux'
PRESETS=('default')
default_image='/boot/initramfs-linux.img'
" > /etc/mkinitcpio.d/linux.preset

mkinitcpio -P linux

# =====
mkdir -p /home/vagrant/.ssh
echo "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEA6NF8iallvQVp22WDkTkyrtvp9eWW6A8YVr+kz4TjGYe7gHzIw+niNltGEFHzD8+v1I2YJ6oXevct1YeS0o9HZyN1Q9qgCgzUFtdOKLv6IedplqoPkcmF0aYet2PkEDo3MlTBckFXPITAMzF8dJSIFo9D8HfdOV0IAdx4O7PtixWKn5y2hMNG0zQPyUecp4pzC6kivAIhyfHilFR61RGL+GPXQ2MWZWFYbAGjyiYJnAmCP3NOTd0jMZEnDkbUvxhMmBYSdETk1rRgm+R4LOzFUGaHqHDLKLX+FIPKcF96hrucXzcWyLbIbEgE98OHlnVYCzRdK8jlqm8tehUc9c9WhQ== vagrant insecure public key" >> /home/vagrant/.ssh/authorized_keys

chmod 700 /home/vagrant/.ssh
chmod 600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh
systemctl enable sshd

# =====
sed -i '$a ching ALL=(ALL:ALL) NOPASSWD:ALL' /etc/sudoers
chmod 440 /etc/sudoers

# =====
bootctl install

rm -fRv /boot/loader/entries/*

mkdir -p "/etc/pacman.d/hooks"

echo "
default  arch.conf
timeout  0
" > /boot/loader/loader.conf

echo "
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw,relatime add_efi_memmap random.trust_cpu=on rng_core.default_quality=1000 nomodeset nowatchdog mitigations=off quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3
" > /boot/loader/entries/arch.conf

echo "
[Trigger]
Type = Package
Operation = Upgrade
Target = systemd

[Action]
Description = Upgrading systemd-boot ...
When = PostTransaction
Exec = /usr/bin/systemctl restart systemd-boot-update.service
" > /etc/pacman.d/hooks/100-systemd-boot.hook

# =====
yes | pacman -Scc || true

exit
