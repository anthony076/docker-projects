// 由 build.sh 傳遞進來的 iso_mirror 變數
variable "iso_mirror" {
  type = string
}

// 由 build.sh 傳遞進來的 iso_name 變數
variable "iso_name" {
  type = string
}

// 由 build.sh 傳遞進來的 output_file 變數
variable "output_file" {
  type = string
}

// =======================
locals {
  scripts_folder_name = "scripts"
  files_folder_name   = "files"
}

locals {
  # path.root 將 *.pkr.hcl 檔案當前的目錄，設置為根目錄
  root_folder =    "${path.root}"
  # /scripts
  scripts_folder = "${local.root_folder}/${local.scripts_folder_name}"
  # /files
  files_folder =   "${local.root_folder}/${local.files_folder_name}"
}

locals {
  # 位於遠端虛擬主機的dest目錄 = /tmp
  remote_dest_folder =  "/tmp"
  # /tmp/scripts
  scripts_dest_folder = "${local.remote_dest_folder}/${local.scripts_folder_name}"
  # /tmp/files
  files_dest_folder =   "${local.remote_dest_folder}/${local.files_folder_name}"
}

locals {
  // 用於變更安裝系統的ssh用戶
  iso_ssh_user = "root"
  iso_ssh_password = "root"
}

source "virtualbox-iso" "arch64-base" {
  guest_os_type = "ArchLinux_64"
  headless      = true

  cpus           = 2
  memory         = 2048
  firmware       = "efi"
  rtc_time_base  = "UTC"
  nested_virt    = true
  gfx_controller = "none"

  http_directory           = "scripts"
  boot_command             = [
    # 將root使用者的密碼更改為root
    #     step1，透過 curl 取得 bootstrap.sh 的內容後，
    #     step2，利用 bash 執行 並透過 -s 傳遞 bootstrap.sh 需要的參數
    "curl http://{{ .HTTPIP }}:{{ .HTTPPort }}/bootstrap.sh | bash -s -- ${local.iso_ssh_user} ${local.iso_ssh_password}<enter>"
  ]
  boot_keygroup_interval   = "250ms"
  boot_wait                = "2m"

  ssh_username     = "${local.iso_ssh_user}"
  ssh_password     = "${local.iso_ssh_password}"
  ssh_wait_timeout = "30s"
  shutdown_command = "sudo shutdown -P now 'Vagrant is shutting down this VM'"

  disk_size                = 65536
  hard_drive_interface     = "sata"
  hard_drive_nonrotational = true
  hard_drive_discard       = true

  iso_url       = "${var.iso_mirror}/iso/latest/${var.iso_name}"
  iso_checksum  = "file:${var.iso_mirror}/iso/latest/sha256sums.txt"
  iso_interface = "sata"

  guest_additions_mode = "disable"

  vboxmanage = [
    # VM > Setting > System > Motherboard > Enable EFI
    ["modifyvm", "{{ .Name }}", "--firmware", "efi64"],
    # 關閉 bios 的開機選單
    ["modifyvm", "{{ .Name }}", "--biosbootmenu", "disabled"],

    # 設置開機順序
    ["modifyvm", "{{ .Name }}", "--boot1", "dvd"],
    ["modifyvm", "{{ .Name }}", "--boot2", "disk"],
    ["modifyvm", "{{ .Name }}", "--boot3", "none"],
    ["modifyvm", "{{ .Name }}", "--boot4", "none"],
  ]

  vboxmanage_post = [
    # 設置安裝完成後的開機順序
    ["modifyvm", "{{ .Name }}", "--boot1", "disk"],
    ["modifyvm", "{{ .Name }}", "--boot2", "none"],
    ["modifyvm", "{{ .Name }}", "--boot3", "none"],
    ["modifyvm", "{{ .Name }}", "--boot4", "none"],
  ]
}

build {
  sources = ["source.virtualbox-iso.arch64-base"]

  # 複製檔案
  # 將 scripts目錄和 files目錄，複製到遠端虛擬主機的/tmp的路徑中，

  provisioner "file" {
    # 指定檔案來源
    sources     = [
      "${local.scripts_folder}",  # /scripts
      "${local.files_folder}"     # /files
    ]
    
    # 指定遠方主機存放檔案的目錄
    # 即 /tmp/scripts 和 /tmp/files
    destination = "${local.remote_dest_folder}"
  }

  provisioner "shell" {
    inline = [
      # 變更 /tmp/scripts 目錄的權限
      "chmod -R +x ${local.scripts_dest_folder}",
      
      # 執行 install.sh，並傳遞 SCRIPT_DIR 和 FILES_DIR 參數
      #     SCRIPT_DIR = /tmp/scripts
      #     FILES_DIR = /tmp/files
      "${local.scripts_dest_folder}/install.sh ${local.scripts_dest_folder} ${local.files_dest_folder}"
    ]
  }

  post-processor "vagrant" {
    keep_input_artifact = false
    output              = "dist\\${var.output_file}.box"
  }
}