## packer-vbox-arch-optionB

- 參考來源，[michael-slx/vagrant-arch64-base](https://github.com/michael-slx/vagrant-arch64-base)

- archlinux-iso : `archlinux-2022.12.01-x86_64.iso`

- 此範例會建立以下的 virtualbox 環境
  - 磁碟分區

    | Path      | FileSystem | Size  | Usage                        |
    | --------- | ---------- | ----- | ---------------------------- |
    | /dev/sda1 | vFAT       | 512MB | /boot (EFI system partition) |
    | /dev/sda2 | ext4       | 8GB   | / (Root file system)         |
    | /dev/sda3 | (Swap)     | 1.5GB | Swap memory partition        |

  - 安裝期使用的軟體: linux(內核) + base + base-devel

  - 網路:
    - systemd-networkd network manager
    - systemd-resolved DNS resolver
    - systemd-resolvconf compatibility layer
    - DHCP configuration for IPv4 wired networks

  - 開機
    - 使用 GPT 格式的磁碟分區表
    - 使用 systemd-boot 取代 archlinux 內建的 bootloader
    - Automatic (delayed) mounting of EFI system partition
    - Immediate RW mounting of root partition (systemd-remount-fs.service masked)
    - InitRamFs uses systemd and is optimized for fast booting
    - Silent booting using kernel command line options
    - Machine ID generated on first boot

  - 禁用
    - CPU exploit mitigations，禁用防禦CPU漏洞攻擊的功能
      ```shell
      RUB_CMDLINE_LINUX_DEFAULT="quiet splash mitigations=off"
      ```
    - Watchdogs，禁用 watchdog

    - KMS(Kernel Mode Setting)，禁用KMS，KMS允許核心在顯示裝置驅動程序和顯示設備之間直接通信
      - KMS，內核設置模式，允許在內核空間而非用戶空間中設置顯示分辨率和深度的方法
      - 在內核啟動參數中設置nomodeset以禁用KMS
      - 參考，[內核模式設置](https://wiki.archlinux.org/title/Kernel_mode_setting)
    
    - DRM(Direct Rendering Manager)，禁用DRM，DRM允許應用程序和顯示驅動程序之間進行安全的交互的框架

  - 其他
    - 允許透過 kernel command line options 使用 HWRNG 產生隨機數
    - 使用 reflector 管理 pacman-mirror-list，只使用 https-mirror，且會於每周第一次開機時自動執行

  - vagrant
    - 啟用 sshd
    - 添加 vagrant用戶和公鑰

- 若要手動進行指令測試
  - archlinux 預設會啟動 sshd，但沒有設置 root 帳號的密碼

  - step1，在 virtualbox 開啟 port-22 的 port-forwarding
  - step2，進入 shell 後，`$ passwd root` 設置
  - step3，在windows上透過 `$ ssh root@localhost` 進行連接
  - step4，透過`$ nano /mnt/install.sh`，複製 install.sh 的內容
  - step5，執行 install.sh，`$ bash /mnt/install.sh`
  
  - 注意，mount 的操作，會將 /mnt 路徑的內容清空，
    因此，chroot.sh 必須在 install.sh 執行完畢後才建立

  - step6，透過`$ nano /mnt/chroot.sh`，複製 chroot.sh 的內容
  - step7，執行`$ arch-chroot /mnt bash chroot.sh`

  - step8，透過`$ nano /mnt/clean.sh`，複製 clean.sh 的內容
  - step9，執行`$ bash clean.sh`
  - step10，關機後，移除光碟，勾選`vm > settings > system > motherboard > Enable EFI`後，才能正常開機

  - 注意，每次重新啟動VM，都需要重新設置 root 帳號的密碼

- 以下指令的執行流程
  - 概念，
    - step1，進入安裝介面，並設置 root 密碼
    - step2，建立安裝環境 (call `install.sh `in arch64-base.pkr.hcl)
    - step3，透過 chroot 建立實際作業系統環境 (call `chroot.sh` in install.sh)

  - 參考，[packer配置檔](arch64-base.pkr.hcl)

## step1，設置安裝系統的 root 用戶，(不需要透過 arch-chroot 執行)
- 此步驟會在 arch64-base.pkr.hcl 的 boot_command 中執行

- 命令，`$ echo "root:root" | chpasswd`

## step2，建立安裝環境，(不需要透過 arch-chroot 執行)

- 以下所有命令，會在 arch64-base.pkr.hcl 的 provisioner "shell" 中透過調用 install.sh 執行
  -  FILES_DIR 和 SCRIPT_DIR 的變數值，會在 arch64-base.pkr.hcl 中傳遞給 install.sh
  -  $FILES_DIR = /tmp/files
  -  $SCRIPT_DIR = /tmp/scripts

--- 

- 建立磁碟分區表，

  - 磁碟分區規劃
    ```shell
    # EFI分區：這是一個小的分區，用於存儲 EFI-bootloader，使用 FAT32格式，
    # 大小 = 1048577-sector * 512Bytes = 536,871,424 = 512MB
    /dev/sda1 : start=        2048, size=     1048577   # 512M

    # root分區：這是系統的主分區，用於存儲操作係統文件和應用程式。通常使用ext4格式
    # 大小 = 16779265-sector * 512Bytes = 8,590,983,680 bytes = 8GB
    /dev/sda2 : start=     1052672, size=    16779265   # 8G
    
    # SWAP分區：這是一個交換分區，用於交換記憶體。通常使用 linux-swap
    # 大小 = 3135488-sector * 512Bytes = 1,605,369,856 bytes = 1.5GB
    /dev/sda3 : start=    17833984, size=     3135488   # 1.5G
    ```

  - 利用 sgdisk 建立GPT的磁碟分區，推薦
    ```shell
    # 清除 partition-table，並將MBR轉換為GPT
    sgdisk -o -g /dev/sda

    # 格式: sgdisk -n <編號>:<start-sector>:<end-sector> -c <編號>:新名 -t <編號>:<類型> /dev/sda
    # start-sector 和 end-sector 可指定為0，代表第一個可用地址或最後一個可用地址

    # 建立 efi 分區
    sgdisk -n 1:0:+512M -c 1:"efi" -t 1:ef00 /dev/sda
    
    # 建立 root 分區
    sgdisk -n 2:0:+8G -c 2:"root" -t 2:8304 /dev/sda

    # 建立 swap 分區
    sgdisk -n 3:0:0 -c 3:"swap" -t 3:8200 /dev/sda
    ```

- 將磁碟分區進行格式化
  - 命令，sda1 格式化為 FAT32，`$ mkfs.fat -F32 -n "EFI" /dev/sda1`
  - 命令，sda2 格式化為 ext4，`$ mkfs.ext4 -L "ROOT" /dev/sda2`
  - 命令，sda3 格式化為 swap，`$ mkswap -L "SWAP" /dev/sda3`
  - 命令，啟用 swap 分區，`$ swapon /dev/sda3`

- 將各磁碟分區掛載到系統
  - <font color=red> 注意 mount 順序，掛載路徑最短的磁碟分區要先掛載 </font>
    - 掛載順序錯誤，會導致 fstab 中缺少分區
    - 因此，sda2 (/mnt) 必須要比 sda1 (/mnt/boot) 先掛載

  - 掛載 sda2，
    - 命令，`$ mount /dev/sda2 /mnt`

  - 掛載 sda1，注意，sda1 是efi-bootloader 專用的分區，需要掛載到 /mnt/boot 位置
    - 命令，建立路徑，`$ mkdir -p /mnt/boot`
    - 命令，掛載 sda1 分區到 /mnt/boot，`mount /dev/sda1 /mnt/boot`

  - 掛載 sda3，
    - 注意，sda3 是 swap 分區，使用者無法直接存取，由系統自動調用，不需要掛載

- 建立磁碟掛載表(fstab)，
  - fstab 記錄了系統中所有掛載的文件系統的信息

  - 命令，建立目錄，`$ mkdir -p /mnt/etc`
  - 命令，`$ genfstab -p /mnt > /mnt/etc/fstab` 或 `$ genfstab -U /mnt > /mnt/etc/fstab`
    - -p，使用文件系统路径来识别文件系统 (path)，例如，`/dev/sda1  /  ext4  defaults  0  1`
    - -L 或 -U，使用 UUID 来识别文件系统 (label)，例如，`UUID=12345678-1234-1234-1234-1234567890ab / ext4 defaults 0 1`
    - 推薦使用 uuid 取代 path，在更換硬碟或者換了硬碟的分區時，不會因為path不同產生問題

- 修改 fstab: 將 /boot 分區的 rw 標誌替換為 rw,noauto,x-systemd.automount
  - 命令，`$ sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab`
    - rw: /boot 分區可讀可寫
    - noauto: 禁止開機自動掛載 /boot 分區
    - x-systemd.automount: 改由systemd在需要時自動掛載 /boot 分區

---

- 安裝GPL驗證工具，`$ pacman -Sy archlinux-keyring --noconfirm`

- 安裝內核+基本工具+指定套件到真實系統，`$ pacstrap /mnt base linux nano systemd-resolvconf openssh reflector sudo`
  - systemd-resolvconf，用於DNS解析
  - reflector，用於 pacman 尋找最快的鏡像源

- 透過 arch-chroot 執行 [step3](#step3設置-archlinux-作業系統-需要透過-arch-chroot-執行)

---

(脫離 arch-chroot 後，才執行以下命令)

- 設置DNS解析
  - 命令，`$ ln -sf "/run/systemd/resolve/stub-resolv.conf" /mnt/etc/resolv.conf`
    - 將 /mnt/etc/resolv.conf 指向 stub-resolv.conf
    - 使用 systemd-resolved 服務來管理DNS解析，提高DNS解析的效率

- 安裝後清理
  - 命令，清除機器的標識符，`$ rm -f /mnt/etc/machine-id`，避免主機衝突和防追蹤
  - 命令，將變更寫入硬碟，`$ sync`
  - 命令，卸載，`$ umount -R /mnt`，確保係統安裝完成並且運行正常
  - 命令，卸載 swap 分區，`$ swapoff /dev/sda3`，確保係統安裝完成並且運行正常

## step3，設置 archlinux 作業系統 (需要透過 arch-chroot 執行)

- 以下所有命令，會寫在 chroot.sh 檔案中，並在 install.sh 中透過 arch-chroot命令調用 chroot.sh
  -  $FILES_DIR = /setup/files
  -  $SCRIPT_DIR =/setup/scripts

- 注意，以下命令需要透過`arch-chroot命令`執行
  - 方法1，透過 `arch-chroot /mnt` 進入後，再執行以下命令
  - 方法2，將以下命令寫成 chroot.sh，再透過 `arch-chroot /mnt chroot.sh` 執行

- 同步系統時鐘
  - 命令，`$ ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime`
  - 命令，`$ hwclock --systohc`

- 設置語系
  ```shell
  echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen

  locale-gen
  
  echo 'LANG=en_US.UTF-8' > /etc/locale.conf
  ```

- 設置鍵盤，`$ echo 'KEYMAP=us' > /etc/vconsole.conf`

- 設置主機名，`$ echo 'vagrant-pc' > /etc/hostname`

- 設置 /etc/hosts
  - `$ echo '127.0.0.1   localhost' >> /etc/hosts`
  - `$ echo '::1         localhost' >> /etc/hosts`

- 設置網路，
  - systemd-networkd.service 配置檔，`/etc/systemd/network/20-default-wired.network`
    ```shell
    # 配置預設的有線網路連接，包含了有關網路連接的諸如IP位址、子網掩碼、網關等信息
    [Match]
    Name=en*

    [Network]
    DHCP=ipv4
    LinkLocalAddressing=ipv4
    IPv6AcceptRA=no

    [DHCPv4]
    UseDomains=yes
    ```
  - 命令，`$ systemctl enable systemd-networkd.service systemd-resolved.service`
    - systemd-networkd.service: 網路管理工具，它可以管理網路接口、配置網路連接和管理網路流量
    - systemd-resolved.service: DNS解析服務，它可以通過使用更加安全和高效的協議來解析域名

- 設置 timezone，`$ ln -sf /usr/share/zoneinfo/Europe/Zurich /etc/localtime`

- 設置 ZSH
  ```shell
  pacman -Sy zsh

  ZSH_BINARY="$(chsh -l | grep zsh | head -1)"

  chsh -s "$ZSH_BINARY"
  touch /root/.zshrc

  git clone https://github.com/ohmyzsh/ohmyzsh.git /home/vagrant/.oh-my-zsh
  chown -R vagrant:vagrant /home/vagrant/.oh-my-zsh
  cp $FILES_DIR/zshrc /home/vagrant/.zshrc
  ```
  
- 添加 vagrant 用戶
  ```shell
  useradd -m -s "$ZSH_BINARY" vagrant

  cat <<eof | chpasswd
  root:root
  vagrant:vagrant
  eof
  ```

- 設置 pacman
  ```shell
  # 啟用 CheckSpace，檢查空間是否足夠
  sed -i '/^#.*CheckSpace/s/^#//' /etc/pacman.conf
  
  # 啟用 Color，控制输出的颜色
  sed -i '/^#.*Color/s/^#//' /etc/pacman.conf

  # 啟用 VerbosePkgLists，用來控製輸出的詳細信息
  sed -i '/^#.*VerbosePkgLists/s/^#//' /etc/pacman.conf

  # 啟用 NoExtract 欄位
  # NoExtract 欄位用於指定套件，該套件不會被下載到係統並解壓縮，也不會在係統中佔用空間。
  sed -i '/^#NoExtract/s/^#//' /etc/pacman.conf

  # 將 pacman-mirrorlist 添加到 NoExtract 欄位中時
  # pacman-mirrorlist 不會保留在系統中，使用預設的軟體源 (/etc/pacman.d/mirrorlist)
  sed -i '/^NoExtract /s/$/ pacman-mirrorlist/' /etc/pacman.conf
  ```

- 禁用服務
  - 禁用DRM，`$ systemctl mask modprobe@drm`
  - 禁用systemd-remount-fs，`$ systemctl mask systemd-remount-fs.service`
    > 因為在 fstab 中設置了 x-systemd.automount 的掛載選項，因此可以關閉 systemd-remount-fs.service，以加快開機速度

- 透過 reflector 管理鏡像源
  - 檔案，reflector配置檔，`/etc/xdg/reflector/reflector.conf`
    ```shell
    --save /etc/pacman.d/mirrorlist
    --protocol https
    --latest 10
    --sort rate
    ```

  - 檔案，建立 reflector服務，`/usr/lib/systemd/system/reflector-firstboot.service`
    ```shell
    [Unit]
    Description=Run Reflector on first boot
    Wants=network-online.target
    After=network-online.target nss-lookup.target
    ConditionPathIsReadWrite=/etc
    ConditionFirstBoot=yes

    [Service]
    Type=oneshot
    RemainAfterExit=yes
    ExecStart=/usr/bin/sh -c 'systemctl start reflector.service'

    [Install]
    WantedBy=multi-user.target
    ```
  
  - 命令，啟動 reflector服務，`$ systemctl enable reflector.timer reflector-firstboot.service`

- 配置和產生 initramfs 虛擬系統系統
  - mkinitcpio 是一個創建 initramfs 的 bash 腳本
    - /etc/mkinitcpio.conf 是主要配置文件，
    - /etc/mkinitcpio.d/linux.preset 是設置配置文件
    - 當 mkinitcpio.conf 沒有設置時，才會從 linux.preset 中取用設置

  - 檔案，mkinitcpio的主要配置文件，`/etc/mkinitcpio.conf`
    ```shell
    # vim:set ft=sh

    # 虛擬系統系統要包含的核心模塊
    MODULES=(ext4 ahci)
    
    # 虛擬系統系統要包含的工具
    BINARIES=(fsck fsck.ext4 e2fsck)
    
    # 虛擬系統系統要包含的檔案
    FILES=()

    # HOOK 是一個 shell 腳本，在生成 initramfs 時會被執行
    #   用來定義要在 initramfs 中包含哪些驅動、模塊和其他配置。
    #   Hooks 欄位中列出的hook文件會按照順序執行
    HOOKS=(base systemd autodetect modconf sd-vconsole)

    # 用於壓縮 initramfs 的工具
    COMPRESSION="cat"
    ```

  - 檔案，mkinitcpio的預設配置文件，`/etc/mkinitcpio.d/linux.preset`
    ```shell
    # mkinitcpio preset file for the 'linux' package

    ALL_config="/etc/mkinitcpio.conf"
    ALL_kver="/boot/vmlinuz-linux"

    # 選擇配置組合組
    #     先使用default配置組合來生成initramfs，若失敗，改用 fallback 配置組合來生成initramfs

    #     default 配置組合，會使用 base, udev, autodetect, modconf, block, keyboard, keymap, encrypt, filesystems, fsck 等 hook-shell-script

    #     fallback 是備用的配置組合，在係統啓動時如果遇到問題而無法使用預設配置組合，就會使用 fallback 配置組合
    #PRESETS=('default' 'fallback')

    PRESETS=('default')

    default_image="/boot/initramfs-linux.img"
    ```

  - 命令，重新產生 initramfs 檔案，`$ mkinitcpio -P linux`，使用 linux.preset 產生initramfs檔案

- 配置 ssh
  ```shell
  mkdir -p /home/vagrant/.ssh

  cat $FILES_DIR/vagrant.pub >> /home/vagrant/.ssh/authorized_keys

  chmod 700 /home/vagrant/.ssh
  chmod 600 /home/vagrant/.ssh/authorized_keys
  chown -R vagrant:vagrant /home/vagrant/.ssh
  systemctl enable sshd
  ```

- 設置無密碼用戶 (設置 sudoers)
  - 檔案，`/etc/sudoers`，添加 `vagrant ALL=(ALL) NOPASSWD: ALL`

  - 命令，`$ chmod 440 /etc/sudoers`

- 安裝和配置 bootloader
  - 命令，安裝 uefi-bootloader，`$ bootctl install`
    - 此命令利用 systemd-boot 安裝 uefi-bootloader 到 EFI 分區中，
      並創建開機菜單配置文件。它也會創建默認的開機配置文件，以便系統能夠開機
  
  - 命令，刪除 uefi-bootloader 的預設配置，`$ rm -fRv /boot/loader/entries/*`
    - -f: 強制刪除
    - -R: 包含刪除子目錄
    - -v: 顯示訊息

  - 命令，建立/etc/pacman.d/hooks目錄，`$ [[ -d "/etc/pacman.d/hooks" ]] || mkdir -p "/etc/pacman.d/hooks"`

  - 檔案，配置 grub 啟動器，`/boot/loader/loader.conf`
    ```shell
    #　指定預設的選項，使用 arch.conf 進行啟動
    default  arch.conf
    timeout  0
    ```

  - 檔案，grub 的配置文件，`/boot/loader/entries/arch.conf`
    ```shell
    # 開機選項的標題
    title   Arch Linux
    # 內核檔案的位置
    linux   /vmlinuz-linux
    # initramfs 檔案的位置
    initrd  /initramfs-linux.img
    # 內核啟動選項
    #   root=LABEL=ROOT 指定根分區
    #   resume=LABEL=SWAP 指定交換分區
    #   rootflags=rw,relatime 設定根分區為讀寫並設定訪問時間
    #   add_efi_memmap 啓用 EFI 記憶體映射
    #   random.trust_cpu=on 使內核信任 CPU 內置的隨機數生成器
    #   rng_core.default_quality=1000 設定隨機數生成器的預設質量
    #   nomodeset 禁用內核模式設定
    #   nowatchdog 禁用看門狗定時器
    #   mitigations=off 禁用安全增強措施
    #   quiet 抑製大多數日誌消息
    #   loglevel=3 設定日誌級別為3
    #   rd.systemd.show_status=auto 在啓動過程中顯示systemd狀態
    #   rd.udev.log_priority=3 設定 udev 的日誌優先級為3
    options root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw,relatime add_efi_memmap random.trust_cpu=on rng_core.default_quality=1000 nomodeset nowatchdog mitigations=off quiet loglevel=3 rd.systemd.show_status=auto rd.udev.log_priority=3
    ```

  - 檔案，建立自動更新的服務，`/etc/pacman.d/hooks/100-systemd-boot.hook`
    ```shell
    # 當你安裝或升級內核時，100-systemd-boot.hook 會自動運行，
    # 用於更新 systemd-boot 配置文件中的內核信息
    [Trigger]
    Type = Package
    Operation = Upgrade
    Target = systemd

    [Action]
    Description = Upgrading systemd-boot ...
    When = PostTransaction
    Exec = /usr/bin/systemctl restart systemd-boot-update.service
    ```

- 清理套件
  ```shell  
  # 取得未使用套件的列表
  #   -Q  查詢已安裝的軟體包
  #   -d  查詢已安裝的軟體包中的依賴關係
  #   -t  查詢已安裝的軟體包中的測試軟體包
  #   -q  以簡潔的形式顯示軟體包名稱
  UNUSED_PKGS=$(pacman -Qdtq || true)

  # 移除未使用的套件
  if [[ ! -z "$UNUSED_PKGS" ]]; then
    pacman -Rs --noconfirm $UNUSED_PKGS || true
  fi

  # 清理 pacman 的快取
  yes | pacman -Scc || true
  ```

- 離開 arch-chroot 環境，`$ exit`
