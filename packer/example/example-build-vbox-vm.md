
[來源](https://github.com/shiguredo/packer-templates/tree/develop/ubuntu-14.04)

```json
{
  // 建立 virtualbox 的 vm 環境
  "builders": [
    {
      "type": "virtualbox-iso",   // 完成建立後，會執行 provisioners > virtualbox-iso > scripts
      "boot_command": [
        "<esc><wait>",
        "<esc><wait>",
        "<enter><wait>",
        "/install/vmlinuz<wait>",
        " auto<wait>",
        " console-setup/ask_detect=false<wait>",
        " console-setup/layoutcode=us<wait>",
        " console-setup/modelcode=pc105<wait>",
        " debian-installer=en_US<wait>",
        " fb=false<wait>",
        " initrd=/install/initrd.gz<wait>",
        " kbd-chooser/method=us<wait>",
        " keyboard-configuration/layout=USA<wait>",
        " keyboard-configuration/variant=USA<wait>",
        " locale=en_US<wait>",
        " netcfg/get_hostname=ubuntu-1404<wait>",
        " netcfg/get_domain=vagrantup.com<wait>",
        " noapic<wait>",
        " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/preseed.cfg<wait>",
        " -- <wait>",
        "<enter><wait>"
      ],

      "boot_wait": "10s",
      "disk_size": 40960,
      "guest_os_type": "Ubuntu_64",
      "http_directory": "http",
      "iso_checksum": "2ac1f3e0de626e54d05065d6f549fa3a",
      "iso_checksum_type": "md5",
      "iso_url": "http://releases.ubuntu.com/14.04/ubuntu-14.04.4-server-amd64.iso",
      "ssh_username": "vagrant",
      "ssh_password": "vagrant",
      "ssh_port": 22,
      "ssh_wait_timeout": "10000s",
      "shutdown_command": "echo 'shutdown -P now' > /tmp/shutdown.sh; echo 'vagrant'|sudo -S sh '/tmp/shutdown.sh'",
      "vboxmanage": [
        [ "modifyvm", "{{.Name}}", "--memory", "512" ],
        [ "modifyvm", "{{.Name}}", "--cpus", "1" ]
      ]
    },
  ],

  // 建立環境後，自動執行的 scripts
  "provisioners": [
    {
      "type": "shell",
      "execute_command": "echo 'vagrant'|sudo -S sh '{{.Path}}'",
      "override": {
        "virtualbox-iso": {
          "scripts": [
            "scripts/base.sh",        // 基本環境設置: apt update、設置用戶、設置免密碼、變更 ssh 設置
            "scripts/vagrant.sh",     // 添加 vagrant 憑證
            "scripts/virtualbox.sh",  // 安裝 virtualbox 依賴
            "scripts/cleanup.sh",     // 清理不必要的檔案
            "scripts/zerodisk.sh"     // 縮小硬碟空間
          ]
        },
      }
    }
  ],
  
  // 建立映像檔後，自動執行的 scripts
  "post-processors": [
    {
      "type": "vagrant",
      "override": {
        "virtualbox": {
          "output": "ubuntu-14-04-x64-virtualbox.box"
        },
      }
    }
  ],

}
```