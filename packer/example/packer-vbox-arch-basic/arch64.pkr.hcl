
source "virtualbox-iso" "arch64" {
  # packer 自動啟動的 server 入口目錄，用來傳遞檔案給VM
  http_directory = "scripts"
  ssh_username = "root"
  ssh_password = "root"
  
  # ISO 下載地址
  iso_urls = [
    "file://C:\\Users\\ching\\Desktop\\archlinux-2022.12.01-x86_64.iso",
    "http://free.nchc.org.tw/arch/iso/2022.12.01/archlinux-2022.12.01-x86_64.iso"
  ]

  iso_checksum = "sha256:de301b9f18973e5902b47bb00380732af38d8ca70084b573ae7cf36a818eb84c"

  # VM 靜態設置 for virtualbox
  
  guest_os_type = "ArchLinux_64"  
  disk_size = 10000

  # 必要，注意，每台電腦需要的硬體配置不同，以下的參數值就不一樣，不能共用硬體參數
  # 需要手動安裝ISO，測試成功啟動下的硬體配置
  vboxmanage = [        
    ["modifyvm", "{{.Name}}", "--ioapic=on"],
    ["modifyvm", "{{.Name}}", "--cpus=2"],
    ["modifyvm", "{{.Name}}", "--memory=4096"],
    
    #["modifyvm", "{{.Name}}", "--apic=on"],
    #["modifyvm", "{{.Name}}", "--acpi=on"],
    #["modifyvm", "{{.Name}}", "--x2apic=on"],
    #["modifyvm", "{{.Name}}", "--bios-apic=x2apic"],
    #["modifyvm", "{{.Name}}", "--hwvirtex=on"],
    #["modifyvm", "{{.Name}}", "--nested-hw-virt=on"],
    #["modifyvm", "{{.Name}}", "--paravirt-provider=default"],
    #["modifyvm", "{{.Name}}", "--pae=on"],
    #["modifyvm", "{{.Name}}", "--long-mode=on"],
    #["modifyvm", "{{.Name}}", "--nested-paging=on"],
    #["modifyvm", "{{.Name}}", "--large-pages=on"],
    #["modifyvm", "{{.Name}}", "--tpm-type=2.0"],

    #["storagectl", "{{.Name}}", "--name", "IDE Controller", "--remove"],
    #["storagectl", "{{.Name}}", "--name", "SATA Controller", "--hostiocache=on"],

  ]

  headless = "false"
  
  # VM上電後，到系統穩定且可以輸入命令之前的等待時間，
  # /archlinux-2022.12.01-x86_64.iso 啟動 > 進入選單 > 預設無動作15秒後進入安裝系統 > 硬體測試約 30s > 進入 shell 等待使用者輸入，總耗時約1m20秒
  
  boot_wait = "1m30s"  

  boot_command = [
    # 確保在 boo_command 執行後，SSH 是有效的
    "<enter>",

    # ==== 設置 ssh ====
    # 方法1: 不使用 root 登入ssh，需要手動配置 vagrant 帳號，從 http-server 下載 ssh 的設置檔
    #   原語句 "/usr/bin/curl -O http://{{ .HTTPIP }}:{{ .HTTPPort }}/enable-ssh.sh<enter><wait10>"
    #   在 windows 上會抓到 10.0.0.2 的值，造成 HTTPIP 錯誤
    #   若 .HTTPIP 為未取正確的IP，檢查 virtualbox > File > Tools > NetManager > IPv4 Prefix 的值

    #"/usr/bin/curl -O http://192.168.56.1:{{ .HTTPPort }}/enable-ssh.sh<enter><wait10>",
    #"/usr/bin/bash ./enable-ssh.sh<enter>"

    # 方法2: 使用 root 登入ssh，只需要配置 root 的密碼
    "echo root:root | chpasswd<enter><wait5>",
  ]

  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
}

build {
  name = "arch-builder"
  sources = ["sources.virtualbox-iso.arch64"]

  provisioner "shell" {
    inline = [
      "cat /etc/ssh/sshd_config"
    ]
  }
}