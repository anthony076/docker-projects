## packer-vbox-arch
- 測試環境
  - windows 11 
  - packer v1.8.5
  - virtualbox Version 7.0.4 r154605 (Qt5.15.2)
  - iso: archlinux-2022.12.01-x86_64.iso

## 注意事項

- 2022.12.01 版的 archlinux-iso，預設進入安裝系統後會自動啟動 sshd

  透過 `$ systemctl status sshd` 可檢查其他版本的 archlinux-iso是否啟動 sshd

  /etc/ssh/sshd_config 的預設值
  - port 22
  - 允許使用 root 登入

  若需要在安裝系統中啟用 ssh，需要建立 root 密碼後，才能正常登入
  `$ echo root:root | chpasswd`

  <font color=blue>在本範例中啟用 ssh 的流程</font>
  
  - 在 boot_command 執行階段，先設置 root 密碼，讓 packer 可以連線至 sshd
  - 在 provisioner 執行階段才添加新用戶，並重新配置 ssh

- 在 virtualbox 中，packer 的內建變數 {{.HTTPIP}} 的值是錯誤的
  
  在執行 packer build 後，packer 會自動啟動 http-server，用於傳遞檔案給VM，

  VM 會在安裝系統中，透過 `$ curl {{.HTTPIP}}:{{.HTTPPORT}}` 向 windows-host 發出請求並取得指定檔案，

  但實際上 packer 會將 {{.HTTPIP}} 設置為 10.0.2.2 是虛擬機內網的IP，而不是實際 http-server 的IP，
  若不修改會造成VM無法獲取 windows-host 中的配置檔的錯誤

  實際的 {{.HTTPIP}} 可透過 `$ vboxmanage list hostonlyifs` 取得