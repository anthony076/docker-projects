#!/usr/bin/env bash

#PASSWORD=$(/usr/bin/openssl passwd -crypt 'vagrant')
PASSWORD=$(/usr/bin/openssl passwd 'vagrant')

# Vagrant-specific configuration

# 建立使用者
/usr/bin/useradd --password ${PASSWORD} --comment 'Vagrant User' --create-home --user-group vagrant

## 設置 ssh
echo -e 'vagrant\nvagrant' | /usr/bin/passwd vagrant
echo 'Defaults env_keep += "SSH_AUTH_SOCK"' > /etc/sudoers.d/10_vagrant
echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/10_vagrant
/usr/bin/chmod 0440 /etc/sudoers.d/10_vagrant
/usr/bin/systemctl start sshd.service