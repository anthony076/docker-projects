# sync system-time
timedatectl set-ntp 1

# set time-zone
timedatectl set-timezone Asia/Taipei

# partition disk
sgdisk -n 1:2048:1050623 -c 1:"efi" -t 1:ef00 /dev/sda
sgdisk -n 2:1050624:4196351 -c 2:"swap" -t 2:8200 /dev/sda
sgdisk -n 3:4196352:0 -c 3:"root" -t 3:8304 /dev/sda

# format disk
mkfs.vfat -F32 -n EFI /dev/sda1
mkswap -L SWAP /dev/sda2
mkfs.ext4 -L ROOT /dev/sda3

# mount disk
mount /dev/sda3 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
swapon /dev/sda2

# create filesystem-table
genfstab -U /mnt > /mnt/etc/fstab
sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab

# install system tools
echo "
Server = https://mirror.archlinux.tw/ArchLinux/\$repo/os/\$arch
Server = https://archlinux.cs.nycu.edu.tw/\$repo/os/\$arch
Server = https://free.nchc.org.tw/arch/\$repo/os/\$arch
" > /etc/pacman.d/mirrorlist

# multi-thead download
pacman -Sy axel --noconfirm
sed -i 's/#XferCommand = \/usr\/bin\/curl -L -C - -f -o %o %u/XferCommand = \/usr\/bin\/axel -n 5 -a -o %o %u/g' /etc/pacman.conf

# GPL keyring
pacman -Sy archlinux-keyring --noconfirm

# install system tools
pacstrap /mnt base linux
