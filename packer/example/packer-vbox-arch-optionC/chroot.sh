set -e

# 使用 archlinux-2022.12.01-x86_64.iso ，即使不安裝 efibootmgr 也能正常起動

pacman -S openssh sudo virtualbox-guest-utils-nox efibootmgr --noconfirm
systemctl enable vboxservice.service

# timezone
ln -sf /usr/share/zoneinfoAsia/Taipei /etc/localtime

# sync system-time
hwclock --systohc
systemctl enable systemd-timesyncd.service

# set locale
echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo 'LANG=en_US.UTF-8' > /etc/locale.conf

# set keyboard
echo 'KEYMAP=us' > /etc/vconsole.conf

# set hostname
echo 'vagrant' > /etc/hostname

# set localhost DNS
echo '127.0.0.1   localhost' >> /etc/hosts
echo '::1         localhost' >> /etc/hosts

# set wired-network
echo "
[Match]
Name=en*

[Network]
DHCP=ipv4
LinkLocalAddressing=ipv4
IPv6AcceptRA=no

[DHCPv4]
UseDomains=yes
" > /etc/systemd/network/20-default-wired.network

# 啟用網路和解析器
systemctl enable systemd-networkd.service systemd-resolved.service

# add user
useradd -m -s /bin/bash vagrant

cat <<eof | chpasswd
root:vagrant
vagrant:vagrant
eof

# set ssh
mkdir -p /home/vagrant/.ssh
chmod 700 /home/vagrant/.ssh
chown -R vagrant:vagrant /home/vagrant/.ssh

curl https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant.pub > /home/vagrant/.ssh/authorized_keys
chmod 600 /home/vagrant/.ssh/authorized_keys

systemctl enable sshd

# add user
echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/vagrant
chmod 440 /etc/sudoers

# set pacman
sed -i '/^#.*CheckSpace/s/^#//' /etc/pacman.conf
sed -i '/^#.*Color/s/^#//' /etc/pacman.conf
sed -i '/^#.*VerbosePkgLists/s/^#//' /etc/pacman.conf

sed -i '/^#NoExtract/s/^#//' /etc/pacman.conf
sed -i '/^NoExtract /s/$/ pacman-mirrorlist/' /etc/pacman.conf

# disable service
systemctl mask systemd-remount-fs.service systemd-fsck-root.service

# generate initramrf、BOOTX64.EFI、
[[ -d /boot/EFI/BOOT ]] || mkdir -p /boot/EFI/BOOT
[[ -d /etc/kernel ]] || mkdir -p /etc/kernel
echo 'root=LABEL=ROOT resume=LABEL=SWAP rootflags=rw fsck.mode=skip nomodeset quiet' > /etc/kernel/cmdline

echo "
MODULES=(ext4 ahci)
BINARIES=()
FILES=()
HOOKS=(base systemd autodetect modconf sd-vconsole)
COMPRESSION='cat'
" > /etc/mkinitcpio.conf

echo "
ALL_config='/etc/mkinitcpio.conf'
ALL_kver='/boot/vmlinuz-linux'
PRESETS=('default')
default_image='/boot/initramfs-linux.img'
default_efi_image='/boot/EFI/BOOT/BOOTX64.EFI'
default_options='--cmdline /etc/kernel/cmdline'
" > /etc/mkinitcpio.d/linux.preset

mkinitcpio -P linux

# set bootloader
[[ -f "/boot/EFI/BOOT/BOOTX64.EFI" ]] || exit 5

# 非必要，使用 archlinux-2022.12.01-x86_64.iso ，即使不安裝 efibootmgr 也能正常起動
#efibootmgr --create --disk /dev/sda --part 1 --loader '/EFI/BOOT/BOOTX64.EFI' --label "Arch Linux"

UNUSED_PKGS=$(pacman -Qdtq || true)

if [[ ! -z "$UNUSED_PKGS" ]]; then
  pacman -Rs --noconfirm $UNUSED_PKGS || true
fi

yes | pacman -Scc || true