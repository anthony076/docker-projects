## packer-vbox-arch-optionC

- 參考來源，[michael-slx/vagrant-arch64-template](https://github.com/michael-slx/vagrant-arch64-template)

- archlinux-iso : `archlinux-2022.12.01-x86_64.iso`

- 此範例會建立以下的 virtualbox 環境
  - 磁碟分區

    | Path      | FileSystem | Size  | Usage                        |
    | --------- | ---------- | ----- | ---------------------------- |
    | /dev/sda1 | vFAT       | 512MB | /boot (EFI system partition) |
    | /dev/sda2 | (Swap)     | 1.5GB | Swap memory partition        |
    | /dev/sda3 | ext4       | 8GB   | / (Root file system)         |

  - 安裝的軟體
    - initial-install期: linux(內核) + base
    - chroot-install期: openssh、sudo、virtualbox-guest-utils-nox、efibootmgr

  - 網路:
    - systemd-networkd network manager
    - systemd-resolved for DNS resolver

  - 開機
    - 使用 GPT 格式的磁碟分區表
    - Automatic (delayed) mounting of EFI system partition
    - Optimized boot process
      - Minimal Initrd
      - No bios bootloader, use BOOTX64.EFI instead for fast boot
  
  - 禁用
    - KMS(Kernel Mode Setting)，禁用KMS，KMS允許核心在顯示裝置驅動程序和顯示設備之間直接通信
      - KMS，內核設置模式，允許在內核空間而非用戶空間中設置顯示分辨率和深度的方法
      - 在內核啟動參數中設置nomodeset以禁用KMS
      - 參考，[內核模式設置](https://wiki.archlinux.org/title/Kernel_mode_setting)

    - 禁用開機自動掛載，改由 systemd 掛載
    - systemd-remount-fs.service
    - systemd-fsck-root.service
  
  - vagrant
    - 啟用 sshd
    - 添加 vagrant用戶和公鑰

- 若要手動進行指令測試
  - archlinux 預設會啟動 sshd，但沒有設置 root 帳號的密碼

  - step1，在 virtualbox 開啟 port-22 的 port-forwarding
  - step2，進入 shell 後，`$ passwd root` 設置
  - step3，在windows上透過 `$ ssh root@localhost` 進行連接
  - step4，透過`$ nano /mnt/install.sh`，複製 initial.sh 的內容
  - step5，執行 install.sh，`$ bash /mnt/initial.sh`
  
  - 注意，mount 的操作，會將 /mnt 路徑的內容清空，
    因此，chroot.sh 必須在 install.sh 執行完畢後才建立

  - step6，透過`$ nano /mnt/chroot.sh`，複製 chroot.sh 的內容
  - step7，執行`$ arch-chroot /mnt bash chroot.sh`

  - step8，透過`$ nano /mnt/clean.sh`，複製 clean.sh 的內容
  - step9，執行`$ bash clean.sh`
  
  - step10，關機後，移除光碟，勾選`vm > settings > system > motherboard > Enable EFI`後，才能正常開機

  - 注意，每次重新啟動VM，都需要重新設置 root 帳號的密碼