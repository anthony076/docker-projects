- [from](https://github.com/daimatz/arch64-packer/blob/master/template.json)

- 其他範例
  - 推薦，[A minimalistic Arch Linux-based development Vagrant box and template](https://github.com/michael-slx/vagrant-arch64-template)
  - 推薦，[A minimalistic, fast-booting Arch Linux box for Vagrant.](https://github.com/michael-slx/vagrant-arch64-base)
  - [pisarenko-net/arch-bootstrapper](https://github.com/pisarenko-net/arch-bootstrapper/blob/main/vagrant/packer/arch-base.json)
  - [ChrisOrlando/packer-archlinux-minimal](https://github.com/ChrisOrlando/packer-archlinux-minimal/blob/master/archlinux.json)

  
```shell
{
  "builders": [
    {
      "type": "virtualbox-iso",
      "guest_os_type": "ArchLinux_64",
      "iso_url": "http://ftp.jaist.ac.jp/pub/Linux/ArchLinux/iso/2014.06.01/archlinux-2014.06.01-dual.iso",
      "iso_checksum": "61bfd735e62336a800d27a94e3e55d5bbe99d6bf",
      "iso_checksum_type": "sha1",
      "disk_size": 40960,
      "ssh_username": "vagrant",
      "ssh_password": "vagrant",
      "http_directory": "http",
      "boot_command": [
        "<enter><wait10><wait10>",
        "curl -O http://{{ .HTTPIP }}:{{ .HTTPPort }}/setup.sh<enter><wait>",
        "sh setup.sh http://{{ .HTTPIP }}:{{ .HTTPPort }}<enter>"
      ],
      "vboxmanage": [
        ["modifyvm", "{{.Name}}", "--memory", "1024"],
        ["modifyvm", "{{.Name}}", "--cpus", "2"]
      ],
      "shutdown_command": "sudo shutdown -h now"
    }
  ],

  "provisioners": [
    {
      "type": "shell",
      "scripts": ["scripts/vagrant.sh"]
    }
  ],
  "post-processors": ["vagrant"]
}
```