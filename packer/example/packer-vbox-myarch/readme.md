## packer-vbox-myarch

- 硬體環境
  - cpu = 2
  - memory = 4096
  - disk = 10G
  - efi boot
  - graphic-controller = vmsvga
  - feature: 
    - 中斷控制器: 啟用 ioapic、apic、x2apic，多個中斷控制器
    - 記憶體: 啟用 pae、long-mode、large-pages、nested-paging，更多的記憶體資源
    - 虛擬化: 啟用 hwvirtex、paravirt-provider，使用虛擬化加速
    - 禁用
      - virtualbox: nested-hw-virt，禁用嵌套虛擬機
      - kernel:
        - fsck.mode=skip，跳過檔案系統檢查
        - nomodeset，在加載X之前不加載視頻驅動程序，而改用BIOS模式
        - quiet，開機不顯示檢測結果
      - service
        - systemd-remount-fs.service，關閉磁碟分區的自動掛載，加快開機用
        - systemd-fsck-root.service，關閉 root 分區的檔案系統檢查，加快開機用

- 系統環境
  - 使用 EFI 和 SWAP 磁碟分區
  - fstab 關閉 EFI 分區的自動掛載
    ```shell
    sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab
    ```
  - 啟用 pacman 的多線程下載 (by axel)
  - 設置有線網路
  - 共享目錄: `~/share`
  - 自定義 initramfs 
    - MODULES: ext4 ahci
    - HOOKS: base systemd autodetect modconf sd-vconsole

- 注意事項
  - 若需要啟用桌面系統，並啟用3D加速，需要安裝和啟用 virtualbox-guest-utils，
    virtualbox-guest-utils 有提供高階的繪圖功能和效果
 
- 執行流程
  - flow
    ```mermaid
    flowchart LR
    b1[build.bat] --> b2[build.pkr.hcl] --> b3[initial.sh]
    b3 --> b4[chroot.sh\nuser-root] 
    b4 --> b5[aur.sh\nuser-vagrant]
    b5 --> b6[other script]
    ```

  - `build.pkr.hcl`
    - 執行 boot_command 前的 boot_wait 設置為 1m30s
      > 注意，若主機的資源不足，開機初始化的時間可能會超過 1m30s，適時調整合適的值

    - provisioner "file" 會將所有的 shell-script 上傳到臨時的檔案系統中，
      透過 ISO_SCRIPTS 和 ISO_FILES 環境變數，將目錄上傳的路徑傳遞給 initial.sh

    - 透過 vagrantfile_template 將 Vagrantfile 打包進 box 檔案中

  - `initial.sh`
    - 注意，mount 指令會將 /mnt 路徑清空並掛載新內容，在執行 mount 指令前，不要將檔案複製到 /mount 路徑下

    - 注意，arch-chroot `不能直接直接執行命令`，任何命令都必須`寫在 shell-script` 中執行  
      - 若以`root身份` 進入arch-root，`只能透過 bash 執行 shell-script`，
        例如，`$ arch-chroot /mnt bash test.sh`
      
      - 若以`一般使用者身份` 進入arch-root，可以`直接執行 shell-script`，不需要透過 bash，
        例如，`$ arch-chroot su vagranttest.sh`

      - 差異
        
        arch-chroot 的目的是切換到安裝目錄並在安裝目錄中執行命令，
        而`不是直接執行命令`，因此需要提供 shell 環境執行腳本
        
        若進入一般使用者後，預設都會開啟 shell 環境 (執行useradd命令時添加了 shell)，
        因此不需要提供 shell 環境執行腳本

    - LINE:52，MNT_SCRIPTS 和 MNT_FILES 指定了 shell-script 在實際檔案系統的路徑
    - LINE:63，透過 cp 將 shell-script 從臨時檔案系統轉移到實際檔案系統
    - LINE:67，執行 arch-chroot 指令時，透過 env 提供 MNT_SCRIPTS 和 MNT_FILES 環境變數，
      供 chroot.sh 使用
    - LINE:73，安裝 paru (aur-helper) `必須使用一般使用者進行安裝`

  - `chroot.sh`
    - 部分 systemd 的命令無法使用
    - paru 不能在 root 環境下執行

  - `aur.sh`
    - 限制: 進入 arch-chroot，並切換到一般使用者後
      - 不能使用相對路徑，透過 $HOME 存取 /home/使用者，例如
      - 下載需要指定絕對路徑，例如，git clone $HOME/tmp
      - cd $HOME 取得 cd ~
      - rm -rf $HOME/tmp

## Todos

- tranlate shell commands to function
- enable parallel-download in pacman.conf
- add fcitx5 and dbus-x11 for chewing-input
- combine the start sequence of startx and rdp into xinitrc
- release swapfile version
- add more tool
  - zellij
  - gitui
  - ripgrep
  - dust
  - autojump
  - proceps
  - exa
  - bat
  - duf
  - fish-relative-plugin
  - procps
  - ranger
  - mlocate
  - neovim
  - fzf
  - desktop
    - replace awesome with bspwm
    - rofi
    - feh
    - picom
    - fcitx5
    - gedit
    
## v0.4 (rdp-version)

- [packer hcl file](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-myarch)

- base: v0.3

- 安裝套件
    - archlinux x64 
    - archlinux-keyring
    - pacstrap: base、linux
    - pcaman: 
        - openssh、sudo、efibootmgr、virtualbox-guest-utils
        - git rust base-devel fakeroot pkgconf
        - nano、man-db、man-pages、which、fish、fzf、bash-completion
        - xorg-xauth
    - aur: paru
    - rdp:
      - xf86-video-vmware
      - xorg-server、xorg-xinit
      - ttf-mononoki-nerd、xterm、awesome

- 移除 pacman 的 quiet download
- 移除 reflector
- 加入縮小磁碟空間策略
- 使用[官方建議](https://developer.hashicorp.com/vagrant/docs/boxes/base)設置帳號
- 共享目錄: `~/share`

## v0.3 (x11-version)

- [packer hcl file](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-myarch)

- base: v0.2
- 安裝套件
    - archlinux x64 
    - archlinux-keyring
    - pacstrap: base、linux
    - pcaman: 
        - openssh、sudo、virtualbox-guest-utils-nox、efibootmgr、reflector
        - git
        - nano、man-db、man-pages、which、fish、bash-completion
        - xorg-xauth
    - aur: paru

- 啟用 ssh-x11-farwading，
  - SSH 連線前，需要在主機上啟動 x11-server，例如，vcxsrv 
  - 使用 `$ ssh -Y vagrant@localhost -p 2222` 進行連接
  - 或透過 vagrant 進行連接，`$ vagrant ssh`

- 使用[官方建議](https://developer.hashicorp.com/vagrant/docs/boxes/base)設置帳號

- 共享目錄: `~/share`

## v0.2 (dailywork-version)

- [packer hcl file](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-myarch)

- base: v0.1
- 安裝套件
    - archlinux x64 
    - archlinux-keyring
    - pacstrap: base、linux
    - pcaman: 
        - openssh、sudo、virtualbox-guest-utils-nox、efibootmgr、reflector
        - git
        - nano、man-db、man-pages、which、fish、bash-completion
    - aur: paru

- 僅提供 TTY 介面，未安裝 Desktop
- 使用[官方建議](https://developer.hashicorp.com/vagrant/docs/boxes/base)設置帳號
- 共享目錄: `~/share`

## v0.1 (base-version)

- [packer hcl file](https://gitlab.com/anthony076/docker-projects/-/tree/main/packer/example/packer-vbox-myarch)

- 安裝套件
    - archlinux x64 
    - archlinux-keyring
    - pacstrap: 
        - base
        - linux
    - pcaman: 
        - openssh
        - sudo
        - virtualbox-guest-utils-nox
        - efibootmgr

- 僅提供 TTY 介面，未安裝 Desktop
- 使用[官方建議](https://developer.hashicorp.com/vagrant/docs/boxes/base)設置帳號
- 共享目錄: `~/share`

## Ref

- [setup the account following the official suggestions](https://developer.hashicorp.com/vagrant/docs/boxes/base)
