
source "virtualbox-iso" "arch64" {
  
  # ==== 設置iso ====

  iso_urls = [
    "file://C:\\Users\\ching\\Desktop\\archlinux-2022.12.01-x86_64.iso",
    "http://free.nchc.org.tw/arch/iso/2022.12.01/archlinux-2022.12.01-x86_64.iso"
  ]

  iso_checksum = "sha256:de301b9f18973e5902b47bb00380732af38d8ca70084b573ae7cf36a818eb84c"

  # ==== packer 相關設置 ====

  # 設置 packer-http-server 的根目錄，用於 boot-command 時下載檔案
  http_directory = "scripts"

  # 設置 packer 透過 ssh 登入的帳號與密碼
  ssh_username = "root"
  ssh_password = "root"
  
  # 關閉無頭模式
  headless = "false"

  # 執行 boot_command 前的等待
  boot_wait = "1m30s"  

  # ==== 硬體設置 ====
  
  guest_os_type = "ArchLinux_64"  
  cpus = 2
  memory = 4096
  nested_virt = false # 是否允許在虛擬機中建立另一個虛擬機
  disk_size = 10000   # 10000MB=10GB
  
  # 設置 bootloader
  firmware = "efi"

  # 設置光碟機控制器類型
  iso_interface = "sata"

  # 主硬碟的控制器類型，
  hard_drive_interface = "sata"

  # 設置主硬碟類型為SSD
  hard_drive_nonrotational = true

  # 是否支持 TRIM 功能
  hard_drive_discard = true

  # 設置顯示控制器類型
  gfx_controller = "vmsvga"

  # guest_additions 安裝的方式
  guest_additions_mode = "disable"
  
  # 進階硬體設置
  vboxmanage = [        
    [ "modifyvm", "{{.Name}}", "--firmware", "EFI" ],
    
    # 設置中斷控制器
    ["modifyvm", "{{.Name}}", "--ioapic=on"], # 啟用單核中斷控制器
    ["modifyvm", "{{.Name}}", "--apic=on"],    # 啟用多核中斷控制器
    ["modifyvm", "{{.Name}}", "--x2apic=on"], # 啟用進階中斷控制器

    # 設置記憶體
    ["modifyvm", "{{.Name}}", "--pae=on"],  # 啟用物理地址擴展
    ["modifyvm", "{{.Name}}", "--long-mode=on"], # 啟用 64bit 模式
    ["modifyvm", "{{.Name}}", "--large-pages=on"], # 使用更大的分頁空間
    ["modifyvm", "{{.Name}}", "--nested-paging=on"], # 共享主機記憶體分頁
    
    # 設置虛擬化
    ["modifyvm", "{{.Name}}", "--hwvirtex=on"], # 啟用虛擬化
    ["modifyvm", "{{.Name}}", "--nested-hw-virt=off"],  # 禁用嵌套虛擬機
    ["modifyvm", "{{.Name}}", "--paravirt-provider=default"], # 自動選擇虛擬化接口
  ]

  boot_command = [
    "<enter>",
    "echo root:root | chpasswd<enter><wait5>",
  ]

  shutdown_command = "echo 'packer' | sudo -S shutdown -P now"
}


build {
  name = "arch-builder"

  sources = ["sources.virtualbox-iso.arch64"]

  # 將目錄複製到 archlinux-iso 建立的(臨時的)檔案系統中
  # 注意，文件上傳的位置，必須是ssh_username可存取的位置

  # ./scripts -> /tmp/scripts
  provisioner "file" {
    source = "${path.root}/scripts"
    destination = "/tmp/scripts"
  }

  # ./files -> /tmp/files
  provisioner "file" {
    source = "${path.root}/files"
    destination = "/tmp/files"
  }

  provisioner "shell" {
    inline = [
      "echo ISO_SCRIPTS=$ISO_SCRIPTS",
      "echo ISO_FILES=$ISO_FILES",
      "bash $ISO_SCRIPTS/initial.sh"
    ]

    # 建立 scripts 和 files 路徑的環境變數，供 initial.sh 使用
    env = {
      "ISO_SCRIPTS" = "/tmp/scripts"
      "ISO_FILES" = "/tmp/files"
    }
  }
  
  post-processors {  
    post-processor "vagrant" {
      output = "output/myarch.box"
      # 將 Vagrantfile 打包進 box 檔案中
      vagrantfile_template = "files/Vagrantfile"
    }
  }  

}