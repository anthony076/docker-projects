set -e 

# 限制: aur.sh 只能透過一般使用者安裝，且 aur.sh 不能執行 pacman 的安裝或反安裝的命令
# 限制: 進入 arch-chroot，並切換到一般使用者後
#   - 不能使用相對路徑，透過 $HOME 存取 /home/使用者，例如
#   - 下載需要指定絕對路徑，例如，git clone $HOME/tmp
#   - cd $HOME 取得 cd ~
#   - rm -rf $HOME/tmp

echo ===== AUR START =====

# 必須指令下載路徑，且必須使用 $HOME 代表 /home/vagrant
git clone https://aur.archlinux.org/paru.git $HOME/paru

cd $HOME/paru
makepkg -fsri --noconfirm

cd $HOME

rm -rf $HOME/paru $HOME/.cargo
yes | paru -Scc || true

echo ===== AUR END =====

