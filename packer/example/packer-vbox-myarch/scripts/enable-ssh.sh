
# 建立 vagrant 用戶
useradd -m -s /bin/bash -G wheel vagrant
# 變更密碼
echo "vagrant:vagrant" | chpasswd
# 使用戶在sudo之後繼續連接到遠程主機而不需要重新驗證
echo 'Defaults env_keep += "SSH_AUTH_SOCK"' > /etc/sudoers.d/vagrant
# 設置 NOPASS
echo "vagrant ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/vagrant
# 變更權限
chmod 440 /etc/sudoers
# 重新啟動 sshd
systemctl restart sshd.service
