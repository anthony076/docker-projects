set -e

echo ===== RDP START =====

# 安裝
#paru -S --noconfirm --needed xrdp xorgxrdp
su vagrant -c "paru -S --noconfirm --needed xrdp xorgxrdp"

# 配置 xorg.conf，啟用 3D + 載入顯示器驅動
echo '
Section "Module"
Load "glx"
EndSection

Section "Device"
  Identifier  "VMware Graphics"
  Driver      "vmware"
EndSection
' > /etc/X11/xorg.conf

# 關閉不必要的啟動項
sed -i 's/twm \&/#twm \&/1' /etc/X11/xinit/xinitrc
sed -i 's/xclock -geometry 50x50-1+1 \&/#xclock -geometry 50x50-1+1 \&/1' /etc/X11/xinit/xinitrc
sed -i 's/xterm -geometry 80x50+494+51 \&/#xterm -geometry 80x50+494+51 \&/1' /etc/X11/xinit/xinitrc
sed -i 's/xterm -geometry 80x20+494-0 \&/#xterm -geometry 80x20+494-0 \&/1' /etc/X11/xinit/xinitrc
sed -i 's/exec xterm -geometry 80x66+0+0 -name login/#exec xterm -geometry 80x66+0+0 -name login/1' /etc/X11/xinit/xinitrc

# 設置以 awesome 啟動
echo "awesome" > /etc/xrdp/startwm.sh

# 使一般用戶能夠登入
echo "allowed_users = anybody" > /etc/X11/Xwrapper.config

# 啟用服務
systemctl enable xrdp

echo ===== RDP DONE =====