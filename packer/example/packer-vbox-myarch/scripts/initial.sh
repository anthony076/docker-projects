set -e

echo ===== INITIAL START =====

# sync system-time
timedatectl set-ntp 1

# set time-zone
timedatectl set-timezone Asia/Taipei

# partition disk
sgdisk -n 1:2048:1050623 -c 1:"efi" -t 1:ef00 /dev/sda
sgdisk -n 2:1050624:4196351 -c 2:"swap" -t 2:8200 /dev/sda
sgdisk -n 3:4196352:0 -c 3:"root" -t 3:8304 /dev/sda

# format disk
mkfs.vfat -F32 -n EFI /dev/sda1
mkswap -L SWAP /dev/sda2
mkfs.ext4 -L ROOT /dev/sda3

# mount disk
mount /dev/sda3 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
swapon /dev/sda2

# create filesystem-table
mkdir -p /mnt/etc
genfstab -U /mnt > /mnt/etc/fstab
sed -i -E '/\/boot/ s/(rw,\S*)/\1,noauto,x-systemd.automount/' /mnt/etc/fstab

# install system tools
echo "
Server = https://mirror.archlinux.tw/ArchLinux/\$repo/os/\$arch
Server = https://archlinux.cs.nycu.edu.tw/\$repo/os/\$arch
Server = https://free.nchc.org.tw/arch/\$repo/os/\$arch
" > /etc/pacman.d/mirrorlist

# multi-thead download
pacman -Sy axel --noconfirm
sed -i 's/#XferCommand = \/usr\/bin\/curl -L -C - -f -o %o %u/XferCommand = \/usr\/bin\/axel -n 5 -a -o %o %u/g' /etc/pacman.conf

# GPL keyring
pacman -Sy archlinux-keyring --noconfirm

# install system tools
pacstrap /mnt base linux

# ===== install chroot.sh、aur.sh、rdp.sh

# 指定 scripts 和 files 目錄在真實系統中的路徑 (不包含/mnt的前綴)
MNT_SCRIPTS="/setup/scripts"
MNT_FILES="/setup/files"

# 將檔案從 archlinux-iso 建立的(臨時的)檔案系統，複製到掛載的(真實的)檔案系統中
#   ISO_SCRIPTS 和 ISO_FILES 環境變數由 build.pkr.hcl 建立
#   $ISO_SCRIPTS = /tmp/scripts
#   $ISO_FILES = /tmp/files

mkdir -p /mnt$MNT_SCRIPTS
mkdir -p /mnt$MNT_FILES

cp -R "$ISO_SCRIPTS"/* "/mnt$MNT_SCRIPTS"   # /tmp/scripts -> /mnt/setup/scripts
cp -R "$ISO_FILES"/* "/mnt$MNT_FILES"       # /tmp/files -> /mnt/setup/files

# 執行 chroot.sh ，並傳遞環境變數
arch-chroot /mnt env MNT_SCRIPTS=$MNT_SCRIPTS MNT_FILES=$MNT_FILES bash $MNT_SCRIPTS/chroot.sh

# 執行 aur.sh，安裝 paru
#   - arch-chroot 切換到一般使用者才能安裝 aur
#   - 限制: arch-chroot 切換到一般使用者時，不能直接執行命令，但可以執行 shell-script
#   - 錯誤用法: arch-chroot /mnt su vagrant bash $MNT_SCRIPTS/aur.sh
#   - 正確用法: arch-chroot /mnt su vagrant $MNT_SCRIPTS/aur.sh
arch-chroot /mnt su vagrant $MNT_SCRIPTS/aur.sh

# 執行 rdp.sh，安裝 rdp 環境
arch-chroot /mnt env MNT_SCRIPTS=$MNT_SCRIPTS MNT_FILES=$MNT_FILES bash $MNT_SCRIPTS/rdp.sh

# 執行 clean.sh，執行清理
arch-chroot /mnt env MNT_SCRIPTS=$MNT_SCRIPTS MNT_FILES=$MNT_FILES bash $MNT_SCRIPTS/clean.sh

# ==== clear and unmount ====
rm -rf $MNT_SCRIPTS $MNT_FILES
sync

umount -Rv /mnt
swapoff /dev/sda2

echo ===== INITIAL DONE =====