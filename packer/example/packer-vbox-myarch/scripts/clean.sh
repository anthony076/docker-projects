set -e 

echo ===== CLEAN START =====

# remove unused packages
UNUSED_PKGS=$(pacman -Qdtq || true)
if [[ ! -z "$UNUSED_PKGS" ]]; then
  pacman -Rs --noconfirm $UNUSED_PKGS || true
fi

# clean pacman
yes | pacman -Scc || true
rm -f /var/log/pacman.log

# clean record
rm -f /mnt/etc/machine-id
rm -f ~/.bash_history

# zero out
COUNT=$(df -m | grep /dev/sda3 | awk '{printf $4}')
dd if=/dev/zero of=/EMPTY bs=1M count=$COUNT
rm -f /EMPTY

echo ===== CLEAN DONE =====