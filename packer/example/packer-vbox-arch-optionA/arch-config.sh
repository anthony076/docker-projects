ROOT_PASS=
VAGRANT_PASS=

# 設置主機名
echo 'myarch-pc' > /etc/hostname

# 設置時區
/usr/bin/ln -s /usr/share/zoneinfo/UTC /etc/localtime

# 設置鍵盤布局
echo 'KEYMAP=us' > /etc/vconsole.conf

# 設置系統使用的語系
/usr/bin/sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
/usr/bin/locale-gen

# 建立開機用的虛擬檔案系統(initramfs)
/usr/bin/mkinitcpio -p linux

# 建立 root 帳號
echo -e "$ROOT_PASS\n$ROOT_PASS" | passwd root

# 禁用可預測網路接口名，改用傳統接口名
/usr/bin/ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules

# 啟用 dhcp-service
/usr/bin/systemctl enable dhcpcd@eth0.service

# 配置 sshd
/usr/bin/sed -i 's/#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config
/usr/bin/systemctl enable sshd.service

# 安裝並啟用 rngd
/usr/bin/pacman -S --noconfirm rng-tools
/usr/bin/systemctl enable rngd

# 建立 vagrant 的 ssh 用戶
/usr/bin/useradd --comment 'Vagrant User' --create-home --user-group vagrant

echo -e "$VAGRANT_PASS\n$VAGRANT_PASS" | passwd vagrant

# 建立 sudoer
echo 'Defaults env_keep += "SSH_AUTH_SOCK"' > /etc/sudoers.d/10_vagrant
echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/10_vagrant
/usr/bin/chmod 0440 /etc/sudoers.d/10_vagrant

# 建立公鑰
/usr/bin/install --directory --owner=vagrant --group=vagrant --mode=0700 /home/vagrant/.ssh
/usr/bin/curl --output /home/vagrant/.ssh/authorized_keys --location https://raw.github.com/mitchellh vagrant/master/keys/vagrant.pub
/usr/bin/chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys
/usr/bin/chmod 0600 /home/vagrant/.ssh/authorized_keys

# 移除 gptfdisk 套件
/usr/bin/pacman -Rcns --noconfirm gptfdisk
