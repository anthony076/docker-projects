## packer-vbox-arch-optionA

- 參考來源，[elasticdog/packer-arch](https://github.com/elasticdog/packer-arch)

- archlinux-iso : `archlinux-2022.12.01-x86_64.iso`

- 此範例會建立以下的 virtualbox 環境
  - 64-bit
  - 20 GB disk
  - 512 MB memory
  - 單一ext4格式的 root分區，沒有 efi-分區，沒有 swap-分區
  - 使用 GPT 格式的磁碟分區表
  - 使用 syslinux 取代 archlinux 內建的 bootloader
  - 作業系統安裝內核 + base + base-devel
  - 啟用 sshd
  - 添加 vagrant用戶和公鑰

- 若要手動進行指令測試
  - archlinux 預設會啟動 sshd，但沒有設置 root 帳號的密碼
  - step1，在 virtualbox 開啟 port-22 的 port-forwarding
  - step2，進入 shell 後，`$ passwd root` 設置
  - step3，在windows上透過 `$ ssh root@localhost` 進行連接

  - 注意，每次重新啟動VM，都需要重新設置 root 帳號的密碼

- 以下指令的執行流程
  - 參考，[packer配置檔](arch-template.json)
  - [step1] enable-ssh
  - [step2-7] install-base.sh
  - [step8] install-virtualbox.sh
  - [step9] cleanup.sh
  - pack to box

## step1，建立 vagrant 的 ssh 用戶

```shell
# 為 vagrant 建立密碼
PASSWORD=$(/usr/bin/openssl passwd 'vagrant')

# 建立 vagrant 使用者
#   --password ${PASSWORD}，設置密碼
#   --create-home 為 vagrant 用戶建立 home 目錄
#   --user-group vagrant 建立同名的群組
/usr/bin/useradd --password ${PASSWORD} --create-home --user-group vagrant

# 變更vagrant用戶的密碼
echo -e 'vagrant\nvagrant' | /usr/bin/passwd vagrant

# 建立 sudoers 的配置檔，並添加 SSH_AUTH_SOCK 變數
#   建立 SSH_AUTH_SOCK 環境變量
#   當用戶使用 SSH 連接到遠程主機時，SSH 客戶端會創建一個套接字，並將其路徑添加到 SSH_AUTH_SOCK 環境變量中
#   此設置可以使用戶在sudo之後繼續連接到遠程主機而不需要重新驗證
echo 'Defaults env_keep += "SSH_AUTH_SOCK"' > /etc/sudoers.d/10_vagrant

# 將 vagrant 用戶添加到 sudoers，並設置免密碼
echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/10_vagrant

# 變更 10_vagrant 的檔案權限
#     0: 不設置特殊權限
#     4: 檔案擁有者可讀/不可寫/不可執行
#     4: 同群組可讀/不可寫/不可執行
#     0: 不同群組不可讀/不可寫/不可執行
/usr/bin/chmod 0440 /etc/sudoers.d/10_vagrant

# 重新啟動 ssh 服務
/usr/bin/systemctl start sshd.service
```

## step2，磁碟配置

- 磁碟分割需要搭配開機磁區(MBR)使用，參考，[linux的開機流程](https://gitlab.com/anthony076/linux-usage/-/blob/main/linux-core/01_bootup-flow.md)

```shell
# ==== 磁碟清理 ====

# 利用 sgdisk命令，清空硬碟上的所有分區，抹除硬碟上的所有分區資訊
#     sgdisk命令，用於管理 GPT 分區表的工具，可以用來創建、修改和刪除 GPT 分區
/usr/bin/sgdisk --zap /dev/sda

# 清空硬碟上的所有數據 (清除 /dev/sda 上的 magic-string 和 )
#     if：輸入文件，/dev/zero是特殊文件，每次讀取其內容時都會得到 0 值
#     of：輸出文件。此處為 /dev/sda，這是一個硬碟設備文件
#     bs：設置讀取塊的大小
#     count：讀取塊的數量
#     從 /dev/zero 讀取2048塊，每塊大小為512 bytes的數據，
#     並將其寫入 /dev/sda 這個硬碟設備中。這樣就會清空硬碟上的所有數據
/usr/bin/dd if=/dev/zero of=/dev/sda bs=512 count=2048

# 清除磁盤分區 magic-strings 和 signatures
#     wipefs命令，用於清除文件係統類型的磁盤分區標記的命令。
#     --all，清除所有支援的文件係統類型的磁盤分區標記
#     /dev/sda，要清除標記的磁盤設備
/usr/bin/wipefs --all /dev/sda

# ==== 建立GPT啟動分區 ====

# 建立 GPT 格式的 root 磁碟分區
#     - --new，代表在 /dev/sda 上創建一個新的 GPT 分區
#     1:0:0 代表磁區編號為1，起始扇區位置和終止位置均為0，代表不分割，保持一整塊磁碟分區
#     - 範例，建立一個從預設起始地址開始的10G的磁碟分區
#     sgdisk -n 2:0:+10G /dev/sdb
/usr/bin/sgdisk --new=1:0:0 /dev/sda

# 設置GPT分區的屬性值，設置GPT分區標記為EFI系統使用的分區
#     1:set:2，代表分區1寫入屬性值2
#     在 sgdisk v1.0.9.1 中，
#     0=清除屬性，1=傳統BIOS可啟動屬性，2=EFI系統屬性，3=傳統BIOS可啟動屬性+EFI系統屬性
/usr/bin/sgdisk /dev/sda --attributes=1:set:2

# 將 root 磁碟分區格式化為ext4
#     -F: 強制建立文件系統，不管目標分區是否已經被使用。
#     -m 0: 在新建文件系統不保留任何閒置塊，刪除時立刻刪除，不將檔案標記為閒置塊
#     -L root : 指定檔案系統的標籤
/usr/bin/mkfs.ext4 -F -m 0 -q -L root /dev/sda1

# 掛載 root 磁碟分區
#     -o noatime: 不更新inode的訪問時間，可減少磁盤訪問次數，提高磁盤使用壽命和性能
#     errors=remount-ro : 在遇到錯誤時，將文件重新掛載為只讀模式。
/usr/bin/mount -o noatime,errors=remount-ro /dev/sda1 /mnt

# 利用 df -h 查看掛載的分區
```

## step3，設置pacman 軟體源

```shell

# 若 COUNTRY 變數不存在，設置 COUNTRY=US
COUNTRY=${COUNTRY:-TW}

# 設置官方MIRRORLIST的網址
# 官方查詢網址，https://archlinux.org/mirrorlist/

MIRRORLIST="https://archlinux.org/mirrorlist/?country=${COUNTRY}&protocol=http&protocol=https&ip_version=4&use_mirror_status=on"

# 獲取官方MIRRORLIST的網址，並寫入/etc/pacman.d/mirrorlist
# -s，silent 模式
curl -s "$MIRRORLIST" |  sed 's/^#Server/Server/' > /etc/pacman.d/mirrorlist

# 檢視下載結果
# cat /etc/pacman.d/mirrorlist
```

## step4，安裝內核和安裝系統的相關軟體

```shell

# ==== 安裝GPL驗證工具 ====

# 安裝內核和第三方軟體前，需要先安裝 archlinux-keyring，
# 以確保您的系統可以驗證下載的套件的GPL簽名
# 參考，https://bbs.archlinux.org/viewtopic.php?id=282191
pacman -Sy archlinux-keyring --noconfirm

# ==== 安裝系統和指定軟體 ====

# 透過 pacstrap: 初始化根文件系统 + 安裝系統基礎庫、工具、內核
#   base，包含了係統所需的基本工具和庫文件，必要
#   base-devel，開發者所需的工具和庫文件，選用
#   linux，Arch Linux 所使用的內核，必要
/usr/bin/pacstrap /mnt base base-devel linux

# 切換到真實的 root 分區，並透過 pacman 安裝第三方的軟體
#   gptfdisk，管理 GPT 分區錶的工具
#   syslinux，輕量級的bootloader，用啓動 Linux 內核
#   dhcpcd，DHCP 客戶端
#   netctl，管理網路連接的工具
/usr/bin/arch-chroot /mnt pacman -S --noconfirm gptfdisk openssh syslinux dhcpcd netctl

# ==== 安裝和配置 syslinux (bootloader) ====

# 使用 syslinux 前，要確保磁碟上沒有 bootloader，否則會和系統內建的 bootloader 衝突

# 安裝 syslinux
#   -i 安裝 Syslinux
#   -a Set Boot flag on boot partiton
#   -m 安裝 Syslinux MBR
/usr/bin/arch-chroot /mnt syslinux-install_update -i -a -m

# 將 syslinux.cfg 配置檔中的 sda3 替換為 sda1
#     ROOT_PARTITION = /dev/sda1
#     ${ROOT_PARTITION##/dev/}，去除ROOT_PARTITION變數中的/dev/，得到 sda1
/usr/bin/sed -i "s|sda3|sda1|" "/mnt/boot/syslinux/syslinux.cfg"

# 將 TIMEOUT 設置為 10s
/usr/bin/sed -i 's/TIMEOUT 50/TIMEOUT 10/' "/mnt/boot/syslinux/syslinux.cfg"

# 產生磁碟分區掛載表(file-system-table)，或稱文件系統表(fstab)
/usr/bin/genfstab -p /mnt >> "/mnt/etc/fstab"

# 查看 fstab 的內容
cat /mnt/etc/fstab
```

## step5，設置 archlinux 作業系統

- 注意，以下命令需要透過 /usr/bin/arch-chroot 命令執行
  - step1，將以下命令寫入 `/mnt/arch-config.sh` 的檔案中
  - step2，透過 `$ /usr/bin/arch-chroot /mnt bash arch-config.sh` 執行下列的命令

- 設置主機名，`$ echo 'vagrant-pc' > /etc/hostname`

- 設置時區，`$ /usr/bin/ln -s /usr/share/zoneinfo/UTC /etc/localtime`

- 設置鍵盤布局，`$ echo 'KEYMAP=us' > /etc/vconsole.conf`
  - /etc/vconsole.conf 是一個配置文件，用於存儲終端的設定
  - 設定終端使用美式鍵盤佈局

- 設置系統使用的語系，
  - step1，設置語系，`$ /usr/bin/sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen`
  - step2，產生指定語系相關的檔案，`$ /usr/bin/locale-gen`

- 建立開機用的虛擬檔案系統(initramfs)，`$ /usr/bin/mkinitcpio -p linux`
  - 需要在安裝內核之後執行
  - -p，代表 preset，指定使用 linux 的配置檔產生 initramfs 

- 設置 root 密碼，`$ echo -e "root\n密碼" | passwd root`

- 配置網路
  - 配置1，`禁用可預測網路接口名`，改用傳統接口名(ex: eth0)
    - 可預測網路接口名，用於有多個網路接口可用的主機，參考，[可預測的網絡接口名稱](https://systemd.io/PREDICTABLE_INTERFACE_NAMES/)

    - `$ /usr/bin/ln -s /dev/null /etc/udev/rules.d/80-net-setup-link.rules`
  
  - 配置2，`啟用 dhcp-service`，
    - `$ /usr/bin/systemctl enable dhcpcd@eth0.service`
    - 用於啟動 dhcp-client 的服務
    - dhcp-client 用於獲取 IP 位址、子網路遮罩、網關和 DNS 伺服器等網路設定，建議啟用
    - archlinux 預設不會啟用 dhcp-service，需要手動啟動

  - 配置3，配置 sshd
    - 禁用DNS，`$ /usr/bin/sed -i 's/#UseDNS yes/UseDNS no/' /etc/ssh/sshd_config`
    - 重啟ssdh，`$ /usr/bin/systemctl enable sshd.service`

  - 配置4，安裝並啟用 rngd
    - 用於解決重開機後sshd連接緩慢的問題，參考，
      - [task-58355](https://bugs.archlinux.org/task/58355)
      - [記一次tomcat在linux上麵啓動超級慢](https://blog.51cto.com/u_15338614/3690002)
  
    - step1，`$ /usr/bin/pacman -S --noconfirm rng-tools`
    - step1，`$ /usr/bin/systemctl enable rngd`

- 設置 Vagrant 相關的配置，
  - 配置1，建立 vagrant 用戶，
    - 說明見，[step1](#step1建立-vagrant-的-ssh-用戶)
      - step1 只會在虛擬檔案系統中建立臨時的ssh 用戶，切換到實際的檔案系統後 ssh 用戶就無法使用，

      - 此處是在實際的檔案系統慎建立 ssh 用戶，步驟相同，是進入實際系統後使用的

    - 1-1，建立用戶，`$ /usr/bin/useradd --comment 'Vagrant User' --create-home --user-group vagrant`
  
    - 1-2，設置vagrant密碼，`$ echo -e "vagrant\n密碼" | passwd vagrant`
  
  - 配置2，將用戶添加到 sudoers 的配置檔
    - 2-1，`$ echo 'Defaults env_keep += "SSH_AUTH_SOCK"' > /etc/sudoers.d/10_vagrant`
    - 2-2，`$ echo 'vagrant ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers.d/10_vagrant`
    - 2-3，`$ /usr/bin/chmod 0440 /etc/sudoers.d/10_vagrant`

  - 配置3，建立 vagrant 用戶的 ssh 公鑰

    - 3-1，利用 install 命令建立 .ssh 目錄並設置權限和擁有者
      - install命令用於建立文件、複製文件，並設定文件的屬性
      - `$ /usr/bin/install --directory --owner=vagrant --group=vagrant --mode=0700 /home/vagrant/.ssh`
        - `--directory參數`，指定建立的文件為目錄
        - `--owner=vagrant參數`，指定文件的擁有者為 vagrant
        - `--group=vagrant參數`，指定文件的擁有群組為 vagrant
        - `--mode=0700參數`，設置文件的存取權限

    - 3-2，下載 vagrant 提供的 ssh 公鑰，並放置在 /home/vagrant/.ssh 的目錄中
      - `$ /usr/bin/curl --output /home/vagrant/.ssh/authorized_keys --location https://raw.github.com/mitchellh vagrant/master/keys/vagrant.pub`
        - `--location參數`，保存到本地端
        - `--output參數`，變更檔案名

    - 3-3，變更公鑰的擁有者，`$ /usr/bin/chown vagrant:vagrant /home/vagrant/.ssh/authorized_keys`
  
    - 3-4，變更公鑰的存取權限，`$ /usr/bin/chmod 0600 /home/vagrant/.ssh/authorized_keys`，僅擁有者可讀寫


- 移除 gptfdisk 套件，`$ /usr/bin/pacman -Rcns --noconfirm gptfdisk`
  - -R，刪除依賴套件
  - -c，清除該套件及其相關檔案
  - -n，忽略依賴性檢查
  - -s，刪除該套件及其相依套件的共用庫

## step6，避免 shutdown-race-condition
- 將 /root/poweroff.timer 的檔案，複製到 /mnt/etc/systemd/system/poweroff.timer"

  - 注意，/root 是 archlinux 安裝系統的預設目錄，在 packer 中，poweroff.timer會透過內建的 server 上傳到 /root 目錄中，
    - 詳見，[arch-template.json的boot_command](arch-template.json)
    - 若不是透過 packer 安裝 archlinux，需要手動建立 poweroff.timer 檔案
  
  - 命令，確保 poweroff.timer 檔案存在，`$ cat /root/poweroff.timer`

  - 命令，`$ /usr/bin/install --mode=0644 /root/poweroff.timer "/mnt/etc/systemd/system/poweroff.timer"`
  
  - 參考，[workaround for shutdown race condition](http://comments.gmane.org/gmane.linux.arch.general/48739)

## step7，關閉網路和重開機
- 卸載掛載的磁區
  ```shell
  /usr/bin/sleep 3
  /usr/bin/umount /mnt
  ```
  
- 關閉網路，
  
  手動關閉網路可以確保主機的SSH連線確實斷開
  ```shell
  for i in $(/usr/bin/ip -o link show | /usr/bin/awk -F': ' '{print $2}'); do /usr/bin/ip link set ${i} down; done
  ```

- 重開機，`$ /usr/bin/systemctl reboot`

- 重啟後，擇 `Boot existing OS` 的選項進入系統，或移除archiso光碟，改從硬碟開機

- 透過 ssh，以 vagrant 登入，`$ ssh vagrant@localhost`，預設不允許使用 root 登入

## step8，安裝 virtualbox-guest-additions

從硬碟開機進入系統後

```shell
# 切換到 root (因為尚未安裝 sudo，sudo 指令不可用)
su

/usr/bin/pacman -Sy

# 安裝 virtualbox-guest-additions 和 NFS 工具
/usr/bin/pacman -S --noconfirm virtualbox-guest-utils-nox nfs-utils

# 啟用 virtualbox-guest-additions 服務
/usr/bin/systemctl enable vboxservice.service

# 啟用 rpcbind 服務，
#     讓系統中的其他程序可以使用 RPC 通信，
#     例如 NFS(Network File System) 和 NIS(Network Information Service)
/usr/bin/systemctl enable rpcbind.service

# 啟用 virtualbox 存取共享目錄的帳戶
#     將使用者帳號 vagrant 新增到 vagrant 和 vboxsf 群組中
/usr/bin/usermod --append --groups vagrant,vboxsf vagrant
```

## step9，清理磁碟空間+減少磁碟空間大小

```shell
# 清理 pacman 的快取
/usr/bin/pacman -Scc --noconfirm

# 概念1: 將0值寫入檔案中，vagrant 遇到連續為0的磁碟空間會進行壓縮，從而減小 box 的體積
# 概念2: 所有的數據都需要跟檔案建立關係，透過檔案將0寫入硬碟後，檔案就不需要了

# step1，建立 /zerofile.XXXXX 的臨時檔，並將檔名保存在環境變數zerofile中
#     /zerofile.XXXXX 是檔名模板，mktemp命令會將 XXXXX 替換為隨機字串
zerofile=$(/usr/bin/mktemp /zerofile.XXXXX)

# step2，將0寫入臨時檔中
#/usr/bin/dd if=/dev/zero of="$zerofile" bs=1M
/usr/bin/dd if=/dev/zero of="$zerofile"   # 速度緩慢

# step3，確保將0寫入檔案中(確保寫入硬碟中，而不是保存在內存中)
/usr/bin/sync

# step4，建立臨時檔
/usr/bin/rm -f "$zerofile"
```

## Ref
- [sgdisk的使用](https://www.796t.com/article.php?id=160400)