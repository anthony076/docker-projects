
```json
// 將 vagrant-cloud 上的 box 重新包裝
{
  "builders": [
    {
      "communicator": "ssh",
      "source_path": "hashicorp/precise64",
      "provider": "virtualbox",
      "add_force": true,
      "type": "vagrant"
    }
  ],
  "provisioners": [],
  "post-processors": []
}
```