## 說明
- 本範例使用 WSL2 作為 sshd-server，並測試各種客戶端的連線方式和配置

## wsl-sshd-server 的初始化設置
- 測試環境 WSL2 @ Ubuntu-20.04

- 更新apt，`$ sudo apt update && sudo apt upgrade`

- 重新安裝 openssh-server，
  - wsl 預設不會自動啟動 sshd，也不允許啟動，因此需要重新安裝 sshd
  - `$ sudo apt remove openssh-server -y && sudo apt install openssh-server -y`

- 重新安裝的 openssh-server，
  - 安裝後，會自動在 `/etc/ssh` 目錄中，`預先產生公私鑰`
  - 不會建立 `~/.ssh` 的目錄

- 預設的 sshd 配置檔，`/etc/ssh/sshd_config`，
  - 注意，在sshd的配置檔中，`註解行不是無效而是預設值`，要變更時再手動去掉註解(#)
  
  - Port 22，連接埠

  - HostKey 的預設位置，
    - 注意，利用 `HostKey 私鑰路徑` 手動添加
    - 注意，若以下公私鑰不存在，可透過 `ssh-keygen -A` 自動添加

    - #HostKey /etc/ssh/ssh_host_rsa_key
    - #HostKey /etc/ssh/ssh_host_ecdsa_key
    - #HostKey /etc/ssh/ssh_host_ed25519_key

  - PasswordAuthentication no
  - PubkeyAuthentication yes
  - X11Forwarding yes
  - 未配置前無法使用 publickey 或密碼登入

## 密碼登入實作
- Server 端 (被連接端)
  - 配置前修改權限
    - `sudo chmod 777 /etc/ssh`
    - `sudo chmod 777 /etc/ssh/sshd_config`

  - 配置，修改 `/etc/ssh/sshd_config`
    
    - `PubkeyAuthentication no`，
    - `PasswordAuthentication yes`
  
  - 修改完配置後，重新啟動 sshd-server，`sudo service ssh start`

- Client 端 (主動連接端)
  - 注意，`若 ssh 指令不提供登入帳號`，就會以執行此命令的使用者登入
  - 注意，Client 端不需要啟動 sshd-server，可直接使用 ssh 命令

  - 方法1，使用 `WSL-Client` 登入，輸入，`ssh ching@localhost`

  - 方法2，使用 `Putty` 登入
    - Session > SSH > 輸入 IP=localhost PORT=20 > Open

  - 方法3，使用 `docker` 登入，
    - `ssh ching@host.docker.internal`

## 免密碼登入實作
- Server 端
  - 配置前修改權限
    - `sudo chmod 777 /etc/ssh`
    - `sudo chmod 777 /etc/ssh/sshd_config`

  - 配置，修改 `/etc/ssh/sshd_config`
    
    - `PubkeyAuthentication yes`，
    - `PasswordAuthentication yes`
  
  - 修改完配置後，重新啟動 sshd-server，`sudo service ssh start`

- Client 端，以 docker 為例

  - step0，確保 sshd-server 在開啟狀態

  - step1，在 client 端上產生公私鑰 (產生 client-public-key)
    > `ssh-keygen -q -N "" -t rsa -f $HOME/.ssh/id_rsa`

  - step2，將 client-public-key 傳遞給 server 端
    - 方法1，透過 `ssh-copy-id` 指令，將 client-public-key 傳遞給 server
      - 推薦此方法，快速方便不容易出錯

      - 執行，`ssh-copy-id -i /root/.ssh/id_rsa.pub ching@host.docker.internal`
      - 注意，此指令不需要 server 設置 `PubkeyAuthentication yes` 也可以執行
      - 此命令正確執行後，server 會在自動將 client 指定的公鑰，
        添加到 `~/.ssh/authorized_keys` 的檔案中，並設置存取權限為 600

    - 方法2，連線進入 server，由 client 在 server 主機上手動建立 authorized_keys 檔案
      - 不推薦此方法，麻煩繁瑣容易出錯

      - 預先複製 client 端 `/root/.ssh/id_rsa.pub` 的檔案內容
        ```
        ssh-rsa 公鑰內容 帳號@IP
        ```
      - 透過 SSH 連線進 server 端
        `ssh ching@host.docker.internal`

      - 透過 `echo "複製內容" >> ~/.ssh/authorized_keys`，手動添加到 authorized_keys 檔案中

      - 修改 authorized_keys 檔案的權限
        `sudo chmod 600 /home/ching/.ssh/authorized_keys`

  - step3，client 端使用自己產生的私鑰
    - 停止 server 端的執行，`sudo service ssh stop`
    
    - 修改 server 端的配置，`nano /etc/ssh/sshd_config`
      - `PubkeyAuthentication yes`，允許 client 以公鑰登入
      - `PasswordAuthentication no`，不允許 client 以公鑰登入
    
    - 重新啟動 server 端上的 ssd-server，`sudo service ssh start`
    - 客戶端透過 `ssh username@server_host` 進行連線
