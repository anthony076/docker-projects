## x11 概述
- X 或 X11，Linux 上的圖形介面系統

  在 Linux 中的圖形介面稱為 `X-Windows-System`，簡稱為 `X` 或 `X11`，屬於 `Server/Client` 的架構，
  因此 X-Windows-System 有可分為 X-server 和 X-client，X-Windows-System 是跨網路且跨平台的

  開發 X-Windows-System 時，開發者希望這個視窗介面`不要與硬體有強列的相關性`，若具有強烈的相關性，
  就等於是一個作業系統，如此一來的X應用性就會被侷限。因此 X 在最開始開發時，就是`以應用程式的概念來開發`，
  而不是作為作業系統來開發，因此 X-Windows-System `對 Linux 來說僅僅是一個軟體`

  1987年更改X版本到X11，這一版本取得了明顯的進步，後來的視窗介面改良都是架構於此一版本，
  因此後來X視窗也稱為 X11。

  1992年 xfree86(http://www.xfree86.org/)計畫展開，該計畫持續在改進 X11R6 的功能性，
  包含對新硬體的支援以及更多的新增功能開發

  後來因為一些授權的問題導致 xfree86 無法繼續提供類似 GPL 的自由軟體，改由 Xorg基金會接手 X11R6 的維護，
  並在2004年發布X11R6.8版本，更在 2005年發布 X11R7.x版本

  - 在 Unix Like 上的圖形使用介面(GUI) 被稱為 X 或 X11
  - X11 是 Server/Client 架構的軟體，而不是作業系統
  - X11 是利用網路架構來進行圖形介面的執行和繪製

## 透過 SSH 進行 x11 的轉發
- 注意，x11 不需要 ssh 也可以遠端繪製畫面，透過 ssh 可以保障畫面不會被截取
  
  [docker 不透過 ssh 直接存取 x11 範例](../../docker/readme.md#範例-在-docker-中使用-x11-的最小設置-不使用ssh)

- SSH 和 X 的腳色在架構上是相反的，`SSH-Client(X-Server) <-> SSH-Server(X-Client)`
  
  以 SSH 的角度來說，執行 sshd 命令的主機為 SSH-Server 端，
  透過 ssh 命令連接到遠端，此時，執行ssh命令的主機為 SSH-Client 端

  當 SSH-Client 透過 ssh 命令連接到 SSH-Server 後，若在 SSH-Server 端執行了需要 gui 的應用，
  此時 SSH-Server 扮演的是 X-Client 的腳色，SSH-Server 會將繪製gui畫面的操作，包裝為網路的請求，
  透過 ssh 將繪製畫面的請求傳遞給 SSH-Client 端

  SSH-Client 客戶端收到繪製畫面的請求後，再將這些請求轉發給 SSH-Client 主機上的 X-Server，
  並交由 X-Server 進行畫面的繪製

  - ssh連接路徑: ssh-client -> ssh -> ssh-server -> 執行 gui 應用
  - 繪製畫面路徑: `x-server` <- ssh-client <- ssh <- ssh-server <- `x-client` <- gui 應用

  因此
  - 對 SSH-Server(X-Client) 來說: `SSH-Server 和 X-Client 都包含在 openssh` 的套件中，不需要額外安裝 X-Client 的套件

  - 對 SSH-Client(X-Server) 來說，
    - 若 SSH-Client 為 Linux 系統:
      - ssh-client 被安裝在 `openssh` 的套件中
      - x-server 是 Linux 的內建安裝，不需要額外安裝

    - 若 SSH-Client 為 Windows 系統，
      - ssh-client 被安裝在 `openssh` 或 `putty` 的套件中
      - windows 未內建 x-server 的套件，因此`需要手動安裝有實作 xserver 的套件`，例如，xming

- 注意，同樣是 `openssh` 的配置，SSH-Client(X-Server) 和 SSH-Server(X-Client) 是不一樣的
  - [SSH-Server(X-Client) 的配置](#ssh-serverx-client-的配置)
    
    <font color=red>注意，需要設置 DISPLAY 變數，ssh -Y ... 的命令才會對 SSH-Server 端設置 DISPLAY 環境變數</font>

  - [SSH-Client(X-Server) 的配置](#ssh-clientx-server-的配置)

## SSH-Server(X-Client) 的配置
- step1，安裝 xauth 套件，
  
  `$ sudo apt install xauth`

  openssh 內的 xclient 會利用 xauth 進行與 xserver 進行連線認證

- step2，確保 `/etc/ssh/sshd_config` 的檔案中，有設置以下選項

  ```shell
  X11Forwarding yes  
  ```

- step3，重啟 sshd-server

  修改後，重啟 sshd 服務，
  `$ sudo service sshd restart`
  
## SSH-Client(X-Server) 的配置
- 範例，以 linux 的 openssh 為例
  - 注意，使用 linux 的 ssh-client 不需要安裝 xserver，xserver 已內建

  - 需求1，確保`/etc/ssh/sshd_config`的檔案中，有開啟以下選項
    ```shell
    ForwardAgent yes
    ForwardX11 yes
    ForwardX11Trusted yes
    ```
  
  - 需求2，設置 DISPLAY 變數 
    - 方法1，設置臨時變數，`$ export DISPLAY="127.0.0.1:10.0"`
    - 方法2，設置永久變數，

      `$ nano ~/.bashrc`
      
      添加以下
      ```shell
      export PATH=$PATH:$HOME/.local/bin:$HOME/bin
      export DISPLAY="127.0.0.1:10.0"
      ```

  - 需求3，透過 -X 或 -Y 進行 ssh 連線
    
    `$ ssh -X 帳號@IP -p 連接埠` 或 `$ ssh -Y 帳號@IP -p 連接埠`


- 範例，以 windows 的 putty(SSH-client) + xming(xserver) 為例
  - 需求1，啟用 xming ，並使用預設配置

  - 需求2，設置 putty
    在 `connection > SSH > X11`，
    - 勾選 enable x11 forwarding
    - 在 x display location 欄位填入，localhost:0.0
  
  - 需求3，設置 DISPLAY 
    
    putty 會自動設置 DISPLAY，使用 putty 時不需要在 CMD 中設置 DISPLAY 變數

  - 需求4，透過 putty 進行 SSH 連線

- 範例，以 windows 的 openssh(SSH-client) + xming(xserver) 為例
  - 需求1，啟用 xming ，並使用預設配置

  - 需求2，設置 DISPLAY 變數 
    - 方法1，設置臨時變數，`$ set DISPLAY=127.0.0.1:0.0`
    - 方法2，設置永久變數，`$ setx DISPLAY "127.0.0.1:0.0"`
    - 注意，若設置 setx ，需要重啟cmd視窗 或 重啟vscode 後才會生效

  - 需求3，透過 -X 或 -Y 進行 ssh 連線
    
    `$ ssh -X 帳號@IP -p 連接埠` 或 `$ ssh -Y 帳號@IP -p 連接埠`

    - ssh -Y 或 ssh -X 的指定，會在 SSH-server 端設置 DISPLAY 的環境變數
    - 注意，在 SSH-client 端有設置 DISPLAY 的變數，`ssh -Y `的命令才會對 SSH-server 設置 DISPLAY 的變數，
      
      若在 SSH-server 輸入 `$ echo $DISPLAY` 的輸出結果為空，確保在 SSH-client 端有設置 DISPLAY 的變數

## 在 winodws 上，實作 xserver 套件的選擇
- [Xming](https://sourceforge.net/projects/xming/)，使用簡單，但最新版需要收費，且已不再更新
- [VcXsrv](https://sourceforge.net/projects/vcxsrv/)，簡單、易用、開源、免費、
- [MobaXterm](https://mobaxterm.mobatek.net/download.html)，ssh-client + xserver
- [x410](https://x410.dev/)

## [範例] docker + ssh + gui 應用
<img src="example-docker+ssh+gui.png" width=800 height=auto>