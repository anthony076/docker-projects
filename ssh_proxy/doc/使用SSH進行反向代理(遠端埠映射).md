## 遠端埠綁定與反向代理 (Remote-Port-forwarding)
- 通訊流程
  ```mermaid
  flowchart LR
  subgraph 同網域\n
    b1(客戶端) <-- 直連\n未加密 --> b2(sshd-server\n反向代理) 
  end
  b2 <-- SSH --> b3(目標主機\n遠端埠綁定\nssh -R) 
  ```

  注意，反向代理時，只有 sshd-server 和目標主機之間才會建立SSH通道，
  
  sshd-server 和客戶端之間不會建立 SSH 通道，兩者是可以直接通訊的

- 遠端埠映射的命令，是透過`在目標主機上執行 ssh -R 命令`實現，會執行以下的功能
  - 建立`客戶端本地埠`和`目標主機`之間的映射，客戶端和 sshd-server 之間是透過 sshd 的通訊埠，不需要映射
  - 在`目標主機`和 sshd-server 之間建立SSH通道
  - 客戶端 <--> sshd-server <-- SSH --> 目標主機

    `目標主機`上會建立一個背景程式，監聽`來自sshd-server的遠端請求`，
    經過`目標主機處理請求`後，將結果經由SSH通道轉發給 sshd-server，

    - sshd-server 收到客戶端的請求後，會將`請求`透過`SSH通道`轉發給目標主機
    - 目標主機處理請求，並將結果透過`SSH通道`返回給 sshd-server
    - sshd-server 收到來自目標主機的結果後，再將結果轉發給客戶端
    - 注意，sshd-server 只轉發請求和結果，但不會處理請求
  
  - `ssh -R` 會使得遠端的 sshd-server 監聽客戶端的port，
    - 因為 客戶端和 sshd-server 處於`同一個網域`，因此客戶端發送的請求也會自動送往 sshd-server
    - 若客戶端和 sshd-server 不在同一個網域，則客戶端 和 sshd-server 就需要建立正向代理，
      sshd-server 才能收到客戶端的請求

- sshd-server 的設置，
  - 見 /etc/ssh/sshd_config 的配置檔
  - `#AllowAgentForwarding yes`，預設允許使用 agent 轉發
  - `#AllowTcpForwarding yes`，必要，預設允許使用 tcp 轉發
  - `#GatewayPorts no`，預設 sshd-server 只允許綁定 localhost 的端口
    - Remote-Forwarding 時，若客戶端與 sshd-server 在同網域就不需要設置為 yes
    - Remote-Forwarding 時，若客戶端與 sshd-server 在不同網域就需要設置為 yes

## 建立遠端埠綁定的語法
  > ssh -fCNR [客戶端IP]:[客戶端port]:[目標主機IP]:[目標主機port] [sshd-server登入帳號]@[sshd-server-IP]
  
  - f參數，背景執行
  - C參數，允許壓縮，非必要
  - N參數，不執行指令，不會開啟另一個 shell，debug 時可取消N參數，可用於與 sshd-server 交互
  - R參數，執行遠端埠映射

- [客戶端IP]:[客戶端port] ，用來監聽客戶端的請求
  - 若未指定 [客戶端IP] ，預設會到客戶端的 localhost
  - 若允許客戶端以外的IP，將 [客戶端IP] 設置為 0.0.0.0 
- [目標主機IP]:[目標主機port]，告知 sshd-server 轉發的目的地
  
- 注意，`客戶端`可以和 ssh-server 是`同一個主機上的服務或應用`，
  或是同網域內，sshd-server 可直連但是`不同主機的服務或應用`

## 範例，反向代理範例
- 測試環境，
  - 流程
    ```mermaid
    flowchart LR
    
    subgraph windows-host
      b0[瀏覽器發出請求\nlocalhost:7788] <--> b1[wsl\nsshd-server\n反向代理] 
    end

    subgraph docker
      b1 <-- "SSH\nroot@localhost:2223" --> b2[目標主機\n執行遠端綁定\nssh -R]
      b2 <--> b4[flask-app\nlocalhost:5566]
    end
    ```

  - 利用 wsl 模擬客戶端環境和 sshd-server，docker-container 模擬目標資源
  - docker 內的主機需要透過 SSH 連接上 WSL 內的 sshd-server

- step1，建立 image，`docker build -f Docker-reverse-proxy -t reverse-proxy .`
  
- step2，執行 container，`docker run --rm -it reverse-proxy`

- step3，在 wsl 中設置並啟動 sshd-server
  - [openssh 在 wsl 中的初始化](使用SSH進行遠端連線.md)
  
  - 在 wsl 配置 sshd-config
    - 設置 root 密碼，`sudo passwd root`

    - 更改 sshd-server 的連接埠，`sudo sed -i 's/#Port 22/Port 2223/' /etc/ssh/sshd_config`
      
    - 允許 root 登入，`sudo sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config` 

    - 允許密碼登入，`sudo sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config` 

    - 允許轉發，`sudo sed -i 's/#AllowTcpForwarding yes/AllowTcpForwarding yes/' /etc/ssh/sshd_config`
    
    - 允許 remote-forwarding，`sudo sed -i 's/#GatewayPorts no/GatewayPorts yes/' /etc/ssh/sshd_config`

  - 啟動 sshd-server，`sudo service ssh start`

  - 確認 sshd-server 已啟動，`ps aux | grep ssh` 或 `sudo service ssh status`
  
  - 在 docker 中測試是否能與 sshd-server 連接，`ssh root@host.docker.internal -p2223`
    
    注意，若 sshd-server 無法啟動，嘗試將 sshd-server 的通訊埠更改為其他位置，
    wsl 和 windows 會共用埠，有機會和 windows 上的 port 發生衝突

- step4，在 docker 中啟動遠端綁定
  
  `ssh -fCNR 0.0.0.0:7788:localhost:5566 root@host.docker.internal -p2223`
  - docker 透過 root@host.docker.internal -p2223 與 sshd-server 建立 SSH 通道
  - 將客戶端 0.0.0.0:7788 的請求，映射到 docker 內 localhost:5566 的目標資源

- step5，在 docker 中啟動 flask-app

  `python app.py`

- step6，測試
  - 在 wsl 中，輸入 `curl http:127.0.0.1:7788`
  - windows-host 的瀏覽器中，輸入 `http:127.0.0.1:7788`

# Ref
- [rsshd 反向代理的docker範例](https://github.com/efrecon/rsshd)