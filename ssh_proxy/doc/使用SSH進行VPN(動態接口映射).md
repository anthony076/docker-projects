## 動態端口映射 (Dynamic-Port-forwarding)

- 動態端口映射不綁定端口，客戶端`任意端口`發起的請求，都會透過 sshd-server 轉發到目標主機上

- 動態端口映射雖然不綁定客戶端/目標主機的端口，但仍然需要 `sock-proxy`

- 動態端口映射`綁定的是 sock-proxy 的端口`
  - 流程
    ```mermaid
      flowchart LR
      
      subgraph 客戶端
        b1[port:80] --> a1[sock-proxy\nport:1234]
        b2[port:8080] --> a1
        b3[port:any] --> a1
      end

      subgraph 目標主機
        a1 -- SSH\nport:22 --> a2[sshd-server\nsock-proxy\nport1234]
        a2 --> a3[port80]
        a2 --> a4[port8080]
        a2 --> a5[port:any]
      end
      ```

  - sock-proxy @ 客戶端，
    - 客戶端需要`自行準備 sock-proxy`

    - windows 內建全局的 sock-proxy，[參考](https://www.anoopcnair.com/how-to-configure-proxy-settings-in-windows-11/)
    
    - 動態端口映射不綁定端口，但 sshd-server `需要固定端口接收請求`，
      因此需要在客戶端準備 sock-proxy 收集客戶端發起的請求，
      並透過客戶端的 sock-proxy 利用`單一端口`發送給 sshd-server
  
  - sock-proxy @ 目標主機端，
    - sshd-server 內建 sock-server 的功能，`不需要特別準備 sock-proxy`

    - 雖然動態端口映射不會綁定目標主機的特定端口，但目標主機`透過SSH通道收到請求`後，
      需要在目標主機上`發起實際的請求`，因此在目標主機上需要一個 socks-proxy 
      `接收SSH通道上接收的請求`，並透過 socks-proxy `發起實際的請求`

- 建立動態端口映射的語法
  > ssh -fCND [客戶端IP]:[sock-proxy端口] [sshd-server登入帳號]@[sshd-serverIP]
    
  - f參數，背景執行
  - C參數，允許壓縮，非必要
  - N參數，不執行指令，不會開啟另一個 shell，debug 時可取消N參數，可用於與 sshd-server 交互

  - 若不指定 [客戶端IP]，預設綁定到客戶端的 localhost 上
  - 若要接受任意IP的請求，[客戶端IP] 設置為 0.0.0.0
  - 所有傳送給 sock-proxy 的請求，才會被發送到目標主機上

- 其他使用場景
  - VPN
  - 若某個服務A限制只限某個網域區段B的IP存取，將外網IP偽裝為網域B的IP後，就可以存取服務A
  - 使用公共區域網路時，透過動態端口映射加密通訊

## 動態接口映射的風險和相關工具
- 動態接口映射對外的端口都是開放的，若不做限制容易造成攻擊
  
- 工具，Fail2Ban 阻擋不名連線
  
  Fail2Ban 會監看 SSH 服務的 log 來偵測登入失敗的 IP，
  當這些 IP 的失敗次數達到一定值時，利用防火牆來暫時停止該 IP 的連線請求，過一定時間後再恢復。
  Fail2Ban 可以拿來擋掉最基本的暴力攻擊。

- 工具，Port Knocking 條件的開啟 SSH Port
  - Client 必須用特殊的順序來對 SSH Server 上的某些 Port 發出連線請求後，SSH Server 才會開放 Client 連線的技巧，
  例如依序對 Port 1000、2000、3000 發出請求，才會對你開放 Port 22，
  這樣的好處是平時 Port 22 就會是關閉的狀態，讓攻擊者以為 SSH 沒有開放，減少被攻擊的機會，
  
  - [參考](https://www.youtube.com/watch?v=IBR3oLqGBj4)