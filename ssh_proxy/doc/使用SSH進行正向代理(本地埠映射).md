## 本地埠映射與正向代理 (Local-Port-forwarding)
- 先執行客戶端的本地埠映射，才能使 sshd-server 執行正向代理
  - 概念

    先透過客戶端的 `本地埠的映射`，才能使遠端的 `sshd-server` 收到並轉發來自客戶端的請求給目標主機(正向代理)，

    透過本地埠映射+正向代理，使得`客戶端`可以透過 `localhost:port` 取得`遠端目標主機的結果`

  - 通訊流程
    ```mermaid
    flowchart LR
    c1(客戶端\n本地埠映射\nssh -L) <-- SSH --> c2(sshd-server\n正向代理) 
    
    subgraph 同網域
      c2 <-- 直連\n未加密 --> c3(目標主機)
    end
    ```

    注意，正向代理時，只有 sshd-server 和客戶端之間才會建立SSH通道，
    
    sshd-server 和目標主機之間不會建立 SSH 通道，兩者是可以直接通訊的

- 本地埠映射的命令，是透過`在客戶端上執行 ssh -L 命令`實現，會執行以下的功能
  - 建立`客戶端本地埠`和`目標主機`之間的映射，客戶端和 sshd-server 之間是透過 sshd 的通訊埠，不需要映射
  - 在`客戶端`和 sshd-server 之間建立SSH通道
  - 客戶端 <-- SSH --> sshd-server <--> 目標主機
   
    在`客戶端`建立一個背景程式，監聽`客戶端的本地請求`，並經由SSH通道轉發給 sshd-server，

    - sshd-server 透過`SSH通道`收到客戶端的請求後，會將`請求`轉發給目標主機
    - 目標主機處理請求，並將結果返回給 sshd-server
    - sshd-server 收到來自目標主機的結果後，再將結果透過`SSH通道`轉發給客戶端
    - 注意，sshd-server 只轉發請求和結果，但不會處理請求

- sshd-server 的設置，
  - 見 /etc/ssh/sshd_config 的配置檔
  - `#AllowAgentForwarding yes`，預設允許使用 agent 轉發
  - `#AllowTcpForwarding yes`，必要，預設允許使用 tcp 轉發

## 建立本地埠映射的語法
> ssh -fCNL [客戶端IP]:[客戶端port]:[目標主機IP]:[目標主機port] [sshd-server登入帳號]@[sshd-server-IP]

- f參數，背景執行
- C參數，允許壓縮，非必要
- N參數，不執行指令，不會開啟另一個 shell，debug 時可取消N參數，可用於與 sshd-server 交互
- L參數，執行本地埠映射

- [客戶端IP]:[客戶端port] ，用來監聽客戶端的請求
  - 若未指定 [客戶端IP] ，預設會到客戶端的 localhost
  - 若允許客戶端以外的IP，將 [客戶端IP] 設置為 0.0.0.0 
- [目標主機IP]:[目標主機port]，告知 sshd-server 轉發的目的地

- 注意，`目標主機`可以和 ssh-server 是`同一個主機上的服務或應用`，
  或是同網域內，sshd-server 可直連但是`不同主機的服務或應用`

## 範例，正向代理範例
- 測試環境，
  - 流程
    ```mermaid
    flowchart LR
    
    subgraph windows-host
      b0[瀏覽器發出請求\nlocalhost:7788] <--> b1[wsl\n執行本地\nssh -L] 
    end

    subgraph docker
      b1 <-- "SSH\nroot@localhost:22" --> b2[sshd-server\n正向代理] 
      b2 <--> b3[flask-app\n目標資源\nlocalhost:5566]
    end
    ```
  - 利用 wsl 模擬客戶端環境，docker-container 模擬 sshd-server 和 目標資源
  - 在 docker-container 中，利用 flask-app 模擬目標資源

- step1，建立 image，`docker build -f Docker-port-forwarding -t port-forward .`
  
- step2，執行 container，`docker run --rm -d -p 22:22 port-forward`

- step3，測試 container 中的 sshd-server 和 flask-app 是否正常執行，
  - 在 `wsl` 中 
    - 連接到 sshd-server，`ssh root@localhost`
    - 輸入 `curl 127.0.0.1:5566` 進行與 sshd-server 的連接測試

  - 在 `windows` 中 
    - 連接到 sshd-server，`ssh root@localhost -p 22`，注意，windows 的 openssh 預設使用 port 2222
    - 輸入 `curl 127.0.0.1:5566` 進行測試

- step4，在 wsl 建立本地埠，
  - 注意，在 docker 中的 sshd-server 需要在開啟狀態，才能進行本地埠映射
  
  - 在 wsl 中輸入，
    - `ssh -fCNL 0.0.0.0:7788:localhost:5566 root@localhost` 或
    - `ssh -fCNL *:7788:localhost:5566 root@localhost`

  - 參數，`0.0.0.0:7788` ，代表 ssh -L 接受來自 0.0.0.0:7788 的請求

  - 參數，`localhost:5566`，是flask-app 的存取位址，作為正向代理主機的sshd-server(和 flask-app 同網域)
    ，在收到 wsl 的請求後，會主動將請求發往 localhost:5566

  - 參數，`root@localhost`，是 sshd-server 的登入帳號和網址，
    在 step3 中已經測試過，可以透過` ssh root@localhost -p 22` 存取 sshd-server，

- step5，測試本地埠+正向代理
  - 在 wsl 中輸入，`curl http://127.0.0.1:7788` 就可以看到 flask-app 返回的結果，
  - 因為 wsl 和 windows 是同網域，因此可以透過瀏覽器輸入，http://127.0.0.1:7788 也可以看到結果

- (Optional) 使用 putty 進行 本地埠

  注意，putty 需要手動登入sshd-server，本地埠的才會生效

  <img src="local-port-forwarding-putty.png" width=800 height=auto>

  
