## 使用socket與proxy通訊
- 常見的proxy有以下幾種類型
  - ssh-proxy

  - http-proxy
  
  - socks-proxy
    - socket4-proxy
    - socket5-proxy

  - 參考，[線上免費代理](https://spys.one/en/socks-proxy-list/)

- SOCKS 是[對話層(session-layer)的api接口](https://zh.wikipedia.org/wiki/SOCKS)，
  用於兩個主機之間建立連接(session)
  
- 對於各種類型的 proxy `都可以使用 Socket-api 進行通訊`，但是`通訊的流程`有差異，不能混用，
  例如，`http-proxy` 是透過 HTTP CONNECT 發起連接請求，`socks-proxy` 是在請求數據包中的特定位置，標誌為CONNECT連接請求

- `http-proxy`，使用 socket 連接後，會添加符合HTTP協議的Header後，再轉發給 http類型的目標主機
  
  `socks-proxy` 使用 socket 連接後，`不會改寫或添加目的應用層協議的內容`，只負責轉送給任意類型的目標主機

- `socks-proxy` 可轉發 udp 連接，http-proxy 可轉發 tcp 連接

## 與 http-proxy 進行通訊
- 透過 `CONNECT` 方法，與 http-proxy 發起連接
  - step1，透過 sock.sendall()，發起 `CONNECT` 的請求
  - step2，透過 sock.recv()，接收 server 的回應
  - step3，檢視回應內容，判斷是否有錯誤出現
  - 完整範例，參考 [SocksiPy]([../SocksiPy/socksi.py](https://github.com/Talanor/SocksiPy)) Line-253
  
- 簡易範例
  ```python
  # ==== 發出連接請求 ====
  sock.sendall(
      b"CONNECT %s:%s HTTP/1.1\r\nHost: %s\r\n\r\n" % (
          addr, str(destport), destaddr
      )
  )
  
  # ==== 接收回應 ====
  resp = sock.recv(1)

  while resp.find("\r\n\r\n") == -1:
      resp = resp + sock.recv(1)

  # 判斷異常
  statusline = resp.splitlines()[0].split(" ", 2)

  if statusline[0] not in ("HTTP/1.0", "HTTP/1.1"):
      sock.close()
      raise GeneralProxyError((1, _generalerrors[1]))
  try:
      statuscode = int(statusline[1])
  except ValueError:
      sock.close()
      raise GeneralProxyError((1, _generalerrors[1]))
  if statuscode != 200:
      sock.close()
      raise HTTPError((statuscode, statusline[2]))
  ```

## 與 Socket4-proxy 進行通訊
- `請求連接` 的數據包格式 
  - VN，1byte，SOCK版本，
    - VN=4，for socket4
    - VN=5，for socket5

  - CMD，1byte，SOCK的請求碼，
    - 第一次建立連接時，CMD為請求連接的類型
    - CMD=1，CONNECT 請求
    - CMD=2，BIND 請求
  
  - DSTPORT，2byte，目的主機的PORT
  - DSTIP，4byte，目的主機的IP
  - USERID，可變長度，使用者 ID
  - NULL，1byte，固定為 0

- `回應連接請求` 的數據包格式 
  - VN，1byte，回應碼的版本，VN=0

  - CMD，1byte，proxy 的回覆碼，
    - CMD=90，允許連接
    - CMD=91，拒絕連接或連接失敗
    - CMD=92，proxy 無法連接到客戶端的 userid 造成連接的失敗
    - CMD=93，客戶端回應的 userid 和請求的不同，造成的連接失敗
  
- 注意，若連接失敗，proxy 會立刻斷開連接，
  若請求被允許，代理伺服器就充當客戶端與目的主機之間進行雙向傳遞，
  對客戶端而言，就如同直接在與目的主機相連。

- 與 Socket4-proxy 的通訊流程
  - step1，將 主機名 轉換為 32bit-byte IP
    ```python
    >>> url = "www.google.com"
    >>> ip = socket.gethostbyname(url)  # '172.217.163.36'
    >>> ip_32bit = socket.inet_aton(ip) # b'\xac\xd9\xa3$' = 0xACD9A324
    ```

  - step2，將 port 轉換為 byte
    ```python
    >>> port = 5566
    >>> struct.pack(">H", port) # b'\x15\xbe'
    ```

  - step3，建立請求連接的數據包
    ```python  
    # b"\x04" = VN = socket4
    # b"\x01" = CMD = CONNECT 請求
    # b'\x15\xbe' = DSTPORT = 目標主機的PORT
    # b'\xac\xd9\xa3$' = DSTIP =目標主機的IP (0xAC_D9_A3_24)
    req = b"\x04\x01" + b'\x15\xbe' + b'\xac\xd9\xa3$' 
    ```

  - step4，透過 sock.sendall()，發送請求連接的數據包
  - step5，透過 sock.recv(8)，接收 server 的回應
  - step6，檢視回應的數據包內容，判斷是否有錯誤出現

  - 完整範例，參考 [SocksiPy]([../SocksiPy/socksi.py](https://github.com/Talanor/SocksiPy)) Line-187

- 簡易範例
  ```python

  # ==== 建立請求的數據包 ====
  ipaddr = socket.inet_aton(socket.gethostbyname(destaddr))
  port = struct.pack(">H", destport)

  # b"\x04" = VN = socket4
  # b"\x01" = CD = CONNECT 請求
  # b'\x15\xbe' = DSTPORT = 目標主機的PORT
  # b'\xac\xd9\xa3$' = DSTIP =目標主機的IP (0xAC_D9_A3_24)
  req = b"\x04\x01" + struct.pack(">H", destport) + ipaddr

  # ==== 傳送連接請求，並接收回應 ====
  sock.sendall(req)
  resp = sock.recvall(8)

  # ==== 根據回應判斷是否連接成功 ====
  if resp[0] != 0x00:
      # VN 位
      sock.close()
      raise GeneralProxyError((1, _generalerrors[1]))
  
  if resp[1] != 0x5A:
      # CMD 位，
      # 0x5A = 0x90 = 允許連接
      sock.close()
      if ord(resp[1]) in (91, 92, 93):
          sock.close()
          raise Socks4Error(
              (ord(resp[1]), _socks4errors[ord(resp[1]) - 90])
          )
      else:
          raise Socks4Error((94, _socks4errors[4]))  
  ```

## 與 Socket5-proxy 進行通訊
- Socket5 比 Socket4 多了 驗證/IPv6/UDP 的支援，
  因此，需要先發送`認證請求`，確認認證方式後，才發送`連接請求`

- 透過確認請求，client 提供可用的認證方式給 proxy 供選擇，
  proxy 會回覆選擇並確認最後的認證方式

- `認證請求`的數據包格式
  - VER，1byte，SOCKS 的版本，對 Socket5，VER = 0x05
  - NMETHODS，1byte，METHODS欄位的長度
  - METHODS，1-255 byte，客戶端支援的認證方式列表
    - 0x00 不需要認證
    - 0x01 GSSAPI
    - 0x02 使用者名稱、密碼認證
    - 0x03 - 0x7F由IANA分配（保留）
      - 0x03: 握手挑戰認證協定
      - 0x04: 未分派
      - 0x05: 回應挑戰認證方法
      - 0x06: 傳輸層安全
      - 0x07: NDS認證
      - 0x08: 多認證框架
      - 0x09: JSON參數塊
      - 0x0A–0x7F: 未分派
    - 0x80 - 0xFE為私人方法保留
    - 0xFF 無可接受的方法

- `回應認證請求`的數據包格式
  - VER，1byte，SOCKS 的版本，對 Socket5，VER = 0x05
  - METHOD，1byte，伺服器端選中的方法，與確認數據包一致
    - 0xFF表示沒有一個認證方法被選中，客戶端需要關閉連接。

- `連接請求`的數據包格式
  - VER，1byte，SOCK版本，
    - VER=4，for socket4
    - VER=5，for socket5

  - CMD，1byte，SOCK的請求碼，
    - 第一次建立連接時，CD為請求類型
    - CMD=1，CONNECT 請求
    - CMD=2，BIND 請求
    - CMD=3，UDP 轉發
  
  - RSV，1byte，RSV=0，保留位

  - ATYP，1byte，DST_ADDR 欄位的類型
    - ATYP=1，DST_ADDR 欄位為 IPv4類型的IP，4byte長
    - ATYP=3，域名
    - ATYP=4，DST_ADDR 欄位為 IPv6類型的IP，16byte長

  - DST_ADDR，目的主機的位址，動態長度，依 ATYP 的值決定長度
  - DST_PORT，目的主機的PORT

- `回應連接請求`的數據包格式
  - VER，1byte，SOCK版本，
    - VER=4，for socket4
    - VER=5，for socket5

  - REP，1byte，proxy 的回答欄位，
    - REP=0x0，連接成功
    - REP=0x1，普通 socks 服務器連接失敗
    - REP=0x2，現有規則不允許連接 
    - REP=0x3，網路不可用
    - REP=0x4，主機不可用
    - REP=0x5，連線被拒
    - REP=0x6，TTL超時
    - REP=0x7，不支援的命令
    - REP=0x8，不支援的位址類型
    - REP=0x09 - 0xff，未定義
  
  - RSV，1byte，RSV=0，保留位
  
  - ATYP，與 `連接請求數據包` 相同
  - DST_ADDR，與 `連接請求數據包` 相同
  - DST_PORT，與 `連接請求數據包` 相同

- 與 Socket5-proxy 的通訊流程
  - step1，發送`認證請求`的數據包
    ```python
    # b"\x05" = VER = socket5
    # b"\x02" = NMETHODS = METHODS欄位的長度 = 2 byte
    # b'\x00\x02' = METHODS = 密碼認證
    sock.sendall(b"\x05\x02\x00\x02")

    # b"\x05" = VER = socket5
    # b"\x01" = NMETHODS = METHODS欄位的長度 = 1 byte
    # b'\x00' = METHODS = 不需要認證
    sock.sendall(b"\x05\x01\x00")
    ```

  - step2，檢視回應的數據包內容，判斷是否有錯誤出現
  
  - (Optional) 若使用密碼認證，需要在 proxy 回覆認證請求後，
    再次傳送 ID 和 PASS 給 proxy
    ```python
    sock.sendall(
        b"\x01%c%s%c%s" % (
            chr(len(self.username)),
            self.username,
            chr(len(self.password)),
            self.password
        )
    )
    ```
  
  - step3，發送`連接請求`的數據包
    - 3-1，將 主機名 轉換為 32bit-byte IP
      ```python
      >>> url = "www.google.com"
      >>> ip = socket.gethostbyname(url)  # '172.217.163.36'
      >>> ip_32bit = socket.inet_aton(ip) # b'\xac\xd9\xa3$' = 0xACD9A324
      ```

    - 3-2，將 port 轉換為 byte
      ```python
      >>> port = 5566
      >>> struct.pack(">H", port) # b'\x15\xbe'
      ```

    - 3-3，建立`請求連接`的數據包
      ```python  
      # b"\x05" = VER = socket5
      # b"\x01" = CMD = CONNECT 請求
      # b'\x00' = RSV = 保留位，固定為 0
      # b'\x01' = ATYP = IP類型為 IPv4
      # b'\xac\xd9\xa3$' = DST_ADDR = 目標主機的IP (0xAC_D9_A3_24)
      # b'\x15\xbe' = DST_PORT = 目標主機的PORT
      req = b"\x05\x01\x00\x01" + b'\xac\xd9\xa3$' + b'\x15\xbe'
      ```

    - 3-4，透過 sock.sendall()，發送`請求連接`的數據包
    - 3-5，透過 sock.recvall(4)，接收 server 的回應
    - 3-6，檢視回應的數據包內容，判斷是否有錯誤出現
  
  - 完整範例，參考 [SocksiPy]([../SocksiPy/socksi.py](https://github.com/Talanor/SocksiPy)) Line-81
  
- 簡易範例，
  ```python
  
  # ==== 認證請求 ====
  # 認證請求
  sock.sendall(b"\x05\x01\x00")

  # 接收 proxy 回覆
  chosen_auth = sock.recvall(2)
  
  # 檢視回覆結果，判斷是否有錯誤
  if chosen_auth[0] != 0x5:
      sock.close()
      raise GeneralProxyError((1, _generalerrors[1]))

  elif chosen_auth[1] == 0x00:
      # 不需要認證
      pass
  elif chosen_auth[1] == 0x02:
      # 需要密碼認證，傳送 ID 和 PASS 給 proxy
      pass

  # ==== 連接請求 ====
  req = b"\x05\x01\x00"

  ipaddr = socket.inet_aton(destaddr)
  req = req + b"\x01" + ipaddr
  req = req + struct.pack(">H", destport)
  sock.sendall(req)

  # 取得回覆
  resp = sock.recvall(4)

  # 檢視回覆結果
  if resp[0] != 0x05:
      sock.close()
      raise GeneralProxyError((1, _generalerrors[1]))
  elif resp[1] != 0x00:
      sock.close()
      if ord(resp[1]) <= 8:
          raise Socks5Error(ord(resp[1]), _generalerrors[ord(resp[1])])
      else:
          raise Socks5Error(9, _generalerrors[9])
  ```
  
## 手動建立代理伺服器
- [利用 nodejs 建立 http-proxy](https://github.com/kylemocode/2020-it-iron-man-challenge/blob/master/http-proxy/app.js)


## Ref
- [SocksiPy](https://github.com/Talanor/SocksiPy)
- [SOCKS on WIKI](https://zh.wikipedia.org/wiki/SOCKS)

