## Docker-autossh
- 利用 ssh 實現內網穿透
- 利用 autossh 實現`自動重連`的內網穿透

  autossh 的用法與 ssh 相同，但 autossh 可實現斷線自動重連，
  見 [使用autossh取代ssh](#使用-autossh-取代-ssh-命令)

- [docker-forward-proxy](Docker-forward-proxy)
- [docker-reverse-proxy](Docker-reverse-proxy)

## [功能] SSH 的功能

- 功能1，加密連接，在遠端使用 `sshd 命令` 啟動 ssh-server 和客戶端之間的 1v1 連接
  - 使用場景: 兩個電腦之間的`非對稱加密`連接，需要以相同的通訊埠
  - 在遠端，透過 `service ssh start` 或 `/bin/sshd` 命令啟動 sshd
  - 在本地端，透過 openssh 的 `ssh` 命令進行連接
  - client 的連接埠需要與 server 一致，
  - server 端會直接處理 client 的請求，並直接返回結果
  - 範例: [ssh客戶端連接範例](doc/使用SSH進行遠端連線.md)

- 功能2，代理，使用 `ssh 命令 + sshd-server`，用於 不同網域的資源存取(內網穿透)
  - 使用場景: 利用`不同通訊埠的映射`實現`請求轉發`
  - 範例
    - [正向代理(本地埠映射)範例](doc/使用SSH進行正向代理(本地埠映射).md)
    - [反向代理(遠端埠映射)範例](doc/使用SSH進行反向代理(遠端埠映射).md)
    - [鏈式代理(正向+反向)範例](doc/更安全的內網穿透(正向+反向).md)
    - [VPN(動態端口映射)範例](doc/使用SSH進行VPN(動態接口映射).md)

  - 比較，`客戶端連接`和`代理`的區別
    - SSH 客戶端連接需要使用`相通的通訊埠`進行連接，
    - 代理的本質是`通訊埠映射`，可以使用不同的通訊埠
  
  - 不同場景造成代理方式的差異
    - 正向代理或反向代理`已經可以實現內網穿透`，實現可以在不同網域中存取資源，

      但依據使用的環境不同，造成正向代理或反向代理在該環境中，哪一種更容易實現內外網串聯
      
    - 場景1，利用本地端口轉發的正向代理，用於目標主機`不可上網`
      ```mermaid
      flowchart LR
      c1(客戶端\n本地埠綁定\nssh -L) <-- SSH --> c2(sshd-server\n正向代理) 
      
      subgraph 同網域
        c2 <-- 直連\n未加密 --> c3(目標主機)
      end
      ```

    - 場景2，利用遠程端口轉發的反向代理，用於目標主機`可上網`
      ```mermaid
      flowchart LR
      subgraph 同網域\n
        b1(客戶端) <-- 直連\n未加密 --> b2(sshd-server\n反向代理) 
      end
      b2 <-- SSH --> b3(目標主機\n遠端埠綁定\nssh -R) 
      ```

    - 場景3，正向代理+反向代理，用於更安全的內網穿透
      ```mermaid
      flowchart LR
      b1[客戶端\nssh -L] <-- SSH --> b2[雲主機\nsshd-server\n正向/反向代理]
      b2 <-- SSH --> b3(目標主機\n遠端埠綁定\nssh -R) 
      ```

  - 比較，`正向/反向代理`的異同

    - 兩者都會使用 `ssh 命令`建立`連接埠的映射`
      - 正向代理，在客戶端執行`本地埠`映射(ssh -L)，監聽客戶端的請求
      - 反向代理，在目標主機實現`遠端埠`映射(ssh -R)，遠端指的是 sshd-server，監聽遠端的請求

    - 兩者都會與 sshd-server `建立SSH通道`，透過 sshd-server 將請求轉發給目標主機
      - 正向代理，由`客戶端與 sshd-server ` 建立SSH通道，sshd-server `和目標主機直連且同網域`
      - 反向代理，由`目標主機與 sshd-server` 建立SSH通道，sshd-server `和客戶端直連且同網域`

    - 兩者都會`監聽請求`，透過建立的SSH通道，`將請求轉發給 sshd-server`，
      - 正向代理，在客戶端監聽客戶端的請求，透過 sshd-server 轉發給目標主機
      - 反向代理，在目標主機監聽來自 `sshd-server 的請求`，在目標主機處理請求後，將結果透過 sshd-server 轉發給客戶端

- 功能3，透過 SSH 傳遞檔案
  - 將遠端檔案複製到本地端，scp file user@host:/path/to/file
  - 將本地端檔案複製到遠端，scp user@host:/path/to/file /local/path/to/file
  - 將本地端多個檔案複製到遠端，scp file1 file2 user@host:/path/to/directory
  - 將本地端目錄複製到遠端，scp -r /path/to/directory user@host:/path/to/directory

- 功能4，執行 gui 的應用
  - [ssh + x11](doc/%E9%80%8F%E9%81%8Essh%E5%9F%B7%E8%A1%8Cgui%E7%9A%84%E6%87%89%E7%94%A8-x11.md)，
  - [直接使用 x11，不透過 ssh](../docker/readme.md#在-docker-中使用-x11)

- 功能5，測試SSH實際連線速率
  > yes | pv | ssh $host "cat > /dev/null"

- 功能6，免登入，直接在遠端執行命令並取得結果
  > ssh 登入帳號@sshd-server的IP -p 連接埠 -- 要執行的指令

## SSH 名詞解釋

- hostkey，`給連線主機的公鑰`，存放在 server 端，用於記錄`私鑰的存放位置`
  - 當使用者連線時，ssh 會根據此位置尋找`對應的公鑰(私鑰名.pub)`，並將公鑰傳遞給 client，
  - client 會保存此公鑰在 `known_hosts` 檔案中

- known_hosts，`已知的主機`，存放在 client 端，用於紀錄與 sshd-server `通訊用的公鑰`
  - known_hosts 會保留訪問過的服務器的名稱和/或 IP 地址以及公鑰。
  - 若公鑰不同（例如冒充服務器的中間人），SSH 會發出很大的警告。
  - 此公鑰用於與 server 通訊時，解開 server 的加密訊息

- authorized_keys，`信任清單`，存放在 server 端上，用來記錄 client 端`登入用的公鑰`
  - 當 client 使用`免密碼登入`時，才會使用到 authorized_keys
  - 在 client 產生公私鑰後，將用戶的公鑰傳遞給 server 端，server 端會保存在 `~/.ssh/authorized_keys`
  - 當 client 使用私鑰連線後，server 會比對保存的公鑰，若正確就可以免密碼登入

## SSH 建立連接的過程
- 分為5個階段
  - 階段1，決定SSH版本期
  - 階段2，決定加密算法和密鑰期
  - 階段3，server 驗證 client 身份期
  - 階段4，client 向 server 發起連接請求期
  - 階段5，交互對話期

- 階段1，決定SSH版本期
  - `server 提供 -> client 決定 -> server 覆核`
  
  - 1-1，client 透過TCP三次握手和 server 建立TCP連接
  - 1-2，server 發送自己可用的 SSH 版本給 client
  - 1-3，client 將要使用的 SSH 版本發給 server
  - 1-4，server 判斷是否支援 client 決定的SSH版本，若是，進入下一個階段
  
- 階段2，決定密鑰和加密演算法期
  - `先決定算法，再計算密鑰`

  - 2-1，client 發送自己支持的算法列表給 server，
    包含 `公鑰算法`列表、`加密算法`列表、`消息驗證算法`列表、`壓縮算法`列表 和 `消息認證碼`(Message Authentication Code，MAC)

  - 2-2，server 從 client 發送的列表中`決定`要使用的算法，並通知 client
    - server 會從列表中`由左向右的順序`，依序檢視是否可用
    - 決定後，通知 client 要使用的算法，若沒有可用算法，協商失敗

  - 2-3，client 根據SSH的版本，提供`密鑰交換算法`的使用參數給 server
    
    例如，`若決定使用 SSH2 的DH算法`，client 就可以設置密鑰交換算法的 Min、Numbers、Max 等參數值

  - 2-4，server 傳遞用於產生DH公鑰的`參數P`和`參數G` 給 client
  
  - 2-5，client 端
    - 產生公鑰和私鑰a，
    - 利用 `客戶端私鑰a`、`參數P`、`參數G`，計算`客戶端DH公鑰e`
    - 傳遞客戶端DH公鑰e 給 server
  
  - 2-6，server 端
    - 產生公鑰和私鑰b，
    - 根據`客戶端的DH公鑰e` + `服務端的私鑰b`，計算 `共享密鑰K`、`服務端DH公鑰f`、`參數H的簽名`
      
      注意，參數H的簽名，是利用服務端的私鑰b計算出的)

    - 傳遞 服務端DH公鑰f 和 參數H的簽名

  - 2-7，client 端
    - 根據 `服務端DH公鑰f` 和 `客戶端私鑰a`，計算出共享密鑰K和`參數H`
    - 根據 server 提供的 `參數H的簽名`，client 可以驗證計算出來的`共享密鑰K`是否正確，
      client 計算出的參數H的簽名，需要和 server 提供的一致
    - client 端發送 New Keys 的保存，表示密鑰交換成功
    - 後續的通訊都使用`共享密鑰K`進行通訊，
   
- 階段3，驗證身份期，
  - 3-1，server 將 `/etc/ssh/sshd_config` 中，`hostkey` 指向的 server-public-key，
    傳遞給 client，讓 client 保存在 known_hosts 的檔案中

  - 3-2，client 提供登入的帳號、密碼、client-public-key 給 server，讓 server 保存在 authorized_keys 的檔案中

  - 3-3，server 透過 `client-public-key` + `共享密鑰K`，隨機加密一段字符串，並發送給 client

  - 3-4，client 透過 `共享密鑰K` + `server-public-key` 得到原始字符串，並透過`共享密鑰K`重新加密後，發送給 server

  - server 端檢視 client 發送的字串是否一致，若一致代表認證成功

## ssh-keygen 的使用
- 自動產生所有類型的公私鑰，
  - 通常用於 sshd-server
  - `$ ssh-keygen -A`，會存放在 `/etc/ssh` 的目錄中

- 產生當前用戶的 RSA 密鑰，`$ ssh-keygen -q -N "" -t rsa -f $HOME/.ssh/id_rsa`
  - 通常用於 ssh-client 
  - `-q`，quiet mode，安靜模式
  - `-N`，set passphrase，設置密鑰密碼 
  - `-t`，type，加密類型
  - `-f`，file，指定公私鑰的保存位置，公鑰會以 ssh_host_rsa_key.pub 結尾

## 密碼登入的設置
- 在 server 產生 `server-public/private-key`，並將 `server-public-key` 交給 client 保存
  
  client 會將 server-public-key 保存在 `known_hosts` 的檔案中

- client 第一次連線到 server 時，若沒有該 server 的 server-public-key，
  client 端會詢問是否添加公鑰

  - 若是，sshd-server 會將從 hostkey 指定的檔案中，尋找對應的公鑰，並傳遞給 client，
    client 會將此公鑰添加到 `.ssh/known_hosts` 的檔案中

  - 若否，會取消密碼輸入，並停止連線，下次再重新詢問是否要添加公鑰

- client 必須要完成公鑰的添加，才會繼續詢問密碼

- client 添加公鑰後，下次連線仍然需要以密碼登入

- 開啟密碼登入
  - 修改 `/etc/ssh/sshd_config`
  - 設置`PasswordAuthentication yes`

## 免密碼登入的設置

- 在 client 產生 `client-public/private-key`，並將 `client-public-key` 交給 server 保存

  server 會將 client-public-key 保存在 `authorized_keys` 的檔案中

- 實作免密碼登入
  - step0，確保 sshd-server 在開啟狀態

  - step1，在 client 端上產生公私鑰

    `ssh-keygen -q -N "" -t rsa -f $HOME/.ssh/id_rsa`
  
  - step2，將公鑰傳遞給 server 端
    - 方法1，推薦，透過 `ssh-copy-id` 指令，將 client-public-key 傳遞給 server
      - `ssh-copy-id -i <client-public-key的檔案位置> <登入帳號@serverIP>`
      - 注意，使用 ssh-copy-id 指令時，不需要 server 設置 `PubkeyAuthentication yes`
      - 此命令正確執行後，server 會在自動將內容添加到 ~/.ssh/authorized_keys 的檔案中

    - 方法2，不推薦，連線進 server，由 client 手動自行修改 server 的 authorized_keys 檔案
  
  - step3，client 端使用自己產生的私鑰
    
    透過 `ssh username@server_host` 進行連線

  - 完整範例，參考，[使用SSH進行遠端連線](doc/使用SSH進行遠端連線.md)

## 常用命令

- 產生公私鑰對
  - 預設安裝路徑為`/root/.ssh/`
  - 方法，以互動模式建立，`ssh-keygen -t ed25519 -C "your_email@example.com"`
  - 方法，直接建立，
    - `ssh-keygen -t 類型 -N 密碼 -f 保存位置`
    - `ssh-keygen -t ed25519 -N "" -f ~/.ssh/mykey`
  
- 連接到 sshd-server
  - `$ ssh -p 埠號 帳號@遠端主機IP`，以密碼登入
  - `$ ssh -p 埠號 帳號@遠端主機IP -i 私鑰檔案`，以私鑰登入
  - `$ ssh -p 埠號 帳號@遠端主機IP commands 命令`，執行當行命令

- `$ ssh-keyscan <遠端 或 本地IP 或 localhost>` : 獲取指定IP的主機所提供的公鑰

  注意，本地IP或localhost 僅適用於本地有啟用sshd-server 下才有效，且獲取時需要在sshd-server開放端口，否則有可能會獲取失敗
 
- `$ ssh-add <私鑰的檔案位置>`
  - 將本地產生的私鑰，添加到本地端的 ssh-agent，使用戶連接ssh遠端時，不需要每次都輸入私鑰

  - ssh-agent 會將私鑰保存在內存中而不是文件系統中，可以降低被盜用的風險

  - 透過`$ eval "$(ssh-agent -s)"`啟動ssh-agent，並設置相關的環境變數
    
    注意，s 代表 script-mode

    此命令會啟動ssh-agent，並打印環境變數，若只執行`ssh-agent -s`並不會使打印出來的環境變數生效，
    而是需要透過 eval 才能使打印出來的環境變數生效

    透過`$ ssh-agent -k`可關閉 ssh-agent

- `ssh-copy-id -i 公鑰文件 登入帳號@遠端ip`
  - 將本地端的公鑰，傳遞給遠端的 sshd-server
  - 若不指定 -i 參數，預設使用`~/.ssh/authorized_keys`

- 使用範例

  ```shell
  # 產生公私鑰對
  ssh-keygen -t ed25519 -N "" -f ~/.ssh/mykey
  
  # 啟動 ssh-agent
  eval (ssh-agent -c)
  
  # 將私鑰添加到 ssh-agent
  ssh-add /root/.ssh/mykey

  # 顯示加入ssh-add 的密鑰的摘要，內容會與 ~/.ssh/mykey 的不同
  ssh-add -l    
  ```
  
## sshd-config 的設定值
- SSH 連接相關設定
  - `Port 22`，client 連接 sshd-server 的通訊埠
  - `HostKey <path>`，指定 server-private-key 的位置，傳遞給 client 通訊用的 server-public-key
  - `PermitRootLogin no`，是否允許 client 以 root 帳號登入
  - `PubkeyAuthentication no`，是否允許 client 免密碼登入(是否允許 client 以公鑰登入)
  - `PasswordAuthentication yes`，是否允許 client 以密碼登入

- 代理相關設定
  - `AllowAgentForwarding yes`，預設允許使用 agent 轉發
  - `AllowTcpForwarding yes`，預設允許使用 tcp 轉發
  - `GatewayPorts no`，預設讓 Remote-Forwarding 只能夠綁定在 SSH Server 的 localhost 上， 

- 防止中斷相關設定
  - `ClientAliveInterval 0`，每 0 秒發送一次 keepalive 的空包
  - `ClientAliveCountMax 3`，若發送的空包失敗3次就會斷開連接

- 其他設定
  - `X11Forwarding yes`，允許 x11 port 的轉發

## SSH 命令常用參數
- `-f`，連線後，在背景執行 ssh 指令
- `-C`，允許壓縮資料，常用於速度優化
- `-N`，不打開終端與 sshd-server 進行交互
- `-R`，遠端映射，將遠端主機的通訊埠映射到本機的通訊埠
- `-L`，本地端映射，將本機的通訊埠A映射到本機的通訊埠B
- `-p`，遠端主機的連接埠
- `-v`，顯示連接過程中的詳細信息，用於 debug
- `-n`，將 stdin 轉向到 /dev/null，可以避免每個連線都占用一個 tty
- `-T`，禁止分配偽終端
- `-t`，強制分配偽終端
- `-X`，開啟 x11 轉發功能
- `-x`，關閉 x11 轉發功能
- `-y`，開啟信任 x11 轉發功能
- `-F`，指定ssh指令的配置文件
- `-A`，啟用 Agent Forwarding，
  - 使用本地伺服器的 SSH 金鑰(SSH Key)，透過遠端的伺服器(remote.server)轉送，這樣就不需要把重要的金鑰再放一份到 remote.server 
  
  - 例如，本地伺服器的金鑰透過 remote.server 連線到 github.com
    ```
    // local ----SSH---> remote.server ---- SSH ----> github.com
    ssh -A your.user.name@remote.server -- ssh git@github.com
    ```
    

- 用於主機連接 (ssh-client)，`ssh 登入帳號@主機IP -p連接埠`
- 用於反向代理，`ssh -fCNR ...`
- 用於正向代理，`ssh -fCNL ...`

## ssh-config 保存主機資訊，可簡化輸入的命令
- 將命令參數保存在 config 檔案中，可以簡化命令參數的輸入

- config 檔案需要放在 .ssh/config 的路徑

- config 可保存多台主機的訊息，並透過 `ssh 主機名` 載入對應的主機設定值

- 若不使用自定義別名，也可以透過 `ssh -o "指令"` 執行單行的 ssh-sript 的命令

- config 檔除了可以簡化命令參數的輸入外，另外有提供單行的指令可提供進階的功能
  - 指令，Home 主機名，定義主機名
  - 指令，Hostname，當前主機的IP
  - 指令，Port，當前主機的port
  - 指令，User，當前主機的登入帳號
  - 指令，Identityfile，指定傳遞給當前主機的私鑰位置
  - 指令，ProxyCommand，代理命令
    - 先自動連上其中一台機器，再自動從那台機器連到內網的其他機器，
      可以避免內網穿透的雲主機保存私鑰
    - 透過此命令，可以把用來認證的私鑰只保存在本機，而不會儲存在雲主機中
  - 指令，Compression，壓縮資料
  - 減少重複連線的時間
    - ControlMaster   auto
    - ControlPath     /tmp/ssh-%r@%h:%p
  - 指令，ControlPersist，延長連線時間

- 範例，保存不同主機的設定值
  ```
  # - master
  Host            master                # 代號
  Hostname        192.168.11.24        # IP or Domain name
  Port            2222                # 指定埠口
  User            jonny                # 使用者名稱
  identityfile    ~/.ssh/id_rsa_24    # 指定金鑰

  # - slave
  Host            slave                # 代號
  Hostname        192.168.11.25        # IP or Domain name
  Port            2223                # 指定埠口
  User            jonny                # 使用者名稱
  identityfile    ~/.ssh/id_rsa_25    # 指定金鑰
  ```

- 範例，ProxyCommand 的使用
  ```
  // .ssh/conifg
  // 先連上 pc-a 機器，然後再從 pc-a 連 pc-b

  Host pc-a
  Hostname aa.aa.aa.aa
  User usera

  Host pc-b
  User userb
  
  // pc-b 的命令由 pc-a 代理
  ProxyCommand ssh -q -W %h:%p pc-a 
  ```
  
## 工具，autossh 實現自動重連
- 為什麼需要 autossh
  ssh 反向代理會因為超時而自動關閉，可利用 autossh 或 ssh-connection-daemon 改善，
  autossh 是強化版的反向代理，可實現自動重連

- 和 SSH 的功能基本一致，SSH 的命令列參數可用於 autossh
  - 比 SSH 多了一個 `-M` 參數，用於 Monitor 指定埠，當指定埠失去連接後會自動重連
  - 比 SSH 少了一個 `-f` 參數，autossh 預設在背景執行，不需要額外添加 -f 參數 
  - 以 autossh 取代後的反向代理指令
    > autossh -M 7281 -fCNR 7280:localhost:22 external@456.456.456.456
    - `-M 7281` 是 autossh 用來監測內網主機A是否正常的 port，若 7281 沒反應時就會自動重啟

- 透過 autossh 建立反向代理
  ```shell
  shell> nano /etc/rc.d/rc.local

  // 新增以下
  autossh -M 7281 -fCNR 7280:localhost:22 root@123.123.123.123

  shell> chmod x /etc/rc.d/rc.local
  ```

## OpenSSH on Windows
- 官方文檔，https://github.com/PowerShell/Win32-OpenSSH/wiki/ssh.exe-examples

- 相關路徑
  - server-public-key 預設的安裝位置，`C:\ProgramData\ssh`
  - client-public-key 預設的安裝位置，`C:\Users\ching/.ssh/id_rsa`
  - 執行檔和配置檔的預設位置，`C:\Windows\System32\OpenSSH`

## SSH 風險和工具
- 工具，Port Knocking 條件的開啟 SSH Port
  - Client 必須用特殊的順序來對 SSH Server 上的某些 Port 發出連線請求後，SSH Server 才會開放 Client 連線的技巧，
  例如依序對 Port 1000、2000、3000 發出請求，才會對你開放 Port 22，
  這樣的好處是平時 Port 22 就會是關閉的狀態，讓攻擊者以為 SSH 沒有開放，減少被攻擊的機會，
  
  - [參考](https://www.youtube.com/watch?v=IBR3oLqGBj4)
  
## 建立代理鍊
- [與一般代理的通訊基礎](doc/利用socket與proxy通訊.md)
- [建立代理鍊](doc/建立代理鍊.md)

## 其他注意事項
- 端口映射時(本地映射或遠端映射)，映射的位置最好大於 1024，因為低位置的 port 通常 root 使用

## Ref
- [SSH Tunneling (Port Forwarding) 詳解](https://johnliu55.tw/ssh-tunnel.html)
- [ssh config 的使用](https://m2kar.cn/post/20200314-ssh-to-lan/)
- [增進 SSH 使用效率](https://chusiang.gitbooks.io/working-on-gnu-linux/content/20.ssh_config.html)
- [25個SSH Commands的使用技巧](http://max-linux-space.blogspot.com/2011/07/25ssh-commands.html)