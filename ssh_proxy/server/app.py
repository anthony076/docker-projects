from flask import Flask

import random

app = Flask(__name__)

@app.route("/")
def hello():
    return str(random.randint(100, 200))+"\n"

if __name__ == '__main__':
    app.run(debug=True, use_reloader=True, host="0.0.0.0", port=5566)