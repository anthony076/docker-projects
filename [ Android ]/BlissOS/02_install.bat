
@echo off

cd %~dp0

qemu-system-x86_64.exe ^
-M q35 ^
-m 4096 -smp 4 ^
-accel whpx,kernel-irqchip=off ^
-machine vmport=off ^
-boot d ^
-bios ovmf.fd ^
-drive file=Bliss16-OFFICIAL.qcow2,if=virtio ^
-cdrom Bliss-v16.9.6-x86_64-OFFICIAL-foss-20240603.iso ^
-device qemu-xhci,id=xhci ^
-usb -device usb-tablet -device usb-kbd ^
-display sdl,gl=on,show-cursor=off ^
-device virtio-vga-gl ^
-net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555


