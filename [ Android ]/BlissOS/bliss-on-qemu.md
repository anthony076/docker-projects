## BlissOS basic

- 安裝 Bliss
  - 不一定要使用UEFI，可以使用傳統的GRUB開機
    - [使用vmware安裝](https://www.youtube.com/watch?v=WA9QpmhteEI)
    - [使用virtualbox安裝](https://www.youtube.com/watch?v=137jBpJniNs)
    - [使用virtualbox安裝](https://www.youtube.com/watch?v=kM6hhVhudzg)

  - 利用第三方工具將Bliss安裝在本機硬碟
    - [Android-x86-installer](https://github.com/Xtr126/Android-x86-installer)
    - [Advanced Android-x86 Installer](https://github.com/supremegamers/awin-installer-dev)
    - [Install any Android-x86 OS on Windows with Advanced Android-x86 Installer](https://www.youtube.com/watch?v=y5-4SuZzwMs)

- BlissOS on Windows-QEMU 的問題

  - UEFI 開機問題
    - [UEFI基礎和獲取fd檔案的方法](../../[%20Virtual%20Machine%20]/qemu/qemu-uefi.md)

    - 要安裝 BlissOS，需要使用UEFI開機

    - 若要使用UEFI開機，需要指定`-bios fd檔案路徑`，用於使用 UEFI 開機，否則 qemu 預設會使用傳統的GRUB開機

  - 3D遊戲加速問題
    - 在 windows 上流暢運行 bliss 需要`virgl3d加速器`(Virglrenderer)，用於3d遊戲的加速
    - virgl3d需要 qemu-kvm 的支持，通常只用於 linux

      解決方法是，使用qemu-3dfx專案提供的patch後再自行編譯qemu，patch會讓virgl在Windows上的qemu正常運作，
      參考，[BlissOS on compiled-qemu (WORK)](#blissos-on-compiled-qemu-work)

      > 注意，不是所有版本的qemu都被qemu-3dfx支援

  - 其他問題
    - 安裝後重啟，無法進入畫面，只有顯示黑畫面
      - 解決方式:
        - 原理，藉著關閉某些驅動的載入，加速進入系統的時間
        - 進入選單後，選中 BlissOS-15.8.2 2022-12-14，按下e進入編輯
        - 將`quiet`改成以下，用於不載入特定顯示驅動， 修改後，按下 ctrl+x 後，套用並立即重新開機
          - 範例，`nomodeset xforcevesa`
          - 範例，`nomodeset xforcevesa video=1920x1080 DPI=400`
        - 成功進入桌面後，按alt + F1
        - 輸入以下
          ```
          mkdir /mnt/sda
          mount /dev/block/sda1 /mnt/sda
          vi /mnt/sda/grub/menu.lst
          把GRUB啟動項目的 "quiet"替換成"nomodeset xforcevesa"
          ```

- 下載
  - [各種版本的qemu源碼下載](https://download.qemu.org/)
  - [各種版本的MSYS下載](https://github.com/msys2/msys2-installer/releases)
  - [edk2專案](https://github.com/tianocore/tianocore.github.io/wiki/EDK-II)
  - [qemu-3dfx專案](https://github.com/kjliew/qemu-3dfx)
  - [qemu-3dfx專案中的patch](https://raw.githubusercontent.com/kjliew/qemu-3dfx/master/virgil3d/0001-Virgil3D-with-SDL2-OpenGL.patch)，
    注意，該patch目前只支援到 qemu 8.2
  - [Virgl on Upstream windows builds](https://gitlab.com/qemu-project/qemu/-/issues/1461)

## BlissOS on installed-qemu (WORK)

- 測試環境 : 
  - Windows 11 23H2 with hyperV Enable
  - 安裝版的 QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5)
  - [Bliss-v15.8.2-x86_64-UNOFFICIAL-gapps-20221214.iso](https://drive.google.com/drive/folders/1_SrSWBMhhpsAqRP4i1JY7DIVrliDVVNc)

- step，取得 ovmf.fd
  
  [從clearlinux直接獲取ovmf.fd](https://github.com/clearlinux/common)

- step，create image，`qemu-img create -f qcow2 Bliss15-UNOFFICIAL.qcow2 20G`

- step，install command，

  `-bios edk2-x86_64.fd`選項不可省略，
  - 添加此行無法進入安裝畫面，不添加此行，在安裝過程中就不會顯示 Do you want to install EFI GRUB2 的選項
  - 不安裝 EFI GRUB2 bootloader，重開機後就無法進入系統

  ```batch
  qemu-system-x86_64.exe ^
  -M q35 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  @REM -bios edk2-x86_64.fd ^
  -drive file=Bliss15-UNOFFICIAL.qcow2,if=virtio ^
  -cdrom Bliss-v15.8.2-x86_64-UNOFFICIAL-gapps-20221214.iso ^
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet -device usb-kbd ^
  -device virtio-vga-gl ^
  -display sdl,gl=on,show-cursor=on ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555
  ```

- step，<a id="build-partition-1">建立 partition</a> 
  - `yes`，use GPT

  - Create/Modify partitions > select `20.0 GiB` > `NEW`
    ```
    First Sector : <Enter>  (default = 2048)
    Size : +512M <Enter>
    Hex Code or GUID : 8300 <Enter>  (default = 8300 = Linux filesystem)
    Partition name : EFI    
    ```

  - select `19.5 GiB` > `NEW`
    ```
    First Sector : <Enter> (default = 1050624)
    Size : <Enter> (default = 40892383)
    Hex Code or GUID : 8300 <Enter> 
    Partition name : Android <Enter>
    ```

  - use arrow keys to select `WRITE` > `yes`
  - use arrow keys to select `QUIT`

- step，安裝 EFI GRUB2
  
  use arrow keys to select `vda2` partition > `ext4` for filesystem-format > `YES` to install EFI GRUB2

  Cannot mount /dev/vda1, Do you want to format it > Select `YES`

  Do you want to install /system directory as read-write > Select `NO`

- step，BlissOS

  安裝完成後，選擇 Reboot

  進入選單後，`BlissOS-15.8.2 2022-12-14 Installation` 選項會消失，會變成 `BlissOS-15.8.2 2022-12-14`，
  點擊 BlissOS-15.8.2 2022-12-14 可進入系統

- step，安裝後的執行命令

  ``batch`
  qemu-system-x86_64.exe ^
  -M q35 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -boot c ^
  -bios ovmf.fd ^
  -drive file=Bliss15-UNOFFICIAL.qcow2,if=virtio ^
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet -device usb-kbd ^
  -display sdl,gl=on,show-cursor=off ^
  -device virtio-vga-gl ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555
  ```

## BlissOS on compiled-qemu (WORK)

- 測試環境 : 
  - Windows 11 23H2 with hyperV Enable
  - [MSYS Installer 2024-01-13](https://github.com/msys2/msys2-installer/releases/download/2024-01-13/msys2-x86_64-20240113.exe)
  - [qemu v8.2.1](https://download.qemu.org/qemu-8.2.1.tar.xz)
  - [patch](https://raw.githubusercontent.com/kjliew/qemu-3dfx/master/virgil3d/0001-Virgil3D-with-SDL2-OpenGL.patch)
  - [Bliss-v15.8.2-x86_64-UNOFFICIAL-gapps-20221214.iso](https://drive.google.com/drive/folders/1_SrSWBMhhpsAqRP4i1JY7DIVrliDVVNc)
  - 注意，此方法有啟用 virgil3d，對3d遊戲會更為順暢

- step，安裝和配置編譯環境
  
  以下命令在<font color=blue>MSYS2 UCRT64</font>中執行

  ```shell
  # 更新 pacman，更新後需要重開 MSYS shell
  pacman -Syu

  # 安裝必要套件
  pacman -S make mingw-w64-ucrt-x86_64-diffutils \
  mingw-w64-ucrt-x86_64-gcc mingw-w64-ucrt-x86_64-glib2 \
  mingw-w64-ucrt-x86_64-libslirp mingw-w64-ucrt-x86_64-make \
  mingw-w64-ucrt-x86_64-pixman mingw-w64-ucrt-x86_64-pkgconf \
  mingw-w64-ucrt-x86_64-SDL2 mingw-w64-ucrt-x86_64-virglrenderer \
  ninja patch python

  # 建立code目錄
  mkdir code && cd code
  wget https://download.qemu.org/qemu-8.2.1.tar.xz
  
  # 出現 Cannot create symlink to ‘/opt/X11/include’: No such file or directory 可以忽略
  tar xvf qemu-8.2.1.tar.xz   
  cd qemu-8.2.1
  
  # 下載 patch
  wget https://raw.githubusercontent.com/kjliew/qemu-3dfx/master/virgil3d/0001-Virgil3D-with-SDL2-OpenGL.patch

  # 套用 patch，套用前檢視 patch 的內容，qemu 的版本需要一致
  patch -p0 -i 0001-Virgil3D-with-SDL2-OpenGL.patch
  # 若成功會出現以下
  # patching file ./include/ui/egl-helpers.h
  # patching file ./meson.build
  # Hunk #1 succeeded at 1430 (offset -1 lines).
  # Hunk #2 succeeded at 2123 (offset -1 lines).
  # Hunk #3 succeeded at 4364 (offset -8 lines).
  # patching file ./ui/egl-helpers.c
  # patching file ./ui/meson.build

  # 配置qemu
  ./configure --target-list=x86_64-softmmu --prefix=/ucrt64 --enable-whpx \
  --disable-tcg --enable-virglrenderer --enable-sdl \
  --enable-slirp --disable-capstone --disable-tools --disable-guest-agent \
  --disable-gtk

  # 編譯 qemu
  make -j4

  # 安裝 qemu
  make DESTDIR=~/qemu-virgl install
  
  # 複製必要依賴
  cd ~/qemu-virgl/msys64/ucrt64/
  cp $(ldd qemu-system-x86_64.exe | grep -i /ucrt64/ | awk '{print $3}') .

  # 在MSYS中安裝 qemu-img.exe 的工具
  pacman -S mingw-w64-ucrt-x86_64-qemu-image-util

  # 測試是否可用
  qemu-img.exe --version

  # 回到目錄
  cd ~/qemu-virgl/

  # 將qemu-virgl移動到更簡潔的目錄，移動到 C:\qemu-virgl
  mv msys64/ucrt64/ /c/qemu-virgl

  # 手動將 C:\qemu-virgl 添加到環境變數中，並重啟 MSYS2 UCRT64

  # 切換到專案目錄
  cd Desktop/bliss-on-qemu/

  # 建立磁碟映像檔
  qemu-img create -f qcow2 Bliss15-UNOFFICIAL.qcow2 20G

  # 建立 fd
  cat /c/qemu-virgl/share/edk2-i386-vars.fd /c/qemu-virgl/share/edk2-i386-code.fd > compiled-edk2-x86_64.fd
  ```

  因為MSYS2 UCRT64無法執行batch檔案，切換回 windows-prompt，
  注意，以下命令在 windows-prompt 中執行

- step，執行安裝命令
  
  - 不添加`-boot d`也有機會可以進入安裝畫面 
  ```
  qemu-system-x86_64.exe ^
  -M q35 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -boot d ^
  -bios compiled-edk2-x86_64.fd ^
  -drive file=Bliss15-UNOFFICIAL.qcow2,if=virtio ^
  -cdrom Bliss-v15.8.2-x86_64-UNOFFICIAL-gapps-20221214.iso ^
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet -device usb-kbd ^
  -device virtio-vga-gl ^
  -display sdl,gl=on,show-cursor=on ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555
  ```

  <font color=red>注意，執行安裝命令後，沒有看到安裝畫面前，不要調整qemu的視窗，否則會啟動失敗</font>

  <font color=red>注意，安裝命令有機會失敗，可多試幾次</font>

- Issue : 進入安裝畫面後，點擊 BlissOS-15.8.2 2022-12-14 Installation 的選項後，會進入黑畫面
  
  解決方法 : 改用[從clearlinux獲取ovmf.fd](https://github.com/clearlinux/common)

  並將`-bios compiled-edk2-x86_64.fd`替換為`-bios ovmf.fd`

  修改後的安裝命令如下，
  ```batch
  @REM 可進入cd-rom的選單
  @REM 點擊 BlissOS-15.8.2 2022-12-14 Installation 後，可進入 partition 的畫面
  qemu-system-x86_64.exe ^
  -M q35 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -boot menu=on ^
  -bios ovmf.fd ^
  -drive file=Bliss15-UNOFFICIAL.qcow2,if=virtio ^
  -cdrom Bliss-v15.8.2-x86_64-UNOFFICIAL-gapps-20221214.iso ^
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet -device usb-kbd ^
  -display sdl,gl=on,show-cursor=off ^
  -device virtio-vga-gl ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555
  ```

- step，建立 partition 
  
  參考`BlissOS on installed-qemu (WORK)`的[建立partition](#build-partition-1)

## BlissOS on installed-qemu with Bliss-16.9.6 (WORK)

- 測試環境 : 
  - Windows 11 23H2 with hyperV Enable
  - 安裝版的 QEMU emulator version 8.2.0 (v8.2.0-12045-g3d58f9b5c5)
  - Bliss-v16.9.6-x86_64-OFFICIAL-foss-20240603.iso

- step，取得 ovmf.fd
  
  [從clearlinux直接獲取ovmf.fd](https://github.com/clearlinux/common)

- step，建立硬碟映像，`qemu-img create -f qcow2 Bliss15-UNOFFICIAL.qcow2 20G`

- step，執行安裝命令
  
  ```
  qemu-system-x86_64.exe ^
  -M q35 ^
  -m 4096 -smp 4 ^
  -accel whpx,kernel-irqchip=off ^
  -machine vmport=off ^
  -boot d ^
  -bios ovmf.fd ^
  -drive file=Bliss16-OFFICIAL.qcow2,if=virtio ^
  -cdrom Bliss-v16.9.6-x86_64-OFFICIAL-foss-20240603.iso ^
  -device qemu-xhci,id=xhci ^
  -usb -device usb-tablet -device usb-kbd ^
  -display sdl,gl=on,show-cursor=off ^
  -device virtio-vga-gl ^
  -net nic,model=virtio-net-pci -net user,hostfwd=tcp::4444-:5555
  ```

  <font color=red>注意，執行安裝命令後，沒有看到安裝畫面前，不要調整qemu的視窗，否則會啟動失敗</font>

  <font color=red>注意，安裝命令有機會失敗，可多試幾次</font>

- step，Select `BlissOS-16.9.6 2024-06-03 Installation`

- step，建立硬碟分區

  - Select `Create/Modify partition` > Select `Yes, switch to cgdisk` > select `20.0 GiB` > `NEW`

    ```
    First Sector : <Enter>  (default = 2048)
    Size : +512M <Enter>
    Hex Code or GUID : ef00 <Enter>  (ef00=EFI System partition)
    Partition name : ESP
    ```

  - select `19.5 GiB` > `NEW`
    ```
    First Sector : <Enter> (default = 1050624)
    Size : <Enter> (default = 40892383)
    Hex Code or GUID : 8300 <Enter> 
    Partition name : Android <Enter>
    ```

  - use arrow keys to select `WRITE` > `yes`
  - use arrow keys to select `QUIT`

- step，安裝 EFI GRUB2
  
  - use arrow keys to select `vda1` partition > select `fat32` (format vda1 to fat32) > Keep `ESP` (change drive name to ESP) > `YES`, sure to format vda1
  - use arrow keys to select `vda2` partition > select `ext4` (format vda2 to fat32) > Keep `BlissOS` (change drive name to BlissOS) > `YES`, sure to format vda2
  - select `NO`, not prepare for OTA update
  - select `Grub2 EFI Bootloader` to install EFI GRUB2

- step，安裝 BlissOS

  安裝完成後，選擇 Reboot

  進入選單後，`BlissOS-16.9.6 2024-06-03 Installation` 選項會消失，會變成 `BlissOS-16.9.6 2024-06-03`，
  點擊 BlissOS-16.9.6 2024-06-03 可進入系統

## ref 

- [Running Android games on Windows 10/11 using QEMU+Hyper-V+virglrenderer+Bliss OS](https://guanzhang.medium.com/running-android-games-on-windows-10-11-4eea9be9a06b)

- [install BlissOS with qemu script](https://docs.blissos.org/installation/install-in-a-virtual-machine/install-in-qemu/)

- 安裝後重啟，無法進入畫面，只有顯示黑畫面
  - [Android X86 不能正常進去畫面](https://home.gamer.com.tw/artwork.php?sn=5169251)
  - [修改範例](https://youtu.be/WA9QpmhteEI?si=gwCCnltUJJ56RMrm&t=270)
  - [Stuck on logo during Android-x86 boot](https://android.stackexchange.com/questions/224237/stuck-on-logo-during-android-x86-boot)
  - [How to add "nomodeset xforcevesa" and fix a not booting Android virtual machine](https://www.youtube.com/watch?v=yKwdovcclxg)