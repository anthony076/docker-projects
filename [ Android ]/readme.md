
## ADB (android-debug-bridge) 的使用

- 注意，使用以下命令前，應該在android裝置上先啟用`adb debug`的功能

- 連接前
  - 啟用 server : `adb start-server`
  - 停用 server : `adb kill-server`
  - 列出已連接的android裝置 : `adb devices`

- shell 
  - 進入設備的shell : `adb shell`
  - 在設備上運行命令 : `adb shell 命令`

- 與 package 相關的操作(pm=package-manager)
  - 列出所有packages : `adb shell pm list packages`
  - 打印出package的APK文件位置 : `adb shell pm path 應用包名`

- 傳輸文件
  - 推送文件到android : `adb push 本地文件位置 遠端文件位置`
  - 從android拉取文件 : `adb pull 遠端文件位置 本地文件位置`

- APK 相關
  - 安裝APK : `adb install APK文件位置`
  - 安裝拆分的APK : `adb install-multiple base.apk位置 split_config文件.apk split_main.apk`
  - 卸載APK : `adb uninstall 應用名`

- 重啟
  - 重啟系統 : `adb reboot`
  - 重啟並進入恢復模式 : `adb reboot recovery`
  - 重啟並進入bootloader模式 : `adb reboot bootloader`

- 查看裝備日誌 : `adb logcat`

## 拆分的APK

- 拆分 APK（Split APK）的支持是從 Android 5.0 Lollipop（API Level 21）開始引入的

  為了優化應用的分發和安裝，引入了拆分 APK 的概念，以減少應用的安裝包大小，提升安裝速度，
  並且只安裝用戶設備需要的資源。拆分 APK 是一種模塊化的方式，可以根據設備的具體配置下載和安裝所需的部分。

  可透過`adb install-multiple`命令安裝拆分的APK

## android OS 的選擇

- android-x86
  - 進度緩慢
  - 不相容ARM架構APP，需要裝Intel研發的閉源libhoudini轉譯器，才能安裝ARM架構的APP，
    且Android-x86的libhoudini只相容32位元的ARM架構APP
  - 對於64bit的遊戲無法執行，例如，原神

- Remix OS
  - 基於 android-x86
  - 簡潔，穩定，

- Phoenix OS
  - 基於 android-x86
  - 廣告多，閉源
  - 含有大量中國軟體

- Bliss OS
  - 基於 android-x86，仍活躍更新中
  - 支援許多新硬體，Android版本也有更新
  - 可安裝在實體系統或是虛擬機，在電腦流暢玩手機遊戲，內建Play商店
  - 更自由開源的「電腦玩手遊」解決方案，沒有模擬器商業廣告綁架的問題
  - 適合電腦操作，有強制旋轉、按鍵映射、遊戲模式、模擬觸控點擊、KernelSU（更難偵測到的root權限）等功能
  - 內建的 libhoudini轉譯器 支援ARM64架構的APP，可執行ARM架構64bit的應用

- Prime OS
  - 基於 android-x86
  - 已停止開發

- 其他選擇
  - [Free Android-x86 Alternatives](https://alternativeto.net/software/android-x86/?license=free)
  - [GrapheneOS](https://grapheneos.org/usage)
  - [Waydroid](https://waydro.id/)，基於容器的安卓系統，需要linux特性才能使用

## 遊戲模擬器

- 遊戲模擬器，如 NOX、bluestack、雷電、等模擬器是基於 Hypervisor + Android OS，
  不同於傳統的 hypervisor，如 virtualbox、vmware、qemu，遊戲模擬器使用的是自帶的輕量Hypervisor

- bluestack : 輕量的 Hypervisor + android-x86
