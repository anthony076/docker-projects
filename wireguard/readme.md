## WireGuard

- 優缺點
  - 優，簡單但速度極快的 VPN 解決方案
  - 優，利用了高級加密技術，
    如the Noise protocol framework, Curve25519, ChaCha20, Poly1305, BLAKE2, SipHash24, HKDF

## Ref
- [Top 20 Best Free Open Source VPN](https://cloudinfrastructureservices.co.uk/best-free-open-source-vpn/)
- [透過 wireguard 架設屬於自己的VPN伺服器](https://www.youtube.com/watch?v=ZfeZNbqIWTo)