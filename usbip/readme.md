## Docker-usbip
- 利用 usbip 透過網路共享 usb 裝置

## Ref
- [usbip-ssh-docker](https://github.com/Fermium/usbip-ssh-docker/issues)

- [usbip on archlinux](https://wiki.archlinux.org/title/USB/IP)

- [Accessing host devices from inside a Docker container](https://github.com/ARMmbed/connectedhomeip/wiki/Accessing-host-devices-from-inside-a-Docker-container)

- [Linux端 USBIP 測試](https://blog.csdn.net/weixin_43891775/article/details/111681923)

- [wsl+usbip 的測試](https://gitlab.com/anthony076/spinalhdl_study/-/blob/main/doc/tool-03-usbip.md)

- [基於usbip 實現共享usb 設備](https://www.jianshu.com/p/5224ed4fd50f)
- [在 wsl 中存取 usb](https://docs.microsoft.com/zh-tw/windows/wsl/connect-usb)